<art>
    <ui>s13054-014-0592-z</ui>
    <ji>1364-8535</ji>
    <fm>
       <dochead>Commentary</dochead>
       <bibl>
          <title>
             <p>Protein in nutritional support: the newborn hero for the critically ill?</p>
          </title>
          <aug>
             <au id="A1">
                <snm>Oshima</snm>
                <fnm>Taku</fnm>
                <insr iid="I1"/>
                <email>Oshima.Taku@hcuge.ch</email>
             </au>
             <au id="A2">
                <snm>Heidegger</snm>
                <mi>P</mi>
                <fnm>Claudia</fnm>
                <insr iid="I2"/>
                <email>Claudia-Paula.Heidegger@hcuge.ch</email>
             </au>
             <au ca="yes" id="A3">
                <snm>Pichard</snm>
                <fnm>Claude</fnm>
                <insr iid="I1"/>
                <email>claude.pichard@unige.ch</email>
             </au>
          </aug>
          <insg>
             <ins id="I1">
                <p>Nutrition Unit, Geneva University Hospital, Geneva 14 1211, Switzerland</p>
             </ins>
             <ins id="I2">
                <p>Department of Anaesthesiology, Pharmacology and Intensive Care, Geneva University Hospital, Geneva 14 1211, Switzerland</p>
             </ins>
          </insg>
          <source>Critical Care</source>
          <issn>1364-8535</issn>
          <pubdate>2014</pubdate>
          <volume>18</volume>
          <issue>6</issue>
          <fpage>592</fpage>
          <url>http://ccforum.com/content/18/6/592</url>
          <xrefbib>
             <pubidlist>
                <pubid idtype="doi">10.1186/s13054-014-0592-z</pubid>
             <pubid idtype="pmpid">25672435</pubid></pubidlist>
          </xrefbib>
       </bibl>
       <history>
          <pub>
             <date>
                <day>17</day>
                <month>11</month>
                <year>2014</year>
             </date>
          </pub>
       </history>
       <cpyrt>
          <year>2014</year>
          <collab>Oshima et al.; licensee BioMed Central Ltd.</collab>
          <note>The licensee has exclusive rights to distribute this article, in any medium, for 12 months following its publication. After this time, the article is available under the terms of the Creative Commons Attribution License (<url>http://creativecommons.org/licenses/by/4.0</url>), which permits unrestricted use, distribution, and reproduction in any medium, provided the original work is properly credited. The Creative Commons Public Domain Dedication waiver (<url>http://creativecommons.org/publicdomain/zero/1.0/</url>) applies to the data made available in this article, unless otherwise stated.</note>
       </cpyrt>
       <abs>
          <sec>
             <st>
                <p>Abstract</p>
             </st>
             <p>In their current review, Weijs and colleagues highlight the importance of protein and amino acid provision for improving clinical outcome in critically ill patients. The interdependence between energy and protein is highlighted. They call for urgent research to develop new methods to evaluate protein and amino acid requirements, accurately and conveniently, in order to optimize nutrition support for critically ill patients.</p>
             <p>Appropriate nutrition delivery for critically ill patients remains a highly debated issue. Energy, a critical factor for life, was until now the superstar of nutrition support. It now faces a rival or, more correctly, a partner in function, namely protein. This is a chance to take a close look at protein, the new hero in the field of critical care nutrition, and the struggles it encounters in becoming the true superstar.</p>
          </sec>
       </abs>
    </fm>
    <bdy><sec><st><p/></st><p>In their current review, Weijs and colleagues <abbrgrp>
             <abbr bid="B1">1</abbr>
          </abbrgrp> highlight the importance of protein and amino acid provision for critically ill patients, the combination of which for adequate energy provision has been shown by an increasing number of studies.</p>
       <p>Providing nutrition support for critically ill patients is generally recommended as a standard of care. This approach is confirmed by recent studies which demonstrated the benefits of early and adequate nutrition support, whenever possible by the enteral route. The nutrition target is often defined as calculated calories/kg body weight per day, or measured by indirect calorimetry. But what about the protein target and its measurement?</p>
       <p>Disturbances of protein metabolism are observed as a physiologic response to stress, and are reflected by important nitrogen loss and muscle wasting which are proportional to the severity of illness. Amino acid administration acts as a main signal for protein synthesis in healthy conditions, but does not reduce protein breakdown <abbrgrp>
             <abbr bid="B2">2</abbr>
          </abbrgrp>. This signal is blunted in critically ill patients due to anabolic resistance <abbrgrp>
             <abbr bid="B3">3</abbr>
          </abbrgrp>. Ishibashi and colleagues <abbrgrp>
             <abbr bid="B4">4</abbr>
          </abbrgrp> suggested that protein catabolism over a 10-day period soon after ICU admission was reduced by 50% when protein intake increased and allowed a reduction of muscle loss of 1.1 to 1.5&#160;g/kg of dry fat-free mass/day (that is, lean tissue without water content). A higher level of protein intake (1.9&#160;g/kg/day) did not confer any additional protein-sparing advantage, and 1.2&#160;g/kg/day was suggested as the optimum dose. These suggestions for protein intake level of at least 1.2&#160;g/kg/day are in accordance with the current recommendations <abbrgrp>
             <abbr bid="B5">5</abbr>
          </abbrgrp>,<abbrgrp>
             <abbr bid="B6">6</abbr>
          </abbrgrp>. On the other hand, a previous study by Weijs and colleagues <abbrgrp>
             <abbr bid="B7">7</abbr>
          </abbrgrp> suggested that protein intake of 1.5&#160;g/kg/day improves clinical outcome. Indeed, a number of contradictory studies have resulted in variations of the recommended protein provision over the past decades. In the 1970s, provision as high as 3.5&#160;g/kg was recommended <abbrgrp>
             <abbr bid="B8">8</abbr>
          </abbrgrp>, but this was then reduced to 1.2&#160;g/kg/day by 2006 <abbrgrp>
             <abbr bid="B9">9</abbr>
          </abbrgrp>. During the same period, the recommended provision of calories has decreased from 40 to 45 to 20 to 30&#160;kcal/kg/day. It is likely that such a decrease in energy administration has an impact on protein needs, because energy deficit increases proteolysis to fuel obligatory gluconeogenesis. Currently, there is a trend among experts to increase daily protein provision up to 1.5 to 1.8&#160;g/kg/day, and to reconsider the protein/energy ratio to optimize the respective amounts of protein and energy administered <abbrgrp>
             <abbr bid="B10">10</abbr>
          </abbrgrp>. Robust data are still lacking to support final recommendations. The importance of both components was shown recently by Weijs and colleagues <abbrgrp>
             <abbr bid="B7">7</abbr>
          </abbrgrp>, demonstrating that the achievement of defined protein and energy targets was associated with a 50% decrease in 28-day mortality in mechanically ventilated patients, whereas reaching only the energy target did not impact mortality.</p>
       <p>Apart from highly contributing to the gross mass of body protein, amino acids are signaling molecules in a number of cellular processes, including those related to stress-related catabolism. This implies that not only the amount of amino acid but also their composition profile is important. It should be recalled that muscle wasting and protein loss during the acute phase of critical illnesses could only be mitigated by adequate and optimal supplementation of amino acids, along with energy and micronutrients.</p>
       <p>Optimal nutrition for critically ill patients through the enteral route, though it is strongly recommended in recent guidelines, is difficult in some patients as gastrointestinal tolerance limits the rate of administration and absorption. For instance, diarrhea is commonly observed in critically ill patients and strongly associated with antibiotics, as well as with enteral nutrition if covering more than 60% of energy needs <abbrgrp>
             <abbr bid="B11">11</abbr>
          </abbrgrp>. This frequently limits the prescription of full enteral nutrition support. In such cases, exclusive parenteral nutrition <abbrgrp>
             <abbr bid="B12">12</abbr>
          </abbrgrp>, supplemental parenteral nutrition <abbrgrp>
             <abbr bid="B13">13</abbr>
          </abbrgrp> or a specific administration of amino acids to supplement enterally administered protein may be a therapeutic option. Nutrients administered through the enteral route must be digested, a process which increases the energy and oxygen demand of the intestine, which in turn increases splanchnic blood flow <abbrgrp>
             <abbr bid="B14">14</abbr>
          </abbrgrp>. This effect may not be tolerated by very hemodynamically unstable patients.</p>
       <p>The recommendations for protein provision in guidelines <abbrgrp>
             <abbr bid="B5">5</abbr>
          </abbrgrp>,<abbrgrp>
             <abbr bid="B6">6</abbr>
          </abbrgrp> are derived from expert opinion, rather than from robust scientific evidence. This situation mostly results from the complexity and the costs of measuring protein synthesis and degradation, and the current lack of analytical methods that can obtain results sufficiently rapidly to enable the prescription of nutrition support to be altered appropriately. Weijs and colleagues give a great overview of the complexity involved in assessing protein and specific amino acid requirements, and point out the interdependence between energy and protein provision. We join their call for urgent research to develop new methods to evaluate protein and amino acid requirements, accurately and conveniently, in order to optimize nutrition support for critically ill patients.</p></sec><sec>
          <st>
             <p>Competing interests</p>
          </st>
          <p>The authors declare that they have no competing interests.</p>
       </sec>
    </bdy>
    <bm>
       <refgrp>
          
          <bibl id="B1">
             <title>
                <p>Proteins and amino acids are fundamental to optimal nutrition support in critically ill patients</p>
             </title>
             <aug>
                <au>
                   <snm>Weijs</snm>
                   <fnm>PJM</fnm>
                </au>
                <au>
                   <snm>Cynober</snm>
                   <fnm>L</fnm>
                </au>
                <au>
                   <snm>DeLegge</snm>
                   <fnm>M</fnm>
                </au>
                <au>
                   <snm>Kreymann</snm>
                   <fnm>G</fnm>
                </au>
                <au>
                   <snm>Wernerman</snm>
                   <fnm>J</fnm>
                </au>
                <au>
                   <snm>Wolfe</snm>
                   <fnm>RR</fnm>
                </au>
             </aug>
             <source>Crit Care</source>
             <pubdate>2014</pubdate>
             <xrefbib>
                <pubidlist>
                   <pubid idtype="pmpid" link="fulltext">24410863</pubid>
                </pubidlist>
             </xrefbib>
          </bibl>
          <bibl id="B2">
             <title>
                <p>Metabolic response to injury and sepsis: changes in protein metabolism</p>
             </title>
             <aug>
                <au>
                   <snm>Biolo</snm>
                   <fnm>G</fnm>
                </au>
                <au>
                   <snm>Toigo</snm>
                   <fnm>G</fnm>
                </au>
                <au>
                   <snm>Ciocchi</snm>
                   <fnm>B</fnm>
                </au>
                <au>
                   <snm>Situlin</snm>
                   <fnm>R</fnm>
                </au>
                <au>
                   <snm>Iscra</snm>
                   <fnm>F</fnm>
                </au>
                <au>
                   <snm>Gullo</snm>
                   <fnm>A</fnm>
                </au>
                <au>
                   <snm>Guarnieri</snm>
                   <fnm>G</fnm>
                </au>
             </aug>
             <source>Nutrition</source>
             <pubdate>1997</pubdate>
             <volume>13</volume>
             <fpage>52S</fpage>
             <lpage>57S</lpage>
             <xrefbib>
                <pubidlist>
                   <pubid idtype="doi">10.1016/S0899-9007(97)00206-2</pubid>
                   <pubid idtype="pmpid" link="fulltext">9290110</pubid>
                </pubidlist>
             </xrefbib>
          </bibl>
          <bibl id="B3">
             <title>
                <p>Anabolic resistance in critically ill patients</p>
             </title>
             <aug>
                <au>
                   <snm>Rennie</snm>
                   <fnm>M</fnm>
                </au>
             </aug>
             <source>Crit Care Med</source>
             <pubdate>2009</pubdate>
             <volume>37</volume>
             <fpage>S398</fpage>
             <lpage>S399</lpage>
             <xrefbib>
                <pubidlist>
                   <pubid idtype="doi">10.1097/CCM.0b013e3181b6ec1f</pubid>
                   <pubid idtype="pmpid" link="fulltext">20046126</pubid>
                </pubidlist>
             </xrefbib>
          </bibl>
          <bibl id="B4">
             <title>
                <p>Optimal protein requirements during the first 2&#160;weeks after the onset of critical illness</p>
             </title>
             <aug>
                <au>
                   <snm>Ishibashi</snm>
                   <fnm>N</fnm>
                </au>
                <au>
                   <snm>Plank</snm>
                   <fnm>LD</fnm>
                </au>
                <au>
                   <snm>Sando</snm>
                   <fnm>K</fnm>
                </au>
                <au>
                   <snm>Hill</snm>
                   <fnm>GL</fnm>
                </au>
             </aug>
             <source>Crit Care Med</source>
             <pubdate>1998</pubdate>
             <volume>26</volume>
             <fpage>1529</fpage>
             <lpage>1535</lpage>
             <xrefbib>
                <pubidlist>
                   <pubid idtype="doi">10.1097/00003246-199809000-00020</pubid>
                   <pubid idtype="pmpid" link="fulltext">9751589</pubid>
                </pubidlist>
             </xrefbib>
          </bibl>
          <bibl id="B5">
             <title>
                <p>ESPEN Guidelines on parenteral nutrition: intensive care</p>
             </title>
             <aug>
                <au>
                   <snm>Singer</snm>
                   <fnm>P</fnm>
                </au>
                <au>
                   <snm>Berger</snm>
                   <fnm>MM</fnm>
                </au>
                <au>
                   <snm>Van den Berghe</snm>
                   <fnm>G</fnm>
                </au>
                <au>
                   <snm>Biolo</snm>
                   <fnm>G</fnm>
                </au>
                <au>
                   <snm>Calder</snm>
                   <fnm>P</fnm>
                </au>
                <au>
                   <snm>Forbes</snm>
                   <fnm>A</fnm>
                </au>
                <au>
                   <snm>Griffiths</snm>
                   <fnm>R</fnm>
                </au>
                <au>
                   <snm>Kreyman</snm>
                   <fnm>G</fnm>
                </au>
                <au>
                   <snm>Leverve</snm>
                   <fnm>X</fnm>
                </au>
                <au>
                   <snm>Pichard</snm>
                   <fnm>C</fnm>
                </au>
             </aug>
             <source>Clin Nutr</source>
             <pubdate>2009</pubdate>
             <volume>28</volume>
             <fpage>387</fpage>
             <lpage>400</lpage>
             <xrefbib>
                <pubidlist>
                   <pubid idtype="doi">10.1016/j.clnu.2009.04.024</pubid>
                   <pubid idtype="pmpid" link="fulltext">19505748</pubid>
                </pubidlist>
             </xrefbib>
          </bibl>
          <bibl id="B6">
             <title>
                <p>Guidelines for the provision and assessment of nutrition support therapy in the adult critically ill patient: Society of Critical Care Medicine (SCCM) and American Society for Parenteral and Enteral Nutrition (A.S.P.E.N.)</p>
             </title>
             <aug>
                <au>
                   <snm>McClave</snm>
                   <fnm>SA</fnm>
                </au>
                <au>
                   <snm>Martindale</snm>
                   <fnm>RG</fnm>
                </au>
                <au>
                   <snm>Vanek</snm>
                   <fnm>VW</fnm>
                </au>
                <au>
                   <snm>McCarthy</snm>
                   <fnm>M</fnm>
                </au>
                <au>
                   <snm>Roberts</snm>
                   <fnm>P</fnm>
                </au>
                <au>
                   <snm>Taylor</snm>
                   <fnm>B</fnm>
                </au>
                <au>
                   <snm>Ochoa</snm>
                   <fnm>JB</fnm>
                </au>
                <au>
                   <snm>Napolitano</snm>
                   <fnm>L</fnm>
                </au>
                <au>
                   <snm>Cresci</snm>
                   <fnm>G</fnm>
                </au>
             </aug>
             <source>JPEN J Parenter Enteral Nutr</source>
             <pubdate>2009</pubdate>
             <volume>33</volume>
             <fpage>277</fpage>
             <lpage>316</lpage>
             <xrefbib>
                <pubidlist>
                   <pubid idtype="doi">10.1177/0148607109335234</pubid>
                   <pubid idtype="pmpid" link="fulltext">19398613</pubid>
                </pubidlist>
             </xrefbib>
          </bibl>
          <bibl id="B7">
             <title>
                <p>Optimal protein and energy nutrition decreases mortality in mechanically ventilated, critically ill patients: a prospective observational cohort study</p>
             </title>
             <aug>
                <au>
                   <snm>Weijs</snm>
                   <fnm>PJM</fnm>
                </au>
                <au>
                   <snm>Stapel</snm>
                   <fnm>SN</fnm>
                </au>
                <au>
                   <snm>de Groot</snm>
                   <fnm>SD</fnm>
                </au>
                <au>
                   <snm>Driessen</snm>
                   <fnm>RH</fnm>
                </au>
                <au>
                   <snm>de Jong</snm>
                   <fnm>E</fnm>
                </au>
                <au>
                   <snm>Girbes</snm>
                   <fnm>AR</fnm>
                </au>
                <au>
                   <snm>Strack van Schijndel</snm>
                   <fnm>RJ</fnm>
                </au>
                <au>
                   <snm>Beishuizen</snm>
                   <fnm>A</fnm>
                </au>
             </aug>
             <source>JPEN J Parenter Enteral Nutr</source>
             <pubdate>2012</pubdate>
             <volume>36</volume>
             <fpage>60</fpage>
             <lpage>68</lpage>
             <xrefbib>
                <pubidlist>
                   <pubid idtype="doi">10.1177/0148607111415109</pubid>
                   <pubid idtype="pmpid" link="fulltext">22167076</pubid>
                </pubidlist>
             </xrefbib>
          </bibl>
          <bibl id="B8">
             <title>
                <p>Appropriate protein provision in critical illness: a systematic review and narrative review</p>
             </title>
             <aug>
                <au>
                   <snm>Hoffer</snm>
                   <fnm>LJ</fnm>
                </au>
                <au>
                   <snm>Bistrian</snm>
                   <fnm>BR</fnm>
                </au>
             </aug>
             <source>Am J Clin Nutr</source>
             <pubdate>2012</pubdate>
             <volume>96</volume>
             <fpage>591</fpage>
             <lpage>600</lpage>
             <xrefbib>
                <pubidlist>
                   <pubid idtype="doi">10.3945/ajcn.111.032078</pubid>
                   <pubid idtype="pmpid" link="fulltext">22811443</pubid>
                </pubidlist>
             </xrefbib>
          </bibl>
          <bibl id="B9">
             <title>
                <p>Perspective: How to evaluate studies on perioperative nutrition? Considerations about the definition of optimal nutrition for patients and its key role in the comparison of the results of studies on nutritional intervention</p>
             </title>
             <aug>
                <au>
                   <snm>Sauerwein</snm>
                   <fnm>HP</fnm>
                </au>
                <au>
                   <snm>Strack van Schijndel</snm>
                   <fnm>RJ</fnm>
                </au>
             </aug>
             <source>Clin Nutr</source>
             <pubdate>2007</pubdate>
             <volume>26</volume>
             <fpage>154</fpage>
             <lpage>158</lpage>
             <xrefbib>
                <pubidlist>
                   <pubid idtype="doi">10.1016/j.clnu.2006.08.001</pubid>
                   <pubid idtype="pmpid" link="fulltext">16996171</pubid>
                </pubidlist>
             </xrefbib>
          </bibl>
          <bibl id="B10">
             <title>
                <p>The ratio of energy expenditure to nitrogen loss in diverse patient groups - a systematic review</p>
             </title>
             <aug>
                <au>
                   <snm>Kreymann</snm>
                   <fnm>G</fnm>
                </au>
                <au>
                   <snm>DeLegge</snm>
                   <fnm>MH</fnm>
                </au>
                <au>
                   <snm>Luft</snm>
                   <fnm>G</fnm>
                </au>
                <au>
                   <snm>Hise</snm>
                   <fnm>ME</fnm>
                </au>
                <au>
                   <snm>Zaloga</snm>
                   <fnm>GP</fnm>
                </au>
             </aug>
             <source>Clin Nutr</source>
             <pubdate>2012</pubdate>
             <volume>31</volume>
             <fpage>168</fpage>
             <lpage>175</lpage>
             <xrefbib>
                <pubidlist>
                   <pubid idtype="doi">10.1016/j.clnu.2011.12.004</pubid>
                   <pubid idtype="pmpid" link="fulltext">22385731</pubid>
                </pubidlist>
             </xrefbib>
          </bibl>
          <bibl id="B11">
             <title>
                <p>Diarrhoea in the intensive care unit: respective contribution of feeding and antibiotics</p>
             </title>
             <aug>
                <au>
                   <snm>Thibault</snm>
                   <fnm>R</fnm>
                </au>
                <au>
                   <snm>Graf</snm>
                   <fnm>S</fnm>
                </au>
                <au>
                   <snm>Clerc</snm>
                   <fnm>A</fnm>
                </au>
                <au>
                   <snm>Delieuvin</snm>
                   <fnm>N</fnm>
                </au>
                <au>
                   <snm>Heidegger</snm>
                   <fnm>CP</fnm>
                </au>
                <au>
                   <snm>Pichard</snm>
                   <fnm>C</fnm>
                </au>
             </aug>
             <source>Crit Care</source>
             <pubdate>2013</pubdate>
             <volume>17</volume>
             <fpage>R153</fpage>
             <xrefbib>
                <pubidlist>
                   <pubid idtype="doi">10.1186/cc12832</pubid>
                   <pubid idtype="pmpid" link="fulltext">23883438</pubid>
                </pubidlist>
             </xrefbib>
          </bibl>
          <bibl id="B12">
             <title>
                <p>Early parenteral nutrition in critically ill patients with short-term relative contraindications to early enteral nutrition: a randomized controlled trial</p>
             </title>
             <aug>
                <au>
                   <snm>Doig</snm>
                   <fnm>GS</fnm>
                </au>
                <au>
                   <snm>Simpson</snm>
                   <fnm>F</fnm>
                </au>
                <au>
                   <snm>Sweetman</snm>
                   <fnm>EA</fnm>
                </au>
                <au>
                   <snm>Finfer</snm>
                   <fnm>SR</fnm>
                </au>
                <au>
                   <snm>Cooper</snm>
                   <fnm>DJ</fnm>
                </au>
                <au>
                   <snm>Heighes</snm>
                   <fnm>PT</fnm>
                </au>
                <au>
                   <snm>Davies</snm>
                   <fnm>AR</fnm>
                </au>
                <au>
                   <snm>O'Leary</snm>
                   <fnm>M</fnm>
                </au>
                <au>
                   <snm>Solano</snm>
                   <fnm>T</fnm>
                </au>
                <au>
                   <snm>Peake</snm>
                   <fnm>S</fnm>
                </au>
             </aug>
             <source>JAMA</source>
             <pubdate>2013</pubdate>
             <volume>309</volume>
             <fpage>2130</fpage>
             <lpage>2138</lpage>
             <xrefbib>
                <pubidlist>
                   <pubid idtype="doi">10.1001/jama.2013.5124</pubid>
                   <pubid idtype="pmpid" link="fulltext">23689848</pubid>
                </pubidlist>
             </xrefbib>
          </bibl>
          <bibl id="B13">
             <title>
                <p>Optimization of energy provision with supplemental parenteral nutrition (SPN) improves the clinical outcome of critically ill patients: a randomized controlled trial</p>
             </title>
             <aug>
                <au>
                   <snm>Heidegger</snm>
                   <fnm>CP</fnm>
                </au>
                <au>
                   <snm>Berger</snm>
                   <fnm>MM</fnm>
                </au>
                <au>
                   <snm>Graf</snm>
                   <fnm>S</fnm>
                </au>
                <au>
                   <snm>Zingg</snm>
                   <fnm>W</fnm>
                </au>
                <au>
                   <snm>Darmon</snm>
                   <fnm>P</fnm>
                </au>
                <au>
                   <snm>Costanza</snm>
                   <fnm>MC</fnm>
                </au>
                <au>
                   <snm>Thibault</snm>
                   <fnm>R</fnm>
                </au>
                <au>
                   <snm>Pichard</snm>
                   <fnm>C</fnm>
                </au>
             </aug>
             <source>Lancet</source>
             <pubdate>2013</pubdate>
             <volume>381</volume>
             <fpage>385</fpage>
             <lpage>393</lpage>
             <xrefbib>
                <pubidlist>
                   <pubid idtype="doi">10.1016/S0140-6736(12)61351-8</pubid>
                   <pubid idtype="pmpid" link="fulltext">23218813</pubid>
                </pubidlist>
             </xrefbib>
          </bibl>
          <bibl id="B14">
             <title>
                <p>Changes in superior mesenteric artery blood flow after oral, enteral, and parenteral feeding in humans</p>
             </title>
             <aug>
                <au>
                   <snm>Gatt</snm>
                   <fnm>M</fnm>
                </au>
                <au>
                   <snm>MacFie</snm>
                   <fnm>J</fnm>
                </au>
                <au>
                   <snm>Anderson</snm>
                   <fnm>A</fnm>
                </au>
                <au>
                   <snm>Howell</snm>
                   <fnm>G</fnm>
                </au>
                <au>
                   <snm>Reddy</snm>
                   <fnm>B</fnm>
                </au>
                <au>
                   <snm>Suppiah</snm>
                   <fnm>A</fnm>
                </au>
                <au>
                   <snm>Renwick</snm>
                   <fnm>I</fnm>
                </au>
                <au>
                   <snm>Mitchell</snm>
                   <fnm>CJ</fnm>
                </au>
             </aug>
             <source>Crit Care Med</source>
             <pubdate>2009</pubdate>
             <volume>37</volume>
             <fpage>171</fpage>
             <lpage>176</lpage>
             <xrefbib>
                <pubidlist>
                   <pubid idtype="doi">10.1097/CCM.0b013e318192fb44</pubid>
                   <pubid idtype="pmpid" link="fulltext">19050615</pubid>
                </pubidlist>
             </xrefbib>
          </bibl>
       </refgrp>
    </bm>
 </art>