<art>
    <ui>s40337-014-0025-z</ui>
    <ji>2050-2974</ji>
    <fm>
       <dochead>Letter to the Editor</dochead>
       <bibl>
          <title>
             <p>On the differentiation between trait and state food craving: Half-year retest-reliability of the <it>Food Cravings Questionnaire-Trait-reduced</it> (FCQ-T-r) and the <it>Food Cravings Questionnaire-State</it> (FCQ-S)</p>
          </title>
          <aug>
             <au ca="yes" id="A1">
                <snm>Meule</snm>
                <fnm>Adrian</fnm>
                <insr iid="I1"/>
                <insr iid="I2"/>
                <email>adrian.meule@uni-wuerzburg.de</email>
             </au>
             <au id="A2">
                <snm>Teran</snm>
                <mnm>Beck</mnm>
                <fnm>Carina</fnm>
                <insr iid="I1"/>
                <email>carinabeckteran@gmx.de</email>
             </au>
             <au id="A3">
                <snm>Berker</snm>
                <fnm>Jasmin</fnm>
                <insr iid="I1"/>
                <email>jasmin.berker@diessen.info</email>
             </au>
             <au id="A4">
                <snm>Gr&#252;ndel</snm>
                <fnm>Tilman</fnm>
                <insr iid="I1"/>
                <email>tilman.gruendel@web.de</email>
             </au>
             <au id="A5">
                <snm>Mayerhofer</snm>
                <fnm>Martina</fnm>
                <insr iid="I1"/>
                <email>martina.mayerhofer@web.de</email>
             </au>
             <au id="A6">
                <snm>Platte</snm>
                <fnm>Petra</fnm>
                <insr iid="I1"/>
                <email>platte@psychologie.uni-wuerzburg.de</email>
             </au>
          </aug>
          <insg>
             <ins id="I1">
                <p>Institute of Psychology, University of W&#252;rzburg, W&#252;rzburg, Germany</p>
             </ins>
             <ins id="I2">
                <p>Hospital for Child and Adolescent Psychiatry, LWL University Hospital of the Ruhr University Bochum, Hamm, Germany</p>
             </ins>
          </insg>
          <source>Journal of Eating Disorders</source>
          <issn>2050-2974</issn>
          <pubdate>2014</pubdate>
          <volume>2</volume>
          <issue>1</issue>
          <fpage>25</fpage>
          <url>http://www.jeatdisord.com/content/2/1/25</url>
          <xrefbib>
             <pubidlist>
                <pubid idtype="doi">10.1186/s40337-014-0025-z</pubid>
                <pubid idtype="pmpid">25356313</pubid>
             </pubidlist>
          </xrefbib>
       </bibl>
       <history>
          <rec>
             <date>
                <day>23</day>
                <month>6</month>
                <year>2014</year>
             </date>
          </rec>
          <acc>
             <date>
                <day>17</day>
                <month>8</month>
                <year>2014</year>
             </date>
          </acc>
          <pub>
             <date>
                <day>6</day>
                <month>9</month>
                <year>2014</year>
             </date>
          </pub>
       </history>
       <cpyrt>
          <year>2014</year>
          <collab>Meule et al.; licensee BioMed Central Ltd.</collab>
       </cpyrt>
       <kwdg>
          <kwd>Food craving</kwd>
          <kwd>Food Cravings Questionnaires</kwd>
          <kwd>Retest-reliability</kwd>
          <kwd>University students</kwd>
       </kwdg>
       <abs>
          <sec>
             <st>
                <p>Abstract</p>
             </st>
             <sec>
                <st>
                   <p>Background</p>
                </st>
                <p>Food craving refers to an intense desire to consume a specific food. The <it>Food Cravings Questionnaires</it> (FCQs) assess food cravings on a trait and a state level.</p>
             </sec>
             <sec>
                <st>
                   <p>Method</p>
                </st>
                <p>The current study examined half-year retest-reliability of the <it>Food Cravings Questionnaire-Trait-reduced</it> (FCQ-T-r) and the <it>Food Cravings Questionnaire-State</it> (FCQ-S) and reports associations with current food deprivation in female students.</p>
             </sec>
             <sec>
                <st>
                   <p>Results</p>
                </st>
                <p>The FCQ-T-r had higher retest-reliability (<it>r</it>
                   <sub>tt</sub>&#8201;=&#8201;.74) than the FCQ-S (<it>r</it>
                   <sub>tt</sub>&#8201;=&#8201;.39). Although trait food craving was correlated with state food craving, it was unaffected by current food deprivation.</p>
             </sec>
             <sec>
                <st>
                   <p>Conclusions</p>
                </st>
                <p>Although state and trait food craving are interdependent, the FCQs are able to differentiate between the two. As scores of the FCQ-T-r represent a stable trait, but are also sensitive to changes in eating behavior, they may be useful for the investigation of the course of eating disorders and obesity.</p>
             </sec>
          </sec>
       </abs>
    </fm>
    <bdy><sec><st><p/></st><p>Food craving refers to an intense desire or urge to consume a specific food, of which the most often craved one is chocolate in western societies <abbrgrp>
             <abbr bid="B1">1</abbr>
          </abbrgrp>. Food cravings are common and, thus, experienced by most people from time to time <abbrgrp>
             <abbr bid="B1">1</abbr>
          </abbrgrp>. However, more frequent and more intense food cravings are associated with eating disorders such as bulimia nervosa or binge eating disorder and obesity <abbrgrp>
             <abbr bid="B2">2</abbr>
          </abbrgrp>,<abbrgrp>
             <abbr bid="B3">3</abbr>
          </abbrgrp>.</p>
       <p>The <it>Food Cravings Questionnaires</it> (FCQs) assess habitual experiences of food craving and current food craving <abbrgrp>
             <abbr bid="B4">4</abbr>
          </abbrgrp>. The <it>Food Cravings Questionnaire-Trait</it> (FCQ-T; sample items: &#8220;If I give in to a food craving, all control is lost.&#8221;, &#8220;If I am craving something, thoughts of eating it consume me.&#8221;) contains 39 items with response categories ranging from <it>never/not applicable</it> to <it>always</it> while the <it>Food Cravings Questionnaire-State</it> (FCQ-S; sample items: &#8220;I have an intense desire to eat [one or more specific foods].&#8221;, &#8220;I know I&#8217;m going to keep on thinking about [one or more specific foods] until I actually have it.&#8221;) contains 15 items with response categories ranging from <it>strongly disagree</it> to <it>strongly agree</it>. Most recently, a reduced version of the FCQ-T (FCQ-T-r, 15 items) has been introduced <abbrgrp>
             <abbr bid="B5">5</abbr>
          </abbrgrp>.</p>
       <p>Scores of the FCQ-T/FCQ-T-r and the FCQ-S are weakly, positively correlated with each other <abbrgrp>
             <abbr bid="B5">5</abbr>
          </abbrgrp>. Moreover, scores of the FCQ-T/FCQ-T-r predict food-cue elicited craving as indicated by increases in FCQ-S scores <abbrgrp>
             <abbr bid="B5">5</abbr>
          </abbrgrp>&#8211;<abbrgrp>
             <abbr bid="B7">7</abbr>
          </abbrgrp>. Nevertheless, previous findings indicate that the FCQs indeed differentially assess trait and state food craving. For example, scores of the FCQ-S are affected by momentary physiological and psychological states and environmental circumstances such as length of food deprivation, current affect, food intake, and food-cue exposure <abbrgrp>
             <abbr bid="B4">4</abbr>
          </abbrgrp>,<abbrgrp>
             <abbr bid="B8">8</abbr>
          </abbrgrp>&#8211;<abbrgrp>
             <abbr bid="B12">12</abbr>
          </abbrgrp>. Scores of the FCQ-T are largely unaffected by such factors (ibid.). Moreover, three-week retest-reliability for the FCQ-T is high (<it>r</it>
          <sub>tt</sub>&#8201;&gt;&#8201;.80) while it is, expectedly, lower for the FCQ-S (<it>r</it>
          <sub>tt</sub>&#8201;&lt;&#8201;.60) <abbrgrp>
             <abbr bid="B4">4</abbr>
          </abbrgrp>,<abbrgrp>
             <abbr bid="B9">9</abbr>
          </abbrgrp>,<abbrgrp>
             <abbr bid="B11">11</abbr>
          </abbrgrp>. Yet, the FCQ-T is also sensitive to changes in eating behavior as scores were decreased after bariatric surgery and behavioral weight-loss treatment in obese individuals <abbrgrp>
             <abbr bid="B13">13</abbr>
          </abbrgrp>,<abbrgrp>
             <abbr bid="B14">14</abbr>
          </abbrgrp> or after a biofeedback-based intervention in high trait food cravers <abbrgrp>
             <abbr bid="B15">15</abbr>
          </abbrgrp>.</p>
       <p>To examine if habitual experiences of food craving actually represent a stable trait, half-year retest-reliability of the FCQ-T-r and the FCQ-S was evaluated in a student sample. Relationships between the two measures and associations with current food deprivation were also explored as a further indication for the differentiation between state and trait food craving.</p></sec><sec>
          <st>
             <p>1
 						Method</p>
          </st>
          <p>This study adhered to the guidelines outlined in the Declaration of Helsinki as revised in 2008. Female university freshmen (<it>N</it>&#8201;=&#8201;133; age <it>M</it>&#8201;=&#8201;20.08&#160;years, <it>SD</it>&#8201;=&#8201;2.68; body mass index <it>M</it>&#8201;=&#8201;22.01&#160;kg/m<sup>2</sup>, <it>SD</it>&#8201;=&#8201;2.66) completed paper-and-pencil versions of the FCQ-T-r and FCQ-S among other measures in the laboratory at the beginning of the first semester (first measurement, M1). One hundred and twenty-one participants completed those measures again at the beginning of the second semester (second measurement, M2). Mean time between measurements was <it>M</it>&#8201;=&#8201;170.65&#160;days (<it>SD</it>&#8201;=&#8201;9.00, Range: 143&#8211;183). Mean food deprivation (time since the last meal) was <it>M</it>
             <sub>M1</sub>&#8201;=&#8201;2.40 (<it>SD</it>&#8201;=&#8201;2.28, Range: 0&#8211;15) and <it>M</it>
             <sub>M2</sub>&#8201;=&#8201;3.11 (<it>SD</it>&#8201;=&#8201;3.78, Range: 0.25-21.50) hours. Mean FCQ-T-r scores were <it>M</it>
             <sub>M1</sub>&#8201;=&#8201;38.77 (<it>SD</it>&#8201;=&#8201;10.12, Range: 20&#8211;62) and <it>M</it>
             <sub>M2</sub>&#8201;=&#8201;36.70 (<it>SD</it>&#8201;=&#8201;11.02, Range: 19&#8211;79). Mean FCQ-S scores were <it>M</it>
             <sub>M1</sub>&#8201;=&#8201;34.64 (<it>SD</it>&#8201;=&#8201;9.82, Range: 15&#8211;59) and <it>M</it>
             <sub>M2</sub>&#8201;=&#8201;31.98 (<it>SD</it>&#8201;=&#8201;11.48, Range: 15&#8211;55). Internal consistency of the FCQ-T-r was &#945;<sub>M1</sub>&#8201;=&#8201;.90 and &#945;<sub>M2</sub>&#8201;=&#8201;.93. Internal consistency of the FCQ-S was &#945;<sub>M1</sub>&#8201;=&#8201;.89 and &#945;<sub>M2</sub>&#8201;=&#8201;.94. Pearson correlations were computed between scores at the first and second measurement for the FCQ-T-r, the FCQ-S, and food deprivation. Furthermore, correlations between FCQ-T-r scores, FCQ-S scores, and food deprivation were calculated for each measurement separately. Finally, we explored if controlling for BMI (using partial correlations) influenced any of these correlations.</p>
       </sec>
       <sec>
          <st>
             <p>2
 						Results</p>
          </st>
          <p>Retest-reliability of the FCQ-T-r was <it>r</it>
             <sub>tt</sub>&#8201;=&#8201;.74 (<it>p</it>&#8201;&lt;&#8201;.001; Figure&#160;<figr fid="F1">1</figr>) and was <it>r</it>
             <sub>tt</sub>&#8201;=&#8201;.39 (<it>p</it>&#8201;&lt;&#8201;.001) for the FCQ-S. Scores of the FCQ-T-r and FCQ-S were weakly correlated with each other (<it>r</it>
             <sub>M1</sub>&#8201;=&#8201;.18, <it>p</it>&#8201;=&#8201;.04; <it>r</it>
             <sub>M2</sub>&#8201;=&#8201;.33, <it>p</it>&#8201;&lt;&#8201;.001). Food deprivation of the two measurement was weakly correlated (<it>r</it>&#8201;=&#8201;.16, <it>p</it>&#8201;=&#8201;.08). Scores of the FCQ-T-r were unrelated to current food deprivation (<it>r</it>
             <sub>M1</sub>&#8201;=&#8201;&#8722;.05, <it>ns</it>; <it>r</it>
             <sub>M2</sub>&#8201;=&#8201;&#8722;.01, <it>ns</it>). Scores of the FCQ-S were weakly correlated with current food deprivation (<it>r</it>
             <sub>M1</sub>&#8201;=&#8201;.28, <it>p</it>&#8201;=&#8201;.001; <it>r</it>
             <sub>M2</sub>&#8201;=&#8201;.32, <it>p</it>&#8201;&lt;&#8201;.001). Controlling for BMI did not change any of these correlations.</p>
          
             <fig id="F1">
                <title>
                   <p>Figure 1</p>
                </title>
                <caption>
                   <p>Scatterplot showing the correlation between scores on the</p>
                </caption>
                <text>
                   <p>
                      <b>Scatterplot showing the correlation between scores on the</b>
                      <b>
                         <it>Food Cravings Questionnaire-Trait-reduced</it>
                      </b>
                      <b>(FCQ-T-r) between the two measurements (</b>
                      <b>
                         <it>r</it>
                      </b>
                      <sub>
                         <b>tt</b>
                      </sub>&#8201;
                                     <b>=&#8201;.74).</b>
                   </p>
                </text>
                <graphic file="s40337-014-0025-z-1"/>
             </fig>
          
       </sec>
       <sec>
          <st>
             <p>3
 						Discussion</p>
          </st>
          <p>The current results suggest that the FCQ-T-r has high retest-reliability and, thus, does indeed assess experiences of food craving as a stable trait. Although the correlation was somewhat lower than three-week retest-reliability reported for the FCQ-T <abbrgrp>
                <abbr bid="B4">4</abbr>
             </abbrgrp>,<abbrgrp>
                <abbr bid="B9">9</abbr>
             </abbrgrp>,<abbrgrp>
                <abbr bid="B11">11</abbr>
             </abbrgrp>, we would argue that this nonetheless represents a high retest-reliability, considering the long time span of almost half a year. FCQ-S scores on the two measurements were also positively correlated with each other, but this correlation was substantially lower than that of the FCQ-T-r. Current food deprivation was positively correlated with FCQ-S, but not FCQ-T-r scores, suggesting that FCQ-T-r scores are largely unaffected by such state-dependent variables.</p>
          <p>Interpretation of findings is limited by the fact that all participants were female university students, who were predominantly normal-weight. Thus, future studies need to examine if similar results can be obtained in other samples, for example in men or in clinical samples. Moreover, we did not instruct participants to keep a similar food deprivation period for both measurements or assess current affect, which has been shown to be related to FCQ-S scores <abbrgrp>
                <abbr bid="B11">11</abbr>
             </abbrgrp>. Thus, it may be that the FCQ-S indeed has high retest-reliability if such conditions are kept constant.</p>
          <p>Nevertheless, the current results support that trait and state food cravings are interdependent, but that the FCQs indeed differentially assess both constructs. The FCQ-T-r could be a useful tool for the investigation of the course of eating disorders and obesity as it measures a stable trait on the one hand, but is sensitive to treatment changes on the other hand. Thus, it could be used to monitor treatment changes or may even be able to predict treatment success or, at the end of treatment, to predict relapse in patients with eating disorders and obesity.</p>
       </sec>
       <sec>
          <st>
             <p>Competing interests</p>
          </st>
          <p>The authors declare that they have no competing interests.</p>
       </sec>
       <sec>
          <st>
             <p>Authors&#8217; contributions</p>
          </st>
          <p>AM and PP designed the study. CBT, JB, TG, and MM collected the data. AM analyzed the data and wrote the first draft of the manuscript. All authors read and approved the final manuscript.</p>
       </sec>
    </bdy>
    <bm>
       <ack>
          <sec>
             <st>
                <p>Acknowledgements</p>
             </st>
             <p>There are no acknowledgements to report.</p>
          </sec>
       </ack>
       <refgrp>
          
          <bibl id="B1">
             <title>
                <p>The psychology of food craving</p>
             </title>
             <aug>
                <au>
                   <snm>Hill</snm>
                   <fnm>AJ</fnm>
                </au>
             </aug>
             <source>Proc Nutr Soc</source>
             <pubdate>2007</pubdate>
             <volume>66</volume>
             <fpage>277</fpage>
             <lpage>285</lpage>
             <xrefbib>
                <pubidlist>
                   <pubid idtype="doi">10.1017/S0029665107005502</pubid>
                   <pubid idtype="pmpid" link="fulltext">17466108</pubid>
                </pubidlist>
             </xrefbib>
          </bibl>
          <bibl id="B2">
             <title>
                <p>Psychological characteristics of morbidly obese candidates for bariatric surgery</p>
             </title>
             <aug>
                <au>
                   <snm>Abil&#233;s</snm>
                   <fnm>V</fnm>
                </au>
                <au>
                   <snm>Rodr&#237;guez-Ruiz</snm>
                   <fnm>S</fnm>
                </au>
                <au>
                   <snm>Abil&#233;s</snm>
                   <fnm>J</fnm>
                </au>
                <au>
                   <snm>Mellado</snm>
                   <fnm>C</fnm>
                </au>
                <au>
                   <snm>Garc&#237;a</snm>
                   <fnm>A</fnm>
                </au>
                <au>
                   <snm>P&#233;rez de la Cruz</snm>
                   <fnm>A</fnm>
                </au>
                <au>
                   <snm>Fern&#225;ndez-Santaella</snm>
                   <fnm>MC</fnm>
                </au>
             </aug>
             <source>Obes Surg</source>
             <pubdate>2010</pubdate>
             <volume>20</volume>
             <fpage>161</fpage>
             <lpage>167</lpage>
             <xrefbib>
                <pubidlist>
                   <pubid idtype="doi">10.1007/s11695-008-9726-1</pubid>
                   <pubid idtype="pmpid" link="fulltext">18958537</pubid>
                </pubidlist>
             </xrefbib>
          </bibl>
          <bibl id="B3">
             <title>
                <p>State and trait food craving in people with bulimic eating disorders</p>
             </title>
             <aug>
                <au>
                   <snm>Van den Eynde</snm>
                   <fnm>F</fnm>
                </au>
                <au>
                   <snm>Koskina</snm>
                   <fnm>A</fnm>
                </au>
                <au>
                   <snm>Syrad</snm>
                   <fnm>H</fnm>
                </au>
                <au>
                   <snm>Guillaume</snm>
                   <fnm>S</fnm>
                </au>
                <au>
                   <snm>Broadbent</snm>
                   <fnm>H</fnm>
                </au>
                <au>
                   <snm>Campbell</snm>
                   <fnm>IC</fnm>
                </au>
                <au>
                   <snm>Schmidt</snm>
                   <fnm>U</fnm>
                </au>
             </aug>
             <source>Eat Behav</source>
             <pubdate>2012</pubdate>
             <volume>13</volume>
             <fpage>414</fpage>
             <lpage>417</lpage>
             <xrefbib>
                <pubidlist>
                   <pubid idtype="doi">10.1016/j.eatbeh.2012.07.007</pubid>
                   <pubid idtype="pmpid" link="fulltext">23121801</pubid>
                </pubidlist>
             </xrefbib>
          </bibl>
          <bibl id="B4">
             <title>
                <p>The development and validation of the state and trait Food-Cravings Questionnaires</p>
             </title>
             <aug>
                <au>
                   <snm>Cepeda-Benito</snm>
                   <fnm>A</fnm>
                </au>
                <au>
                   <snm>Gleaves</snm>
                   <fnm>DH</fnm>
                </au>
                <au>
                   <snm>Williams</snm>
                   <fnm>TL</fnm>
                </au>
                <au>
                   <snm>Erath</snm>
                   <fnm>SA</fnm>
                </au>
             </aug>
             <source>Behav Ther</source>
             <pubdate>2000</pubdate>
             <volume>31</volume>
             <fpage>151</fpage>
             <lpage>173</lpage>
             <xrefbib>
                <pubidlist>
                   <pubid idtype="doi">10.1016/S0005-7894(00)80009-X</pubid>
                </pubidlist>
             </xrefbib>
          </bibl>
          <bibl id="B5">
             <title>
                <p>A short version of the <it>Food Cravings Questionnaire - Trait</it>: The FCQ-T-reduced</p>
             </title>
             <aug>
                <au>
                   <snm>Meule</snm>
                   <fnm>A</fnm>
                </au>
                <au>
                   <snm>Hermann</snm>
                   <fnm>T</fnm>
                </au>
                <au>
                   <snm>K&#252;bler</snm>
                   <fnm>A</fnm>
                </au>
             </aug>
             <source>Front Psychol</source>
             <pubdate>2014</pubdate>
             <volume>5</volume>
             <fpage>&#4447;</fpage>
             <note>doi:10.3389/fpsyg.2014.00190</note>
          </bibl>
          <bibl id="B6">
             <title>
                <p>Double trouble: Trait food craving and impulsivity interactively predict food-cue affected behavioral inhibition</p>
             </title>
             <aug>
                <au>
                   <snm>Meule</snm>
                   <fnm>A</fnm>
                </au>
                <au>
                   <snm>K&#252;bler</snm>
                   <fnm>A</fnm>
                </au>
             </aug>
             <source>Appetite</source>
             <pubdate>2014</pubdate>
             <volume>79</volume>
             <fpage>174</fpage>
             <lpage>182</lpage>
             <xrefbib>
                <pubidlist>
                   <pubid idtype="doi">10.1016/j.appet.2014.04.014</pubid>
                   <pubid idtype="pmpid" link="fulltext">24768896</pubid>
                </pubidlist>
             </xrefbib>
          </bibl>
          <bibl id="B7">
             <title>
                <p>High-calorie food-cues impair working memory performance in high and low food cravers</p>
             </title>
             <aug>
                <au>
                   <snm>Meule</snm>
                   <fnm>A</fnm>
                </au>
                <au>
                   <snm>Skirde</snm>
                   <fnm>AK</fnm>
                </au>
                <au>
                   <snm>Freund</snm>
                   <fnm>R</fnm>
                </au>
                <au>
                   <snm>V&#246;gele</snm>
                   <fnm>C</fnm>
                </au>
                <au>
                   <snm>K&#252;bler</snm>
                   <fnm>A</fnm>
                </au>
             </aug>
             <source>Appetite</source>
             <pubdate>2012</pubdate>
             <volume>59</volume>
             <fpage>264</fpage>
             <lpage>269</lpage>
             <xrefbib>
                <pubidlist>
                   <pubid idtype="doi">10.1016/j.appet.2012.05.010</pubid>
                   <pubid idtype="pmpid" link="fulltext">22613059</pubid>
                </pubidlist>
             </xrefbib>
          </bibl>
          <bibl id="B8">
             <title>
                <p>Relationship of gender and eating disorder symptoms to reported cravings for food: construct validation of state and trait craving questionnaires in Spanish</p>
             </title>
             <aug>
                <au>
                   <snm>Cepeda-Benito</snm>
                   <fnm>A</fnm>
                </au>
                <au>
                   <snm>Fernandez</snm>
                   <fnm>MC</fnm>
                </au>
                <au>
                   <snm>Moreno</snm>
                   <fnm>S</fnm>
                </au>
             </aug>
             <source>Appetite</source>
             <pubdate>2003</pubdate>
             <volume>40</volume>
             <fpage>47</fpage>
             <lpage>54</lpage>
             <xrefbib>
                <pubidlist>
                   <pubid idtype="doi">10.1016/S0195-6663(02)00145-9</pubid>
                   <pubid idtype="pmpid" link="fulltext">12631504</pubid>
                </pubidlist>
             </xrefbib>
          </bibl>
          <bibl id="B9">
             <title>
                <p>Psychometric properties of the State and Trait Food Cravings Questionnaires among overweight and obese persons</p>
             </title>
             <aug>
                <au>
                   <snm>Vander Wal</snm>
                   <fnm>JS</fnm>
                </au>
                <au>
                   <snm>Johnston</snm>
                   <fnm>KA</fnm>
                </au>
                <au>
                   <snm>Dhurandhar</snm>
                   <fnm>NV</fnm>
                </au>
             </aug>
             <source>Eat Behav</source>
             <pubdate>2007</pubdate>
             <volume>8</volume>
             <fpage>211</fpage>
             <lpage>223</lpage>
             <xrefbib>
                <pubidlist>
                   <pubid idtype="doi">10.1016/j.eatbeh.2006.06.002</pubid>
                   <pubid idtype="pmpid" link="fulltext">17336791</pubid>
                </pubidlist>
             </xrefbib>
          </bibl>
          <bibl id="B10">
             <title>
                <p>Short-term effect of eggs on satiety in overweight and obese subjects</p>
             </title>
             <aug>
                <au>
                   <snm>Vander Wal</snm>
                   <fnm>JS</fnm>
                </au>
                <au>
                   <snm>Marth</snm>
                   <fnm>JM</fnm>
                </au>
                <au>
                   <snm>Khosla</snm>
                   <fnm>P</fnm>
                </au>
                <au>
                   <snm>Jen</snm>
                   <fnm>K-LC</fnm>
                </au>
                <au>
                   <snm>Dhurandhar</snm>
                   <fnm>NV</fnm>
                </au>
             </aug>
             <source>J Am Coll Nutr</source>
             <pubdate>2005</pubdate>
             <volume>24</volume>
             <fpage>510</fpage>
             <lpage>515</lpage>
             <xrefbib>
                <pubidlist>
                   <pubid idtype="doi">10.1080/07315724.2005.10719497</pubid>
                   <pubid idtype="pmpid" link="fulltext">16373948</pubid>
                </pubidlist>
             </xrefbib>
          </bibl>
          <bibl id="B11">
             <title>
                <p>Food cravings discriminate differentially between successful and unsuccessful dieters and non-dieters. Validation of the Food Craving Questionnaires in German</p>
             </title>
             <aug>
                <au>
                   <snm>Meule</snm>
                   <fnm>A</fnm>
                </au>
                <au>
                   <snm>Lutz</snm>
                   <fnm>A</fnm>
                </au>
                <au>
                   <snm>V&#246;gele</snm>
                   <fnm>C</fnm>
                </au>
                <au>
                   <snm>K&#252;bler</snm>
                   <fnm>A</fnm>
                </au>
             </aug>
             <source>Appetite</source>
             <pubdate>2012</pubdate>
             <volume>58</volume>
             <fpage>88</fpage>
             <lpage>97</lpage>
             <xrefbib>
                <pubidlist>
                   <pubid idtype="doi">10.1016/j.appet.2011.09.010</pubid>
                   <pubid idtype="pmpid" link="fulltext">21983051</pubid>
                </pubidlist>
             </xrefbib>
          </bibl>
          <bibl id="B12">
             <title>
                <p>Food-cue affected motor response inhibition and self-reported dieting success: a pictorial affective shifting task</p>
             </title>
             <aug>
                <au>
                   <snm>Meule</snm>
                   <fnm>A</fnm>
                </au>
                <au>
                   <snm>Lutz</snm>
                   <fnm>APC</fnm>
                </au>
                <au>
                   <snm>Krawietz</snm>
                   <fnm>V</fnm>
                </au>
                <au>
                   <snm>St&#252;tzer</snm>
                   <fnm>J</fnm>
                </au>
                <au>
                   <snm>V&#246;gele</snm>
                   <fnm>C</fnm>
                </au>
                <au>
                   <snm>K&#252;bler</snm>
                   <fnm>A</fnm>
                </au>
             </aug>
             <source>Front Psychol</source>
             <pubdate>2014</pubdate>
             <volume>5</volume>
             <fpage>&#4447;</fpage>
             <note>doi:10.3389/fpsyg.2014.00216</note>
          </bibl>
          <bibl id="B13">
             <title>
                <p>Relationship of cravings with weight loss and hunger. Results from a 6 month worksite weight loss intervention</p>
             </title>
             <aug>
                <au>
                   <snm>Batra</snm>
                   <fnm>P</fnm>
                </au>
                <au>
                   <snm>Das</snm>
                   <fnm>SK</fnm>
                </au>
                <au>
                   <snm>Salinardi</snm>
                   <fnm>T</fnm>
                </au>
                <au>
                   <snm>Robinson</snm>
                   <fnm>L</fnm>
                </au>
                <au>
                   <snm>Saltzman</snm>
                   <fnm>E</fnm>
                </au>
                <au>
                   <snm>Scott</snm>
                   <fnm>T</fnm>
                </au>
                <au>
                   <snm>Pittas</snm>
                   <fnm>AG</fnm>
                </au>
                <au>
                   <snm>Roberts</snm>
                   <fnm>SB</fnm>
                </au>
             </aug>
             <source>Appetite</source>
             <pubdate>2013</pubdate>
             <volume>69</volume>
             <fpage>1</fpage>
             <lpage>7</lpage>
             <xrefbib>
                <pubidlist>
                   <pubid idtype="doi">10.1016/j.appet.2013.05.002</pubid>
                   <pubid idtype="pmpid" link="fulltext">23684901</pubid>
                </pubidlist>
             </xrefbib>
          </bibl>
          <bibl id="B14">
             <title>
                <p>Effects of laparoscopic sleeve gastrectomy on attentional processing of food-related information: evidence from eye-tracking</p>
             </title>
             <aug>
                <au>
                   <snm>Giel</snm>
                   <fnm>KE</fnm>
                </au>
                <au>
                   <snm>Rieber</snm>
                   <fnm>N</fnm>
                </au>
                <au>
                   <snm>Enck</snm>
                   <fnm>P</fnm>
                </au>
                <au>
                   <snm>Friederich</snm>
                   <fnm>HC</fnm>
                </au>
                <au>
                   <snm>Meile</snm>
                   <fnm>T</fnm>
                </au>
                <au>
                   <snm>Zipfel</snm>
                   <fnm>S</fnm>
                </au>
                <au>
                   <snm>Teufel</snm>
                   <fnm>M</fnm>
                </au>
             </aug>
             <source>Surg Obes Relat Dis</source>
             <pubdate>2014</pubdate>
             <volume>10</volume>
             <fpage>277</fpage>
             <lpage>282</lpage>
             <xrefbib>
                <pubidlist>
                   <pubid idtype="doi">10.1016/j.soard.2013.09.012</pubid>
                   <pubid idtype="pmpid" link="fulltext">24355326</pubid>
                </pubidlist>
             </xrefbib>
          </bibl>
          <bibl id="B15">
             <title>
                <p>Heart rate variability biofeedback reduces food cravings in high food cravers</p>
             </title>
             <aug>
                <au>
                   <snm>Meule</snm>
                   <fnm>A</fnm>
                </au>
                <au>
                   <snm>Freund</snm>
                   <fnm>R</fnm>
                </au>
                <au>
                   <snm>Skirde</snm>
                   <fnm>AK</fnm>
                </au>
                <au>
                   <snm>V&#246;gele</snm>
                   <fnm>C</fnm>
                </au>
                <au>
                   <snm>K&#252;bler</snm>
                   <fnm>A</fnm>
                </au>
             </aug>
             <source>Appl Psychophysiol Biofeedback</source>
             <pubdate>2012</pubdate>
             <volume>37</volume>
             <fpage>241</fpage>
             <lpage>251</lpage>
             <xrefbib>
                <pubidlist>
                   <pubid idtype="doi">10.1007/s10484-012-9197-y</pubid>
                   <pubid idtype="pmpid" link="fulltext">22688890</pubid>
                </pubidlist>
             </xrefbib>
          </bibl>
       </refgrp>
    </bm>
 </art>