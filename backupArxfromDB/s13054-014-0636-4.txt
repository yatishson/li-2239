<art>
    <ui>s13054-014-0636-4</ui>
    <ji>1364-8535</ji>
    <fm>
       <dochead>Commentary</dochead>
       <bibl>
          <title>
             <p>Endogenous glycosaminoglycan anticoagulation in extracorporeal membrane oxygenation</p>
          </title>
          <aug>
             <au ca="yes" id="A1">
                <snm>MacLaren</snm>
                <fnm>Graeme</fnm>
                <insr iid="I1"/>
                <insr iid="I2"/>
                <insr iid="I3"/>
                <email>gmaclaren@iinet.net.au</email>
             </au>
             <au id="A2">
                <snm>Monagle</snm>
                <fnm>Paul</fnm>
                <insr iid="I3"/>
                <insr iid="I4"/>
                <insr iid="I5"/>
                <email>Paul.Monagle@rch.org.au</email>
             </au>
          </aug>
          <insg>
             <ins id="I1">
                <p>Cardiothoracic Intensive Care Unit, National University Health System, 5 Lower Kent Ridge Road, Singapore, 119074, Singapore</p>
             </ins>
             <ins id="I2">
                <p>Paediatric Intensive Care Unit, The Royal Children&#8217;s Hospital, Flemington Road, Parkville 3052, VIC, Australia</p>
             </ins>
             <ins id="I3">
                <p>Department of Paediatrics, The University of Melbourne, Flemington Road, Parkville 3052, VIC, Australia</p>
             </ins>
             <ins id="I4">
                <p>Clinical Sciences Research, Murdoch Childrens Research Institute, Flemington Road, Parkville 3052, VIC, Australia</p>
             </ins>
             <ins id="I5">
                <p>Department of Haematology, The Royal Children&#8217;s Hospital, Flemington 5 Road, Parkville 3052, VIC, Australia</p>
             </ins>
          </insg>
          <source>Critical Care</source>
          <issn>1364-8535</issn>
          <pubdate>2014</pubdate>
          <volume>18</volume>
          <issue>6</issue>
          <fpage>636</fpage>
          <url>http://ccforum.com/content/18/6/636</url>
          <xrefbib>
             <pubidlist>
                <pubid idtype="doi">10.1186/s13054-014-0636-4</pubid>
             <pubid idtype="pmpid">25629374</pubid></pubidlist>
          </xrefbib>
       </bibl>
       <history>
          <pub>
             <date>
                <day>21</day>
                <month>11</month>
                <year>2014</year>
             </date>
          </pub>
       </history>
       <cpyrt>
          <year>2014</year>
          <collab>MacLaren and Monagle; licensee BioMed Central Ltd.</collab>
          <note>The licensee has exclusive rights to distribute this article, in any medium, for 12 months following its publication. After this time, the article is available under the terms of the Creative Commons Attribution License (<url>http://creativecommons.org/licenses/by/4.0</url>), which permits unrestricted use, distribution, and reproduction in any medium, provided the original work is properly credited. The Creative Commons Public Domain Dedication waiver (<url>http://creativecommons.org/publicdomain/zero/1.0/</url>) applies to the data made available in this article, unless otherwise stated.</note>
       </cpyrt>
       <abs>
          <sec>
             <st>
                <p>Abstract</p>
             </st>
             <p>A heparin-like effect was recently described in infants, children, and adults receiving bivalirudin while supported on extracorporeal membrane oxygenation following cardiopulmonary bypass. This is most likely due to endogenous release of glycosaminoglycans from vascular endothelium and mast cells and is associated with longer duration of extracorporeal membrane oxygenation and an increased incidence of sepsis. Further investigation into this effect should include patients without recent cardiopulmonary bypass, exclude the presence of covalent antithrombin-heparin complexes, and employ a variety of different heparinases for thromboelastography. The phenomenon may partially explain the heterogeneity of anticoagulation requirements in patients on extracorporeal life support.</p>
          </sec>
       </abs>
    </fm>
    <bdy>
       <sec>
          <st>
             <p>Introduction</p>
          </st>
          <p>Extracorporeal membrane oxygenation (ECMO) has been used for over 40&#160;years in patients with refractory respiratory failure or shock. Over this time, there have been continuous modifications and improvements in both the circuitry and clinical management of ECMO. However, one perennial problem has been the need to anticoagulate blood passing through the circuit. Finding a balance between the risks of bleeding and clot formation is arguably the most challenging problem in ECMO today, especially in complex, vulnerable populations such as infants, multitrauma patients, or when ECMO is deployed after cardiopulmonary bypass (CPB). Unfractionated heparin (UFH) remains the most commonly used anticoagulant for ECMO. Heparin is a naturally occurring glycosaminoglycan (GAG) that is usually bound to endothelium but can be secreted into the bloodstream. Other GAGs, including heparan sulphate, chondroitin sulphate, and dermatan sulphate, are known to prolong anticoagulation tests and thus exhibit a heparin-like effect (HLE), although they are not usually used as therapeutic agents.</p>
       </sec>
       <sec>
          <st>
             <p>Commentary</p>
          </st>
          <p>In this issue of <it>Critical Care</it>, Ranucci and colleagues <abbrgrp>
                <abbr bid="B1">1</abbr>
             </abbrgrp> demonstrate a novel phenomenon in a cohort of post-cardiotomy patients receiving bivalirudin on ECMO. Based on thromboelastography with and without heparinase, the authors documented HLE in over half the cohort, even although none of the patients received UFH after the conclusion of CPB. HLE was associated with a longer duration of ECMO and an increased incidence of sepsis. The authors speculated that the HLE was due to release of native heparinoids from the glycocalyx and mast cells.</p>
          <p>There are many aspects to consider with these intriguing new findings. A variety of heparinases are commercially available, but the specifics of the ones used in this study were not provided, so it is unknown whether the heparinase used was specific for cleaving heparin and heparin sulphate or whether it cleaved a wider range of GAGs such as dermatan or chondroitin sulphates. The nature of the molecule causing the HLE thus remains to be elucidated. The authors may be correct in their hypothesis that GAGs released from the vascular endothelium were responsible, but an increase in GAGs may also have been secondary to increased production in other tissues or because of inadequate hepatic clearance. It is equally unclear whether they were released as a consequence of sepsis or as a result of damaged endothelium which might later render the patients more vulnerable to sepsis. In other words, were GAGs the cause of the apparent differences in clinical outcome, a consequence, or completely unrelated?</p>
          <p>If the HLE was unrelated to endogenous GAG release, what other reasons might explain the authors&#8217; findings? All of the patients in the study received UFH during CPB. Given that the HLE was seen predominantly in the first 7&#160;days post-bypass, one possible explanation is that there was a reservoir of heparin in an extravascular compartment which later redistributed into the blood after CPB was completed. In patients who have CPB with UFH, there is often a rebound of measures of UFH activity in the 24&#160;hours following reversal with protamine. However, after following a control group who were not on ECMO, the authors found nothing to support this hypothesis, so attributing HLE to heparin rebound appears unlikely.</p>
          <p>A more intriguing alternative is that the persistent effect over the first week after CPB represented a residual covalent antithrombin-heparin complex (ATH) formed during the high-concentration UFH of CPB. ATH may not be well reversed by protamine and is known to have a very long half-life <abbrgrp>
                <abbr bid="B2">2</abbr>
             </abbrgrp>,<abbrgrp>
                <abbr bid="B3">3</abbr>
             </abbrgrp>. ATH most likely forms under the conditions of CPB, but little research has been done to establish how frequently or to what extent it occurs in patients. A simple means of investigating this further would be to repeat the study in patients who are on ECMO for reasons unrelated to CPB, where no heparin was used prior to ECMO. Alternatively, ATH levels could be measured and correlated to the R-time on thromboelastography.</p>
          <p>This study has a number of limitations, which are acknowledged by the authors. It was a small, retrospective, single-center study with an extremely heterogeneous population combining neonatal, pediatric, and adult patients. A variety of ECMO machines were used, the surface coatings of which can elicit different pathophysiological responses from vascular endothelium. Additionally, the study described neither where the blood samples for the thromboelastography were taken from nor whether the samples were taken from heparinized lines, raising the possibility of heparin contamination. Despite these limitations, it appears probable, given the data presented, that the authors have demonstrated a potentially important new finding with clinical implications that mandate further research.</p>
       </sec>
       <sec>
          <st>
             <p>Conclusions</p>
          </st>
          <p>The uncertainty surrounding optimal anticoagulation practices for ECMO was recently highlighted by the Extracorporeal Life Support Organization in its anticoagulation guidelines <abbrgrp>
                <abbr bid="B4">4</abbr>
             </abbrgrp>. Despite decades of research, the complex interactions of coagulation and inflammation pathways during ECMO are poorly understood. The findings, underlying hypotheses, and potential clinical implications of the present study will be of interest to ECMO clinicians. Further research from other centers and in non-CPB patients is warranted to confirm whether the phenomenon is reproducible and, if so, to determine the mechanism. A hitherto undetected HLE might go a considerable way toward explaining the heterogeneity of anticoagulation requirements in ECMO patients. By elucidating the mechanism of HLE, we will not only learn more about the pathophysiological role of GAGs in critical illness, we may yet make anticoagulation in ECMO safer and simpler.</p>
       </sec>
       <sec>
          <st>
             <p>Abbreviations</p>
          </st>
          <p>ATH: Antithrombin-heparin complex</p>
          <p>CPB: Cardiopulmonary bypass</p>
          <p>ECMO: Extracorporeal membrane oxygenation</p>
          <p>GAG: Glycosaminoglycan</p>
          <p>HLE: Heparin-like effect</p>
          <p>UFH: Unfractionated heparin</p>
       </sec>
       <sec>
          <st>
             <p>Competing interests</p>
          </st>
          <p>The authors declare that they have no competing interests.</p>
       </sec>
    </bdy>
    <bm>
       <refgrp>
          
          <bibl id="B1">
             <title>
                <p>Heparin-like effect in postcardiotomy extracorporeal membrane oxygenation patients</p>
             </title>
             <aug>
                <au>
                   <snm>Ranucci</snm>
                   <fnm>M</fnm>
                </au>
                <au>
                   <snm>Baryshnikova</snm>
                   <fnm>E</fnm>
                </au>
                <au>
                   <snm>Isgro</snm>
                   <fnm>G</fnm>
                </au>
                <au>
                   <snm>Carlucci</snm>
                   <fnm>C</fnm>
                </au>
                <au>
                   <snm>Cotza</snm>
                   <fnm>M</fnm>
                </au>
                <au>
                   <snm>Carboni</snm>
                   <fnm>G</fnm>
                </au>
                <au>
                   <snm>Ballotta</snm>
                   <fnm>A</fnm>
                </au>
             </aug>
             <source>Crit Care</source>
             <pubdate>2014</pubdate>
             <volume>18</volume>
             <fpage>504</fpage>
             <xrefbib>
                <pubidlist>
                   <pubid idtype="doi">10.1186/s13054-014-0504-2</pubid>
                   <pubid idtype="pmpid" link="fulltext">25189998</pubid>
                </pubidlist>
             </xrefbib>
          </bibl>
          <bibl id="B2">
             <title>
                <p>Covalent antithrombin-heparin complexes with high anticoagulant activity. Intravenous, subcutaneous, and intratracheal administration</p>
             </title>
             <aug>
                <au>
                   <snm>Chan</snm>
                   <fnm>A</fnm>
                </au>
                <au>
                   <snm>Berry</snm>
                   <fnm>L</fnm>
                </au>
                <au>
                   <snm>O&#8217;Brodovich</snm>
                   <fnm>H</fnm>
                </au>
                <au>
                   <snm>Klement</snm>
                   <fnm>P</fnm>
                </au>
                <au>
                   <snm>Mitchell</snm>
                   <fnm>L</fnm>
                </au>
                <au>
                   <snm>Paranowski</snm>
                   <fnm>B</fnm>
                </au>
                <au>
                   <snm>Monagle</snm>
                   <fnm>P</fnm>
                </au>
                <au>
                   <snm>Andrew</snm>
                   <fnm>M</fnm>
                </au>
             </aug>
             <source>J Biol Chem</source>
             <pubdate>1997</pubdate>
             <volume>272</volume>
             <fpage>2211</fpage>
             <lpage>2217</lpage>
          </bibl>
          <bibl id="B3">
             <title>
                <p>Anticoagulant mechanisms of covalent antithrombin-heparin investigated by thromboelastography. Comparison with unfractionated heparin and low-molecular weight heparin</p>
             </title>
             <aug>
                <au>
                   <snm>Atkinson</snm>
                   <fnm>HM</fnm>
                </au>
                <au>
                   <snm>Mewhort-Buist</snm>
                   <fnm>TA</fnm>
                </au>
                <au>
                   <snm>Berry</snm>
                   <fnm>LR</fnm>
                </au>
                <au>
                   <snm>Chan</snm>
                   <fnm>AK</fnm>
                </au>
             </aug>
             <source>Thromb Haemost</source>
             <pubdate>2009</pubdate>
             <volume>102</volume>
             <fpage>62</fpage>
             <lpage>68</lpage>
             <xrefbib>
                <pubidlist>
                   <pubid idtype="pmpid" link="fulltext">19572069</pubid>
                </pubidlist>
             </xrefbib>
          </bibl>
          <bibl id="B4">
             
                <url>http://www.elso.org/resources/guidelines</url>
             
<note>Extracorporeal Life Support Organization: <b>Guidelines.</b> []</note>
          </bibl>
       </refgrp>
    </bm>
 </art>