<art>
    <ui>s13059-014-0516-x</ui>
    <ji>1465-6906</ji>
    <fm>
       <dochead>Research highlight</dochead>
       <bibl>
          <title>
             <p>A CRISPR design for next-generation antimicrobials</p>
          </title>
          <aug>
             <au ca="yes" id="A1">
                <snm>Beisel</snm>
                <mi>L</mi>
                <fnm>Chase</fnm>
                <insr iid="I1"/>
                <email>cbeisel@ncsu.edu</email>
             </au>
             <au id="A2">
                <snm>Gomaa</snm>
                <mi>A</mi>
                <fnm>Ahmed</fnm>
                <insr iid="I1"/>
                <insr iid="I2"/>
                <email>aamahmo2@ncsu.edu</email>
             </au>
             <au id="A3">
                <snm>Barrangou</snm>
                <fnm>Rodolphe</fnm>
                <insr iid="I3"/>
                <email>rbarran@ncsu.edu</email>
             </au>
          </aug>
          <insg>
             <ins id="I1">
                <p>Department of Chemical and Biomolecular Engineering, North Carolina State University, Raleigh 27695-7905, NC, USA</p>
             </ins>
             <ins id="I2">
                <p>Chemical Engineering Department, Faculty of Engineering, Cairo University, Giza 12613, Egypt</p>
             </ins>
             <ins id="I3">
                <p>Department of Food, Bioprocessing and Nutrition Sciences, North Carolina State University, Raleigh 27695-7624, NC, USA</p>
             </ins>
          </insg>
          <source>Genome Biology</source>
          <issn>1465-6906</issn>
          <pubdate>2014</pubdate>
          <volume>15</volume>
          <issue>11</issue>
          <fpage>516</fpage>
          <url>http://genomebiology.com/2014/15/11/516</url>
          <xrefbib>
             <pubidlist>
                <pubid idtype="doi">10.1186/s13059-014-0516-x</pubid>
             <pubid idtype="pmpid">25417800</pubid></pubidlist>
          </xrefbib>
       </bibl>
       <history>
          <pub>
             <date>
                <day>8</day>
                <month>11</month>
                <year>2014</year>
             </date>
          </pub>
       </history>
       <cpyrt>
          <year>2014</year>
          <collab>Beisel et al.; licensee BioMed Central Ltd.</collab>
          <note>The licensee has exclusive rights to distribute this article, in any medium, for 12 months following its publication. After this time, the article is available under the terms of the Creative Commons Attribution License (<url>http://creativecommons.org/licenses/by/4.0</url>), which permits unrestricted use, distribution, and reproduction in any medium, provided the original work is properly credited. The Creative Commons Public Domain Dedication waiver (<url>http://creativecommons.org/publicdomain/zero/1.0/</url>) applies to the data made available in this article, unless otherwise stated.</note>
       </cpyrt>
       <abs>
          <sec>
             <st>
                <p>Abstract</p>
             </st>
             <p>Two recent publications have demonstrated how delivering CRISPR nucleases provides a promising solution to the growing problem of bacterial antibiotic resistance.</p>
          </sec>
       </abs>
    </fm>
    <meta>
       <classifications>
          <classification id="genomicsofinfectiousdiseases" subtype="series_title" type="BMC">Genomics of infectious diseases</classification>
       </classifications>
    </meta>
    <bdy>
       <sec>
          <st>
             <p>The problem(s) with antibiotics</p>
          </st>
          <p>Once the beacon of modern medicine, antibiotics now threaten to be its undoing. These miracle molecules were originally heralded for their remarkable ability to cure a myriad of microbial infections. However, their overuse in medicine and abuse in animal agriculture has led to the rise of multidrug-resistant pathogens that are increasingly tolerant to our current antibiotic arsenal. Even worse, these same antibiotics indiscriminately kill beneficial bacteria along with the pathogens. The consortia of indigenous residents occupying our internal and external bodily surfaces - our microbiome - have been widely implicated in human health, and their disruption by antibiotics is thought to have equally devastating effects. Accordingly, there is a need for novel antimicrobials that can bypass common modes of multidrug resistance while being selective for individual strains. Two recent papers in <it>Nature Biotechnology</it> by Bikard <it>et al</it>. <abbrgrp>
                <abbr bid="B1">1</abbr>
             </abbrgrp> and Citorik <it>et al</it>. <abbrgrp>
                <abbr bid="B2">2</abbr>
             </abbrgrp> offer a promising solution to the problem of antibiotic resistance by using CRISPR (&#8216;clustered regularly interspaced short palindromic repeats&#8217;)-Cas (&#8216;CRISPR associated&#8217;) systems.</p>
          <p>CRISPR-Cas systems are adaptive immune systems native to bacteria and archaea that employ CRISPR RNAs to recognize and destroy complementary nucleic acids (Figure&#160;<figr fid="F1">1</figr>) <abbrgrp>
                <abbr bid="B3">3</abbr>
             </abbrgrp>. The discovery of one type of CRISPR-Cas system that requires only a single protein for CRISPR-RNA-directed DNA binding and cleavage (Cas9) quickly led to numerous applications, the most popular of which has been genome editing <abbrgrp>
                <abbr bid="B4">4</abbr>
             </abbrgrp>. However, less explored is the potential of these systems to serve as sequence-specific antimicrobials. Early work demonstrated that CRISPR-Cas systems are cytotoxic following incidental self-targeting of the bacterial genome and that they can be used to immunize cells against the spread of multidrug-resistant plasmids <abbrgrp>
                <abbr bid="B5">5</abbr>
             </abbrgrp>-<abbrgrp>
                <abbr bid="B7">7</abbr>
             </abbrgrp>. Original work from the Marraffini group even suggested that CRISPR-Cas systems could be used for the sequence-specific killing of bacteria <abbrgrp>
                <abbr bid="B8">8</abbr>
             </abbrgrp>. Subsequently, we recently reported the concept of CRISPR-Cas systems as programmable antimicrobials <abbrgrp>
                <abbr bid="B9">9</abbr>
             </abbrgrp>, demonstrating that both heterologous and endogenous systems could selectively kill bacterial species and strains. Intriguingly, every sequence in the genome that was targeted led to killing, suggesting that virtually any genomic location could be a distinct target for CRISPR-based antimicrobials <abbrgrp>
                <abbr bid="B9">9</abbr>
             </abbrgrp>. However, an appropriate delivery vehicle was lacking. Now, Bikard <it>et al</it>. <abbrgrp>
                <abbr bid="B1">1</abbr>
             </abbrgrp> and Citorik <it>et al</it>. <abbrgrp>
                <abbr bid="B2">2</abbr>
             </abbrgrp> tackle this challenge as the next major step towards deploying CRISPR-Cas systems as antimicrobial agents.</p>
          
             <fig id="F1">
                <title>
                   <p>Figure 1</p>
                </title>
                <caption>
                   <p>Delivering CRISPR-Cas9 for targeted killing and plasmid removal.</p>
                </caption>
                <text>
                   <p>
                      <b>Delivering CRISPR-Cas9 for targeted killing and plasmid removal.</b> Left: phages are engineered to encode the Cas9 nuclease, a trans-activating crRNA (tracrRNA) and an array of plasmid-targeting or genome-targeting CRISPR RNAs. The CRISPR RNAs are designed to target unique sequences in the bacterial chromosome or in harbored plasmids. Right: injection of the phage DNA into a mixed population of bacteria leads to removal (here depicted with broken lines) of targeted strains or plasmids without impacting the rest of the population. With further development, this strategy has the potential to treat multidrug-resistant infections without impacting beneficial microbes, to remove contaminating microbes from industrial fermentations and to provide further insights into microbial communities.</p>
                </text>
                <graphic file="s13059-014-0516-x-1"/>
             </fig>
          
       </sec>
       <sec>
          <st>
             <p>CRISPR-Cas9 to go</p>
          </st>
          <p>For delivery, both studies employed phagemids - plasmids with phage packaging signals - equipped with sequences encoding the <it>Streptococcus pyogenes</it> Cas9 nuclease, a designed CRISPR RNA and a trans-activating crRNA (tracrRNA) for CRISPR RNA processing <abbrgrp>
                <abbr bid="B10">10</abbr>
             </abbrgrp>. The beauty of this approach is that phages have already evolved to inject their genetic material into the host bacterium.</p>
          <p>The difference between the studies was that Bikard and colleagues <abbrgrp>
                <abbr bid="B1">1</abbr>
             </abbrgrp> used <it>Staphylococcus aureus</it> and its temperate phage &#981;NM1, whereas Citorik and colleagues <abbrgrp>
                <abbr bid="B2">2</abbr>
             </abbrgrp> used <it>Escherichia coli</it> with its filamentous phage M13. Both species are clinically relevant because of their documented antibiotic resistance - particularly multidrug-resistant <it>S. aureus</it> (MRSA). The attraction of the phagemid approach rather than use of the phage itself was that new CRISPR RNA sequences could be readily cloned into the phagemid backbone. The packaged phagemids were then employed to target the genome, which led to extensive and rapid killing upon application of increasing amounts of the packaged phagemid. The phagemids were also employed to target harbored antibiotic-resistance plasmids, which led to efficient plasmid removal. Surprisingly, in the study by Citorik <it>et al</it>. <abbrgrp>
                <abbr bid="B2">2</abbr>
             </abbrgrp>, plasmid removal induced killing. This was traced to the addiction systems of the plasmid that kill the host cell in the absence of plasmid, offering an indirect benefit of targeting some mobile elements encoding drug resistance. Conjugation was also investigated as a means of delivery <abbrgrp>
                <abbr bid="B2">2</abbr>
             </abbrgrp>, although the transfer efficiency was too low to substantially reduce cell counts.</p>
          <p>With any antimicrobial, the immediate question is how microbes evolve resistance. Remarkably, the survivors did not circumvent targeting - instead they either did not receive the CRISPR-Cas system, or they received a defective system, which is in line with previous findings <abbrgrp>
                <abbr bid="B9">9</abbr>
             </abbrgrp>. The consistency of these findings would argue against the emergence of resistance to CRISPR-Cas-mediated targeting. Instead, other bottlenecks are likely to thwart effective targeting, as will be described later in this article.</p>
          <p>Another powerful demonstration of the potential of this technology utilized mixed bacterial communities. The authors relied on two-member or three-member communities of genetic variants of the same strain - a step towards natural communities. In both cases, the authors could specifically eliminate individual target strains while sparing non-target strains. Citorik and colleagues were able to distinguish a single base-pair change between two of the strains, underscoring the specificity of targeting. By exploiting the multiplexable nature of CRISPR, the authors also demonstrated that the CRISPR RNAs also could be readily arrayed to concurrently target more than one strain or plasmid at a time.</p>
          <p>To further extend their results, both studies conducted <it>in vivo</it> experiments. Bikard <it>et al</it>. <abbrgrp>
                <abbr bid="B1">1</abbr>
             </abbrgrp> employed a skin infection model in mice with a co-culture of one targeted fluorescent strain and one non-targeted non-fluorescent strain of <it>S. aureus</it>. Citorik <it>et al</it>. <abbrgrp>
                <abbr bid="B2">2</abbr>
             </abbrgrp> employed an infection model in which larvae of the honeycomb moth <it>Galleria mellonella</it> were fed enterohemorrhagic <it>E. coli</it> (EHEC) O157:H7. In both cases, application of the phagemids had a modest but statistically significant effect on the target strain - either by reducing the fraction of fluorescent <it>S. aureus</it> strains occupying the skin of the mouse or by improving the survival of the flat worms. While there is room for improvement, these findings offer the first step towards the <it>in vivo</it> delivery of CRISPR-Cas systems in clinical and environmental settings.</p>
       </sec>
       <sec>
          <st>
             <p>The path forward</p>
          </st>
          <p>These initial demonstrations open a wide range of applications for the delivery of CRISPR-based antimicrobials that are otherwise poorly addressed by traditional antibiotics. The primary focus of these studies was treating multidrug-resistant infections without compromising the normal flora, either by killing the pathogen or by restoring its susceptibility to antibiotics. However, many more opportunities exist. For instance, these technologies might be used to study natural and synthetic microbial communities, ranging from those populating our digestive tracts to those in the soil. Engineered phages could partially or completely remove individual members in order to study how the whole community responds over time. Separately, engineered phages could clear heavily guarded niches. By opening these niches, beneficial or diagnostic strains could be administered to take hold of the niche and establish long-term residency in the community. A third opportunity is using these phages to prevent the spread of multidrug-resistance markers in natural environments, thereby stymying the further dissemination of resistance. Finally, eliminating contamination of batch fermentations without compromising the production host could combat a common and economically costly industrial problem. New ways of addressing this issue without discarding the batch could be a major financial boon across the food, beverage, biotechnology and therapeutic industries.</p>
          <p>With these applications in mind, a major question is whether use of lytic phages themselves would be sufficient for the same end. Lytic phages are normally strain-specific, replicate as part of the killing process, can be readily isolated from the environment and do not necessarily require any genetic modification. Indeed, lytic bacteriophages are being actively explored as a means of combating multidrug-resistant infections and food contamination. One unique opportunity is incorporating CRISPR-Cas9 into lysogenic bacteriophages, which would greatly expand the set of phages that can be employed as antimicrobials. Another opportunity is using CRISPR-Cas9 to target features that distinguish otherwise-identical strains, such as a recently acquired antibiotic-resistance gene. Finally, CRISPR-Cas9 can be readily programmed to target different species, whereas a new lytic phage would need to be isolated and characterized.</p>
       </sec>
       <sec>
          <st>
             <p>Hurdles ahead</p>
          </st>
          <p>To truly exploit the capabilities of CRISPR-Cas9, delivery vehicles are needed that can inject their cargo into diverse strains. Broad-host-range phages are extremely rare, and those that are known, at best, infect species within a single genus. Despite phages serving as the first model system in molecular biology, little is known about how to alter or expand their host range. We see this as an excellent opportunity to interrogate poorly understood elements of phage biology while generating phages that can infect virtually any host microbe. Alternatively, nanoparticles or outer-membrane vesicles offer additional promising, yet poorly explored, delivery options.</p>
          <p>Using such broad-spectrum delivery vehicles, or any delivery vehicle for that matter, poses a number of challenges that will impact the efficacy of the approach. As evident in these two papers, efficacy dropped substantially in the relatively simple <it>in vivo</it> experiments. The first challenge is that the vehicle needs to reach the site of infection in sufficient numbers to deliver the cargo into all possible strains. In natural communities such as the gut microbiota, this would require the particles to survive ingestion and reach the approximately 100 trillion cells of the digestive tract in locations of varying accessibility, which is a formidable challenge. A second challenge is that appropriate surface receptors would need to be expressed on the cells for phage infection - expression levels of these receptors can vary across the population, depending on the environmental conditions. Third, once injected into the cell, the DNA must bypass the defense systems of the host (for example, restriction-modification systems, native CRISPR-Cas systems) and lead to sufficient expression of CRISPR-Cas9. Finally, the targeting sequence must be carefully selected to avoid incidental killing of other strains, although advances in next-generation sequencing are providing a wealth of data for identifying appropriate sequences. Going forward, further efforts will need to tackle each of these barriers. However, each challenge should be surmountable, potentially yielding versatile tools to study and remodel microbial communities as well as providing tailored antimicrobials for the treatment of multidrug-resistant infections.</p>
       </sec>
       <sec>
          <st>
             <p>Abbreviations</p>
          </st>
          <p>Cas: CRISPR associated</p>
          <p>CRISPR: clustered regularly interspaced short palindromic repeats</p>
          <p>EHEC: enterohemorrhagic <it>E. coli</it>
          </p>
          <p>MRSA: multidrug-resistant <it>S. aureus</it>
          </p>
          <p>tracrRNA: trans-activating crRNA</p>
       </sec>
       <sec>
          <st>
             <p>Competing interests</p>
          </st>
          <p>The authors declare that they have no competing interests.</p>
       </sec>
    </bdy>
    <bm>
       <ack>
          <sec>
             <st>
                <p>Acknowledgments</p>
             </st>
             <p>CLB and RB were supported in part by the National Science Foundation [CBET-1403135] (to CLB and RB), the Kenan Institute of Engineering, Science &amp; Technology (to CLB) and the National Institutes of Health [1R56AI103557-01A1] (to CLB). The contents of this paper are solely the responsibility of the authors and do not necessarily represent the official views of the NIH.</p>
          </sec>
       </ack>
       <refgrp>
          
          <bibl id="B1">
             <note>Bikard D, Euler CW, Jiang W, Nussenzweig PM, Goldberg GW, Duportet X, Fischetti VA, Marraffini LA: <b>Exploiting CRISPR-Cas nucleases to produce sequence-specific antimicrobials.</b>
                <it>Nat Biotechnol</it> 2014, <b>&#4447;:</b>&#4447;. doi:10.1038/nbt.3043.</note>
          </bibl>
          <bibl id="B2">
             <note>Citorik RJ, Mimee M, Lu TK: <b>Sequence-specific antimicrobials using efficiently delivered RNA-guided nucleases.</b>
                <it>Nat Biotechnol</it> 2014, <b>&#4447;:</b>&#4447;. doi:10.1038/nbt.3011.</note>
          </bibl>
          <bibl id="B3">
             <title>
                <p>CRISPR provides acquired resistance against viruses in prokaryotes</p>
             </title>
             <aug>
                <au>
                   <snm>Barrangou</snm>
                   <fnm>R</fnm>
                </au>
                <au>
                   <snm>Fremaux</snm>
                   <fnm>C</fnm>
                </au>
                <au>
                   <snm>Deveau</snm>
                   <fnm>H</fnm>
                </au>
                <au>
                   <snm>Richards</snm>
                   <fnm>M</fnm>
                </au>
                <au>
                   <snm>Boyaval</snm>
                   <fnm>P</fnm>
                </au>
                <au>
                   <snm>Moineau</snm>
                   <fnm>S</fnm>
                </au>
                <au>
                   <snm>Romero</snm>
                   <fnm>DA</fnm>
                </au>
                <au>
                   <snm>Horvath</snm>
                   <fnm>P</fnm>
                </au>
             </aug>
             <source>Science</source>
             <pubdate>2007</pubdate>
             <volume>315</volume>
             <fpage>1709</fpage>
             <lpage>1712</lpage>
             <xrefbib>
                <pubidlist>
                   <pubid idtype="doi">10.1126/science.1138140</pubid>
                   <pubid idtype="pmpid" link="fulltext">17379808</pubid>
                </pubidlist>
             </xrefbib>
          </bibl>
          <bibl id="B4">
             <title>
                <p>Development and applications of CRISPR-Cas9 for genome engineering</p>
             </title>
             <aug>
                <au>
                   <snm>Hsu</snm>
                   <fnm>PD</fnm>
                </au>
                <au>
                   <snm>Lander</snm>
                   <fnm>ES</fnm>
                </au>
                <au>
                   <snm>Zhang</snm>
                   <fnm>F</fnm>
                </au>
             </aug>
             <source>Cell</source>
             <pubdate>2014</pubdate>
             <volume>157</volume>
             <fpage>1262</fpage>
             <lpage>1278</lpage>
             <xrefbib>
                <pubidlist>
                   <pubid idtype="doi">10.1016/j.cell.2014.05.010</pubid>
                   <pubid idtype="pmpid" link="fulltext">24906146</pubid>
                </pubidlist>
             </xrefbib>
          </bibl>
          <bibl id="B5">
             <title>
                <p>The CRISPR/Cas bacterial immune system cleaves bacteriophage and plasmid DNA</p>
             </title>
             <aug>
                <au>
                   <snm>Garneau</snm>
                   <fnm>JE</fnm>
                </au>
                <au>
                   <snm>Dupuis</snm>
                   <fnm>M-&#200;</fnm>
                </au>
                <au>
                   <snm>Villion</snm>
                   <fnm>M</fnm>
                </au>
                <au>
                   <snm>Romero</snm>
                   <fnm>DA</fnm>
                </au>
                <au>
                   <snm>Barrangou</snm>
                   <fnm>R</fnm>
                </au>
                <au>
                   <snm>Boyaval</snm>
                   <fnm>P</fnm>
                </au>
                <au>
                   <snm>Fremaux</snm>
                   <fnm>C</fnm>
                </au>
                <au>
                   <snm>Horvath</snm>
                   <fnm>P</fnm>
                </au>
                <au>
                   <snm>Magad&#225;n</snm>
                   <fnm>AH</fnm>
                </au>
                <au>
                   <snm>Moineau</snm>
                   <fnm>S</fnm>
                </au>
             </aug>
             <source>Nature</source>
             <pubdate>2010</pubdate>
             <volume>468</volume>
             <fpage>67</fpage>
             <lpage>71</lpage>
             <xrefbib>
                <pubidlist>
                   <pubid idtype="doi">10.1038/nature09523</pubid>
                   <pubid idtype="pmpid" link="fulltext">21048762</pubid>
                </pubidlist>
             </xrefbib>
          </bibl>
          <bibl id="B6">
             <title>
                <p>CRISPR-mediated adaptive immune systems in bacteria and archaea</p>
             </title>
             <aug>
                <au>
                   <snm>Sorek</snm>
                   <fnm>R</fnm>
                </au>
                <au>
                   <snm>Lawrence</snm>
                   <fnm>CM</fnm>
                </au>
                <au>
                   <snm>Wiedenheft</snm>
                   <fnm>B</fnm>
                </au>
             </aug>
             <source>Annu Rev Biochem</source>
             <pubdate>2013</pubdate>
             <volume>82</volume>
             <fpage>237</fpage>
             <lpage>266</lpage>
             <xrefbib>
                <pubidlist>
                   <pubid idtype="doi">10.1146/annurev-biochem-072911-172315</pubid>
                   <pubid idtype="pmpid" link="fulltext">23495939</pubid>
                </pubidlist>
             </xrefbib>
          </bibl>
          <bibl id="B7">
             <title>
                <p>Cytotoxic chromosomal targeting by CRISPR/Cas systems can reshape bacterial genomes and expel or remodel pathogenicity islands</p>
             </title>
             <aug>
                <au>
                   <snm>Vercoe</snm>
                   <fnm>RB</fnm>
                </au>
                <au>
                   <snm>Chang</snm>
                   <fnm>JT</fnm>
                </au>
                <au>
                   <snm>Dy</snm>
                   <fnm>RL</fnm>
                </au>
                <au>
                   <snm>Taylor</snm>
                   <fnm>C</fnm>
                </au>
                <au>
                   <snm>Gristwood</snm>
                   <fnm>T</fnm>
                </au>
                <au>
                   <snm>Clulow</snm>
                   <fnm>JS</fnm>
                </au>
                <au>
                   <snm>Richter</snm>
                   <fnm>C</fnm>
                </au>
                <au>
                   <snm>Przybilski</snm>
                   <fnm>R</fnm>
                </au>
                <au>
                   <snm>Pitman</snm>
                   <fnm>AR</fnm>
                </au>
                <au>
                   <snm>Fineran</snm>
                   <fnm>PC</fnm>
                </au>
             </aug>
             <source>PLoS Genet</source>
             <pubdate>2013</pubdate>
             <volume>9</volume>
             <fpage>e1003454</fpage>
             <xrefbib>
                <pubidlist>
                   <pubid idtype="doi">10.1371/journal.pgen.1003454</pubid>
                   <pubid idtype="pmpid" link="fulltext">23637624</pubid>
                </pubidlist>
             </xrefbib>
          </bibl>
          <bibl id="B8">
             <title>
                <p>CRISPR interference can prevent natural transformation and virulence acquisition during in vivo bacterial infection</p>
             </title>
             <aug>
                <au>
                   <snm>Bikard</snm>
                   <fnm>D</fnm>
                </au>
                <au>
                   <snm>Hatoum-Aslan</snm>
                   <fnm>A</fnm>
                </au>
                <au>
                   <snm>Mucida</snm>
                   <fnm>D</fnm>
                </au>
                <au>
                   <snm>Marraffini</snm>
                   <fnm>LA</fnm>
                </au>
             </aug>
             <source>Cell Host Microbe</source>
             <pubdate>2012</pubdate>
             <volume>12</volume>
             <fpage>177</fpage>
             <lpage>186</lpage>
             <xrefbib>
                <pubidlist>
                   <pubid idtype="doi">10.1016/j.chom.2012.06.003</pubid>
                   <pubid idtype="pmpid" link="fulltext">22901538</pubid>
                </pubidlist>
             </xrefbib>
          </bibl>
          <bibl id="B9">
             <title>
                <p>Programmable removal of bacterial strains by use of genome-targeting CRISPR-Cas systems</p>
             </title>
             <aug>
                <au>
                   <snm>Gomaa</snm>
                   <fnm>AA</fnm>
                </au>
                <au>
                   <snm>Klumpe</snm>
                   <fnm>HE</fnm>
                </au>
                <au>
                   <snm>Luo</snm>
                   <fnm>ML</fnm>
                </au>
                <au>
                   <snm>Selle</snm>
                   <fnm>K</fnm>
                </au>
                <au>
                   <snm>Barrangou</snm>
                   <fnm>R</fnm>
                </au>
                <au>
                   <snm>Beisel</snm>
                   <fnm>CL</fnm>
                </au>
             </aug>
             <source>mBio</source>
             <pubdate>2014</pubdate>
             <volume>5</volume>
             <fpage>e00928</fpage>
             <lpage>00913</lpage>
             <xrefbib>
                <pubidlist>
                   <pubid idtype="doi">10.1128/mBio.00928-13</pubid>
                   <pubid idtype="pmpid" link="fulltext">24473129</pubid>
                </pubidlist>
             </xrefbib>
          </bibl>
          <bibl id="B10">
             <title>
                <p>A programmable dual-RNA-guided DNA endonuclease in adaptive bacterial immunity</p>
             </title>
             <aug>
                <au>
                   <snm>Jinek</snm>
                   <fnm>M</fnm>
                </au>
                <au>
                   <snm>Chylinski</snm>
                   <fnm>K</fnm>
                </au>
                <au>
                   <snm>Fonfara</snm>
                   <fnm>I</fnm>
                </au>
                <au>
                   <snm>Hauer</snm>
                   <fnm>M</fnm>
                </au>
                <au>
                   <snm>Doudna</snm>
                   <fnm>JA</fnm>
                </au>
                <au>
                   <snm>Charpentier</snm>
                   <fnm>E</fnm>
                </au>
             </aug>
             <source>Science</source>
             <pubdate>2012</pubdate>
             <volume>337</volume>
             <fpage>816</fpage>
             <lpage>821</lpage>
             <xrefbib>
                <pubidlist>
                   <pubid idtype="doi">10.1126/science.1225829</pubid>
                   <pubid idtype="pmpid" link="fulltext">22745249</pubid>
                </pubidlist>
             </xrefbib>
          </bibl>
       </refgrp>
    </bm>
 </art>