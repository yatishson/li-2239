<art>
    <ui>s13058-014-0467-x</ui>
    <ji>1465-5411</ji>
    <fm>
       <dochead>Viewpoint</dochead>
       <bibl>
          <title>
             <p>A novel mechanism of regulation of the anti-metastatic miR-31 by EMSY in breast cancer</p>
          </title>
          <aug>
             <au id="A1">
                <snm>Mulrane</snm>
                <fnm>Laoighse</fnm>
                <insr iid="I1"/>
                <email>laoighse.mulrane@ucd.ie</email>
             </au>
             <au id="A2">
                <snm>Gallagher</snm>
                <mi>M</mi>
                <fnm>William</fnm>
                <insr iid="I1"/>
                <email>william.gallagher@ucd.ie</email>
             </au>
             <au id="A3" ca="yes">
                <snm>O&#8217;Connor</snm>
                <mi>P</mi>
                <fnm>Darran</fnm>
                <insr iid="I1"/>
                <email>darran.oconnor@ucd.ie</email>
             </au>
          </aug>
          <insg>
             <ins id="I1">
                <p>UCD School of Biomolecular and Biomedical Science, UCD Conway Institute, University College Dublin, Belfield, Dublin 4, Ireland</p>
             </ins>
          </insg>
          <source>Breast Cancer Research</source>
          <issn>1465-5411</issn>
          <pubdate>2014</pubdate>
          <volume>16</volume>
          <issue>6</issue>
          <fpage>467</fpage>
          <url>http://breast-cancer-research.com/content/16/6/467</url>
          <xrefbib>
             <pubidlist>
                <pubid idtype="doi">10.1186/s13058-014-0467-x</pubid>
             </pubidlist>
          </xrefbib>
       </bibl>
       <history>
          <pub>
             <date>
                <day>18</day>
                <month>11</month>
                <year>2014</year>
             </date>
          </pub>
       </history>
       <cpyrt>
          <year>2014</year>
          <collab>Mulrane et al.; licensee BioMed Central Ltd.</collab>
          <note>The licensee has exclusive rights to distribute this article, in any medium, for 6 months following its publication. After this time, the article is available under the terms of the Creative Commons Attribution License (<url>http://creativecommons.org/licenses/by/4.0</url>), which permits unrestricted use, distribution, and reproduction in any medium, provided the original work is properly credited. The Creative Commons Public Domain Dedication waiver (<url>http://creativecommons.org/publicdomain/zero/1.0/</url>) applies to the data made available in this article, unless otherwise stated.</note>
       </cpyrt>
       <abs>
          <sec>
             <st>
                <p>Abstract</p>
             </st>
             <p>miR-31 is well known as an anti-metastatic microRNA (miRNA) in the context of breast cancer. However, to date the mechanism of regulation of this miRNA has yet to be elucidated. The recent publication by Vir&#233; <it>et al.</it> in Molecular Cell [1] demonstrates for the first time that one mechanism of regulation of miR-31 is through the putative oncogene EMSY, whose amplification in breast cancer patients correlates with reduced expression of the miRNA. This regulation is dependent on the DNA-binding transcription factor ETS-1 which recruits EMSY and the histone demethylase KDM5B to the miR-31 promoter, thus repressing its transcription.</p>
          </sec>
       </abs>
    </fm>
    <bdy>
       <sec>
          <st>
             <p>Background</p>
          </st>
          <p>miRNAs are small non-coding RNA species which primarily negatively regulate gene expression, thus affecting a plethora of cancer-associated phenotypes. miR-31, a well-known anti-metastatic miRNA in breast cancer, was first shown to inhibit breast cancer metastasis both <it>in vitro</it> and <it>in vivo</it>
             <abbrgrp>
                <abbr bid="B1">1</abbr>
             </abbrgrp> through the targeting of integrin-&#945;5 (ITGA5), radixin (RDX) and RhoA <abbrgrp>
                <abbr bid="B2">2</abbr>
             </abbrgrp> and later through targeting of WAVE3 <abbrgrp>
                <abbr bid="B3">3</abbr>
             </abbrgrp>. Further investigation reported that suppression of ITGA5, RDX and RhoA recapitulated the phenotype produced by ectopic expression of miR-31 <it>in vitro</it> and <it>in vivo</it>
             <abbrgrp>
                <abbr bid="B4">4</abbr>
             </abbrgrp>. Moreover, re-introduction of miR-31 expression in established xenograft MDA-MB-231 lung metastases was shown to result in metastatic regression, indicating that re-expression of miR-31 may be of therapeutic benefit in clinically advanced patients <abbrgrp>
                <abbr bid="B5">5</abbr>
             </abbrgrp>.</p>
          <p>EMSY, a putative oncogene involved in gene regulation, is known to be amplified in approximately 13% of sporadic breast cancers <abbrgrp>
                <abbr bid="B6">6</abbr>
             </abbrgrp>. However, the locus in which this gene is contained (11q13-14) includes a number of other genes implicated in breast cancer including CCND1, PAK1, CTTN, and FGF3, making it difficult to decipher the exact contribution of EMSY to the poor prognosis associated with amplification of this region. A recent article in Molecular Cell has gone some way to elucidating the function of this gene in breast cancer, describing a role for EMSY in oncogenic transformation as well as invasion/migration in breast cancer through the regulation of miR-31 <abbrgrp>
                <abbr bid="B7">7</abbr>
             </abbrgrp>.</p>
       </sec>
       <sec>
          <st>
             <p>Article</p>
          </st>
          <p>The publication from the Kouzarides group reports that the oncogene EMSY targets miR-31 through co-operation with the transcription factor ETS-1 and the histone lysine demethylase KDM5B, resulting in transcriptional repression of the miRNA and numerous phenotypic effects (Figure&#160;<figr fid="F1">1</figr>).</p>
          
             <fig id="F1">
                <title>
                   <p>Figure 1</p>
                </title>
                <caption>
                   <p>Proposed mechanism of regulation of miR-31 in breast cancer (adapted from</p>
                </caption>
                <text>
                   <p>
                      <b>Proposed mechanism of regulation of miR-31 in breast cancer (adapted from</b>
                      <b>[</b>
                      <abbrgrp>
                         <abbr bid="B7">7</abbr>
                      </abbrgrp>
                      <b>]).</b> ETS-1 recruits oncogene EMSY and H3K4me3 demethylase KDM5B to the promoter of miR-31, repressing transcription of the pri-miRNA.</p>
                </text>
                <graphic file="s13058-014-0467-x-1"/>
             </fig>
          
          <p>Firstly, the group investigated the effect of modified expression of EMSY in cell lines and xenograft models. Ectopic expression of the oncogene in MCF7 cells resulted in increased growth in an orthotopic mammary fat pad xenograft model while an experimental metastasis tail vein injection model of the same cells demonstated increased lung micrometastases relative to the control line. Given the potential role for this gene in transcriptional regulation, a qRT-PCR-based screen was employed to profile 88 miRNAs in MCF7 cells in which EMSY had been depleted revealing 38 to be altered in this context. A second approach was then utilised whereby re-analysis of the METABRIC cohort <abbrgrp>
                <abbr bid="B8">8</abbr>
             </abbrgrp>,<abbrgrp>
                <abbr bid="B9">9</abbr>
             </abbrgrp> for miRNAs altered in EMSY-amplified (cis-acting aberrations of the 11q13-14 locus) versus non-amplified cases revealed dysregulation of 12 miRNAs. As miR-31 was the only miRNA to be identified using both approaches, it was chosen for further studies.</p>
          <p>miR-31 expression levels inversely correlated with EMSY levels in patient samples from the METABRIC dataset and in cell lines with amplification or ectopic expression of EMSY. Finding that expression of EMSY increased both oncogenic transformation and migration <it>in vitro</it>, the Kouzarides group then demonstrated that re-expression of miR-31 had the ability to abrogate both phenotypic effects of EMSY oncogenicity, while modification of the expression of the miRNA alone altered invasion and migration and phenocopied the effects of EMSY depletion.</p>
          <p>The authors then investigated the mechanism of miR-31 repression by EMSY demonstrating recruitment of EMSY to the miR-31 promoter, thus repressing transcription of the primary miRNA (pri-miRNA). However, as EMSY contains no DNA-binding domain another factor was necessary for promoter recruitment. Bioinformatic analysis of the miR-31 promoter revealed binding sites for ETS-1, ETV4/PEA3 and GATA1 with site-directed mutagenesis of any of the ETS-1 binding sites in the promoter reducing promoter activation. ETS-1 was subsequently confirmed to bind to the miR-31 promoter. Furthermore, this transcription factor was then shown to recruit the histone H3K4me3 demethylase KDM5B to the miR-31 promotor, thus inhibiting transcription of miR-31 through the demethylation of Lys4 on histone 3.</p>
       </sec>
       <sec>
          <st>
             <p>Viewpoint</p>
          </st>
          <p>This article is the first to mechanistically anchor an oncogenic role for EMSY and demonstrate a direct mechanism of regulation of miR-31, an important metastasis-associated miRNA, in breast cancer. Numerous mechanisms of miRNA regulation exist, including epigenetic silencing via DNA methylation or histone modification <abbrgrp>
                <abbr bid="B10">10</abbr>
             </abbrgrp>, direct regulation by nuclear receptors <abbrgrp>
                <abbr bid="B11">11</abbr>
             </abbrgrp> or indirect regulation through alteration of the miRNA biogenesis pathway <abbrgrp>
                <abbr bid="B12">12</abbr>
             </abbrgrp>. miR-31 has been seen to be hypermethylated in some cell lines <abbrgrp>
                <abbr bid="B13">13</abbr>
             </abbrgrp> and a recent study demonstrated that the miR-31 promoter also undergoes aberrant methylation in breast cancer patients <abbrgrp>
                <abbr bid="B14">14</abbr>
             </abbrgrp>. However until now, no direct regulatory mechanisms have been described.</p>
          <p>The evidence presented from both <it>in vitro</it> and <it>in vivo</it> studies provides a convincing argument for the classification as EMSY as an oncogene, as well as the direct regulation of miR-31 by this oncogene. However, the story is undoubtedly much more complex, as EMSY may regulate a number of different miRNAs/genes. Nor is this the only potential mechanism of direct regulation of miR-31. Notably, as EMSY amplification is only seen in approximately 13% of sporadic breast cancers, it is imperative to also study the regulation of this miRNA in an EMSY non-amplified context. Moreover, given that there were only 45 samples with EMSY amplification and corresponding miRNA data in the cohort studied, expanding the analysis into larger cohorts would provide additional evidence as to the relevance of this discovery in breast cancer patients.</p>
          <p>Vir&#233; and colleagues have elegantly documented a novel role for EMSY in breast cancer. However, they have also presented the case for a second scenario, that in which the oncogenic properties of this gene may be functionally propagated by potential oncomiRs. Indeed, EMSY depletion in cell lines led to the upregulation of 29 miRNAs and 6 miRNAs were found to be overexpressed in breast cancer patients with amplification of the gene. The mechanism of targeting of these miRNAs by EMSY, either directly or indirectly, has yet to be investigated.</p>
          <p>To date, the focus of the majority of miRNA publications has been to evaluate gene targets observed to affect a functional response. This study highlights a need to fully elucidate the mechanisms of regulation of the miRNAs themselves, not just their targets. This may lead to the discovery of a myriad of feedback loops similar to that between the miR-200 family of miRNAs and the transcriptional repressors ZEB1/SIP1 which functions to regulate epithelial-mesenchymal transition (EMT) <abbrgrp>
                <abbr bid="B15">15</abbr>
             </abbrgrp>. As miRNA inhibitors/mimics have only recently begun to enter clinical trials, ascertaining the mechanism of regulation of key miRNAs may greatly improve the chances of finding a surrogate gene candidate which could be successfully targeted in the clinic.</p>
       </sec>
       <sec>
          <st>
             <p>Abbreviations</p>
          </st>
          <p>EMT: epithelial-mesenchymal transition</p>
          <p>miRNA: microRNA</p>
          <p>pri-miRNA: primary microRNA</p>
       </sec>
       <sec>
          <st>
             <p>Competing interests</p>
          </st>
          <p>The authors declare that they have no competing interests.</p>
       </sec>
    </bdy>
    <bm>
       <ack>
          <sec>
             <st>
                <p>Acknowledgements</p>
             </st>
             <p>Funding is acknowledged from the Irish Research Council for Science, Engineering and Technology (IRCSET), Science Foundation Ireland through the <it>Molecular Therapeutics for Cancer, Ireland</it> Strategic Research Cluster (award 08/SRC/B1410; <url>http://www.mtci.ie</url>) and the Irish Cancer Society Collaborative Cancer Research Centre BREAST-PREDICT grant, CCRC13GAL (<url>http://www.breastpredict.com</url>).</p>
          </sec>
       </ack>
       <refgrp>
          
          <bibl id="B1">
             <title>
                <p>A pleiotropically acting microRNA, miR-31, inhibits breast cancer metastasis</p>
             </title>
             <aug>
                <au>
                   <snm>Valastyan</snm>
                   <fnm>S</fnm>
                </au>
                <au>
                   <snm>Reinhardt</snm>
                   <fnm>F</fnm>
                </au>
                <au>
                   <snm>Benaich</snm>
                   <fnm>N</fnm>
                </au>
                <au>
                   <snm>Calogrias</snm>
                   <fnm>D</fnm>
                </au>
                <au>
                   <snm>Szasz</snm>
                   <fnm>AM</fnm>
                </au>
                <au>
                   <snm>Wang</snm>
                   <fnm>ZC</fnm>
                </au>
                <au>
                   <snm>Brock</snm>
                   <fnm>JE</fnm>
                </au>
                <au>
                   <snm>Richardson</snm>
                   <fnm>AL</fnm>
                </au>
                <au>
                   <snm>Weinberg</snm>
                   <fnm>RA</fnm>
                </au>
             </aug>
             <source>Cell</source>
             <pubdate>2009</pubdate>
             <volume>137</volume>
             <fpage>1032</fpage>
             <lpage>1046</lpage>
             <xrefbib>
                <pubidlist>
                   <pubid idtype="doi">10.1016/j.cell.2009.03.047</pubid>
                   <pubid idtype="pmpid" link="fulltext">19524507</pubid>
                </pubidlist>
             </xrefbib>
          </bibl>
          <bibl id="B2">
             <title>
                <p>Concomitant suppression of three target genes can explain the impact of a microRNA on metastasis</p>
             </title>
             <aug>
                <au>
                   <snm>Valastyan</snm>
                   <fnm>S</fnm>
                </au>
                <au>
                   <snm>Benaich</snm>
                   <fnm>N</fnm>
                </au>
                <au>
                   <snm>Chang</snm>
                   <fnm>A</fnm>
                </au>
                <au>
                   <snm>Reinhardt</snm>
                   <fnm>F</fnm>
                </au>
                <au>
                   <snm>Weinberg</snm>
                   <fnm>RA</fnm>
                </au>
             </aug>
             <source>Genes Dev</source>
             <pubdate>2009</pubdate>
             <volume>23</volume>
             <fpage>2592</fpage>
             <lpage>2597</lpage>
             <xrefbib>
                <pubidlist>
                   <pubid idtype="doi">10.1101/gad.1832709</pubid>
                   <pubid idtype="pmpid" link="fulltext">19875476</pubid>
                </pubidlist>
             </xrefbib>
          </bibl>
          <bibl id="B3">
             <title>
                <p>WAVE3, an actin remodeling protein, is regulated by the metastasis suppressor microRNA, miR-31, during the invasion-metastasis cascade</p>
             </title>
             <aug>
                <au>
                   <snm>Sossey-Alaoui</snm>
                   <fnm>K</fnm>
                </au>
                <au>
                   <snm>Downs-Kelly</snm>
                   <fnm>E</fnm>
                </au>
                <au>
                   <snm>Das</snm>
                   <fnm>M</fnm>
                </au>
                <au>
                   <snm>Izem</snm>
                   <fnm>L</fnm>
                </au>
                <au>
                   <snm>Tubbs</snm>
                   <fnm>R</fnm>
                </au>
                <au>
                   <snm>Plow</snm>
                   <fnm>EF</fnm>
                </au>
             </aug>
             <source>Int J Cancer</source>
             <pubdate>2011</pubdate>
             <volume>129</volume>
             <fpage>1331</fpage>
             <lpage>1343</lpage>
             <xrefbib>
                <pubidlist>
                   <pubid idtype="doi">10.1002/ijc.25793</pubid>
                   <pubid idtype="pmpid" link="fulltext">21105030</pubid>
                </pubidlist>
             </xrefbib>
          </bibl>
          <bibl id="B4">
             <title>
                <p>Concurrent suppression of integrin alpha5, radixin, and RhoA phenocopies the effects of miR-31 on metastasis</p>
             </title>
             <aug>
                <au>
                   <snm>Valastyan</snm>
                   <fnm>S</fnm>
                </au>
                <au>
                   <snm>Chang</snm>
                   <fnm>A</fnm>
                </au>
                <au>
                   <snm>Benaich</snm>
                   <fnm>N</fnm>
                </au>
                <au>
                   <snm>Reinhardt</snm>
                   <fnm>F</fnm>
                </au>
                <au>
                   <snm>Weinberg</snm>
                   <fnm>RA</fnm>
                </au>
             </aug>
             <source>Cancer Res</source>
             <pubdate>2010</pubdate>
             <volume>70</volume>
             <fpage>5147</fpage>
             <lpage>5154</lpage>
             <xrefbib>
                <pubidlist>
                   <pubid idtype="doi">10.1158/0008-5472.CAN-10-0410</pubid>
                   <pubid idtype="pmpid" link="fulltext">20530680</pubid>
                </pubidlist>
             </xrefbib>
          </bibl>
          <bibl id="B5">
             <title>
                <p>Activation of miR-31 function in already-established metastases elicits metastatic regression</p>
             </title>
             <aug>
                <au>
                   <snm>Valastyan</snm>
                   <fnm>S</fnm>
                </au>
                <au>
                   <snm>Chang</snm>
                   <fnm>A</fnm>
                </au>
                <au>
                   <snm>Benaich</snm>
                   <fnm>N</fnm>
                </au>
                <au>
                   <snm>Reinhardt</snm>
                   <fnm>F</fnm>
                </au>
                <au>
                   <snm>Weinberg</snm>
                   <fnm>RA</fnm>
                </au>
             </aug>
             <source>Genes Dev</source>
             <pubdate>2011</pubdate>
             <volume>25</volume>
             <fpage>646</fpage>
             <lpage>659</lpage>
             <xrefbib>
                <pubidlist>
                   <pubid idtype="doi">10.1101/gad.2004211</pubid>
                   <pubid idtype="pmpid" link="fulltext">21406558</pubid>
                </pubidlist>
             </xrefbib>
          </bibl>
          <bibl id="B6">
             <title>
                <p>EMSY links the BRCA2 pathway to sporadic breast and ovarian cancer</p>
             </title>
             <aug>
                <au>
                   <snm>Hughes-Davies</snm>
                   <fnm>L</fnm>
                </au>
                <au>
                   <snm>Huntsman</snm>
                   <fnm>D</fnm>
                </au>
                <au>
                   <snm>Ruas</snm>
                   <fnm>M</fnm>
                </au>
                <au>
                   <snm>Fuks</snm>
                   <fnm>F</fnm>
                </au>
                <au>
                   <snm>Bye</snm>
                   <fnm>J</fnm>
                </au>
                <au>
                   <snm>Chin</snm>
                   <fnm>SF</fnm>
                </au>
                <au>
                   <snm>Milner</snm>
                   <fnm>J</fnm>
                </au>
                <au>
                   <snm>Brown</snm>
                   <fnm>LA</fnm>
                </au>
                <au>
                   <snm>Hsu</snm>
                   <fnm>F</fnm>
                </au>
                <au>
                   <snm>Gilks</snm>
                   <fnm>B</fnm>
                </au>
                <au>
                   <snm>Nielsen</snm>
                   <fnm>T</fnm>
                </au>
                <au>
                   <snm>Schulzer</snm>
                   <fnm>M</fnm>
                </au>
                <au>
                   <snm>Chia</snm>
                   <fnm>S</fnm>
                </au>
                <au>
                   <snm>Ragaz</snm>
                   <fnm>J</fnm>
                </au>
                <au>
                   <snm>Cahn</snm>
                   <fnm>A</fnm>
                </au>
                <au>
                   <snm>Linger</snm>
                   <fnm>L</fnm>
                </au>
                <au>
                   <snm>Ozdag</snm>
                   <fnm>H</fnm>
                </au>
                <au>
                   <snm>Cattaneo</snm>
                   <fnm>E</fnm>
                </au>
                <au>
                   <snm>Jordanova</snm>
                   <fnm>ES</fnm>
                </au>
                <au>
                   <snm>Schuuring</snm>
                   <fnm>E</fnm>
                </au>
                <au>
                   <snm>Yu</snm>
                   <fnm>DS</fnm>
                </au>
                <au>
                   <snm>Venkitaraman</snm>
                   <fnm>A</fnm>
                </au>
                <au>
                   <snm>Ponder</snm>
                   <fnm>B</fnm>
                </au>
                <au>
                   <snm>Doherty</snm>
                   <fnm>A</fnm>
                </au>
                <au>
                   <snm>Aparicio</snm>
                   <fnm>S</fnm>
                </au>
                <au>
                   <snm>Bentley</snm>
                   <fnm>D</fnm>
                </au>
                <au>
                   <snm>Theillet</snm>
                   <fnm>C</fnm>
                </au>
                <au>
                   <snm>Ponting</snm>
                   <fnm>CP</fnm>
                </au>
                <au>
                   <snm>Caldas</snm>
                   <fnm>C</fnm>
                </au>
                <au>
                   <snm>Kouzarides</snm>
                   <fnm>T</fnm>
                </au>
             </aug>
             <source>Cell</source>
             <pubdate>2003</pubdate>
             <volume>115</volume>
             <fpage>523</fpage>
             <lpage>535</lpage>
             <xrefbib>
                <pubidlist>
                   <pubid idtype="doi">10.1016/S0092-8674(03)00930-9</pubid>
                   <pubid idtype="pmpid" link="fulltext">14651845</pubid>
                </pubidlist>
             </xrefbib>
          </bibl>
          <bibl id="B7">
             <title>
                <p>The Breast Cancer Oncogene EMSY Represses Transcription of Antimetastatic microRNA miR-31</p>
             </title>
             <aug>
                <au>
                   <snm>Vire</snm>
                   <fnm>E</fnm>
                </au>
                <au>
                   <snm>Curtis</snm>
                   <fnm>C</fnm>
                </au>
                <au>
                   <snm>Davalos</snm>
                   <fnm>V</fnm>
                </au>
                <au>
                   <snm>Git</snm>
                   <fnm>A</fnm>
                </au>
                <au>
                   <snm>Robson</snm>
                   <fnm>S</fnm>
                </au>
                <au>
                   <snm>Villanueva</snm>
                   <fnm>A</fnm>
                </au>
                <au>
                   <snm>Vidal</snm>
                   <fnm>A</fnm>
                </au>
                <au>
                   <snm>Barbieri</snm>
                   <fnm>I</fnm>
                </au>
                <au>
                   <snm>Aparicio</snm>
                   <fnm>S</fnm>
                </au>
                <au>
                   <snm>Esteller</snm>
                   <fnm>M</fnm>
                </au>
                <au>
                   <snm>Caldas</snm>
                   <fnm>C</fnm>
                </au>
                <au>
                   <snm>Kouzarides</snm>
                   <fnm>T</fnm>
                </au>
             </aug>
             <source>Mol Cell</source>
             <pubdate>2014</pubdate>
             <volume>53</volume>
             <fpage>806</fpage>
             <lpage>818</lpage>
             <xrefbib>
                <pubidlist>
                   <pubid idtype="doi">10.1016/j.molcel.2014.01.029</pubid>
                   <pubid idtype="pmpid" link="fulltext">24582497</pubid>
                </pubidlist>
             </xrefbib>
          </bibl>
          <bibl id="B8">
             <title>
                <p>The genomic and transcriptomic architecture of 2,000 breast tumours reveals novel subgroups</p>
             </title>
             <aug>
                <au>
                   <snm>Curtis</snm>
                   <fnm>C</fnm>
                </au>
                <au>
                   <snm>Shah</snm>
                   <fnm>SP</fnm>
                </au>
                <au>
                   <snm>Chin</snm>
                   <fnm>SF</fnm>
                </au>
                <au>
                   <snm>Turashvili</snm>
                   <fnm>G</fnm>
                </au>
                <au>
                   <snm>Rueda</snm>
                   <fnm>OM</fnm>
                </au>
                <au>
                   <snm>Dunning</snm>
                   <fnm>MJ</fnm>
                </au>
                <au>
                   <snm>Speed</snm>
                   <fnm>D</fnm>
                </au>
                <au>
                   <snm>Lynch</snm>
                   <fnm>AG</fnm>
                </au>
                <au>
                   <snm>Samarajiwa</snm>
                   <fnm>S</fnm>
                </au>
                <au>
                   <snm>Yuan</snm>
                   <fnm>Y</fnm>
                </au>
                <au>
                   <snm>Gr&#228;f</snm>
                   <fnm>S</fnm>
                </au>
                <au>
                   <snm>Ha</snm>
                   <fnm>G</fnm>
                </au>
                <au>
                   <snm>Haffari</snm>
                   <fnm>G</fnm>
                </au>
                <au>
                   <snm>Bashashati</snm>
                   <fnm>A</fnm>
                </au>
                <au>
                   <snm>Russell</snm>
                   <fnm>R</fnm>
                </au>
                <au>
                   <snm>McKinney</snm>
                   <fnm>S</fnm>
                </au>
                <au>
                   <snm>METABRIC</snm>
                   <fnm>Group</fnm>
                </au>
                <au>
                   <snm>Langer&#248;d</snm>
                   <fnm>A</fnm>
                </au>
                <au>
                   <snm>Green</snm>
                   <fnm>A</fnm>
                </au>
                <au>
                   <snm>Provenzano</snm>
                   <fnm>E</fnm>
                </au>
                <au>
                   <snm>Wishart</snm>
                   <fnm>G</fnm>
                </au>
                <au>
                   <snm>Pinder</snm>
                   <fnm>S</fnm>
                </au>
                <au>
                   <snm>Watson</snm>
                   <fnm>P</fnm>
                </au>
                <au>
                   <snm>Markowetz</snm>
                   <fnm>F</fnm>
                </au>
                <au>
                   <snm>Murphy</snm>
                   <fnm>L</fnm>
                </au>
                <au>
                   <snm>Ellis</snm>
                   <fnm>I</fnm>
                </au>
                <au>
                   <snm>Purushotham</snm>
                   <fnm>A</fnm>
                </au>
                <au>
                   <snm>B&#248;rresen-Dale</snm>
                   <fnm>AL</fnm>
                </au>
                <au>
                   <snm>Brenton</snm>
                   <fnm>JD</fnm>
                </au>
                <au>
                   <snm>Tavar&#233;</snm>
                   <fnm>S</fnm>
                </au>
                <au>
                   <snm>Caldas</snm>
                   <fnm>C</fnm>
                </au>
                <au>
                   <snm>Aparicio</snm>
                   <fnm>S</fnm>
                </au>
             </aug>
             <source>Nature</source>
             <pubdate>2012</pubdate>
             <volume>486</volume>
             <fpage>346</fpage>
             <lpage>352</lpage>
             <xrefbib>
                <pubidlist>
                   <pubid idtype="pmpid" link="fulltext">22522925</pubid>
                </pubidlist>
             </xrefbib>
          </bibl>
          <bibl id="B9">
             <title>
                <p>The shaping and functional consequences of the microRNA landscape in breast cancer</p>
             </title>
             <aug>
                <au>
                   <snm>Dvinge</snm>
                   <fnm>H</fnm>
                </au>
                <au>
                   <snm>Git</snm>
                   <fnm>A</fnm>
                </au>
                <au>
                   <snm>Graf</snm>
                   <fnm>S</fnm>
                </au>
                <au>
                   <snm>Salmon-Divon</snm>
                   <fnm>M</fnm>
                </au>
                <au>
                   <snm>Curtis</snm>
                   <fnm>C</fnm>
                </au>
                <au>
                   <snm>Sottoriva</snm>
                   <fnm>A</fnm>
                </au>
                <au>
                   <snm>Zhao</snm>
                   <fnm>Y</fnm>
                </au>
                <au>
                   <snm>Hirst</snm>
                   <fnm>M</fnm>
                </au>
                <au>
                   <snm>Armisen</snm>
                   <fnm>J</fnm>
                </au>
                <au>
                   <snm>Miska</snm>
                   <fnm>EA</fnm>
                </au>
                <au>
                   <snm>Chin</snm>
                   <fnm>SF</fnm>
                </au>
                <au>
                   <snm>Provenzano</snm>
                   <fnm>E</fnm>
                </au>
                <au>
                   <snm>Turashvili</snm>
                   <fnm>G</fnm>
                </au>
                <au>
                   <snm>Green</snm>
                   <fnm>A</fnm>
                </au>
                <au>
                   <snm>Ellis</snm>
                   <fnm>I</fnm>
                </au>
                <au>
                   <snm>Aparicio</snm>
                   <fnm>S</fnm>
                </au>
                <au>
                   <snm>Caldas</snm>
                   <fnm>C</fnm>
                </au>
             </aug>
             <source>Nature</source>
             <pubdate>2013</pubdate>
             <volume>497</volume>
             <fpage>378</fpage>
             <lpage>382</lpage>
             <xrefbib>
                <pubidlist>
                   <pubid idtype="doi">10.1038/nature12108</pubid>
                   <pubid idtype="pmpid" link="fulltext">23644459</pubid>
                </pubidlist>
             </xrefbib>
          </bibl>
          <bibl id="B10">
             <title>
                <p>Dysregulation of microRNAs in cancer: playing with fire</p>
             </title>
             <aug>
                <au>
                   <snm>Melo</snm>
                   <fnm>SA</fnm>
                </au>
                <au>
                   <snm>Esteller</snm>
                   <fnm>M</fnm>
                </au>
             </aug>
             <source>FEBS Lett</source>
             <pubdate>2011</pubdate>
             <volume>585</volume>
             <fpage>2087</fpage>
             <lpage>2099</lpage>
             <xrefbib>
                <pubidlist>
                   <pubid idtype="doi">10.1016/j.febslet.2010.08.009</pubid>
                   <pubid idtype="pmpid" link="fulltext">20708002</pubid>
                </pubidlist>
             </xrefbib>
          </bibl>
          <bibl id="B11">
             <title>
                <p>Regulation of microRNA expression and function by nuclear receptor signaling</p>
             </title>
             <aug>
                <au>
                   <snm>Yang</snm>
                   <fnm>Z</fnm>
                </au>
                <au>
                   <snm>Wang</snm>
                   <fnm>L</fnm>
                </au>
             </aug>
             <source>Cell Biosci</source>
             <pubdate>2011</pubdate>
             <volume>1</volume>
             <fpage>31</fpage>
             <xrefbib>
                <pubidlist>
                   <pubid idtype="doi">10.1186/2045-3701-1-31</pubid>
                   <pubid idtype="pmpid" link="fulltext">21936947</pubid>
                </pubidlist>
             </xrefbib>
          </bibl>
          <bibl id="B12">
             <title>
                <p>miRNA dysregulation in breast cancer</p>
             </title>
             <aug>
                <au>
                   <snm>Mulrane</snm>
                   <fnm>L</fnm>
                </au>
                <au>
                   <snm>McGee</snm>
                   <fnm>SF</fnm>
                </au>
                <au>
                   <snm>Gallagher</snm>
                   <fnm>WM</fnm>
                </au>
                <au>
                   <snm>O'Connor</snm>
                   <fnm>DP</fnm>
                </au>
             </aug>
             <source>Cancer Res</source>
             <pubdate>2013</pubdate>
             <volume>73</volume>
             <fpage>6554</fpage>
             <lpage>6562</lpage>
             <xrefbib>
                <pubidlist>
                   <pubid idtype="doi">10.1158/0008-5472.CAN-13-1841</pubid>
                   <pubid idtype="pmpid" link="fulltext">24204025</pubid>
                </pubidlist>
             </xrefbib>
          </bibl>
          <bibl id="B13">
             <title>
                <p>miR-31 and its host gene lncRNA LOC554202 are regulated by promoter hypermethylation in triple-negative breast cancer</p>
             </title>
             <aug>
                <au>
                   <snm>Augoff</snm>
                   <fnm>K</fnm>
                </au>
                <au>
                   <snm>McCue</snm>
                   <fnm>B</fnm>
                </au>
                <au>
                   <snm>Plow</snm>
                   <fnm>EF</fnm>
                </au>
                <au>
                   <snm>Sossey-Alaoui</snm>
                   <fnm>K</fnm>
                </au>
             </aug>
             <source>Mol Cancer</source>
             <pubdate>2012</pubdate>
             <volume>11</volume>
             <fpage>5</fpage>
             <xrefbib>
                <pubidlist>
                   <pubid idtype="doi">10.1186/1476-4598-11-5</pubid>
                   <pubid idtype="pmpid" link="fulltext">22289355</pubid>
                </pubidlist>
             </xrefbib>
          </bibl>
          <bibl id="B14">
             <title>
                <p>miRNA gene promoters are frequent targets of aberrant DNA methylation in human breast cancer</p>
             </title>
             <aug>
                <au>
                   <snm>Vrba</snm>
                   <fnm>L</fnm>
                </au>
                <au>
                   <snm>Munoz-Rodriguez</snm>
                   <fnm>JL</fnm>
                </au>
                <au>
                   <snm>Stampfer</snm>
                   <fnm>MR</fnm>
                </au>
                <au>
                   <snm>Futscher</snm>
                   <fnm>BW</fnm>
                </au>
             </aug>
             <source>PLoS One</source>
             <pubdate>2013</pubdate>
             <volume>8</volume>
             <fpage>e54398</fpage>
             <xrefbib>
                <pubidlist>
                   <pubid idtype="doi">10.1371/journal.pone.0054398</pubid>
                   <pubid idtype="pmpid" link="fulltext">23342147</pubid>
                </pubidlist>
             </xrefbib>
          </bibl>
          <bibl id="B15">
             <title>
                <p>A double-negative feedback loop between ZEB1-SIP1 and the microRNA-200 family regulates epithelial-mesenchymal transition</p>
             </title>
             <aug>
                <au>
                   <snm>Bracken</snm>
                   <fnm>CP</fnm>
                </au>
                <au>
                   <snm>Gregory</snm>
                   <fnm>PA</fnm>
                </au>
                <au>
                   <snm>Kolesnikoff</snm>
                   <fnm>N</fnm>
                </au>
                <au>
                   <snm>Bert</snm>
                   <fnm>AG</fnm>
                </au>
                <au>
                   <snm>Wang</snm>
                   <fnm>J</fnm>
                </au>
                <au>
                   <snm>Shannon</snm>
                   <fnm>MF</fnm>
                </au>
                <au>
                   <snm>Goodall</snm>
                   <fnm>GJ</fnm>
                </au>
             </aug>
             <source>Cancer Res</source>
             <pubdate>2008</pubdate>
             <volume>68</volume>
             <fpage>7846</fpage>
             <lpage>7854</lpage>
             <xrefbib>
                <pubidlist>
                   <pubid idtype="doi">10.1158/0008-5472.CAN-08-1942</pubid>
                   <pubid idtype="pmpid" link="fulltext">18829540</pubid>
                </pubidlist>
             </xrefbib>
          </bibl>
       </refgrp>
    </bm>
 </art>
