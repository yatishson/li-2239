<art>
    <ui>s40463-014-0036-4</ui>
    <ji>1916-0216</ji>
    <fm>
       <dochead>Original research article</dochead>
       <bibl>
          <title>
             <p>Eyelid and brow asymmetry in patients evaluated for upper lid blepharoplasty</p>
          </title>
          <aug>
             <au ca="yes" id="A1">
                <snm>Macdonald</snm>
                <mi>I</mi>
                <fnm>Kristian</fnm>
                <insr iid="I1"/>
                <email>kristian.macdonald@gmail.com</email>
             </au>
             <au id="A2">
                <snm>Mendez</snm>
                <mi>I</mi>
                <fnm>Adrian</fnm>
                <insr iid="I2"/>
                <email>amendez@ualberta.ca</email>
             </au>
             <au id="A3">
                <snm>Hart</snm>
                <mi>D</mi>
                <fnm>Robert</fnm>
                <insr iid="I3"/>
                <email>drrobhart@hotmail.com</email>
             </au>
             <au id="A4">
                <snm>Taylor</snm>
                <mnm>Mark</mnm>
                <fnm>S</fnm>
                <insr iid="I3"/>
                <email>smtaylorwashu@yahoo.com</email>
             </au>
          </aug>
          <insg>
             <ins id="I1">
                <p>Department of Otolaryngology &#8211; Head &amp; Neck Surgery, University of Ottawa, 1081 Carling Ave, Ottawa K1Y 4G2, ON, Canada</p>
             </ins>
             <ins id="I2">
                <p>Division of Otolaryngology &#8211; Head &amp; Neck Surgery, University of Alberta, Edmonton, AB, Canada</p>
             </ins>
             <ins id="I3">
                <p>Division of Otolaryngology &#8211; Head &amp; Neck Surgery, Dalhousie University, Halifax, NS, Canada</p>
             </ins>
          </insg>
          <source>Journal of Otolaryngology - Head &amp; Neck Surgery</source>
          <issn>1916-0216</issn>
          <pubdate>2014</pubdate>
          <volume>43</volume>
          <issue>1</issue>
          <fpage>36</fpage>
          <url>http://www.journalotohns.com/content/43/1/36</url>
          <xrefbib>
             <pubidlist>
                <pubid idtype="doi">10.1186/s40463-014-0036-4</pubid>
                <pubid idtype="pmpid">25294556</pubid>
             </pubidlist>
          </xrefbib>
       </bibl>
       <history>
          <rec>
             <date>
                <day>29</day>
                <month>5</month>
                <year>2014</year>
             </date>
          </rec>
          <acc>
             <date>
                <day>24</day>
                <month>8</month>
                <year>2014</year>
             </date>
          </acc>
          <pub>
             <date>
                <day>2</day>
                <month>10</month>
                <year>2014</year>
             </date>
          </pub>
       </history>
       <cpyrt>
          <year>2014</year>
          <collab>Macdonald et al.; licensee BioMed Central Ltd.</collab>
          <note>This is an Open Access article distributed under the terms of the Creative Commons Attribution License (<url>http://creativecommons.org/licenses/by/4.0</url>), which permits unrestricted use, distribution, and reproduction in any medium, provided the original work is properly credited. The Creative Commons Public Domain Dedication waiver (<url>http://creativecommons.org/publicdomain/zero/1.0/</url>) applies to the data made available in this article, unless otherwise stated.</note>
       </cpyrt>
       <kwdg>
          <kwd>Eyelid</kwd>
          <kwd>Eyebrow</kwd>
          <kwd>Symmetry</kwd>
          <kwd>Asymmetry</kwd>
          <kwd>Blepharoplasty</kwd>
          <kwd>Informed consent</kwd>
          <kwd>Patient evaluation</kwd>
       </kwdg>
       <abs>
          <sec>
             <st>
                <p>Abstract</p>
             </st>
             <sec>
                <st>
                   <p>Introduction</p>
                </st>
                <p>In evaluation for blepharoplasty, patients often desire improved cosmesis and/or correction of visual field deficits. However, patients are usually unaware of eyelid or brow asymmetry. Furthermore, the prevalence of eyelid and brow asymmetry is infrequently reported in the medical literature.</p>
             </sec>
             <sec>
                <st>
                   <p>Purpose</p>
                </st>
                <p>To determine the prevalence of brow and eyelid asymmetry in patients evaluated for upper lid blepharoplasty.</p>
             </sec>
             <sec>
                <st>
                   <p>Methods</p>
                </st>
                <p>One hundred consecutive patients evaluated for upper lid blepharoplasty were included in the study. Standard pre-operative photographs were taken of all patients using consistent background and photographic equipment. Two of the authors (KM &amp; AM) independently recorded the margin pupil (MPD), central eyebrow (CED), nasal eyebrow (NED) and temporal eyebrow (TED) distances. To test the inter-observer reliability, the senior author (SMT) recorded the same measurements for 10% of randomly selected patients. We calculated 95% confidence intervals to compare symmetry between the right and left sides.</p>
             </sec>
             <sec>
                <st>
                   <p>Results</p>
                </st>
                <p>One hundred patients (94 female, mean age 57.7) were included in the study. The average MPD, CED, NED and TED distances were 0.55&#160;mm (95% CI 0.45-0.65), 1.77&#160;mm (95% CI 1.47-2.07), 1.34&#160;mm (95% CI 1.14-1.54), and 1.78&#160;mm (95% CI 1.50-2.06), respectively. Ninety-three percent of patients had at least one asymmetric measurement of greater than 1&#160;mm. Seventy-five percent of patients studied had at least one measurement greater than 2&#160;mm while 37 percent had at least one greater than 3&#160;mm.</p>
             </sec>
             <sec>
                <st>
                   <p>Conclusion</p>
                </st>
                <p>Brow and eyelid asymmetry is common in patients being evaluated for upper lid blepharoplasty. The facial plastic surgeon should identify and document facial asymmetry pre-operatively, and discuss it with prospective blepharoplasty patients. This will improve informed consent and patient expectations.</p>
             </sec>
          </sec>
       </abs>
    </fm>
    <bdy>
       <sec>
          <st>
             <p>1
 						Background</p>
          </st>
          <p>In evaluation for blepharoplasty, patients often desire improved cosmesis and/or correction of visual field deficits. However, patients are usually unaware of eyelid or brow asymmetry <abbrgrp>
                <abbr bid="B1">1</abbr>
             </abbrgrp>-<abbrgrp>
                <abbr bid="B3">3</abbr>
             </abbrgrp>. This is interesting to note, given the importance of facial symmetry in defining beauty <abbrgrp>
                <abbr bid="B4">4</abbr>
             </abbrgrp>,<abbrgrp>
                <abbr bid="B5">5</abbr>
             </abbrgrp>.</p>
          <p>Preoperatively, it is important to identify and inform the patient of the presence of eyelid and brow asymmetry <abbrgrp>
                <abbr bid="B1">1</abbr>
             </abbrgrp>,<abbrgrp>
                <abbr bid="B6">6</abbr>
             </abbrgrp>. This will allow for a comprehensive surgical plan, and will help ensure reasonable patient expectations. Although there is no clear consensus on what degree of asymmetry is of clinical importance, some authors report that facial asymmetry as little as 1&#160;mm is of significance and warrants attention <abbrgrp>
                <abbr bid="B7">7</abbr>
             </abbrgrp>-<abbrgrp>
                <abbr bid="B9">9</abbr>
             </abbrgrp>.</p>
          <p>The prevalence of eyelid and brow asymmetry is infrequently reported in the medical literature. Song et al. reported a 30% asymmetry of the palpebral fissure in a random population of 594 Koreans <abbrgrp>
                <abbr bid="B8">8</abbr>
             </abbrgrp>. A study examining photos of models in popular magazines found that 10% had asymmetry of several measures of eyelid and brow height <abbrgrp>
                <abbr bid="B9">9</abbr>
             </abbrgrp>.</p>
          <p>It is the senior author&#8217;s (SMT) hypothesis that a significant proportion of patients presenting for evaluation for blepharoplasty have eyelid and/or brow asymmetry. This group is particularly interesting to study, as although they are inquiring about eyelid surgery, most are unaware of any existing asymmetry.</p>
       </sec>
       <sec>
          <st>
             <p>2
 						Methods</p>
          </st>
          <p>This retrospective chart review was approved by the local research ethics board at Dalhousie University in Halifax, NS, Canada. We included consecutive patients who were evaluated for upper lid blepharoplasty. Patients were excluded if they had a documented eyelid or brow asymmetry with a defined cause, including previous surgery and/or facial nerve palsy.</p>
          <p>Preoperative pictures were taken in a standardized fashion with the same camera, at zero magnification, at the same distance from the patient. From these pictures, a senior (KM) and a junior Otolaryngology &#8211; Head &amp; Neck Surgery resident (AM) independently measured the four distances detailed in Figure&#160;<figr fid="F1">1</figr> for each eye. These were the margin pupil (MPD), the central eyebrow (CED), the nasal eyebrow (NED), and the temporal eyebrow (TEP) distances.</p>
          
             <fig id="F1">
                <title>
                   <p>Figure 1</p>
                </title>
                <caption>
                   <p>Definitions of the measurements used in this study. 1</p>
                </caption>
                <text>
                   <p>
                      <b>Definitions of the measurements used in this study. 1</b>. Nasal eyebrow (NEP), the distance from the medical canthus to lower eyebrow. <b>2</b>. Central eyebrow (CEP), the distance from the upper lid margin to the lower eyebrow in the mid-pupil plan. <b>3</b>. Temporal eyebrow (TEP), the distance from the lateral canthus to the lower eyebrow. <b>4</b>. Margin pupil distance (MPD), the distance from the central upper lid margin to the centre of the pupil.</p>
                </text>
                <graphic file="s40463-014-0036-4-1"/>
             </fig>
          
          <p>For each measurement, the mean asymmetry with 95% confidence interval was calculated. We also determined the proportion of patients who had equal to or greater than 1, 2 and 3&#160;mm asymmetry for each measurement. To test for inter-observer reliability, 10% of the patients were randomly selected for independent measurements by the senior author, a facial plastic surgeon (SMT). These measurements and asymmetries were compared to those of the other two authors.</p>
       </sec>
       <sec>
          <st>
             <p>3
 						Results</p>
          </st>
          <p>One hundred consecutive patients who presented for evaluation for upper lid blepharoplasty were included in the analysis. None of the patients were excluded. There were 97 Caucasians and 3 Asian patients in the study cohort. 94 patients were female, and 6 were male. The average age of the group was 57.7 +/- 10&#160;years.</p>
          <p>The 10 sets of randomly selected patients measured by the senior author were compared to those of the first author. Although some of the actual measurements varied slightly, the asymmetries were within 0.1&#160;mm for all 40 measurements.</p>
          <p>The proportions of patients with &#8805;1, 2 and 3&#160;mm of asymmetry are presented in Table&#160;<tblr tid="T1">1</tblr>. In summary, 93% of patients had greater than or equal to 1&#160;mm of asymmetry in at least one of four measurements, 75% had greater than or equal to 2&#160;mm, and 37% had greater than or equal to 3&#160;mm.</p>
          
             <table id="T1">
                <title>
                   <p>Table 1</p>
                </title>
                <caption>
                   <p>
                      <b>Number of patients with asymmetry with 3 different limits</b>
                   </p>
                </caption>
                <tgroup align="left" cols="4">
                   <colspec align="left" colname="c1" colnum="1" colwidth="*"/>
                   <colspec align="left" colname="c2" colnum="2" colwidth="*"/>
                   <colspec align="left" colname="c3" colnum="3" colwidth="*"/>
                   <colspec align="left" colname="c4" colnum="4" colwidth="*"/>
                   <thead>
                      <row valign="top">
                         <entry colname="c1">
                            <p>
                               <b>Measurement</b>
                            </p>
                         </entry>
                         <entry colname="c2">
                            <p>
                               <b>&#8805;1&#160;mm</b>
                            </p>
                         </entry>
                         <entry colname="c3">
                            <p>
                               <b>&#8805;2&#160;mm</b>
                            </p>
                         </entry>
                         <entry colname="c4">
                            <p>
                               <b>&#8805;3&#160;mm</b>
                            </p>
                         </entry>
                      </row>
                   </thead>
                   <tfoot>
                      <p>MPD =&#8201;Margin pupil distance; CED =&#8201;Central eye distance; NED =&#8201;Nasal eye distance; TED =&#8201;Temporal eye distance.</p>
                   </tfoot>
                   <tbody>
                      <row valign="top">
                         <entry colname="c1">
                            <p>
                               <b>MPD</b>
                            </p>
                         </entry>
                         <entry colname="c2">
                            <p>11</p>
                         </entry>
                         <entry colname="c3">
                            <p>3</p>
                         </entry>
                         <entry colname="c4">
                            <p>1</p>
                         </entry>
                      </row>
                      <row valign="top">
                         <entry colname="c1">
                            <p>
                               <b>CED</b>
                            </p>
                         </entry>
                         <entry colname="c2">
                            <p>58</p>
                         </entry>
                         <entry colname="c3">
                            <p>38</p>
                         </entry>
                         <entry colname="c4">
                            <p>18</p>
                         </entry>
                      </row>
                      <row valign="top">
                         <entry colname="c1">
                            <p>
                               <b>NED</b>
                            </p>
                         </entry>
                         <entry colname="c2">
                            <p>50</p>
                         </entry>
                         <entry colname="c3">
                            <p>25</p>
                         </entry>
                         <entry colname="c4">
                            <p>10</p>
                         </entry>
                      </row>
                      <row valign="top">
                         <entry colname="c1">
                            <p>
                               <b>TED</b>
                            </p>
                         </entry>
                         <entry colname="c2">
                            <p>61</p>
                         </entry>
                         <entry colname="c3">
                            <p>41</p>
                         </entry>
                         <entry colname="c4">
                            <p>18</p>
                         </entry>
                      </row>
                      <row valign="top">
                         <entry colname="c1">
                            <p>
                               <b>&#8805;1 measurement of asymmetry</b>
                            </p>
                         </entry>
                         <entry colname="c2">
                            <p>93</p>
                         </entry>
                         <entry colname="c3">
                            <p>75</p>
                         </entry>
                         <entry colname="c4">
                            <p>37</p>
                         </entry>
                      </row>
                   </tbody>
                </tgroup>
             </table>
          
          <p>Figure&#160;<figr fid="F2">2</figr> reports the average asymmetry, in millimeters, for each of the four measurements. The average MPD, NED, TED and CED were 0.55&#160;mm (95% CI 0.44-0.66), 1.34&#160;mm (95% CI 1.14-1.54), 1.78&#160;mm (95% CI 1.49-2.07), and 1.77&#160;mm (95% CI 1.46-2.07), respectively.</p>
          
             <fig id="F2">
                <title>
                   <p>Figure 2</p>
                </title>
                <caption>
                   <p>Average eyelid and brow asymmetries for 100 patients.</p>
                </caption>
                <text>
                   <p>
                      <b>Average eyelid and brow asymmetries for 100 patients.</b> MPD =&#8201;Margin pupil distance.</p>
                </text>
                <graphic file="s40463-014-0036-4-2"/>
             </fig>
          
          <p>Two examples are presented in Figures&#160;<figr fid="F3">3</figr> and <figr fid="F4">4</figr>. Both patients were females in their early 40&#8242;s who had requested a more youthful appearance of their upper eyelids. The first patient (Figure&#160;<figr fid="F3">3</figr>) had a 4&#160;mm asymmetry in the temporal eye distance, with the right side higher than the left. The second patient (Figure&#160;<figr fid="F4">4</figr>) had a more obvious asymmetry, with a central eye distance asymmetry of 5.5&#160;mm. Neither patient noted their asymmetry as a complaint or reason for requesting surgery.</p>
          
             <fig id="F3">
                <title>
                   <p>Figure 3</p>
                </title>
                <caption>
                   <p>Patient 1.</p>
                </caption>
                <text>
                   <p>
                      <b>Patient 1.</b> Asymmetries: MPD =&#8201;0&#160;mm; CED =&#8201;1.5&#160;mm; NED =&#8201;1.5&#160;mm; TED =&#8201;4&#160;mm.</p>
                </text>
                <graphic file="s40463-014-0036-4-3"/>
             </fig>
          
          
             <fig id="F4">
                <title>
                   <p>Figure 4</p>
                </title>
                <caption>
                   <p>Patient 2.</p>
                </caption>
                <text>
                   <p>
                      <b>Patient 2.</b> Asymmetries: MPD =&#8201;0&#160;mm; CED =&#8201;5.5&#160;mm; NED =&#8201;6&#160;mm; TED =&#8201;3.5&#160;mm.</p>
                </text>
                <graphic file="s40463-014-0036-4-4"/>
             </fig>
          
       </sec>
       <sec>
          <st>
             <p>4
 						Discussion</p>
          </st>
          <p>Published literature on facial analysis has stressed the importance of preoperatively identifying facial asymmetry <abbrgrp>
                <abbr bid="B1">1</abbr>
             </abbrgrp>-<abbrgrp>
                <abbr bid="B3">3</abbr>
             </abbrgrp>,<abbrgrp>
                <abbr bid="B10">10</abbr>
             </abbrgrp>. However, the actual prevalence of eyelid and brow asymmetry is rarely reported. Searches online in Medline with the terms &#8220;eyelid asymmetry&#8221;, &#8220;brow asymmetry&#8221;, &#8220;photo analysis and eyelids&#8221;, and &#8220;facial asymmetry and aesthetics&#8221;, yielded few relevant studies.</p>
          <p>Ing et al. evaluated 102 models in popular magazine photographs <abbrgrp>
                <abbr bid="B9">9</abbr>
             </abbrgrp>. They measured 14 ocular parameters, and identified a mean asymmetry of 0.2-2.4&#160;mm. 12 of the models had an asymmetry of 2 standard deviations from the mean, concluding that a significant number of models had substantial facial asymmetry.</p>
          <p>In another study, Song et al. sought to quantify asymmetry of palpebral fissure height in normal Koreans <abbrgrp>
                <abbr bid="B8">8</abbr>
             </abbrgrp>. They recruited 594 patients from the general population, and determined the prevalence of asymmetry greater than 1&#160;mm. They found that 24.2% of males and 26.5% of females had such an asymmetry. The authors called for similar studies in Caucasians.</p>
          <p>Our patient population is particularly interesting, as they are mostly female Caucasians (94%) who sought surgical correction of the upper eyelid. Although they had paid particular attention to their eyes, they were not aware of, or at least did not make note of any asymmetry during their clinic visit. We could not identify other studies in which eyelid or brow asymmetry was assessed in patients presenting for blepharoplasty.</p>
          <p>Despite this, the vast majority of our patients (93%) had, in at least one of the four measurements, asymmetry greater than or equal to 1&#160;mm. Using more stringent criteria, three quarters of patients had an asymmetry greater than or equal to 2&#160;mm, in at least one of the measurements.</p>
          <p>Awareness of the prevalence of asymmetry is important for the facial plastic surgeon. It will enhance the preoperative evaluation, help optimize the surgical plan and improve patient expectations and satisfaction. Once their other concerns are addressed, patients who were not aware of asymmetries preoperatively may be more likely to take notice postoperatively, and could conclude that they are iatrogenic in etiology. It is therefore critical that the facial plastic surgeon identifies potential asymmetry preoperatively and educates the patient appropriately.</p>
       </sec>
       <sec>
          <st>
             <p>5
 						Conclusion</p>
          </st>
          <p>The vast majority of patients presenting for evaluation for upper lid blepharoplasty had eyelid or brow asymmetry greater than or equal to 1&#160;mm. Temporal and central eyebrow distances showed the greatest asymmetry. This knowledge is critical for the facial plastic surgeon, and any potential asymmetry should be identified and discussed preoperatively with blepharoplasty patients.</p>
          <sec>
             <st>
                <p>5.1 Consent</p>
             </st>
             <p>Written informed consent was obtained from the patient for the publication of this report and any accompanying images.</p>
          </sec>
       </sec>
       <sec>
          <st>
             <p>Competing interests</p>
          </st>
          <p>The authors declare that they have no competing interests.</p>
       </sec>
       <sec>
          <st>
             <p>Authors&#8217; contributions</p>
          </st>
          <p>SMT conceived and designed the study, aided in data acquisition, and critically revised the manuscript. KM obtained ethics approval, performed the data analysis, and wrote the initial manuscript draft, including tables and figures. AM participated in data acquisition, and in revising the manuscript. RH participated in revising the manuscript. All authors read and approved the final manuscript.</p>
       </sec>
    </bdy>
    <bm>
       <refgrp>
          
          <bibl id="B1">
             <title>
                <p>Blepharoplasty</p>
             </title>
             <aug>
                <au>
                   <snm>Pastorek</snm>
                   <fnm>N</fnm>
                </au>
             </aug>
             <source>Head &amp; Neck Surgery - Otolaryngology</source>
             <publisher>Lippincott Williams &amp; Wilkins, Philadelphia</publisher>
             <editor>Bailey BJ, Johnson JT</editor>
             <edition>4</edition>
             <pubdate>2006</pubdate>
             <fpage>2611</fpage>
             <lpage>2626</lpage>
          </bibl>
          <bibl id="B2">
             <title>
                <p>Facial asymmetry and attractiveness judgment in developmental perspective</p>
             </title>
             <aug>
                <au>
                   <snm>Kowner</snm>
                   <fnm>R</fnm>
                </au>
             </aug>
             <source>J Exp Psychol Hum Percept Perform</source>
             <pubdate>1996</pubdate>
             <volume>22</volume>
             <issue>3</issue>
             <fpage>662</fpage>
             <lpage>675</lpage>
             <xrefbib>
                <pubidlist>
                   <pubid idtype="doi">10.1037/0096-1523.22.3.662</pubid>
                   <pubid idtype="pmpid" link="fulltext">8666958</pubid>
                </pubidlist>
             </xrefbib>
          </bibl>
          <bibl id="B3">
             <title>
                <p>Eyebrow asymmetry: definition and symmetrical correction using botulinum toxin <it>A</it>
                </p>
             </title>
             <aug>
                <au>
                   <snm>Tiryaki</snm>
                   <fnm>T</fnm>
                </au>
                <au>
                   <snm>Ciloglu</snm>
                   <fnm>NS</fnm>
                </au>
             </aug>
             <source>Aesthet Surg J</source>
             <pubdate>2007</pubdate>
             <volume>27</volume>
             <fpage>513</fpage>
             <lpage>517</lpage>
             <xrefbib>
                <pubidlist>
                   <pubid idtype="doi">10.1016/j.asj.2007.06.005</pubid>
                   <pubid idtype="pmpid" link="fulltext">19341680</pubid>
                </pubidlist>
             </xrefbib>
          </bibl>
          <bibl id="B4">
             <title>
                <p>Symmetry and perceived facial attractiveness: a monozygotic co-twin comparison</p>
             </title>
             <aug>
                <au>
                   <snm>Mealey</snm>
                   <fnm>L</fnm>
                </au>
                <au>
                   <snm>Bridgstock</snm>
                   <fnm>R</fnm>
                </au>
                <au>
                   <snm>Townsend</snm>
                   <fnm>GC</fnm>
                </au>
             </aug>
             <source>J Pers Soc Psychol</source>
             <pubdate>1999</pubdate>
             <volume>76</volume>
             <issue>1</issue>
             <fpage>151</fpage>
             <lpage>158</lpage>
             <xrefbib>
                <pubidlist>
                   <pubid idtype="doi">10.1037/0022-3514.76.1.151</pubid>
                   <pubid idtype="pmpid" link="fulltext">9972560</pubid>
                </pubidlist>
             </xrefbib>
          </bibl>
          <bibl id="B5">
             <title>
                <p>Human facial attractiveness and sexual selection: the role of symmetry and averageness</p>
             </title>
             <aug>
                <au>
                   <snm>Grammer</snm>
                   <fnm>K</fnm>
                </au>
                <au>
                   <snm>Thornhill</snm>
                   <fnm>R</fnm>
                </au>
             </aug>
             <source>J Comp Psychol</source>
             <pubdate>1994</pubdate>
             <volume>108</volume>
             <issue>3</issue>
             <fpage>233</fpage>
             <lpage>242</lpage>
             <xrefbib>
                <pubidlist>
                   <pubid idtype="doi">10.1037/0735-7036.108.3.233</pubid>
                   <pubid idtype="pmpid" link="fulltext">7924253</pubid>
                </pubidlist>
             </xrefbib>
          </bibl>
          <bibl id="B6">
             <title>
                <p>Facial analysis and preoperative evaluation</p>
             </title>
             <aug>
                <au>
                   <snm>Calhoun</snm>
                   <fnm>KH</fnm>
                </au>
                <au>
                   <snm>Stambaugh</snm>
                   <fnm>KI</fnm>
                </au>
             </aug>
             <source>Head &amp; Neck Surgery - Otolaryngology</source>
             <publisher>Lippincott Williams &amp; Wilkins, Philadelphia</publisher>
             <editor>Bailey BJ, Johnson JT</editor>
             <edition>4</edition>
             <pubdate>2006</pubdate>
             <fpage>2481</fpage>
             <lpage>2498</lpage>
          </bibl>
          <bibl id="B7">
             <title>
                <p>Prevalence of palpebral fissure asymmetry in white persons</p>
             </title>
             <aug>
                <au>
                   <snm>Lam</snm>
                   <fnm>BL</fnm>
                </au>
                <au>
                   <snm>Lam</snm>
                   <fnm>S</fnm>
                </au>
                <au>
                   <snm>Walls</snm>
                   <fnm>RC</fnm>
                </au>
             </aug>
             <source>Am J Ophthalmol</source>
             <pubdate>1995</pubdate>
             <volume>120</volume>
             <issue>4</issue>
             <fpage>518</fpage>
             <lpage>522</lpage>
             <xrefbib>
                <pubidlist>
                   <pubid idtype="doi">10.1016/S0002-9394(14)72667-4</pubid>
                   <pubid idtype="pmpid" link="fulltext">7573311</pubid>
                </pubidlist>
             </xrefbib>
          </bibl>
          <bibl id="B8">
             <title>
                <p>Asymmetry of the palpebral fissure and upper eyelid crease in Koreans</p>
             </title>
             <aug>
                <au>
                   <snm>Song</snm>
                   <fnm>WC</fnm>
                </au>
                <au>
                   <snm>Kim</snm>
                   <fnm>SJ</fnm>
                </au>
                <au>
                   <snm>Kim</snm>
                   <fnm>SH</fnm>
                </au>
                <au>
                   <snm>Hu</snm>
                   <fnm>KS</fnm>
                </au>
                <au>
                   <snm>Kim</snm>
                   <fnm>HJ</fnm>
                </au>
                <au>
                   <snm>Koh</snm>
                   <fnm>KS</fnm>
                </au>
             </aug>
             <source>J Plast Reconstr Aesthet Surg</source>
             <pubdate>2007</pubdate>
             <volume>60</volume>
             <fpage>251</fpage>
             <lpage>255</lpage>
             <xrefbib>
                <pubidlist>
                   <pubid idtype="doi">10.1016/j.bjps.2006.04.027</pubid>
                   <pubid idtype="pmpid" link="fulltext">17293281</pubid>
                </pubidlist>
             </xrefbib>
          </bibl>
          <bibl id="B9">
             <title>
                <p>Ocular adnexal asymmetry in models: a magazine photograph analysis</p>
             </title>
             <aug>
                <au>
                   <snm>Ing</snm>
                   <fnm>E</fnm>
                </au>
                <au>
                   <snm>Safarpour</snm>
                   <fnm>A</fnm>
                </au>
                <au>
                   <snm>Ing</snm>
                   <fnm>T</fnm>
                </au>
                <au>
                   <snm>Ing</snm>
                   <fnm>S</fnm>
                </au>
             </aug>
             <source>Can J Ophthalmol</source>
             <pubdate>2006</pubdate>
             <volume>41</volume>
             <fpage>175</fpage>
             <lpage>182</lpage>
             <xrefbib>
                <pubidlist>
                   <pubid idtype="doi">10.1139/I06-005</pubid>
                   <pubid idtype="pmpid" link="fulltext">16767204</pubid>
                </pubidlist>
             </xrefbib>
          </bibl>
          <bibl id="B10">
             <title>
                <p>Scar camouflage</p>
             </title>
             <aug>
                <au>
                   <snm>Hochman</snm>
                   <fnm>H</fnm>
                </au>
                <au>
                   <snm>Beas</snm>
                   <fnm>RA</fnm>
                </au>
             </aug>
             <source>Head &amp; Neck Surgery - Otolaryngology</source>
             <publisher>Lippincott Williams &amp; Wilkins, Philadelphia</publisher>
             <editor>Bailey BJ, Johnson JT</editor>
             <edition>4</edition>
             <pubdate>2006</pubdate>
             <fpage>2411</fpage>
             <lpage>2420</lpage>
          </bibl>
       </refgrp>
    </bm>
 </art>