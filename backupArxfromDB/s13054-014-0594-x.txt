<art>
    <ui>s13054-014-0594-x</ui>
    <ji>1364-8535</ji>
    <fm>
       <dochead>Letter</dochead>
       <bibl>
          <title>
             <p>Search for biomarkers in critically ill patients: a new approach based on nuclear magnetic resonance spectroscopy of mini-bronchoalveolar lavage fluid</p>
          </title>
          <aug>
             <au id="A1">
                <snm>Singh</snm>
                <fnm>Chandan</fnm>
                <insr iid="I1"/>
                <insr iid="I2"/>
                <email>singhchandan.bt@gmail.com</email>
             </au>
             <au id="A2">
                <snm>Rai</snm>
                <mnm>Kumar</mnm>
                <fnm>Ratan</fnm>
                <insr iid="I1"/>
                <email>ratancbmr@gmail.com</email>
             </au>
             <au ca="yes" id="A3">
                <snm>Azim</snm>
                <fnm>Afzal</fnm>
                <insr iid="I3"/>
                <email>draazim2002@gmail.com</email>
             </au>
             <au ca="yes" id="A4">
                <snm>Sinha</snm>
                <fnm>Neeraj</fnm>
                <insr iid="I1"/>
                <email>neeraj.sinha@cbmr.res.in</email>
             </au>
             <au id="A5">
                <snm>Baronia</snm>
                <mnm>Kumar</mnm>
                <fnm>Arvind</fnm>
                <insr iid="I3"/>
                <email>baronia@sgpgi.ac.in</email>
             </au>
          </aug>
          <insg>
             <ins id="I1">
                <p>Centre of Biomedical Research, Sanjay Gandhi Postgraduate Institute of Medical Sciences Campus, Raibarely Road, Lucknow 226014, India</p>
             </ins>
             <ins id="I2">
                <p>School of Biotechnology, Faculty of Science, Banaras Hindu University, Varanasi 221005, India</p>
             </ins>
             <ins id="I3">
                <p>Department of Critical Care Medicine, Sanjay Gandhi Postgraduate Institute of Medical Sciences, Lucknow 226014, India</p>
             </ins>
          </insg>
          <source>Critical Care</source>
          <issn>1364-8535</issn>
          <pubdate>2014</pubdate>
          <volume>18</volume>
          <issue>6</issue>
          <fpage>594</fpage>
          <url>http://ccforum.com/content/18/6/594</url>
          <xrefbib>
             <pubidlist>
                <pubid idtype="doi">10.1186/s13054-014-0594-x</pubid>
             <pubid idtype="pmpid">25672613</pubid></pubidlist>
          </xrefbib>
       </bibl>
       <history>
          <pub>
             <date>
                <day>20</day>
                <month>11</month>
                <year>2014</year>
             </date>
          </pub>
       </history>
       <cpyrt>
          <year>2014</year>
          <collab>Singh et al.; licensee BioMed Central Ltd.</collab>
          <note>This is an Open Access article distributed under the terms of the Creative Commons Attribution License (<url>http://creativecommons.org/licenses/by/4.0</url>), which permits unrestricted use, distribution, and reproduction in any medium, provided the original work is properly credited. The Creative Commons Public Domain Dedication waiver (<url>http://creativecommons.org/publicdomain/zero/1.0/</url>) applies to the data made available in this article, unless otherwise stated.</note>
       </cpyrt><abs>
          <sec>
             <st>
                <p>Abstract</p>
             </st>
             <p>No abstract</p>
          </sec>
       </abs>
    </fm>
    <bdy><sec><st><p/></st><p>Human lungs have the function of gas exchange in the body, performed efficiently by the unique anatomy of the alveoli. The alveolar epithelial lining fluid, reflecting a snapshot of molecular events happening there, can be extracted by bronchoscopic/nonbronchoscopic methods and used to study critical diseases. Although there are methods to study the pathophysiological conditions, there is still a need for newer and faster methods that can provide metabolic information about disease diagnosis, severity and progression.</p>
       <p>In this letter we present nuclear magnetic resonance-based metabolomics (a key component of system biology), which has potential for disease diagnosis and treatment monitoring <abbrgrp>
             <abbr bid="B1">1</abbr>
          </abbrgrp>.</p>
       <p>Acute respiratory distress syndrome (ARDS) is a disease with a high rate of mortality and morbidity worldwide, survival being only up to 40%. There is immense need for biomarkers associated with ARDS, and scientists have been working hard for the last four decades to discover these, without the anticipated success. Bronchoalveolar lavage fluid, mini-bronchoalveolar lavage (mBAL) fluid and serum have been the primary body fluids studied for this purpose. Recently, our group explored small molecular weight metabolites responsible for severity of ARDS, employing metabolomics in mBAL fluid and serum <abbrgrp>
             <abbr bid="B2">2</abbr>
          </abbrgrp>,<abbrgrp>
             <abbr bid="B3">3</abbr>
          </abbrgrp>. Both bronchoalveolar lavage fluid and mBAL fluid can also be used for nuclear magnetic resonance-based metabolomics <abbrgrp>
             <abbr bid="B4">4</abbr>
          </abbrgrp>.</p>
       <p>We used a nonbronchoscopic, catheter inside catheter technique to extract mBAL fluid <abbrgrp>
             <abbr bid="B5">5</abbr>
          </abbrgrp>. Most of the metabolites were characterized and identified (Figure&#160;<figr fid="F1">1</figr>). The branch-chain amino acid, lactate, alanine, lysine, arginine, acetate, succinate, taurine, phenylalanine, betaine and aspartate levels were elevated in the mBAL fluid collected from a diseased patient compared with that from a healthy control (Figure&#160;<figr fid="F1">1</figr>). The proline level was found to decrease in the case of ARDS. The roles of the abovementioned small molecular weight metabolites have been discussed previously <abbrgrp>
             <abbr bid="B2">2</abbr>
          </abbrgrp>. One-dimensional nuclear magnetic resonance spectra can be preprocessed and utilized for unsupervised and supervised chemometric analysis, highlighting the role of key metabolites. Jelly should be avoided during extraction of mBAL fluid because resonance from it dominates the spectrum and masks resonance from small molecular weight metabolites, as shown in Figure&#160;<figr fid="F1">1</figr>. We have summarized the complete procedure in Figure&#160;<figr fid="F2">2</figr> and in detail in one of our earlier studies <abbrgrp>
             <abbr bid="B2">2</abbr>
          </abbrgrp>.</p>
       
          <fig id="F1">
             <title>
                <p>Figure 1</p>
             </title>
             <caption>
                <p>Interference due to jelly. (a)</p>
             </caption>
             <text>
                <p>
                   <b>Interference due to jelly. (a)</b> Spectrum showing resonance from jelly. <b>(b)</b> Spectrum of mini-bronchoalveolar lavage (mBAL) fluid dominated by resonance from jelly. <b>(c)</b> Spectrum showing resonance originating from small metabolites present inside mBAL fluid (control). <b>(d)</b> Various small molecular weight metabolites present inside mBAL fluid of an acute respiratory distress syndrome patient. 1, isoleucine, valine and leucine; 2, ethanol; 3, lactate/threonine; 4, alanine; 5, arginine and lysine; 6, acetate; 7, glutamate; 8, succinate; 9, pyruate; 10, glutamine; 11, aspartate; 12, asparagine; 13, creatine and lysine; 14, histidine; 15, betaine; 16, taurine; 17, choline; 18, glycine; 19, lactate; 20, threonine; 21, &#946;-glucose; 22, uracil/urea; 23, fumaric acid; 24 and 26, tyrosine; 25 and 29, histidine; 27, phenylalanine; 28, uracil; 30, adenine; 31, formate.</p>
             </text>
             <graphic file="s13054-014-0594-x-1"/>
          </fig>
       
       
          <fig id="F2">
             <title>
                <p>Figure 2</p>
             </title>
             <caption>
                <p>Complete process of extracting and processing mini-bronchoalveolar lavage fluid and recording its nuclear magnetic resonance spectrum.</p>
             </caption>
             <text>
                <p>
                   <b>Complete process of extracting and processing mini-bronchoalveolar lavage fluid and recording its nuclear magnetic resonance spectrum.</b> 1D, one-dimensional; 2D, two-dimensional; ARDS, acute respiratory distress syndrome; mBAL, mini-bronchoalveolar lavage; NMR, nuclear magnetic resonance; PBS, phosphate-buffered saline.</p>
             </text>
             <graphic file="s13054-014-0594-x-2"/>
          </fig>
       
       <p>With precautions, researchers will find application for this procedure in the study of diseases such as respiratory failure, interstitial lung disease, sarcoidosis and so forth. Besides the above-mentioned studies, this will be of extreme importance for clinicians as well as basic scientists trying to obtain more information about diseases where the balance of bronchoalveolar lavage fluid is affected.</p></sec><sec>
          <st>
             <p>Abbreviations</p>
          </st>
          <p>ARDS: acute respiratory distress syndrome</p>
          <p>mBAL: mini-bronchoalveolar lavage</p>
       </sec>
       <sec>
          <st>
             <p>Competing interests</p>
          </st>
          <p>The authors declare that they have no competing interests.</p>
       </sec>
       <sec>
          <st>
             <p>Authors&#8217; contributions</p>
          </st>
          <p>CS and RKR carried out sample collection, performed nuclear magnetic resonance analysis and wrote the manuscript. NS and AA designed the work. AA and AKB performed extraction of mBAL fluid from the patients. All authors read and approved the final version of the manuscript.</p>
       </sec>
    </bdy>
    <bm>
       <ack>
          <sec>
             <st>
                <p>Acknowledgements</p>
             </st>
             <p>CS and RKR acknowledge financial support as a fellowship from the Council of Scientific and Industrial Research, India. This work was financially supported by the Department of Biotechnology, India (Grant Number BT/PR12700/BRB/10/719/2009) through which important consumables were purchased. The funding agency had no role in the design of study.</p>
          </sec>
       </ack>
       <refgrp>
          
          <bibl id="B1">
             <title>
                <p>Metabolic profiling, metabolomic and metabonomic procedures for NMR spectroscopy of urine, plasma, serum and tissue extracts</p>
             </title>
             <aug>
                <au>
                   <snm>Beckonert</snm>
                   <fnm>O</fnm>
                </au>
                <au>
                   <snm>Keun</snm>
                   <fnm>HC</fnm>
                </au>
                <au>
                   <snm>Ebbels</snm>
                   <fnm>TMD</fnm>
                </au>
                <au>
                   <snm>Bundy</snm>
                   <fnm>J</fnm>
                </au>
                <au>
                   <snm>Holmes</snm>
                   <fnm>E</fnm>
                </au>
                <au>
                   <snm>Lindon</snm>
                   <fnm>JC</fnm>
                </au>
                <au>
                   <snm>Nicholson</snm>
                   <fnm>JK</fnm>
                </au>
             </aug>
             <source>Nat Protoc</source>
             <pubdate>2007</pubdate>
             <volume>2</volume>
             <fpage>2692</fpage>
             <lpage>2703</lpage>
             <xrefbib>
                <pubidlist>
                   <pubid idtype="doi">10.1038/nprot.2007.376</pubid>
                   <pubid idtype="pmpid" link="fulltext">18007604</pubid>
                </pubidlist>
             </xrefbib>
          </bibl>
          <bibl id="B2">
             <title>
                <p>Metabolic profiling in human lung injuries by high-resolution nuclear magnetic resonance spectroscopy of bronchoalveolar lavage fluid (BALF)</p>
             </title>
             <aug>
                <au>
                   <snm>Rai</snm>
                   <fnm>RK</fnm>
                </au>
                <au>
                   <snm>Azim</snm>
                   <fnm>A</fnm>
                </au>
                <au>
                   <snm>Sinha</snm>
                   <fnm>N</fnm>
                </au>
                <au>
                   <snm>Sahoo</snm>
                   <fnm>J</fnm>
                </au>
                <au>
                   <snm>Singh</snm>
                   <fnm>C</fnm>
                </au>
                <au>
                   <snm>Ahmed</snm>
                   <fnm>A</fnm>
                </au>
                <au>
                   <snm>Saigal</snm>
                   <fnm>S</fnm>
                </au>
                <au>
                   <snm>Baronia</snm>
                   <fnm>A</fnm>
                </au>
                <au>
                   <snm>Gupta</snm>
                   <fnm>D</fnm>
                </au>
                <au>
                   <snm>Gurjar</snm>
                   <fnm>M</fnm>
                </au>
                <au>
                   <snm>Poddar</snm>
                   <fnm>B</fnm>
                </au>
                <au>
                   <snm>Singh</snm>
                   <fnm>RK</fnm>
                </au>
             </aug>
             <source>Metabolomics</source>
             <pubdate>2013</pubdate>
             <volume>9</volume>
             <fpage>667</fpage>
             <lpage>676</lpage>
             <xrefbib>
                <pubidlist>
                   <pubid idtype="doi">10.1007/s11306-012-0472-y</pubid>
                </pubidlist>
             </xrefbib>
          </bibl>
          <bibl id="B3">
             <note>Singh C, Rai R, Azim A, Sinha N, Ahmed A, Singh K, Kayastha A, Baronia AK, Gurjar M, Poddar B, Singh R: <b>Metabolic profiling of human lung injury by 1H high-resolution nuclear magnetic resonance spectroscopy of blood serum.</b>
                <it>Metabolomics</it> 2014. [Epub ahead of print].</note>
          </bibl>
          <bibl id="B4">
             <title>
                <p>Mini-bronchoalveolar lavage fluid can be used for biomarker identification in patients with lung injury by employing 1H NMR spectroscopy</p>
             </title>
             <aug>
                <au>
                   <snm>Singh</snm>
                   <fnm>C</fnm>
                </au>
                <au>
                   <snm>Rai</snm>
                   <fnm>R</fnm>
                </au>
                <au>
                   <snm>Azim</snm>
                   <fnm>A</fnm>
                </au>
                <au>
                   <snm>Sinha</snm>
                   <fnm>N</fnm>
                </au>
                <au>
                   <snm>Baronia</snm>
                   <fnm>A</fnm>
                </au>
             </aug>
             <source>Crit Care</source>
             <pubdate>2013</pubdate>
             <volume>17</volume>
             <fpage>430</fpage>
             <xrefbib>
                <pubidlist>
                   <pubid idtype="doi">10.1186/cc12607</pubid>
                   <pubid idtype="pmpid" link="fulltext">23635380</pubid>
                </pubidlist>
             </xrefbib>
          </bibl>
          <bibl id="B5">
             <title>
                <p>Comparison of bronchoscopic and non-bronchoscopic techniques for diagnosis of ventilator associated pneumonia</p>
             </title>
             <aug>
                <au>
                   <snm>Khilnani</snm>
                   <fnm>GC</fnm>
                </au>
                <au>
                   <snm>Arafath</snm>
                   <fnm>KT</fnm>
                </au>
                <au>
                   <snm>Hadda</snm>
                   <fnm>V</fnm>
                </au>
                <au>
                   <snm>Kapil</snm>
                   <fnm>A</fnm>
                </au>
                <au>
                   <snm>Sood</snm>
                   <fnm>S</fnm>
                </au>
                <au>
                   <snm>Sharma</snm>
                   <fnm>SK</fnm>
                </au>
             </aug>
             <source>Indian J Crit Care Med</source>
             <pubdate>2011</pubdate>
             <volume>15</volume>
             <fpage>16</fpage>
             <lpage>23</lpage>
             <xrefbib>
                <pubidlist>
                   <pubid idtype="doi">10.4103/0972-5229.78218</pubid>
                   <pubid idtype="pmpid" link="fulltext">21633541</pubid>
                </pubidlist>
             </xrefbib>
          </bibl>
       </refgrp>
    </bm>
 </art>