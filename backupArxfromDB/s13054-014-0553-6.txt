<art>
    <ui>s13054-014-0553-6</ui>
    <ji>1364-8535</ji>
    <fm>
       <dochead>Letter</dochead>
       <bibl>
          <title>
             <p>A 'spicy' encephalopathy: synthetic cannabinoids as cause of encephalopathy and seizure</p>
          </title>
          <aug>
             <au id="A1">
                <snm>Louh</snm>
                <mi>K</mi>
                <fnm>Irene</fnm>
                <insr iid="I1"/>
                <insr iid="I2"/>
                <email>ikl2104@columbia.edu</email>
             </au>
             <au ca="yes" id="A2">
                <snm>Freeman</snm>
                <mi>D</mi>
                <fnm>William</fnm>
                <insr iid="I3"/>
                <insr iid="I4"/>
                <insr iid="I5"/>
                <email>Freeman.william1@mayo.edu</email>
             </au>
          </aug>
          <insg>
             <ins id="I1">
                <p>Department of Internal Medicine at Mayo Clinic, Jacksonville 32224, FL, USA</p>
             </ins>
             <ins id="I2">
                <p>Division of Pulmonary, Allergy, and Critical Care Medicine, Columbia University, New York 10032, NY, USA</p>
             </ins>
             <ins id="I3">
                <p>Department of Neurology at Mayo Clinic, Jacksonville 32224, FL, USA</p>
             </ins>
             <ins id="I4">
                <p>Department of Critical Care at Mayo Clinic, Jacksonville 32224, FL, USA</p>
             </ins>
             <ins id="I5">
                <p>Department of Neurosurgery at Mayo Clinic, Jacksonville 32224, FL, USA</p>
             </ins>
          </insg>
          <source>Critical Care</source>
          <issn>1364-8535</issn>
          <pubdate>2014</pubdate>
          <volume>18</volume>
          <issue>5</issue>
          <fpage>553</fpage>
          <url>http://ccforum.com/content/18/5/553</url>
          <xrefbib>
             <pubidlist>
                <pubid idtype="doi">10.1186/s13054-014-0553-6</pubid>
                <pubid idtype="pmpid">25673061</pubid>
             </pubidlist>
          </xrefbib>
       </bibl>
       <history>
          <pub>
             <date>
                <day>20</day>
                <month>10</month>
                <year>2014</year>
             </date>
          </pub>
       </history>
       <cpyrt>
          <year>2014</year>
          <collab>Louh and Freeman; licensee BioMed Central Ltd.</collab>
          <note>This is an Open Access article distributed under the terms of the Creative Commons Attribution License (<url>http://creativecommons.org/licenses/by/4.0</url>), which permits unrestricted use, distribution, and reproduction in any medium, provided the original work is properly credited. The Creative Commons Public Domain Dedication waiver (<url>http://creativecommons.org/publicdomain/zero/1.0/</url>) applies to the data made available in this article, unless otherwise stated.</note>
       </cpyrt><abs>
   <sec>
    <st>
     <p>Abstract</p>
    </st>
     <p>No abstract</p>
    </sec>
  </abs></fm>
    <bdy><sec><st><p/></st><p>Synthetic cannabinoids, often sold under labels such as 'spice', are a popular product sold in incense shops and on the internet. When inhaled, consumers often report experiences similar to marijuana use, thus making synthetic cannabinoids a popular street substitute for marijuana. With increasing use, the number of patients presenting to emergency departments due to the toxic effects of these products has increased. While many reported side effects, including anxiety, agitation, tachycardia, and hypertension <abbrgrp>
             <abbr bid="B1">1</abbr>
          </abbrgrp>,<abbrgrp>
             <abbr bid="B2">2</abbr>
          </abbrgrp>, are transient and relatively mild, reports of more severe consequences, including psychosis and seizures, are increasing <abbrgrp>
             <abbr bid="B2">2</abbr>
          </abbrgrp>,<abbrgrp>
             <abbr bid="B3">3</abbr>
          </abbrgrp> with ICU admissions.</p>
       <p>Spice is often sold as 'incense' in tobacco shops and because of such is not under federal regulations for human consumption (Figure&#160;<figr fid="F1">1</figr> shows the disclaimer on the packaging that it is not to be used for human consumption). According to data from the National Poison Data System, 3.8% of calls regarding intoxication from synthetic cannabinoids in 2010 reported seizures <abbrgrp>
             <abbr bid="B3">3</abbr>
          </abbrgrp>, and in 2013, there were 2,613 calls to poison control centers for synthetic cannabinoid exposure <abbrgrp>
             <abbr bid="B4">4</abbr>
          </abbrgrp>. We find that spice intoxication is challenging to diagnose clinically because the features mimic serotonin syndrome. For example, we recently saw a case of 'spice encephalopathy' that presented as a first-onset seizure but had features of serotonin syndrome (myoclonus, dilated pupils). However, serotonin syndrome and spice intoxication have signs and symptoms that may overlap with other toxic drug ingestion states. Further, when reviewing the literature on &#8216;spice', we found eight other discrete cases demonstrating seizure post-'spice' inhalation. In all reported cases, toxicology screens for cannabis are negative, such as ours was. This is, in fact, to be expected with synthetic cannabinoid intoxication and part of the diagnostic dilemma. All reported cases of spice intoxication are similar in that seizure may manifest within a few hours after smoking the synthetic cannabinoid. In addition, some patients require intubation and mechanical ventilation requiring ICU admission, but most recover quickly. Our patient&#8217;s case was admitted to the ICU after a bag of synthetic spice was noticed along with a partially smoked &#8216;spice cigarette&#8217;, which confirmed the diagnosis. As signs/symptoms can be similar to serotonin syndrome, it is important to review carefully the medical history and medications or historians who find such patients since they may not be able to provide a history themselves. A high degree of clinical suspicion of this drug is required, and discovery of the smoking material (Figure&#160;<figr fid="F1">1</figr>) as in our case can greatly aid in the clinical diagnosis until more accurate laboratory methods can confirm this toxic substance in toxicology tests.</p>
       
          <fig id="F1">
             <title>
                <p>Figure 1</p>
             </title>
             <caption>
                <p>Spice packaging.</p>
             </caption>
             <text>
                <p>
                   <b>Spice packaging.</b> Image of 'spice' incense packaging. Note the description of 'fragrant potpourri' and 'not for human consumption' on packaging.</p>
             </text>
             <graphic file="s13054-014-0553-6-1"/>
          </fig>
       
       <p>'Spice' or synthetic cannabinoid-induced toxicity is an emerging etiology of new-onset seizure and does not appear on conventional drug screens. We feel it is important for critical care providers to be aware of this product in order to recognize and appropriately treat this toxicity with supportive management until symptoms resolve. Obtaining additional history about smoking these substances can be helpful in making a clinical diagnosis until more widespread laboratory testing becomes available.</p></sec><sec>
          <st>
             <p>Competing interests</p>
          </st>
          <p>The authors declare that they have no competing interests.</p>
       </sec>
       <sec>
          <st>
             <p>Authors&#8217; contributions</p>
          </st>
          <p>WDF: conception and design, writing of manuscript, critical revision of manuscript, final approval of the manuscript. IKL: analysis and interpretation of data, writing of manuscript, critical revision of manuscript, final approval of the manuscript.</p>
       </sec>
    </bdy>
    <bm>
       <refgrp>
          
          <bibl id="B1">
             <title>
                <p>'Spice' girls: synthetic cannabinoid intoxication</p>
             </title>
             <aug>
                <au>
                   <snm>Schneir</snm>
                   <fnm>AB</fnm>
                </au>
                <au>
                   <snm>Cullen</snm>
                   <fnm>J</fnm>
                </au>
                <au>
                   <snm>Ly</snm>
                   <fnm>BT</fnm>
                </au>
             </aug>
             <source>J Emerg Med</source>
             <pubdate>2011</pubdate>
             <volume>40</volume>
             <fpage>296</fpage>
             <lpage>299</lpage>
             <xrefbib>
                <pubidlist>
                   <pubid idtype="doi">10.1016/j.jemermed.2010.10.014</pubid>
                   <pubid idtype="pmpid" link="fulltext">21167669</pubid>
                </pubidlist>
             </xrefbib>
          </bibl>
          <bibl id="B2">
             <title>
                <p>Spice drugs are more than harmless herbal blends: a review of the pharmacology and toxicology of synthetic cannabinoids</p>
             </title>
             <aug>
                <au>
                   <snm>Seely</snm>
                   <fnm>KA</fnm>
                </au>
                <au>
                   <snm>Lapoint</snm>
                   <fnm>J</fnm>
                </au>
                <au>
                   <snm>Moran</snm>
                   <fnm>JH</fnm>
                </au>
                <au>
                   <snm>Fattore</snm>
                   <fnm>L</fnm>
                </au>
             </aug>
             <source>Prog Neuropsychopharmacol Biol Psychiatry</source>
             <pubdate>2012</pubdate>
             <volume>39</volume>
             <fpage>234</fpage>
             <lpage>243</lpage>
             <xrefbib>
                <pubidlist>
                   <pubid idtype="doi">10.1016/j.pnpbp.2012.04.017</pubid>
                   <pubid idtype="pmpid" link="fulltext">22561602</pubid>
                </pubidlist>
             </xrefbib>
          </bibl>
          <bibl id="B3">
             <title>
                <p>A characterization of synthetic cannabinoid exposures reported to the National Poison Data System in 2010</p>
             </title>
             <aug>
                <au>
                   <snm>Hoyte</snm>
                   <fnm>CO</fnm>
                </au>
                <au>
                   <snm>Jacob</snm>
                   <fnm>J</fnm>
                </au>
                <au>
                   <snm>Monte</snm>
                   <fnm>AA</fnm>
                </au>
                <au>
                   <snm>Al-Jumaan</snm>
                   <fnm>M</fnm>
                </au>
                <au>
                   <snm>Bronstein</snm>
                   <fnm>AC</fnm>
                </au>
                <au>
                   <snm>Heard</snm>
                   <fnm>KJ</fnm>
                </au>
             </aug>
             <source>Ann Emerg Med</source>
             <pubdate>2012</pubdate>
             <volume>60</volume>
             <fpage>435</fpage>
             <lpage>438</lpage>
             <xrefbib>
                <pubidlist>
                   <pubid idtype="doi">10.1016/j.annemergmed.2012.03.007</pubid>
                   <pubid idtype="pmpid" link="fulltext">22575211</pubid>
                </pubidlist>
             </xrefbib>
          </bibl>
          <bibl id="B4">
             
                <url>https://aapcc.s3.amazonaws.com/files/library/Synthetic_Marijuana_Web_Data_through_12.2013.pdf</url>
             
<note>
                <b>AAPCC.</b> []</note>
          </bibl>
       </refgrp>
    </bm>
 </art>