<art>
    <ui>s40199-014-0061-x</ui>
    <ji>2008-2231</ji>
    <fm>
       <dochead>Erratum</dochead>
       <bibl>
          <title>
             <p>Erratum: Biological activity and microscopic characterization of <it>Lythrum salicaria</it> L</p>
          </title>
          <aug>
             <au id="A1">
                <snm>Manayi</snm>
                <fnm>Azadeh</fnm>
                <insr iid="I1"/>
                <email>manayi@razi.tums.ac.ir</email>
             </au>
             <au id="A2">
                <snm>Khanavi</snm>
                <fnm>Mahnaz</fnm>
                <insr iid="I1"/>
                <insr iid="I2"/>
                <email>khanavim@razi.tums.ac.ir</email>
             </au>
             <au id="A3">
                <snm>Saeidnia</snm>
                <fnm>Soodabeh</fnm>
                <insr iid="I3"/>
                <email>saiednia_s@razi.tums.ac.ir</email>
             </au>
             <au id="A4">
                <snm>Azizi</snm>
                <fnm>Ebrahim</fnm>
                <insr iid="I4"/>
                <email>aziziebr@razi.tums.ac.ir</email>
             </au>
             <au id="A5">
                <snm>Mahmoodpour</snm>
                <mnm>Reza</mnm>
                <fnm>Mohammad</fnm>
                <insr iid="I1"/>
                <email>Mahmoodpor@yahoo.com</email>
             </au>
             <au id="A6">
                <snm>Vafi</snm>
                <fnm>Fatemeh</fnm>
                <insr iid="I1"/>
                <email>hrh_1213@yahoo.com</email>
             </au>
             <au id="A7">
                <snm>Malmir</snm>
                <fnm>Maryam</fnm>
                <insr iid="I5"/>
                <email>m.malmir@yahoo.com</email>
             </au>
             <au id="A8">
                <snm>Siavashi</snm>
                <fnm>Farideh</fnm>
                <insr iid="I6"/>
                <email>Siavashi@yahoo.com</email>
             </au>
             <au ca="yes" id="A9">
                <snm>Hadjiakhoondi</snm>
                <fnm>Abbas</fnm>
                <insr iid="I1"/>
                <insr iid="I3"/>
                <email>abbhadji@razi.tums.ac.ir</email>
             </au>
          </aug>
          <insg>
             <ins id="I1">
                <p>Department of Pharmacognosy, Faculty of Pharmacy, Tehran University of Medical Sciences, Tehran, Iran</p>
             </ins>
             <ins id="I2">
                <p>Traditional Iranian Medicine and Pharmacy Research Center, Tehran University of Medical Sciences, Tehran, Iran</p>
             </ins>
             <ins id="I3">
                <p>Medicinal Plants Research Center, Faculty of Pharmacy, Tehran University of Medical Sciences, Tehran, Iran</p>
             </ins>
             <ins id="I4">
                <p>Department of Pharmacology and Toxicology, Faculty of Pharmacy, Tehran University of Medical Sciences, Tehran, Iran</p>
             </ins>
             <ins id="I5">
                <p>Med.UL, Faculty of Pharmacy, University of Lisbon, Lisbon, Portugal</p>
             </ins>
             <ins id="I6">
                <p>Microbiology Department, Faculty of Sciences, University of Tehran, Tehran, Iran</p>
             </ins>
          </insg>
          <source>DARU Journal of Pharmaceutical Sciences</source>
          <issn>2008-2231</issn>
          <pubdate>2014</pubdate>
          <volume>22</volume>
          <issue>1</issue>
          <fpage>61</fpage>
          <url>http://www.darujps.com/content/22/1/61</url>
          <xrefbib>
             <pubidlist>
                <pubid idtype="doi">10.1186/s40199-014-0061-x</pubid>
                <pubid idtype="pmpid">25299300</pubid>
             </pubidlist>
          </xrefbib>
       </bibl>
       <history>
          <rec>
             <date>
                <day>7</day>
                <month>8</month>
                <year>2014</year>
             </date>
          </rec>
          <acc>
             <date>
                <day>8</day>
                <month>8</month>
                <year>2014</year>
             </date>
          </acc>
          <pub>
             <date>
                <day>9</day>
                <month>10</month>
                <year>2014</year>
             </date>
          </pub>
       </history>
       <cpyrt>
          <year>2014</year>
          <collab>Manayi et al.; licensee BioMed Central Ltd.</collab>
          <note>This is an Open Access article distributed under the terms of the Creative Commons Attribution License (<url>http://creativecommons.org/licenses/by/4.0</url>), which permits unrestricted use, distribution, and reproduction in any medium, provided the original work is properly credited. The Creative Commons Public Domain Dedication waiver (<url>http://creativecommons.org/publicdomain/zero/1.0/</url>) applies to the data made available in this article, unless otherwise stated.</note>
       </cpyrt><abs><sec><st>
 <p>Abstract</p>
 </st>
 <p>No abstract.</p></sec></abs>

    </fm>
    <bdy>
       <sec>
          <st>
             <p>1
 						Text</p>
          </st>
          <p>After publication of this work <abbrgrp>
                <abbr bid="B1">1</abbr>
             </abbrgrp> authors&#8217; noted that the name of 3rd author (Soodabeh Saeidnia) was misspelled. The correct spelling is Soodabeh Saeidnia.</p>
       </sec>
    </bdy>
    <bm>
       <refgrp>
          <title>
             <p>Reference</p>
          </title>
          <bibl id="B1">
             <title>
                <p>Biological activity and microscopic characterization of <it>Lythrum salicaria</it> L</p>
             </title>
             <aug>
                <au>
                   <snm>Azadeh</snm>
                   <fnm>M</fnm>
                </au>
                <au>
                   <snm>Mahnaz</snm>
                   <fnm>K</fnm>
                </au>
                <au>
                   <snm>Soodabeh</snm>
                   <fnm>S</fnm>
                </au>
                <au>
                   <snm>Ebrahim</snm>
                   <fnm>A</fnm>
                </au>
                <au>
                   <snm>Mohammad Reza</snm>
                   <fnm>M</fnm>
                </au>
                <au>
                   <snm>Fatemeh</snm>
                   <fnm>V</fnm>
                </au>
                <au>
                   <snm>Maryam</snm>
                   <fnm>M</fnm>
                </au>
                <au>
                   <snm>Farideh</snm>
                   <fnm>S</fnm>
                </au>
                <au>
                   <snm>Abbas</snm>
                   <fnm>H</fnm>
                </au>
             </aug>
             <source>DARU J Pharm Sci</source>
             <pubdate>2013</pubdate>
             <volume>21</volume>
             <fpage>61</fpage>
             <xrefbib>
                <pubidlist>
                   <pubid idtype="doi">10.1186/2008-2231-21-61</pubid>
                </pubidlist>
             </xrefbib>
          </bibl>
       </refgrp>
    </bm>
 </art>