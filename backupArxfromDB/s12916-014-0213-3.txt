<art>
    <ui>s12916-014-0213-3</ui>
    <ji>1741-7015</ji>
    <fm>
       <dochead>Commentary</dochead>
       <bibl>
          <title>
             <p>Medical education and the healthcare system &#8211; why does the curriculum need to be reformed?</p>
          </title>
          <aug>
             <au ca="yes" id="A1">
                <snm>Quintero</snm>
                <mi>A</mi>
                <fnm>Gustavo</fnm>
                <insr iid="I1"/>
                <email>gustavo.quintero@urosario.edu.co</email>
             </au>
          </aug>
          <insg>
             <ins id="I1">
                <p>School of Medicine and Health Sciences, Universidad del Rosario, Carrera 24 # 63C-69, Bogota, Colombia</p>
             </ins>
          </insg>
          <source>BMC Medicine</source>
          <issn>1741-7015</issn>
          <pubdate>2014</pubdate>
          <volume>12</volume>
          <issue>1</issue>
          <fpage>213</fpage>
          <url>http://www.biomedcentral.com/1741-7015/12/213</url>
          <xrefbib>
             <pubidlist>
                <pubid idtype="doi">10.1186/s12916-014-0213-3</pubid>
             <pubid idtype="pmpid">25387484</pubid></pubidlist>
          </xrefbib>
       </bibl>
       <history>
          <rec>
             <date>
                <day>9</day>
                <month>8</month>
                <year>2014</year>
             </date>
          </rec>
          <acc>
             <date>
                <day>14</day>
                <month>10</month>
                <year>2014</year>
             </date>
          </acc>
          <pub>
             <date>
                <day>12</day>
                <month>11</month>
                <year>2014</year>
             </date>
          </pub>
       </history>
       <cpyrt>
          <year>2014</year>
          <collab>Quintero; licensee BioMed Central Ltd.</collab>
          <note>This is an Open Access article distributed under the terms of the Creative Commons Attribution License (<url>http://creativecommons.org/licenses/by/4.0</url>), which permits unrestricted use, distribution, and reproduction in any medium, provided the original work is properly credited. The Creative Commons Public Domain Dedication waiver (<url>http://creativecommons.org/publicdomain/zero/1.0/</url>) applies to the data made available in this article, unless otherwise stated.</note>
       </cpyrt>
       <kwdg>
          <kwd>Health-illness process</kwd>
          <kwd>Health system based curriculum</kwd>
          <kwd>Transformative education</kwd>
       </kwdg>
       <abs>
          <sec>
             <st>
                <p>Abstract</p>
             </st>
             <p>Medical education has been the subject of ongoing debate since the early 1900s. The core of the discussion is about the importance of scientific knowledge on biological understanding at the expense of its social and humanistic characteristics. Unfortunately, reforms to the medical curriculum are still based on a biological vision of the health-illness process. In order to respond to the current needs of society, which is education&#8217;s main objective, the learning processes of physicians and their instruction must change once again. The priority is the concept of the health-illness process that is primarily social and cultural, into which the biological and psychological aspects are inserted. A new curriculum has been developed that addresses a comprehensive instruction of the biological, psychological, social, and cultural (historical) aspects of medicine, with opportunities for students to acquire leadership, teamwork, and communication skills in order to introduce improvements into the healthcare systems where they work.</p>
          </sec>
       </abs>
    </fm>
    <bdy>
       <sec>
          <st>
             <p>Background</p>
          </st>
          <sec>
             <st>
                <p>From the Flexner report to today</p>
             </st>
             <p>Abraham Flexner, in his famous 1910 report <abbrgrp>
                   <abbr bid="B1">1</abbr>
                </abbrgrp>, proposed the model of medical education that prevailed during the first half of the 20<sup>th</sup> century. However, 15 years after his report, Flexner himself recognized that this new medical curriculum gave precedence to the scientific aspects of medicine over its social and humanistic aspects <abbrgrp>
                   <abbr bid="B2">2</abbr>
                </abbrgrp>. Since then, medical education has been the subject of on-going debate. The core of the discussion revolves around the importance of scientific knowledge on biological understanding at the expense of its social and humanistic characteristics. The chronological evolution of medical education models is summarized in Table&#160;<tblr tid="T1">1</tblr>
                <abbrgrp>
                   <abbr bid="B3">3</abbr>
                </abbrgrp>-<abbrgrp>
                   <abbr bid="B8">8</abbr>
                </abbrgrp>. With globalization and the idea to implement strategies to promote global health, a number of medical schools have taken up the challenge of modifying their curricula in order to educate physicians capable of responding to the current and future trends arising from population health maintenance and the consequent practice in that context. These changes aim to ensure integration between basic and biomedical sciences with clinical sciences and to reduce unnecessary knowledge overload through a new study plan for medicine.</p>
             
                <table id="T1">
                   <title>
                      <p>Table 1</p>
                   </title>
                   <caption>
                      <p>
                         <b>Chronological evolution of medical education models</b>
                      </p>
                   </caption>
                   <tgroup align="left" cols="2">
                      <colspec align="left" colname="c1" colnum="1" colwidth="*"/>
                      <colspec align="left" colname="c2" colnum="2" colwidth="*"/>
                      <tbody>
                         <row valign="top">
                            <entry colname="c1">
                               <p>
                                  <b>1910</b>
                               </p>
                            </entry>
                            <entry colname="c2">
                               <p>Abraham Flexner proposed a curriculum with biological model that prevailed during the first half of the 20<sup>th</sup> century.</p>
                            </entry>
                         </row>
                         <row valign="top">
                            <entry colname="c1">
                               <p>
                                  <b>Mid-1950s</b>
                               </p>
                            </entry>
                            <entry colname="c2">
                               <p>Hugh Rodman and E. Gurney Clark published &#8220;Preventive Medicine for the Doctor in His Community&#8221;, which put forward the concept of a natural history of disease, supporting the idea of preventive medicine as an alternative for physicians to understand individual and community health-illness problems.</p>
                            </entry>
                         </row>
                         <row valign="top">
                            <entry colname="c1">
                               <p>
                                  <b>1974</b>
                               </p>
                            </entry>
                            <entry colname="c2">
                               <p>H.L. Blum and Marc Lalonde introduced the model of health fields, where health-illness process depended on four groups of factors (genetics, behaviour, health services and the environment).</p>
                            </entry>
                         </row>
                         <row valign="top">
                            <entry colname="c1">
                               <p>
                                  <b>1978</b>
                               </p>
                            </entry>
                            <entry colname="c2">
                               <p>Alma Ata Conference adopted the global strategy of Health for All where the focus of medicine was health promotion and illness prevention, and medical schools initiated processes to adapt their curriculum to these schemes.</p>
                            </entry>
                         </row>
                         <row valign="top">
                            <entry colname="c1">
                               <p>
                                  <b>1986</b>
                               </p>
                            </entry>
                            <entry colname="c2">
                               <p>The Ottawa Charter, signed at the international conference adopting health promotion as a new approach in healthcare in order to overcome the shortcomings of the previous models.</p>
                            </entry>
                         </row>
                      </tbody>
                   </tgroup>
                </table>
             
             <p>This approach continues to be based on a biological perspective of the health-illness process and the need to comprehensively incorporate into it the socio-humanistic and population health science fields has not been foreseen. In the few instances where this has been performed, incorporation has been limited to the introduction of isolated issues, without an organic connection with the overall curriculum or tying them in as auxiliary elements in public health sciences and risk factors in preventive medicine. Scientific knowledge and skills competences still hold their supremacy over delving into the dimensions of the human being, necessary for the development of socio-humanistic competences.</p>
          </sec>
          <sec>
             <st>
                <p>Health-illness process</p>
             </st>
             <p>Characterization of the health-illness process is a crucial step prior to medical curriculum design. This will determine the understanding of the reality of health and illness of communities and individuals, and the action that should be taken to prevent disease and restore and maintain health. Thus, the new curriculum will provide better professional training in order to address and intervene the specific healthcare needs.</p>
             <p>Curriculum design should then consider health and illness not as states but rather as processes resulting from the interaction of multiple forms of determination that operate simultaneously in the ambit of individuals, collectives, and in society and culture, all of which have a historical character. In fact, society and culture are not causal factors as imagined in positivist epidemiology, but are the broad and general continents where health and disease occur.</p>
             <p>The health-illness process is, in consequence, primarily a social and cultural process where the biological and the psychological is subsumed and is socially and culturally determined <abbrgrp>
                   <abbr bid="B4">4</abbr>
                </abbrgrp>.</p>
          </sec>
          <sec>
             <st>
                <p>Medical education based on the healthcare system</p>
             </st>
             <p>Physicians should be prepared to address complex systems and to lead such systems in an effort to protect the best interests of patients and communities. This reality makes it necessary to modify the way medicine is taught and learned; this is not just a matter of schooling in basic and clinical sciences. It is crucial to introduce socio-humanism and population health sciences (healthcare system) into the teaching of medicine, in an integrated manner, as well as to provide opportunities for students to train in teamwork, communication, and professionalism in order to be able to practice in an uncertain profession such as medicine. An uncertain profession is one where the professional cannot directly control the outcome of his work <abbrgrp>
                   <abbr bid="B9">9</abbr>
                </abbrgrp>.</p>
             <p>One hundred years after the Flexner report, The Carnegie Foundation for the Advancement of Teaching, the same organization that sponsored his study, conducted an investigation on medical education. Based on this study, four goals for modern medical education were recommended (Table&#160;<tblr tid="T2">2</tblr>) <abbrgrp>
                   <abbr bid="B10">10</abbr>
                </abbrgrp> and a new generation of curriculum reform proposed. As much as Flexner introduced medicine to science, the advent of Problem Based Learning (PBL) made a change in the didactic technique; now, the System Based Curriculum, should &#8220;<it>improve performance of the healthcare system in adapting core professional competencies into specific contexts, on the basis of global knowledge</it>&#8221;. In those three generations of reforms, medical education has moved from informative learning that produced expertise, to formative learning that produced professionals, to transformative learning that is &#8220;<it>about developing leadership attributes; its purpose is to produce an enlightened change agent</it>&#8221; <abbrgrp>
                   <abbr bid="B10">10</abbr>
                </abbrgrp>.</p>
             
                <table id="T2">
                   <title>
                      <p>Table 2</p>
                   </title>
                   <caption>
                      <p>
                         <b>Goals for modern medical education</b>
                      </p>
                   </caption>
                   <tgroup align="left" cols="2">
                      <colspec align="left" colname="c1" colnum="1" colwidth="*"/>
                      <colspec align="left" colname="c2" colnum="2" colwidth="*"/>
                      <tbody>
                         <row valign="top">
                            <entry colname="c1">
                               <p>1.</p>
                            </entry>
                            <entry colname="c2">
                               <p>Standardize the learning outcomes and general competencies and provide options for customizing the learning process, providing opportunities for experiences in research, policy making, education, etc., reflecting the broad role played by physicians.</p>
                            </entry>
                         </row>
                         <row valign="top">
                            <entry colname="c1">
                               <p>2.</p>
                            </entry>
                            <entry colname="c2">
                               <p>In practice, physicians must constantly integrate all aspects of their knowledge, skills and values. They should acquire skills to educate, advocate, innovate, investigate and manage teams.</p>
                            </entry>
                         </row>
                         <row valign="top">
                            <entry colname="c1">
                               <p>3.</p>
                            </entry>
                            <entry colname="c2">
                               <p>Medical schools and teaching hospitals should support the engagement of all physicians-in-training in inquiry, discovery and systems innovation.</p>
                            </entry>
                         </row>
                         <row valign="top">
                            <entry colname="c1">
                               <p>4.</p>
                            </entry>
                            <entry colname="c2">
                               <p>Development of professional values, actions, and aspirations should be the backbone of medical education.</p>
                            </entry>
                         </row>
                      </tbody>
                   </tgroup>
                </table>
             
             <p>The study also considered interdependence in education as a key element in a systems approach because it underscores the ways in which various components interact with each other, necessary to provide inter-professional education that promotes collaborative practice.</p>
          </sec>
          <sec>
             <st>
                <p>The Rosario experience: changing the curriculum to enhance the healthcare system</p>
             </st>
             <p>Colombia is a country with 48.3 million people and an upper middle-income level as defined by the World Bank <abbrgrp>
                   <abbr bid="B11">11</abbr>
                </abbrgrp>, with a life expectancy at birth of 74 years. Three years ago, the government issued a law focusing the attention of healthcare to primary care <abbrgrp>
                   <abbr bid="B12">12</abbr>
                </abbrgrp>. In Colombia, an increasing number of medical schools have implemented curriculum reforms to be in tune with the times but, perhaps, most of them have not been addressed to produce change agents which will satisfy the needs of the healthcare system.</p>
             <p>According to Garcia <abbrgrp>
                   <abbr bid="B13">13</abbr>
                </abbrgrp>, &#8220;<it>medical education is the process for training doctors, subordinate to the dominant economic and social structures in societies in which it takes place</it>&#8221;. Therefore, medical education cannot be divorced from social reality. If medical education is a process, it must be understood as a continuum that begins with undergraduate training but does not end there; it is lifelong learning. Such learning must also seek the welfare of the society where it will be implemented, which in a globalized world, is universal <abbrgrp>
                   <abbr bid="B14">14</abbr>
                </abbrgrp>.</p>
             <p>We have long been discussing the major changes required in the healthcare system and the reforms to be undertaken to achieve them. However, if substantial changes to medicine teaching methods are not introduced, these will not be obtained.</p>
             <p>Commencing in 2013, staff at the Rosario University School of Medicine and Health Sciences, Bogot&#225;, Colombia, implemented an undergraduate curriculum reform in medicine which has implicit variations based on the healthcare needs (Figure&#160;<figr fid="F1">1</figr>), with curriculum attributes for undergraduate medical education in relation to the healthcare system.</p>
             
                <fig id="F1">
                   <title>
                      <p>Figure 1</p>
                   </title>
                   <caption>
                      <p>Curriculum attributes facing the healthcare system.</p>
                   </caption>
                   <text>
                      <p>
                         <b>Curriculum attributes facing the healthcare system.</b> Medical education undergraduate curriculum at the Rosario University. The curriculum&#180;s key characteristics are depicted including those allowing opportunities for students to acquire leadership, teamwork and communication skills in order to deal and introduce improvements into the healthcare systems where they will work. (Original source) Abbreviations: ILAS: Integrative learning activities by system, SPICES: student-centred/teacher-centred, problem-based/information-gathering, integrated/discipline-based, community-based/hospital-based, elective/uniform and systematic/apprenticeship-based.</p>
                   </text>
                   <graphic file="s12916-014-0213-3-1"/>
                </fig>
             
             <p>The curriculum integrates basic/biomedical, clinical, socio-humanistic, and population health sciences through a teaching and learning method termed Integrative Learning Activities System-based, a PBL variant, with opportunities for students to acquire leadership, teamwork, and communication and professionalism skills, in order to introduce competences to improve the healthcare system where they will work <abbrgrp>
                   <abbr bid="B15">15</abbr>
                </abbrgrp>. Teaching in an integrative way allows doctors to practice consequently. The curriculum is supported on learning outcomes and its aim is Teaching for Understanding <abbrgrp>
                   <abbr bid="B16">16</abbr>
                </abbrgrp>, a non-memory method for teaching and learning to cultivate learners&#8217; capability to think creatively, formulate and solve problems, and collaborate in generating new knowledge. We adopted the SPICES model proposed by Harden et al. <abbrgrp>
                   <abbr bid="B17">17</abbr>
                </abbrgrp> with learning focus in early expositions, in both hospital and community settings to benefit primary care in its conception of Primary Health Care Renewal striving for integration of all levels of healthcare.</p>
             <p>The curriculum has a 30% flexibility based on elective/selective components with three graduation scenarios: MD, MD with possibilities to pursue an MSc degree, or as MD with options to obtain a second undergraduate title. All this in order to provide opportunities for students to obtain experience in research, policy making, education, primary care, and other areas, reflecting the broad role played by physicians and the Colombian societal needs.</p>
             <p>The medical curriculum, as a part of the School of Medicine and Health Sciences, which has another five programs in health science areas, favors interprofessional education to provide enough skills for collaborative practice and professionalism, which is the main objective of the school, understood as value-centered education. Communication skills are taught under the &#8220;new Trivium&#8221; conception that provides skills on learning how to learn and acquire cognitive-linguistic competences that make communication a way to manage learning <abbrgrp>
                   <abbr bid="B18">18</abbr>
                </abbrgrp>. Communication is also basic for humanism in medicine. The curriculum also introduces a path to research and innovation through basic/biomedical sciences and clinical sciences (translational medicine), the socio-humanistic sciences, and population health sciences. Another route refers to patient safety, which may contribute to decreasing errors and improving quality in medical practice.</p>
             <p>All of the above are supported by educational resources such as Information and Communication Technologies through the &#8220;Mutis electronic platform&#8221; and the Mentor program, a healthcare integrated network that includes two university hospitals and a primary care setting which allow an early exposure to the clinical environment.</p>
             <p>With the new curriculum, we expect to educate doctors who may lead healthcare system changes that positively affect the wellbeing of individuals and the communities where they work. We aim for a true transformative learning experience in a lower middle-income country.</p>
          </sec>
       </sec>
       <sec>
          <st>
             <p>Conclusions</p>
          </st>
          <p>Medical education calls for a profound change in the way it is taught and learned &#8211; a change which provides the welfare society needs, wherever a doctor practices. Where a doctor can be recognized as a leader capable of introducing such a transformation in an unstable world in which previously controlled diseases re-emerge and new ones arise, population aging increases at a rapid pace, systems face health coverage problems and increasing chronic diseases, and health public policy consumes much of our nations&#8217; gross domestic product.</p>
          <p>Medical education must be based on a healthcare system with global thinking and local implementation in an interconnected world. The reform of undergraduate medical curricula should follow this guide in order to contribute to the medical mission. Our experience shows that it is possible to make curriculum changes in medical programs consistent with the current societal needs &#8211; a medical education based on the healthcare system.</p>
       </sec>
       <sec>
          <st>
             <p>Abbreviation</p>
          </st>
          <p>PBL: Problem based learning</p>
       </sec>
       <sec>
          <st>
             <p>Competing interests</p>
          </st>
          <p>The author declares no competing interests.</p>
       </sec>
       <sec>
          <st>
             <p>Author information</p>
          </st>
          <p>GQ is Special Advisor of the Rectory at Rosario University, and Full Professor of the School of Medicine and Health Sciences, with undergraduate qualifications as a doctor in medicine and surgery, postgraduate qualifications in general surgery, transplant and hepatobiliary surgery, medical microbiology, high management in healthcare, and medical education. He is Correspondent Member of the National Academy of Medicine of Colombia and many other national and international scientific societies. He has published many books, chapters in books, and scientific articles in national and international journals in his academic areas of interest.</p>
       </sec>
    </bdy>
    <bm>
       <ack>
          <sec>
             <st>
                <p>Acknowledgements</p>
             </st>
             <p>The author gratefully acknowledges the contribution of Professor Emilio Quevedo as a director of the group for social studies of the sciences, technologies, and professions at Rosario University, for conceptualizing the health-illness process, cornerstone for the philosophy of curricular reform of the medicine program at our University.</p>
          </sec>
       </ack>
       <refgrp>
          
          <bibl id="B1">
             <aug>
                <au>
                   <snm>Flexner</snm>
                   <fnm>A</fnm>
                </au>
             </aug>
             <source>Medical Education in the United States and Canada</source>
             <publisher>The Carnegie Foundation, New York</publisher>
             <pubdate>1910</pubdate>
          </bibl>
          <bibl id="B2">
             <title>
                <p>American medical education 100&#160;years after the Flexner Report</p>
             </title>
             <aug>
                <au>
                   <snm>Cooke</snm>
                   <fnm>M</fnm>
                </au>
                <au>
                   <snm>Irby</snm>
                   <fnm>D</fnm>
                </au>
                <au>
                   <snm>Sullivan</snm>
                   <fnm>W</fnm>
                </au>
                <au>
                   <snm>Ludmere</snm>
                   <fnm>KM</fnm>
                </au>
             </aug>
             <source>N Engl J Med</source>
             <pubdate>2006</pubdate>
             <volume>355</volume>
             <fpage>1339</fpage>
             <lpage>1344</lpage>
             <xrefbib>
                <pubidlist>
                   <pubid idtype="doi">10.1056/NEJMra055445</pubid>
                   <pubid idtype="pmpid" link="fulltext">17005951</pubid>
                </pubidlist>
             </xrefbib>
          </bibl>
          <bibl id="B3">
             <aug>
                <au>
                   <snm>Laevell</snm>
                   <fnm>HR</fnm>
                </au>
                <au>
                   <snm>Clark</snm>
                   <fnm>EG</fnm>
                </au>
             </aug>
             <source>Preventive Medicine for the Doctor and the Community</source>
             <publisher>McGraw-Hill, New York</publisher>
             <pubdate>1953</pubdate>
          </bibl>
          <bibl id="B4">
             <title>
                <p>Comprensi&#243;n hist&#243;rico cr&#237;tica del proceso salud-enfermedad: base para una reforma curricular en medicina</p>
             </title>
             <aug>
                <au>
                   <snm>Quevedo</snm>
                   <fnm>E</fnm>
                </au>
             </aug>
             <source>Educaci&#243;n M&#233;dica. Dise&#241;o e implementaci&#243;n de un curr&#237;culo basado en resultados del aprendizaje</source>
             <publisher>Colecci&#243;n Pedagog&#237;a, Bogot&#225;</publisher>
             <editor>Quintero GA</editor>
             <pubdate>2012</pubdate>
             <fpage>269</fpage>
             <lpage>314</lpage>
          </bibl>
          <bibl id="B5">
             
                <url>http://whqlibdoc.who.int/publications/9243541358.pdf</url>
             
<note>
                <it>Alma Ata 1978. Atenci&#243;n Primaria en Salud.</it> Geneva: World Health Organization; 1978. []</note>
          </bibl>
          <bibl id="B6">
             <note>
                <b>Fourth congress of the IFFLP: symposium proceedings. Ottawa; June 1986.</b>
                <it>Int J Fertil</it> 1988, <b>33:</b>1&#8211;86.</note>
          </bibl>
          <bibl id="B7">
             <aug>
                <au>
                   <snm>Glouberman</snm>
                   <fnm>S</fnm>
                </au>
             </aug>
             <source>Towards a New Perspective on Health Policy</source>
             <publisher>Canadian Policy Research Networks Inc, Ottawa</publisher>
             <pubdate>2001</pubdate>
          </bibl>
          <bibl id="B8">
             <source>Theoretical Concepts, Effective Strategies and Core Competencies. A Foundation Document to Guide Capacity Development of Health Educators</source>
             <publisher>World Health Organization, Regional Office for the Eastern Mediterranean, Cairo</publisher>
             <pubdate>2012</pubdate>
          </bibl>
          <bibl id="B9">
             <aug>
                <au>
                   <snm>Cohen</snm>
                   <fnm>DK</fnm>
                </au>
             </aug>
             <source>Teaching and its predicaments</source>
             <publisher>Harvard University Press, Cambridge</publisher>
             <pubdate>2011</pubdate>
          </bibl>
          <bibl id="B10">
             <title>
                <p>Health professionals for a new century: transforming education to strengthen health systems in an interdependent world</p>
             </title>
             <aug>
                <au>
                   <snm>Frenk</snm>
                   <fnm>J</fnm>
                </au>
                <au>
                   <snm>Chen</snm>
                   <fnm>L</fnm>
                </au>
                <au>
                   <snm>Bhutta</snm>
                   <fnm>ZA</fnm>
                </au>
                <au>
                   <snm>Cohen</snm>
                   <fnm>J</fnm>
                </au>
                <au>
                   <snm>Crisp</snm>
                   <fnm>N</fnm>
                </au>
                <au>
                   <snm>Evans</snm>
                   <fnm>T</fnm>
                </au>
                <au>
                   <snm>Fineberg</snm>
                   <fnm>H</fnm>
                </au>
                <au>
                   <snm>Garcia</snm>
                   <fnm>P</fnm>
                </au>
                <au>
                   <snm>Ke</snm>
                   <fnm>Y</fnm>
                </au>
                <au>
                   <snm>Kelley</snm>
                   <fnm>P</fnm>
                </au>
                <au>
                   <snm>Kistnasamy</snm>
                   <fnm>B</fnm>
                </au>
                <au>
                   <snm>Meleis</snm>
                   <fnm>A</fnm>
                </au>
                <au>
                   <snm>Naylor</snm>
                   <fnm>D</fnm>
                </au>
                <au>
                   <snm>Pablos-Mendez</snm>
                   <fnm>A</fnm>
                </au>
                <au>
                   <snm>Reddy</snm>
                   <fnm>S</fnm>
                </au>
                <au>
                   <snm>Scrimshaw</snm>
                   <fnm>S</fnm>
                </au>
                <au>
                   <snm>Sepulveda</snm>
                   <fnm>J</fnm>
                </au>
                <au>
                   <snm>Serwadda</snm>
                   <fnm>D</fnm>
                </au>
                <au>
                   <snm>Zurayk</snm>
                   <fnm>H</fnm>
                </au>
             </aug>
             <source>Lancet</source>
             <pubdate>2010</pubdate>
             <volume>376</volume>
             <fpage>1923</fpage>
             <lpage>1958</lpage>
             <xrefbib>
                <pubidlist>
                   <pubid idtype="doi">10.1016/S0140-6736(10)61854-5</pubid>
                   <pubid idtype="pmpid" link="fulltext">21112623</pubid>
                </pubidlist>
             </xrefbib>
          </bibl>
          <bibl id="B11">
             
                <url>http://datos.bancomundial.org/pais/colombia</url>
             
<note>The World Bank: <it>Datos Banco Mundial. Colombia.</it> []</note>
          </bibl>
          <bibl id="B12">
             
                <url>http://hermesoft.esap.edu.co/esap/hermesoft/portal/home_1/rec/Normatividad/ley1438_2011.pdf</url>
             
<note>
                <b>Rep&#250;blica de Colombia. Ley 1438 de 2011.</b> []</note>
          </bibl>
          <bibl id="B13">
             
                <url>http://hist.library.paho.org/Spanish/EMS/a244173.pdf</url>
             
<note>Garc&#237;a JC: <b>Caracter&#237;sticas generales de</b> l<b>a educaci&#243;n m&#233;dica en Am&#233;rica Latina.</b> []</note>
          </bibl>
          <bibl id="B14">
             <title>
                <p>Cambio de paradigma en la educaci&#243;n m&#233;dica actual</p>
             </title>
             <aug>
                <au>
                   <snm>Quintero</snm>
                   <fnm>GA</fnm>
                </au>
             </aug>
             <source>Revista V&#237;a Salud</source>
             <pubdate>2013</pubdate>
             <volume>17</volume>
             <fpage>2</fpage>
             <lpage>4</lpage>
          </bibl>
          <bibl id="B15">
             <aug>
                <au>
                   <snm>Quintero</snm>
                   <fnm>GA</fnm>
                </au>
             </aug>
             <source>Educaci&#243;n M&#233;dica. Dise&#241;o e Implementaci&#243;n de un Curr&#237;culo Basado en Resultados del Aprendizaje</source>
             <publisher>Colecci&#243;n Pedagog&#237;a, Editorial Universidad del Rosario, Bogot&#225;</publisher>
             <pubdate>2012</pubdate>
          </bibl>
          <bibl id="B16">
             <title>
                <p>Ense&#241;anza para la comprensi&#243;n en educaci&#243;n m&#233;dica</p>
             </title>
             <aug>
                <au>
                   <snm>Wiske</snm>
                   <fnm>MS</fnm>
                </au>
             </aug>
             <source>Educaci&#243;n M&#233;dica. Dise&#241;o e Implementaci&#243;n de un Curr&#237;culo Basado en Resultados del Aprendizaje</source>
             <publisher>Colecci&#243;n Pedagog&#237;a, Bogot&#225;</publisher>
             <editor>Quintero GA</editor>
             <pubdate>2012</pubdate>
             <fpage>151</fpage>
             <lpage>166</lpage>
          </bibl>
          <bibl id="B17">
             <title>
                <p>Some educational strategies in curriculum development: the SPICES model</p>
             </title>
             <aug>
                <au>
                   <snm>Harden</snm>
                   <fnm>RM</fnm>
                </au>
                <au>
                   <snm>Sowden</snm>
                   <fnm>S</fnm>
                </au>
                <au>
                   <snm>Dunn</snm>
                   <fnm>WR</fnm>
                </au>
             </aug>
             <source>Med Educ</source>
             <pubdate>1984</pubdate>
             <volume>18</volume>
             <fpage>284</fpage>
             <lpage>297</lpage>
             <xrefbib>
                <pubidlist>
                   <pubid idtype="doi">10.1111/j.1365-2923.1984.tb01024.x</pubid>
                   <pubid idtype="pmpid" link="fulltext">6738402</pubid>
                </pubidlist>
             </xrefbib>
          </bibl>
          <bibl id="B18">
             <title>
                <p>El Nuevo trivium</p>
             </title>
             <aug>
                <au>
                   <snm>Alvarez</snm>
                   <fnm>B</fnm>
                </au>
                <au>
                   <snm>Rodr&#237;guez</snm>
                   <fnm>L</fnm>
                </au>
             </aug>
             <source>Educaci&#243;n M&#233;dica. Dise&#241;o e Implementaci&#243;n de un Curr&#237;culo Basado en Resultados del Aprendizaje</source>
             <publisher>Colecci&#243;n Pedagog&#237;a, Bogot&#225;</publisher>
             <editor>Quintero GA</editor>
             <pubdate>2012</pubdate>
             <fpage>167</fpage>
             <lpage>177</lpage>
          </bibl>
       </refgrp>
    </bm>
 </art>