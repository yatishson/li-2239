<art>
    <ui>s13059-014-0528-6</ui>
    <ji>1465-6906</ji>
    <fm>
       <dochead>Editorial</dochead>
       <bibl>
          <title>
             <p>Next-generation pathogen genomics</p>
          </title>
          <aug>
             <au ca="yes" id="A1">
                <snm>Weinstock</snm>
                <mi>M</mi>
                <fnm>George</fnm>
                <insr iid="I1"/>
                <email>george.weinstock@jax.org</email>
             </au>
             <au id="A2">
                <snm>Peacock</snm>
                <mi>J</mi>
                <fnm>Sharon</fnm>
                <insr iid="I2"/>
                <insr iid="I3"/>
                <email>sjp97@medschl.cam.ac.uk</email>
             </au>
          </aug>
          <insg>
             <ins id="I1">
                <p>The Jackson Laboratory for Genomic Medicine, Farmington, Connecticut, USA</p>
             </ins>
             <ins id="I2">
                <p>Department of Medicine, University of Cambridge, Box 157 Addenbrooke&#8217;s Hospital, Hills Road, Cambridge CB2 0QQ, UK</p>
             </ins>
             <ins id="I3">
                <p>The Wellcome Trust Sanger Institute, Wellcome Trust Genome Campus, Hinxton CB10 1SA, Cambridge, UK</p>
             </ins>
          </insg>
          <source>Genome Biology</source>
          <issn>1465-6906</issn>
          <pubdate>2014</pubdate>
          <volume>15</volume>
          <issue>11</issue>
          <fpage>528</fpage>
          <url>http://genomebiology.com/2014/15/11/528</url>
          <xrefbib>
             <pubidlist>
                <pubid idtype="doi">10.1186/s13059-014-0528-6</pubid>
             <pubid idtype="pmpid">25417942</pubid></pubidlist>
          </xrefbib>
       </bibl>
       <history>
          <pub>
             <date>
                <day>19</day>
                <month>11</month>
                <year>2014</year>
             </date>
          </pub>
       </history>
       <cpyrt>
          <year>2014</year>
          <collab>Weinstock and Peacock; licensee BioMed Central Ltd.</collab>
          <note>The licensee has exclusive rights to distribute this article, in any medium, for 12 months following its publication. After this time, the article is available under the terms of the Creative Commons Attribution License (<url>http://creativecommons.org/licenses/by/4.0</url>), which permits unrestricted use, distribution, and reproduction in any medium, provided the original work is properly credited. The Creative Commons Public Domain Dedication waiver (<url>http://creativecommons.org/publicdomain/zero/1.0/</url>) applies to the data made available in this article, unless otherwise stated.</note>
       </cpyrt>
	   <abs>
   <sec>
    <st>
     <p>Abstract</p>
    </st>
     <p>No abstract</p>
    </sec>
  </abs>
    </fm>
    <bdy>
       <sec>
          <st>
             <p>Editorial</p>
          </st>
          <p>In the early 1990s, one of us was involved in one of the first projects to sequence a bacterial genome, the meager 1.1&#160;Mb chromosome of <it>Treponema pallidum</it>, the causative agent of syphilis. Completing the project ultimately took about seven years (until published in 1998 <abbrgrp>
                <abbr bid="B1">1</abbr>
             </abbrgrp>), over US$1.8 million in National Institutes of Health grants (R01AI031068 and R01AI040390) <abbrgrp>
                <abbr bid="B2">2</abbr>
             </abbrgrp>, and required pooling forces with The Institute for Genomic Research. Recently, that original <it>T. pallidum</it> strain was re-sequenced to get a &#8216;perfect&#8217; sequence, a process that took a few days and cost only hundreds of dollars <abbrgrp>
                <abbr bid="B3">3</abbr>
             </abbrgrp>. The original sequencing was performed with the dideoxy-chain termination technique using slab gel electrophoresis instruments. Newly developed software was used for genome assembly and data management and analysis. The latter re-sequencing was performed with next-generation sequencing (NGS) technology and mature software tools. Such is the enormous progress in microbial genome sequencing in the last 20&#160;years.</p>
          <p>The mind-boggling evolution of DNA sequencing and bioinformatics technologies is driving a new era of pathogen research. Recent studies of old, well-scrutinized pathogens are now greatly extended based on the sequencing of thousands of strains from collections <abbrgrp>
                <abbr bid="B4">4</abbr>
             </abbrgrp>,<abbrgrp>
                <abbr bid="B5">5</abbr>
             </abbrgrp>. This increased density of genetic data for individual species allows new insights and definition of mechanisms, just as an aerial photograph gives a clearer picture of the landscape as the pixel density increases. Such large-scale studies, now possible with the increased throughput and lower cost of sequencing, allow a more comprehensive picture of a species&#8217; gene pool (the pan-genome), population genetic and/or evolutionary analyses, and more accurate insights into epidemiology, to name a few advances. In the realm of epidemiology, NGS of pathogens is now pushing into the applied genomics area of the clinic, with, for example, studies of clinical outbreaks that can now precisely define complex transmission chains <abbrgrp>
                <abbr bid="B6">6</abbr>
             </abbrgrp>,<abbrgrp>
                <abbr bid="B7">7</abbr>
             </abbrgrp>. Perilous clinical challenges posed by new antibiotic-resistant organisms benefit from NGS which can identify mutations, thereby defining mechanisms by which resistance is acquired <abbrgrp>
                <abbr bid="B8">8</abbr>
             </abbrgrp>,<abbrgrp>
                <abbr bid="B9">9</abbr>
             </abbrgrp>, as well as discerning new threats from resistance genes found in whole genome sequences <abbrgrp>
                <abbr bid="B10">10</abbr>
             </abbrgrp>.</p>
          <p>It is in this context of a new era in pathogen genomics that this special issue of <it>Genome Biology</it> and <it>Genome Medicine</it> on the Genomics of Infectious Diseases has been assembled. It coincides with an exhilarating time for pathogen genomics research and covers a broad range of bacterial, viral, and parasitic pathogens. Genomic analysis, and sequencing in particular, is agnostic, and applies equally well to the diverse types of pathogens studied in this special issue. Pathogen genomics continues to be an area of some urgency. We need look no further than the current challenges of containing Ebola virus outbreaks or the emergence and expansion of new antibiotic-resistant bacteria, such as carbapenemase-producing <it>Klebsiella pneumoniae</it>, to be reminded that infectious disease is not, and will never be, a solved problem. Rather, only by dramatic technological innovation, such as offered by NGS, can we keep up with the pathogenic population.</p>
          <p>Genome sequencing continues to advance and provide new tools and applications for pathogen research. Sequencing can now be performed on hundreds of strains in parallel in overnight instrument runs, and this drives forward the data density for the description of genomes and gene expression patterns. Metagenomic application of NGS is another bright new area, affording new culture-independent detection of pathogens in clinical samples as well as illuminating interactions between the pathogen and resident microbiome. One looks forward to future applications of this information to combat infection and restore health, possibly with reduced dependence on antibiotics.</p>
       </sec>
       <sec>
          <st>
             <p>Abbreviation</p>
          </st>
          <p>NGS: Next-generation sequencing</p>
       </sec>
       <sec>
          <st>
             <p>Competing interests</p>
          </st>
          <p>The authors declare that they have no competing interests.</p>
       </sec>
    </bdy>
    <bm>
       <refgrp>
          
          <bibl id="B1">
             <title>
                <p>Complete genome sequence of <it>Treponema pallidum</it>, the syphilis spirochete</p>
             </title>
             <aug>
                <au>
                   <snm>Fraser</snm>
                   <fnm>CM</fnm>
                </au>
                <au>
                   <snm>Norris</snm>
                   <fnm>SJ</fnm>
                </au>
                <au>
                   <snm>Weinstock</snm>
                   <fnm>GM</fnm>
                </au>
                <au>
                   <snm>White</snm>
                   <fnm>O</fnm>
                </au>
                <au>
                   <snm>Sutton</snm>
                   <fnm>GG</fnm>
                </au>
                <au>
                   <snm>Dodson</snm>
                   <fnm>R</fnm>
                </au>
                <au>
                   <snm>Gwinn</snm>
                   <fnm>M</fnm>
                </au>
                <au>
                   <snm>Hickey</snm>
                   <fnm>EK</fnm>
                </au>
                <au>
                   <snm>Clayton</snm>
                   <fnm>R</fnm>
                </au>
                <au>
                   <snm>Ketchum</snm>
                   <fnm>KA</fnm>
                </au>
                <au>
                   <snm>Sodergren</snm>
                   <fnm>E</fnm>
                </au>
                <au>
                   <snm>Hardham</snm>
                   <fnm>JM</fnm>
                </au>
                <au>
                   <snm>McLeod</snm>
                   <fnm>MP</fnm>
                </au>
                <au>
                   <snm>Salzberg</snm>
                   <fnm>S</fnm>
                </au>
                <au>
                   <snm>Peterson</snm>
                   <fnm>J</fnm>
                </au>
                <au>
                   <snm>Khalak</snm>
                   <fnm>H</fnm>
                </au>
                <au>
                   <snm>Richardson</snm>
                   <fnm>D</fnm>
                </au>
                <au>
                   <snm>Howell</snm>
                   <fnm>JK</fnm>
                </au>
                <au>
                   <snm>Chidambaram</snm>
                   <fnm>M</fnm>
                </au>
                <au>
                   <snm>Utterback</snm>
                   <fnm>T</fnm>
                </au>
                <au>
                   <snm>McDonald</snm>
                   <fnm>L</fnm>
                </au>
                <au>
                   <snm>Artiach</snm>
                   <fnm>P</fnm>
                </au>
                <au>
                   <snm>Bowman</snm>
                   <fnm>C</fnm>
                </au>
                <au>
                   <snm>Cotton</snm>
                   <fnm>MD</fnm>
                </au>
                <au>
                   <snm>Fujii</snm>
                   <fnm>C</fnm>
                </au>
                <au>
                   <snm>Garland</snm>
                   <fnm>S</fnm>
                </au>
                <au>
                   <snm>Hatch</snm>
                   <fnm>B</fnm>
                </au>
                <au>
                   <snm>Horst</snm>
                   <fnm>K</fnm>
                </au>
                <au>
                   <snm>Roberts</snm>
                   <fnm>K</fnm>
                </au>
                <au>
                   <snm>Sandusky</snm>
                   <fnm>M</fnm>
                </au>
                <etal/>
             </aug>
             <source>Science</source>
             <pubdate>1998</pubdate>
             <volume>281</volume>
             <fpage>375</fpage>
             <lpage>388</lpage>
             <xrefbib>
                <pubidlist>
                   <pubid idtype="doi">10.1126/science.281.5375.375</pubid>
                   <pubid idtype="pmpid" link="fulltext">9665876</pubid>
                </pubidlist>
             </xrefbib>
          </bibl>
          <bibl id="B2">
             
                <url>http://projectreporter.nih.gov/reporter.cfm?def=1</url>
             
<note>
                <b>NIH RePORTER</b> []</note>
          </bibl>
          <bibl id="B3">
             <title>
                <p>Resequencing of Treponema pallidum ssp. pallidum strains Nichols and SS14: correction of sequencing errors resulted in increased separation of syphilis treponeme subclusters</p>
             </title>
             <aug>
                <au>
                   <snm>P&#283;tro&#353;ov&#225;</snm>
                   <fnm>H</fnm>
                </au>
                <au>
                   <snm>Posp&#237;&#353;ilov&#225;</snm>
                   <fnm>P</fnm>
                </au>
                <au>
                   <snm>Strouhal</snm>
                   <fnm>M</fnm>
                </au>
                <au>
                   <snm>&#268;ejkov&#225;</snm>
                   <fnm>D</fnm>
                </au>
                <au>
                   <snm>Zoban&#237;kov&#225;</snm>
                   <fnm>M</fnm>
                </au>
                <au>
                   <snm>Mikalov&#225;</snm>
                   <fnm>L</fnm>
                </au>
                <au>
                   <snm>Sodergren</snm>
                   <fnm>E</fnm>
                </au>
                <au>
                   <snm>Weinstock</snm>
                   <fnm>GM</fnm>
                </au>
                <au>
                   <snm>Smajs</snm>
                   <fnm>D</fnm>
                </au>
             </aug>
             <source>PLoS One</source>
             <pubdate>2013</pubdate>
             <volume>8</volume>
             <fpage>e74319</fpage>
             <xrefbib>
                <pubidlist>
                   <pubid idtype="doi">10.1371/journal.pone.0074319</pubid>
                   <pubid idtype="pmpid" link="fulltext">24058545</pubid>
                </pubidlist>
             </xrefbib>
          </bibl>
          <bibl id="B4">
             <title>
                <p>Comprehensive identification of single nucleotide polymorphisms associated with beta-lactam resistance within pneumococcal mosaic genes</p>
             </title>
             <aug>
                <au>
                   <snm>Chewapreecha</snm>
                   <fnm>C</fnm>
                </au>
                <au>
                   <snm>Marttinen</snm>
                   <fnm>P</fnm>
                </au>
                <au>
                   <snm>Croucher</snm>
                   <fnm>NJ</fnm>
                </au>
                <au>
                   <snm>Salter</snm>
                   <fnm>SJ</fnm>
                </au>
                <au>
                   <snm>Harris</snm>
                   <fnm>SR</fnm>
                </au>
                <au>
                   <snm>Mather</snm>
                   <fnm>AE</fnm>
                </au>
                <au>
                   <snm>Hanage</snm>
                   <fnm>WP</fnm>
                </au>
                <au>
                   <snm>Goldblatt</snm>
                   <fnm>D</fnm>
                </au>
                <au>
                   <snm>Nosten</snm>
                   <fnm>FH</fnm>
                </au>
                <au>
                   <snm>Turner</snm>
                   <fnm>C</fnm>
                </au>
                <au>
                   <snm>Bentley</snm>
                   <fnm>SD</fnm>
                </au>
                <au>
                   <snm>Parkhill</snm>
                   <fnm>J</fnm>
                </au>
             </aug>
             <source>PLoS Genet</source>
             <pubdate>2014</pubdate>
             <volume>10</volume>
             <fpage>e1004547</fpage>
             <xrefbib>
                <pubidlist>
                   <pubid idtype="doi">10.1371/journal.pgen.1004547</pubid>
                   <pubid idtype="pmpid" link="fulltext">25101644</pubid>
                </pubidlist>
             </xrefbib>
          </bibl>
          <bibl id="B5">
             <title>
                <p>Evolutionary pathway to increased virulence and epidemic group A Streptococcus disease derived from 3,615 genome sequences</p>
             </title>
             <aug>
                <au>
                   <snm>Nasser</snm>
                   <fnm>W</fnm>
                </au>
                <au>
                   <snm>Beres</snm>
                   <fnm>SB</fnm>
                </au>
                <au>
                   <snm>Olsen</snm>
                   <fnm>RJ</fnm>
                </au>
                <au>
                   <snm>Dean</snm>
                   <fnm>MA</fnm>
                </au>
                <au>
                   <snm>Rice</snm>
                   <fnm>KA</fnm>
                </au>
                <au>
                   <snm>Long</snm>
                   <fnm>SW</fnm>
                </au>
                <au>
                   <snm>Kristinsson</snm>
                   <fnm>KG</fnm>
                </au>
                <au>
                   <snm>Gottfredsson</snm>
                   <fnm>M</fnm>
                </au>
                <au>
                   <snm>Vuopio</snm>
                   <fnm>J</fnm>
                </au>
                <au>
                   <snm>Raisanen</snm>
                   <fnm>K</fnm>
                </au>
                <au>
                   <snm>Caugant</snm>
                   <fnm>DA</fnm>
                </au>
                <au>
                   <snm>Steinbakk</snm>
                   <fnm>M</fnm>
                </au>
                <au>
                   <snm>Low</snm>
                   <fnm>DE</fnm>
                </au>
                <au>
                   <snm>McGeer</snm>
                   <fnm>A</fnm>
                </au>
                <au>
                   <snm>Darenberg</snm>
                   <fnm>J</fnm>
                </au>
                <au>
                   <snm>Henriques-Normark</snm>
                   <fnm>B</fnm>
                </au>
                <au>
                   <snm>Van Beneden</snm>
                   <fnm>CA</fnm>
                </au>
                <au>
                   <snm>Hoffmann</snm>
                   <fnm>S</fnm>
                </au>
                <au>
                   <snm>Musser</snm>
                   <fnm>JM</fnm>
                </au>
             </aug>
             <source>Proc Natl Acad Sci U S A</source>
             <pubdate>2014</pubdate>
             <volume>111</volume>
             <fpage>E1768</fpage>
             <lpage>1776</lpage>
             <xrefbib>
                <pubidlist>
                   <pubid idtype="doi">10.1073/pnas.1403138111</pubid>
                   <pubid idtype="pmpid" link="fulltext">24733896</pubid>
                </pubidlist>
             </xrefbib>
          </bibl>
          <bibl id="B6">
             <title>
                <p>Tracking a hospital outbreak of carbapenem-resistant <it>Klebsiella pneumoniae</it> with whole-genome sequencing</p>
             </title>
             <aug>
                <au>
                   <snm>Snitkin</snm>
                   <fnm>ES</fnm>
                </au>
                <au>
                   <snm>Zelazny</snm>
                   <fnm>AM</fnm>
                </au>
                <au>
                   <snm>Thomas</snm>
                   <fnm>PJ</fnm>
                </au>
                <au>
                   <snm>Stock</snm>
                   <fnm>F</fnm>
                </au>
                <au>
                   <snm>Henderson</snm>
                   <fnm>DK</fnm>
                </au>
                <au>
                   <snm>Palmore</snm>
                   <fnm>TN</fnm>
                </au>
                <au>
                   <snm>Segre</snm>
                   <fnm>JA</fnm>
                </au>
             </aug>
             <source>Sci Transl Med</source>
             <pubdate>2012</pubdate>
             <volume>4</volume>
             <fpage>148ra116</fpage>
             <xrefbib>
                <pubidlist>
                   <pubid idtype="doi">10.1126/scitranslmed.3004129</pubid>
                   <pubid idtype="pmpid" link="fulltext">22914622</pubid>
                </pubidlist>
             </xrefbib>
          </bibl>
          <bibl id="B7">
             <title>
                <p>Rapid whole-genome sequencing for investigation of a neonatal MRSA outbreak</p>
             </title>
             <aug>
                <au>
                   <snm>Koser</snm>
                   <fnm>CU</fnm>
                </au>
                <au>
                   <snm>Holden</snm>
                   <fnm>MT</fnm>
                </au>
                <au>
                   <snm>Ellington</snm>
                   <fnm>MJ</fnm>
                </au>
                <au>
                   <snm>Cartwright</snm>
                   <fnm>EJ</fnm>
                </au>
                <au>
                   <snm>Brown</snm>
                   <fnm>NM</fnm>
                </au>
                <au>
                   <snm>Ogilvy-Stuart</snm>
                   <fnm>AL</fnm>
                </au>
                <au>
                   <snm>Hsu</snm>
                   <fnm>LY</fnm>
                </au>
                <au>
                   <snm>Chewapreecha</snm>
                   <fnm>C</fnm>
                </au>
                <au>
                   <snm>Croucher</snm>
                   <fnm>NJ</fnm>
                </au>
                <au>
                   <snm>Harris</snm>
                   <fnm>SR</fnm>
                </au>
                <au>
                   <snm>Sanders</snm>
                   <fnm>M</fnm>
                </au>
                <au>
                   <snm>Enright</snm>
                   <fnm>MC</fnm>
                </au>
                <au>
                   <snm>Dougan</snm>
                   <fnm>G</fnm>
                </au>
                <au>
                   <snm>Bentley</snm>
                   <fnm>SD</fnm>
                </au>
                <au>
                   <snm>Parkhill</snm>
                   <fnm>J</fnm>
                </au>
                <au>
                   <snm>Fraser</snm>
                   <fnm>LJ</fnm>
                </au>
                <au>
                   <snm>Betley</snm>
                   <fnm>JR</fnm>
                </au>
                <au>
                   <snm>Schulz-Trieglaff</snm>
                   <fnm>OB</fnm>
                </au>
                <au>
                   <snm>Smith</snm>
                   <fnm>GP</fnm>
                </au>
                <au>
                   <snm>Peacock</snm>
                   <fnm>SJ</fnm>
                </au>
             </aug>
             <source>N Engl J Med</source>
             <pubdate>2012</pubdate>
             <volume>366</volume>
             <fpage>2267</fpage>
             <lpage>2275</lpage>
             <xrefbib>
                <pubidlist>
                   <pubid idtype="doi">10.1056/NEJMoa1109910</pubid>
                   <pubid idtype="pmpid" link="fulltext">22693998</pubid>
                </pubidlist>
             </xrefbib>
          </bibl>
          <bibl id="B8">
             <title>
                <p>Whole-genome analysis of a daptomycin-susceptible <it>Enterococcus faecium</it> strain and its daptomycin-resistant variant arising during therapy</p>
             </title>
             <aug>
                <au>
                   <snm>Tran</snm>
                   <fnm>TT</fnm>
                </au>
                <au>
                   <snm>Panesso</snm>
                   <fnm>D</fnm>
                </au>
                <au>
                   <snm>Gao</snm>
                   <fnm>H</fnm>
                </au>
                <au>
                   <snm>Roh</snm>
                   <fnm>JH</fnm>
                </au>
                <au>
                   <snm>Munita</snm>
                   <fnm>JM</fnm>
                </au>
                <au>
                   <snm>Reyes</snm>
                   <fnm>J</fnm>
                </au>
                <au>
                   <snm>Diaz</snm>
                   <fnm>L</fnm>
                </au>
                <au>
                   <snm>Lobos</snm>
                   <fnm>EA</fnm>
                </au>
                <au>
                   <snm>Shamoo</snm>
                   <fnm>Y</fnm>
                </au>
                <au>
                   <snm>Mishra</snm>
                   <fnm>NN</fnm>
                </au>
                <au>
                   <snm>Bayer</snm>
                   <fnm>AS</fnm>
                </au>
                <au>
                   <snm>Murray</snm>
                   <fnm>BE</fnm>
                </au>
                <au>
                   <snm>Weinstock</snm>
                   <fnm>GM</fnm>
                </au>
                <au>
                   <snm>Arias</snm>
                   <fnm>CA</fnm>
                </au>
             </aug>
             <source>Antimicrob Agents Chemother</source>
             <pubdate>2013</pubdate>
             <volume>57</volume>
             <fpage>261</fpage>
             <lpage>268</lpage>
             <xrefbib>
                <pubidlist>
                   <pubid idtype="doi">10.1128/AAC.01454-12</pubid>
                   <pubid idtype="pmpid" link="fulltext">23114757</pubid>
                </pubidlist>
             </xrefbib>
          </bibl>
          <bibl id="B9">
             <title>
                <p>Daptomycin-resistant <it>Enterococcus faecalis</it> diverts the antibiotic molecule from the division septum and remodels cell membrane phospholipids</p>
             </title>
             <aug>
                <au>
                   <snm>Tran</snm>
                   <fnm>TT</fnm>
                </au>
                <au>
                   <snm>Panesso</snm>
                   <fnm>D</fnm>
                </au>
                <au>
                   <snm>Mishra</snm>
                   <fnm>NN</fnm>
                </au>
                <au>
                   <snm>Mileykovskaya</snm>
                   <fnm>E</fnm>
                </au>
                <au>
                   <snm>Guan</snm>
                   <fnm>Z</fnm>
                </au>
                <au>
                   <snm>Munita</snm>
                   <fnm>JM</fnm>
                </au>
                <au>
                   <snm>Reyes</snm>
                   <fnm>J</fnm>
                </au>
                <au>
                   <snm>Diaz</snm>
                   <fnm>L</fnm>
                </au>
                <au>
                   <snm>Weinstock</snm>
                   <fnm>GM</fnm>
                </au>
                <au>
                   <snm>Murray</snm>
                   <fnm>BE</fnm>
                </au>
                <au>
                   <snm>Shamoo</snm>
                   <fnm>Y</fnm>
                </au>
                <au>
                   <snm>Dowhan</snm>
                   <fnm>W</fnm>
                </au>
                <au>
                   <snm>Bayer</snm>
                   <fnm>AS</fnm>
                </au>
                <au>
                   <snm>Arias</snm>
                   <fnm>CA</fnm>
                </au>
             </aug>
             <source>MBio</source>
             <pubdate>2013</pubdate>
             <volume>4</volume>
             <fpage>e00281</fpage>
             <lpage>13</lpage>
             <xrefbib>
                <pubidlist>
                   <pubid idtype="doi">10.1128/mBio.00281-13</pubid>
                   <pubid idtype="pmpid" link="fulltext">23882013</pubid>
                </pubidlist>
             </xrefbib>
          </bibl>
          <bibl id="B10">
             <title>
                <p>Transferable vancomycin resistance in a community-associated MRSA lineage</p>
             </title>
             <aug>
                <au>
                   <snm>Rossi</snm>
                   <fnm>F</fnm>
                </au>
                <au>
                   <snm>Diaz</snm>
                   <fnm>L</fnm>
                </au>
                <au>
                   <snm>Wollam</snm>
                   <fnm>A</fnm>
                </au>
                <au>
                   <snm>Panesso</snm>
                   <fnm>D</fnm>
                </au>
                <au>
                   <snm>Zhou</snm>
                   <fnm>Y</fnm>
                </au>
                <au>
                   <snm>Rincon</snm>
                   <fnm>S</fnm>
                </au>
                <au>
                   <snm>Narechania</snm>
                   <fnm>A</fnm>
                </au>
                <au>
                   <snm>Xing</snm>
                   <fnm>G</fnm>
                </au>
                <au>
                   <snm>Di Gioia</snm>
                   <fnm>TS</fnm>
                </au>
                <au>
                   <snm>Doi</snm>
                   <fnm>A</fnm>
                </au>
                <au>
                   <snm>Tran</snm>
                   <fnm>TT</fnm>
                </au>
                <au>
                   <snm>Reyes</snm>
                   <fnm>J</fnm>
                </au>
                <au>
                   <snm>Munita</snm>
                   <fnm>JM</fnm>
                </au>
                <au>
                   <snm>Carvajal</snm>
                   <fnm>LP</fnm>
                </au>
                <au>
                   <snm>Hernandez-Roldan</snm>
                   <fnm>A</fnm>
                </au>
                <au>
                   <snm>Brand&#227;o</snm>
                   <fnm>D</fnm>
                </au>
                <au>
                   <snm>van der Heijden</snm>
                   <fnm>IM</fnm>
                </au>
                <au>
                   <snm>Murray</snm>
                   <fnm>BE</fnm>
                </au>
                <au>
                   <snm>Planet</snm>
                   <fnm>PJ</fnm>
                </au>
                <au>
                   <snm>Weinstock</snm>
                   <fnm>GM</fnm>
                </au>
                <au>
                   <snm>Arias</snm>
                   <fnm>CA</fnm>
                </au>
             </aug>
             <source>N Engl J Med</source>
             <pubdate>2014</pubdate>
             <volume>370</volume>
             <fpage>1524</fpage>
             <lpage>1531</lpage>
             <xrefbib>
                <pubidlist>
                   <pubid idtype="doi">10.1056/NEJMoa1303359</pubid>
                   <pubid idtype="pmpid" link="fulltext">24738669</pubid>
                </pubidlist>
             </xrefbib>
          </bibl>
       </refgrp>
    </bm>
 </art>