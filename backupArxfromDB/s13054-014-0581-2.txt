<art>
    <ui>s13054-014-0581-2</ui>
    <ji>1364-8535</ji>
    <fm>
       <dochead>Journal club critique</dochead>
       <bibl>
          <title>
             <p>Can I get a witness?</p>
          </title>
          <aug>
             <au id="A1">
                <snm>Falade</snm>
                <fnm>Olufunmilayo</fnm>
                <insr iid="I1"/>
                <insr iid="I2"/>
                <email>orej@upmc.edu</email>
             </au>
             <au ca="yes" id="A2">
                <snm>Pinsky</snm>
                <mi>R</mi>
                <fnm>Michael</fnm>
                <insr iid="I1"/>
                <insr iid="I2"/>
                <email>pinskymr@upmc.edu</email>
             </au>
          </aug>
          <insg>
             <ins id="I1">
                <p>The Clinical Research, Investigation, and Systems Modeling of Acute Illness (CRISMA) Center, University of Pittsburgh, Pittsburgh 15261, PA, USA</p>
             </ins>
             <ins id="I2">
                <p>Department of Critical Care Medicine, University of Pittsburgh, Pittsburgh 15261, PA, USA</p>
             </ins>
          </insg>
          <source>Critical Care</source>
          <issn>1364-8535</issn>
          <pubdate>2014</pubdate>
          <volume>18</volume>
          <issue>5</issue>
          <fpage>581</fpage>
          <url>http://ccforum.com/content/18/5/581</url>
          <xrefbib>
             <pubidlist>
                <pubid idtype="doi">10.1186/s13054-014-0581-2</pubid>
                <pubid idtype="pmpid">25672228</pubid>
             </pubidlist>
          </xrefbib>
       </bibl>
       <history>
          <pub>
             <date>
                <day>28</day>
                <month>10</month>
                <year>2014</year>
             </date>
          </pub>
       </history>
       <cpyrt>
          <year>2014</year>
          <collab>Falade and Pinsky.; licensee BioMed Central Ltd.</collab>
          <note>The licensee has exclusive rights to distribute this article, in any medium, for 12 months following its publication. After this time, the article is available under the terms of the Creative Commons Attribution License (<url>http://creativecommons.org/licenses/by/4.0</url>), which permits unrestricted use, distribution, and reproduction in any medium, provided the original work is properly credited. The Creative Commons Public Domain Dedication waiver (<url>http://creativecommons.org/publicdomain/zero/1.0/</url>) applies to the data made available in this article, unless otherwise stated.</note>
       </cpyrt><abs>
   <sec>
    <st>
     <p>Abstract</p>
    </st>
     <p>No abstract</p>
    </sec>
  </abs></fm>
    <bdy>
       <sec>
          <st>
             <p>
 						Expanded abstract</p>
          </st>
          <sec>
             <st>
                <p>Citation</p>
             </st>
             <p>Jabre P, Belpomme V, Azoulay E, Jacob L, Bertrand L, Lapostolle F, Tazarourte K, Bouilleau G, Pinaud V, Broche C, Normand D, Baubet T, Ricard-Hibon A, Istria J, Beltramini A, Alheritiere A, Assez N, Nace L, Vivien B, Turi L, Launay S, Desmaizieres M, Borron SW, Vicaut E, Adnet F: Family presence during cardiopulmonary resuscitation. <it>N Engl J Med</it> 2013, 368:1008&#8211;1018.</p>
          </sec>
          <sec>
             <st>
                <p>Background</p>
             </st>
             <p>The effect of family presence during cardiopulmonary resuscitation (CPR) on family members and the medical team remains controversial.</p>
          </sec>
          <sec>
             <st>
                <p>Methods</p>
             </st>
             <p>The authors enrolled 570 relatives of active cardiac arrest patients receiving CPR by 15 pre-hospital emergency medical service units. The units were randomly assigned either to systematically offer the family member the opportunity to observe CPR (intervention group) or to follow standard practice regarding family presence (control group).</p>
             <p>
                <it>Objective:</it> The primary end point was the proportion of relatives with post-traumatic stress disorder (PTSD)-related symptoms at 90&#160;days. Secondary end points included the presence of anxiety and depression symptoms and the effect of family presence on medical efforts at resuscitation, the well being of the healthcare team, and the occurrence of medicolegal claims.</p>
             <p>
                <it>Design:</it> Prospective cluster-randomized controlled trial.</p>
             <p>
                <it>Setting:</it> Emergency medical service units were deployed to areas of the city across all socioeconomic groups in France from November 2009 to October 2011.</p>
             <p>
                <it>Subjects:</it> Adult family members of adult patients in cardiac arrest occurring at home. Only one first-degree relative per patient was evaluated. The relative was chosen in accordance with the legislation on hospitalization at the request of a third party in the following order of preference: spouse, parent, offspring, sibling. Exclusion criteria were communication barriers with the relative and cardiac arrest cases in which resuscitation was not attempted.</p>
             <p>
                <it>Intervention:</it> For emergency medical service units assigned to the intervention, a medical team member systematically asked family members whether they wished to be present during the resuscitation. A communication guide helped introduce the relative to the resuscitation scene and, when required, to help with the announcement of the death.</p>
          </sec>
          <sec>
             <st>
                <p>Results</p>
             </st>
             <p>In the intervention group, 211 of 266 relatives (79%) witnessed CPR, compared with 131 of 304 relatives (43%) in the control group. In the intention-to-treat analysis, the frequency of PTSD-related symptoms was significantly higher in the control group than in the intervention group (adjusted odds ratio, 1.7; 95% confidence interval (CI), 1.2 to 2.5; <it>P</it> =0.004) and among family members who did not witness CPR than among those who did (adjusted odds ratio, 1.6; 95% CI, 1.1 to 2.5; <it>P</it> =0.02). Relatives who did not witness CPR had symptoms of anxiety and depression more frequently than those who did witness CPR. Family-witnessed CPR did not affect resuscitation characteristics, patient survival, the level of emotional stress in the medical team, and did not result in medicolegal claims.</p>
          </sec>
          <sec>
             <st>
                <p>Conclusions</p>
             </st>
             <p>Family presence during CPR was associated with positive results on psychological variables of family members and did not interfere with medical efforts, cause increased stress in the healthcare team or result in medicolegal conflicts.</p>
          </sec>
       </sec>
       <sec>
          <st>
             <p>
 						Commentary</p>
          </st>
          <p>In the United States alone there are 450,000 cases of cardiopulmonary arrest annually. Eighty percent of these events occur at home with about 90% of the population dying and &gt;50% of those that survive left with permanent brain damage <abbrgrp>
                <abbr bid="B1">1</abbr>
             </abbrgrp>. In-hospital arrests have slightly better survival rates <abbrgrp>
                <abbr bid="B2">2</abbr>
             </abbrgrp>. The desire to have family present during cardiopulmonary resuscitation (CPR) originates from 1987 when a family member insisted on being present during CPR <abbrgrp>
                <abbr bid="B3">3</abbr>
             </abbrgrp>. There have since been several surveys designed to determine healthcare professionals&#8217; preferences. Members of the healthcare team expressed reservations listing anxiety, family serving as barriers to provision of effective care and concerns that the family will suffer psychological ill effects. There has been increasing interest in the effect of family presence during CPR on both the family member and healthcare professionals with suggestions that it could be beneficial to the families as a grieving tool. In the pediatric world, family presence during CPR is the norm. However, resuscitation in pediatric patients differs from those in adults as survival rates are higher among children and the healthcare team is often prepared with the skills required to manage both the sick child and the anxious parent. Resuscitation guidelines advocate for the presence of families, including the European Resuscitation Council Guidelines for resuscitation and the American Heart Association guidelines for CPR and emergency cardiovascular care <abbrgrp>
                <abbr bid="B4">4</abbr>
             </abbrgrp>,<abbrgrp>
                <abbr bid="B5">5</abbr>
             </abbrgrp>. Thus, Jabre and colleagues conducted the first randomized trial of its kind to understand the effects family presence during CPR has on both the family and the healthcare team.</p>
          <p>This study was a well-designed prospective cluster randomized controlled trial and took place in France over 2&#160;years. The primary end point was the presence of post-traumatic stress disorder (PTSD) in the relatives that witnessed CPR at 90&#160;days. The secondary end point was the presence of anxiety and depression in the relatives of the decedents, the well-being of the healthcare team and occurrence of medicolegal claims. The intention-to-treat analysis showed that family members in the control group were more likely to exhibit symptoms of PTSD than those in the intervention group with an odds ratio (OR) of 1.7 (confidence interval (CI) 1.2 to 2.5). Family members who did not witness CPR were more likely to exhibit symptoms of PTSD compared with those that did with an OR of 1.6 (CI 1.1 to 2.5). The study also concluded that family presence did not interfere with medical resuscitation efforts, increase stress among members of the medical team, or result in additional medicolegal conflicts.</p>
          <p>This study had several strengths. First, the inclusion and exclusion criteria were simple and could be rapidly applied in the field. Second, the sample size was adequately powered to observe the desired effect. Third, psychological effects of witnessing CPR or not was assessed using well-evaluated monitoring tools. The study had one significant weakness and a few minor ones. Importantly, there was significant crossover between the intervention and the control group with about 43% of the control group opting to witness CPR. They are listed as getting standard of care but it is unclear what standard care entails and more importantly how that differs from the intervention, especially as the controls were given communication guides too. This raised concern as to what the intervention really was: was it the presence of family during CPR or the presence of a communication guide for the families or both? Other limitations include that the observed variables are subject to cultural differences that may not allow direct extrapolation to North American populations. For example, does being a predominantly Catholic population in a largely paternalistic healthcare system inform patients/relatives responses to the healthcare team and end of life decisions? Randomization of the emergency medical service teams <it>ab initio</it> also allowed for the potential to introduce selection bias. Regardless, psychological benefits persisted for family members offered the possibility to witness the CPR of a relative in a follow-up study performed 1&#160;year after the event <abbrgrp>
                <abbr bid="B6">6</abbr>
             </abbrgrp>.</p>
          <p>This study is an important addition to the evidence in support of family member presence during CPR. In tandem with release of the results of this study, the <it>New England Journal of Medicine</it> ran a clinical vignette about a middle aged woman that required CPR and opened voting polls to determine the opinions of their readership. Despite the results of this article, an overwhelming number of respondents (69%) maintained that they do not think family should be allowed to be present when CPR is being performed on their relatives. Interestingly, France had the majority of respondents in support of family presence during CPR. It is unclear if this is a reflection of the opinion of members of the research group or the general population. This unfortunately suggests that healthcare teams will continue to opt for exempting family members from this sentinel event in the lives of their relatives. This approach may stem from the wish to protect the family members from viewing potentially traumatic therapy that often is unsuccessful. However, cultural differences in physician and population preferences across geographical regions limit broad extrapolation of these results.</p>
       </sec>
       <sec>
          <st>
             <p>
 						Recommendation</p>
          </st>
          <p>The overwhelming number of patients that do not survive cardiac arrest or survive with significant neurologic sequelae makes cardiac arrest an end of life event requiring interventions designed to facilitate the grieving process. We recommend that in the appropriate circumstance, family should be offered the opportunity to be present during CPR. Specific protocols should be instituted to manage this event and a &#8216;communication guide&#8217; as used in the trial should be with the family until CPR is terminated.</p>
       </sec>
       <sec>
          <st>
             <p>Abbreviations</p>
          </st>
          <p>CI: Confidence interval</p>
          <p>CPR: Cardiopulmonary resuscitation</p>
          <p>OR: Odds ratio</p>
          <p>PTSD: Post-traumatic stress disorder</p>
       </sec>
       <sec>
          <st>
             <p>Competing interests</p>
          </st>
          <p>The authors declare that they have no competing interests.</p>
       </sec>
    </bdy>
    <bm>
       <refgrp>
          
          <bibl id="B1">
             <title>
                <p>Neurologic prognosis after cardiac arrest</p>
             </title>
             <aug>
                <au>
                   <snm>Young</snm>
                   <fnm>GB</fnm>
                </au>
             </aug>
             <source>N Engl J Med</source>
             <pubdate>2009</pubdate>
             <volume>361</volume>
             <fpage>605</fpage>
             <lpage>611</lpage>
             <xrefbib>
                <pubidlist>
                   <pubid idtype="doi">10.1056/NEJMcp0903466</pubid>
                   <pubid idtype="pmpid" link="fulltext">19657124</pubid>
                </pubidlist>
             </xrefbib>
          </bibl>
          <bibl id="B2">
             <title>
                <p>Risk of post-traumatic stress symptoms in family members of intensive care unit patients</p>
             </title>
             <aug>
                <au>
                   <snm>Azoulay</snm>
                   <fnm>E</fnm>
                </au>
                <au>
                   <snm>Pochard</snm>
                   <fnm>F</fnm>
                </au>
                <au>
                   <snm>Kentish-Barnes</snm>
                   <fnm>N</fnm>
                </au>
                <au>
                   <snm>Chevret</snm>
                   <fnm>S</fnm>
                </au>
                <au>
                   <snm>Aboab</snm>
                   <fnm>J</fnm>
                </au>
                <au>
                   <snm>Adrie</snm>
                   <fnm>C</fnm>
                </au>
                <au>
                   <snm>Annane</snm>
                   <fnm>D</fnm>
                </au>
                <au>
                   <snm>Bleichner</snm>
                   <fnm>G</fnm>
                </au>
                <au>
                   <snm>Bollaert</snm>
                   <fnm>PE</fnm>
                </au>
                <au>
                   <snm>Darmon</snm>
                   <fnm>M</fnm>
                </au>
                <au>
                   <snm>Fassier</snm>
                   <fnm>T</fnm>
                </au>
                <au>
                   <snm>Galliot</snm>
                   <fnm>R</fnm>
                </au>
                <au>
                   <snm>Garrouste-Orgeas</snm>
                   <fnm>M</fnm>
                </au>
                <au>
                   <snm>Goulenok</snm>
                   <fnm>C</fnm>
                </au>
                <au>
                   <snm>Goldgran-Toledano</snm>
                   <fnm>D</fnm>
                </au>
                <au>
                   <snm>Hayon</snm>
                   <fnm>J</fnm>
                </au>
                <au>
                   <snm>Jourdain</snm>
                   <fnm>M</fnm>
                </au>
                <au>
                   <snm>Kaidomar</snm>
                   <fnm>M</fnm>
                </au>
                <au>
                   <snm>Laplace</snm>
                   <fnm>C</fnm>
                </au>
                <au>
                   <snm>Larch&#233;</snm>
                   <fnm>J</fnm>
                </au>
                <au>
                   <snm>Liotier</snm>
                   <fnm>J</fnm>
                </au>
                <au>
                   <snm>Papazian</snm>
                   <fnm>L</fnm>
                </au>
                <au>
                   <snm>Poisson</snm>
                   <fnm>C</fnm>
                </au>
                <au>
                   <snm>Reignier</snm>
                   <fnm>J</fnm>
                </au>
                <au>
                   <snm>Saidi</snm>
                   <fnm>F</fnm>
                </au>
                <au>
                   <snm>Schlemmer</snm>
                   <fnm>B</fnm>
                </au>
             </aug>
             <source>Am J Respir Crit Care Med</source>
             <pubdate>2005</pubdate>
             <volume>171</volume>
             <fpage>987</fpage>
             <lpage>994</lpage>
             <xrefbib>
                <pubidlist>
                   <pubid idtype="doi">10.1164/rccm.200409-1295OC</pubid>
                   <pubid idtype="pmpid" link="fulltext">15665319</pubid>
                </pubidlist>
             </xrefbib>
          </bibl>
          <bibl id="B3">
             <title>
                <p>Family participation during resuscitation: an option</p>
             </title>
             <aug>
                <au>
                   <snm>Doyle</snm>
                   <fnm>CJ</fnm>
                </au>
                <au>
                   <snm>Post</snm>
                   <fnm>H</fnm>
                </au>
                <au>
                   <snm>Burney</snm>
                   <fnm>R</fnm>
                </au>
                <au>
                   <snm>Maino</snm>
                   <fnm>J</fnm>
                </au>
                <au>
                   <snm>Keefe</snm>
                   <fnm>M</fnm>
                </au>
                <au>
                   <snm>Rhee</snm>
                   <fnm>JK</fnm>
                </au>
             </aug>
             <source>Ann Emerg Med</source>
             <pubdate>1987</pubdate>
             <volume>16</volume>
             <fpage>673</fpage>
             <lpage>675</lpage>
             <xrefbib>
                <pubidlist>
                   <pubid idtype="doi">10.1016/S0196-0644(87)80069-0</pubid>
                   <pubid idtype="pmpid" link="fulltext">3578974</pubid>
                </pubidlist>
             </xrefbib>
          </bibl>
          <bibl id="B4">
             <title>
                <p>European Resuscitation Council Guidelines for Resuscitation 2010 Section 10. The ethics of resuscitation and end-of-life decisions</p>
             </title>
             <aug>
                <au>
                   <snm>Lippert</snm>
                   <fnm>FK</fnm>
                </au>
                <au>
                   <snm>Raffay</snm>
                   <fnm>V</fnm>
                </au>
                <au>
                   <snm>Georgiou</snm>
                   <fnm>M</fnm>
                </au>
                <au>
                   <snm>Steen</snm>
                   <fnm>PA</fnm>
                </au>
                <au>
                   <snm>Bossaert</snm>
                   <fnm>L</fnm>
                </au>
             </aug>
             <source>Resuscitation</source>
             <pubdate>2010</pubdate>
             <volume>81</volume>
             <fpage>1445</fpage>
             <lpage>1451</lpage>
             <xrefbib>
                <pubidlist>
                   <pubid idtype="doi">10.1016/j.resuscitation.2010.08.013</pubid>
                   <pubid idtype="pmpid" link="fulltext">20956043</pubid>
                </pubidlist>
             </xrefbib>
          </bibl>
          <bibl id="B5">
             <title>
                <p>Part 3: ethics: 2010 American Heart Association Guidelines for Cardiopulmonary Resuscitation and Emergency Cardiovascular Care</p>
             </title>
             <aug>
                <au>
                   <snm>Morrison</snm>
                   <fnm>LJ</fnm>
                </au>
                <au>
                   <snm>Kierzek</snm>
                   <fnm>G</fnm>
                </au>
                <au>
                   <snm>Diekema</snm>
                   <fnm>DS</fnm>
                </au>
                <au>
                   <snm>Sayre</snm>
                   <fnm>MR</fnm>
                </au>
                <au>
                   <snm>Silvers</snm>
                   <fnm>SM</fnm>
                </au>
                <au>
                   <snm>Idris</snm>
                   <fnm>AH</fnm>
                </au>
                <au>
                   <snm>Mancini</snm>
                   <fnm>ME</fnm>
                </au>
             </aug>
             <source>Circulation</source>
             <pubdate>2010</pubdate>
             <volume>122</volume>
             <fpage>S665</fpage>
             <lpage>S675</lpage>
             <xrefbib>
                <pubidlist>
                   <pubid idtype="doi">10.1161/CIRCULATIONAHA.110.970905</pubid>
                   <pubid idtype="pmpid" link="fulltext">20956219</pubid>
                </pubidlist>
             </xrefbib>
          </bibl>
          <bibl id="B6">
             <title>
                <p>Clinical decisions. Family presence during cardiopulmonary resuscitation - polling results</p>
             </title>
             <aug>
                <au>
                   <snm>Colbert</snm>
                   <fnm>JA</fnm>
                </au>
                <au>
                   <snm>Adler</snm>
                   <fnm>JN</fnm>
                </au>
             </aug>
             <source>N Engl J Med</source>
             <pubdate>2013</pubdate>
             <volume>368</volume>
             <fpage>e38</fpage>
             <xrefbib>
                <pubidlist>
                   <pubid idtype="doi">10.1056/NEJMclde1307088</pubid>
                   <pubid idtype="pmpid" link="fulltext">23802541</pubid>
                </pubidlist>
             </xrefbib>
          </bibl>
       </refgrp>
    </bm>
 </art>