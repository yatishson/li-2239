<art>
    <ui>s40517-014-0011-3</ui>
    <ji>2195-9706</ji>
    <fm>
       <dochead>Debate</dochead>
       <bibl>
          <title>
             <p>Discussion on &#8216;Spectral analysis of aeromagnetic data for geothermal energy investigation of Ikogosi Warm Spring - Ekiti State, southwestern Nigeria&#8217;</p>
          </title>
          <aug>
             <au id="A1" ca="yes">
                <snm>Nwankwo</snm>
                <mnm>Ikechukwu</mnm>
                <fnm>Levi</fnm>
                <insr iid="I1"/>
                <email>levinwankwo@yahoo.com</email>
             </au>
          </aug>
          <insg>
             <ins id="I1">
                <p>Geophysics Research Group, Department of Physics, University of Ilorin, Ilorin 240003, Nigeria</p>
             </ins>
          </insg>
          <source>Geothermal Energy</source>
          <issn>2195-9706</issn>
          <pubdate>2014</pubdate>
          <volume>2</volume>
          <issue>1</issue>
          <fpage>11</fpage>
          <url>http://www.geothermal-energy-journal.com/content/2/1/11</url>
          <xrefbib>
             <pubidlist>
                <pubid idtype="doi">10.1186/s40517-014-0011-3</pubid>
                <pubid idtype="pmpid"><!-- Need to check source for data--></pubid>
             </pubidlist>
          </xrefbib>
       </bibl>
       <history>
          <rec>
             <date>
                <day>9</day>
                <month>7</month>
                <year>2014</year>
             </date>
          </rec>
          <acc>
             <date>
                <day>27</day>
                <month>8</month>
                <year>2014</year>
             </date>
          </acc>
          <pub>
             <date>
                <day>17</day>
                <month>10</month>
                <year>2014</year>
             </date>
          </pub>
       </history>
       <cpyrt>
          <year>2014</year>
          <collab>Nwankwo; licensee Springer.</collab>
          <note>This is an Open Access article distributed under the terms of the Creative Commons Attribution License (<url>http://creativecommons.org/licenses/by/4.0</url>), which permits unrestricted use, distribution, and reproduction in any medium, provided the original work is properly credited.</note>
       </cpyrt>
       <kwdg>
          <kwd>Spectral analysis</kwd>
          <kwd>Aeromagnetic</kwd>
          <kwd>Geothermal</kwd>
          <kwd>Data window</kwd>
          <kwd>Curie-point depth</kwd>
       </kwdg>
       <abs>
          <sec>
             <st>
                <p>Abstract</p>
             </st>
             <sec>
                <st>
                   <p>Background</p>
                </st>
                <p>The recent paper by Abraham et al. (2014) &#8216;Spectral analysis of aeromagnetic data for geothermal energy investigation of Ikogosi Warm Spring - Ekiti State, southwestern Nigeria&#8217; applied spectral analysis in the interpretation of aeromagnetic data for estimation of curie-point depths using a data window of 55 &#215; 55&#160;km; however, the employment of such small window may not be consistent with derived curie-point depth results.</p>
             </sec>
             <sec>
                <st>
                   <p>Discussion</p>
                </st>
                <p>Here, I would like to clarify and point out the possible errors in the paper.</p>
             </sec>
             <sec>
                <st>
                   <p>Summary</p>
                </st>
                <p>It is suggested that the curie-point depth results be re-computed with appropriate window width. This would tremendously assist researchers in appropriate spectral calculation and curie-point depth investigations.</p>
             </sec>
          </sec>
       </abs>
    </fm>
    <bdy>
       <sec>
          <st>
             <p>1
 						Background</p>
          </st>
          <p>Spectral analysis has been widely acknowledged as a robust method for automated depth estimations from magnetic anomaly data (Nabighian et al. <abbrgrp>
                <abbr bid="B4">2005</abbr>
             </abbrgrp>). The approach considers the anomaly to be caused by an ensemble of magnetic sources in order to determine their average depth. Spector and Grant (<abbrgrp>
                <abbr bid="B9">1970</abbr>
             </abbrgrp>) showed that logarithmic radial-power spectra of gridded magnetic data contain constant-slope segments that can be interpreted as arising from statistical ensembles of sources, or equivalent source layers, at different depths. However, the areal extent of the subset of data analyzed (the window) limits the maximum depth being investigated. For a given horizon depth, there is an optimal window size that detects the horizon with the least noise and greatest stability. Window sizes are chosen to focus on different depths of investigation; small windows target shallow depths, as the shallow sources produce high-frequency anomalies, and larger windows target greater depths with deep-seated bodies (Archimedes Consulting <abbrgrp>
                <abbr bid="B2">2013</abbr>
             </abbrgrp>).</p>
          <p>In a study of different spectral methods (including the centroid method adopted by Okubo et al. <abbrgrp>
                <abbr bid="B5">1985</abbr>
             </abbrgrp> and Tanaka et al. <abbrgrp>
                <abbr bid="B10">1999</abbr>
             </abbrgrp>) of estimating the depth to the bottom of magnetic sources (also regarded as a proxy for an estimate of curie-point depth (CPD)), from magnetic anomaly data, Ravat et al. (<abbrgrp>
                <abbr bid="B6">2007</abbr>
             </abbrgrp>) reiterated using data windows with sufficient width to ascertain that the response of the deepest magnetic layer is captured and by verifying the spectra and computing the depth estimates with the largest possible windows. There has been an agreement among researchers in the field that the dimension of the window analyzed may need to be, in some cases, up to ten times the depth to the bottom (Chiozzi et al. <abbrgrp>
                <abbr bid="B3">2005</abbr>
             </abbrgrp>; Ravat et al. <abbrgrp>
                <abbr bid="B6">2007</abbr>
             </abbrgrp>). Therefore, the purpose of this review is to clarify and point out the possible errors in the recent paper authored by Abraham et al. (<abbrgrp>
                <abbr bid="B1">2014</abbr>
             </abbrgrp>) regarding data window. This would tremendously assist researchers in appropriate spectral calculation and CPD investigations.</p>
       </sec>
       <sec>
          <st>
             <p>2
 						Discussion</p>
          </st>
          <p>In the recent paper by Abraham et al. (<abbrgrp>
                <abbr bid="B1">2014</abbr>
             </abbrgrp>) &#8216;Spectral analysis of aeromagnetic data for geothermal energy investigation of Ikogosi Warm Spring - Ekiti State, southwestern Nigeria&#8217;, 22 randomly selected blocks of dimension 55&#8201;&#215;&#8201;55&#160;km (data window) were utilized for CPD evaluation using spectral centroid method. Consequently, they obtained CPD results ranging between 11.48 and 21.91&#160;km. However, bearing in mind the extent of deep-seated magnetic investigations such as CPD, the utilization of such a small window width may be a fundamental error in the application of spectral methods for aeromagnetic interpretation for geothermal explorations. The authors themselves admitted that the spectrum of a magnetic map only contains depth information to a depth of length (<it>L</it>)/2&#960;, and if the source bodies have bases deeper than <it>L</it>/2&#960;, they cannot be resolved by spectral analysis (Shuey et al. <abbrgrp>
                <abbr bid="B8">1977</abbr>
             </abbrgrp>; Salem et al. <abbrgrp>
                <abbr bid="B7">2000</abbr>
             </abbrgrp>).</p>
          <p>Considering a data window of 55 &#215; 55&#160;km used by Abraham et al. (<abbrgrp>
                <abbr bid="B1">2014</abbr>
             </abbrgrp>), only depth information to a depth of 8.75&#160;km could be satisfactorily resolved by spectral analysis. Consequently, the CPD values reported in the paper could likely be erroneous. The 55-km dimension windows will neither be adequate to analyze 11.48- nor 21.91-km base depths as reported by Abraham et al. (<abbrgrp>
                <abbr bid="B1">2014</abbr>
             </abbrgrp>). To obtain a reliable and accurate CPD result that is up to 21.91&#160;km as reported in the paper, a data window not less than 138&#8201;&#215;&#8201;138&#160;km must be utilized. Ravat et al. (<abbrgrp>
                <abbr bid="B6">2007</abbr>
             </abbrgrp>) underscored the importance of computing the spectra and depths with large windows to ensure that the response of the deepest layer is properly captured. Nevertheless, this is not the case in the reviewed publication.</p>
       </sec>
       <sec>
          <st>
             <p>3
 						Summary</p>
          </st>
          <p>Accordingly, the estimated CPD values and ensuing geothermal gradients and heat flows by Abraham et al. (<abbrgrp>
                <abbr bid="B1">2014</abbr>
             </abbrgrp>) could be erroneous. Therefore, it is recommended that the CPD results be re-computed with appropriate window width.</p>
       </sec>
       <sec>
          <st>
             <p>Abbreviation</p>
          </st>
          <p>CPD: curie-point depth</p>
       </sec>
       <sec>
          <st>
             <p>Competing interests</p>
          </st>
          <p>The author declares that he has no competing interests.</p>
       </sec>
       <sec>
          <st>
             <p>Author's contributions</p>
          </st>
          <p>LIN made all the contributions in the work.</p>
       </sec>
    </bdy>
    <bm>
       <ack>
          <sec>
             <st>
                <p>Acknowledgements</p>
             </st>
             <p>The critical and constructive comments of all the anonymous reviewers and editors are highly acknowledged.</p>
          </sec>
       </ack>
       <refgrp>
          
          <bibl id="B1">
             <title>
                <p>Spectral analysis of aeromagnetic data for geothermal energy investigation of Ikogosi Warm Spring - Ekiti State, southwestern Nigeria</p>
             </title>
             <aug>
                <au>
                   <snm>Abraham</snm>
                   <fnm>EM</fnm>
                </au>
                <au>
                   <snm>Lawal</snm>
                   <fnm>KM</fnm>
                </au>
                <au>
                   <snm>Ekwe</snm>
                   <fnm>AC</fnm>
                </au>
                <au>
                   <snm>Alile</snm>
                   <fnm>O</fnm>
                </au>
                <au>
                   <snm>Murana</snm>
                   <fnm>KA</fnm>
                </au>
                <au>
                   <snm>Lawal</snm>
                   <fnm>AA</fnm>
                </au>
             </aug>
             <source>Geothermal Energy</source>
             <pubdate>2014</pubdate>
             <volume>2</volume>
             <fpage>6</fpage>
             <xrefbib>
                <pubidlist>
                   <pubid idtype="doi">10.1186/s40517-014-0006-0</pubid>
                </pubidlist>
             </xrefbib>
          </bibl>
          <bibl id="B2">
             
                <url>http://www.geoexpro.com/articles/2013/02/mapping-sub-salt-and-sub-basalt-structures-from-magnetic-and-gravity-data</url>
             
<note>Archimedes Consulting (2013) Mapping sub-salt and sub-basalt structures from magnetic and gravity data. . Accessed 2 July 2014</note>
          </bibl>
          <bibl id="B3">
             <title>
                <p>Curie-point depth from spectral analysis of magnetic data in central southern Europe</p>
             </title>
             <aug>
                <au>
                   <snm>Chiozzi</snm>
                   <fnm>P</fnm>
                </au>
                <au>
                   <snm>Matsushima</snm>
                   <fnm>J</fnm>
                </au>
                <au>
                   <snm>Okubo</snm>
                   <fnm>Y</fnm>
                </au>
                <au>
                   <snm>Pasquale</snm>
                   <fnm>V</fnm>
                </au>
                <au>
                   <snm>Verdoya</snm>
                   <fnm>M</fnm>
                </au>
             </aug>
             <source>Phys Earth Planet Int</source>
             <pubdate>2005</pubdate>
             <volume>152</volume>
             <fpage>267</fpage>
             <lpage>276</lpage>
             <xrefbib>
                <pubidlist>
                   <pubid idtype="doi">10.1016/j.pepi.2005.04.005</pubid>
                </pubidlist>
             </xrefbib>
          </bibl>
          <bibl id="B4">
             <title>
                <p>The historical development of the magnetic method in exploration</p>
             </title>
             <aug>
                <au>
                   <snm>Nabighian</snm>
                   <fnm>MN</fnm>
                </au>
                <au>
                   <snm>Grauch</snm>
                   <fnm>VJS</fnm>
                </au>
                <au>
                   <snm>Hansen</snm>
                   <fnm>RO</fnm>
                </au>
                <au>
                   <snm>LaFehr</snm>
                   <fnm>TR</fnm>
                </au>
                <au>
                   <snm>Li</snm>
                   <fnm>Y</fnm>
                </au>
                <au>
                   <snm>Peirce</snm>
                   <fnm>JW</fnm>
                </au>
                <au>
                   <snm>Phillips</snm>
                   <fnm>JD</fnm>
                </au>
                <au>
                   <snm>Ruder</snm>
                   <fnm>ME</fnm>
                </au>
             </aug>
             <source>Geophysics</source>
             <pubdate>2005</pubdate>
             <volume>70</volume>
             <issue>6</issue>
             <fpage>33</fpage>
             <lpage>61</lpage>
          </bibl>
          <bibl id="B5">
             <title>
                <p>Curie point depths of the island of Kyushu and surrounding areas, Japan</p>
             </title>
             <aug>
                <au>
                   <snm>Okubo</snm>
                   <fnm>Y</fnm>
                </au>
                <au>
                   <snm>Graf</snm>
                   <fnm>RJ</fnm>
                </au>
                <au>
                   <snm>Hansent</snm>
                   <fnm>RO</fnm>
                </au>
                <au>
                   <snm>Ogawa</snm>
                   <fnm>K</fnm>
                </au>
                <au>
                   <snm>Tsu</snm>
                   <fnm>H</fnm>
                </au>
             </aug>
             <source>Geophysics</source>
             <pubdate>1985</pubdate>
             <volume>53</volume>
             <fpage>481</fpage>
             <lpage>494</lpage>
             <xrefbib>
                <pubidlist>
                   <pubid idtype="doi">10.1190/1.1441926</pubid>
                </pubidlist>
             </xrefbib>
          </bibl>
          <bibl id="B6">
             <title>
                <p>A study of spectral methods of estimating the depth to the bottom of magnetic sources from near-surface magnetic anomaly data</p>
             </title>
             <aug>
                <au>
                   <snm>Ravat</snm>
                   <fnm>D</fnm>
                </au>
                <au>
                   <snm>Pignatelli</snm>
                   <fnm>A</fnm>
                </au>
                <au>
                   <snm>Nicolosi</snm>
                   <fnm>I</fnm>
                </au>
                <au>
                   <snm>Chiappini</snm>
                   <fnm>M</fnm>
                </au>
             </aug>
             <source>Geophys J Int</source>
             <pubdate>2007</pubdate>
             <volume>169</volume>
             <fpage>421</fpage>
             <lpage>434</lpage>
             <xrefbib>
                <pubidlist>
                   <pubid idtype="doi">10.1111/j.1365-246X.2007.03305.x</pubid>
                </pubidlist>
             </xrefbib>
          </bibl>
          <bibl id="B7">
             <note>Salem A, Ushijima K, Elsiraft A, Mizunaga H (2000) Spectral analysis of aeromagnetic data for geothermal reconnaissance of Quseir area, northern Red Sea, Egypt. In: Proceedings of the world geothermal congress, Kyushu-Tohoku, 28 May&#8211;10 June 2000</note>
          </bibl>
          <bibl id="B8">
             <title>
                <p>Curie depth determination from aeromagnetic spectra</p>
             </title>
             <aug>
                <au>
                   <snm>Shuey</snm>
                   <fnm>RT</fnm>
                </au>
                <au>
                   <snm>Schellinger</snm>
                   <fnm>DK</fnm>
                </au>
                <au>
                   <snm>Tripp</snm>
                   <fnm>AC</fnm>
                </au>
                <au>
                   <snm>Alley</snm>
                   <fnm>LB</fnm>
                </au>
             </aug>
             <source>Geophys J Roy Astr Soc</source>
             <pubdate>1977</pubdate>
             <volume>50</volume>
             <fpage>75</fpage>
             <lpage>101</lpage>
             <xrefbib>
                <pubidlist>
                   <pubid idtype="doi">10.1111/j.1365-246X.1977.tb01325.x</pubid>
                </pubidlist>
             </xrefbib>
          </bibl>
          <bibl id="B9">
             <title>
                <p>Statistical models for interpreting aeromagnetic data</p>
             </title>
             <aug>
                <au>
                   <snm>Spector</snm>
                   <fnm>A</fnm>
                </au>
                <au>
                   <snm>Grant</snm>
                   <fnm>FS</fnm>
                </au>
             </aug>
             <source>Geophysics</source>
             <pubdate>1970</pubdate>
             <volume>35</volume>
             <fpage>293</fpage>
             <lpage>302</lpage>
             <xrefbib>
                <pubidlist>
                   <pubid idtype="doi">10.1190/1.1440092</pubid>
                </pubidlist>
             </xrefbib>
          </bibl>
          <bibl id="B10">
             <title>
                <p>Curie point depth based on spectrum analysis of the magnetic anomaly data in East and Southeast Asia</p>
             </title>
             <aug>
                <au>
                   <snm>Tanaka</snm>
                   <fnm>A</fnm>
                </au>
                <au>
                   <snm>Okubo</snm>
                   <fnm>Y</fnm>
                </au>
                <au>
                   <snm>Matsubayashi</snm>
                   <fnm>O</fnm>
                </au>
             </aug>
             <source>Tectonophysics</source>
             <pubdate>1999</pubdate>
             <volume>306</volume>
             <fpage>461</fpage>
             <lpage>470</lpage>
             <xrefbib>
                <pubidlist>
                   <pubid idtype="doi">10.1016/S0040-1951(99)00072-4</pubid>
                </pubidlist>
             </xrefbib>
          </bibl>
       </refgrp>
    </bm>
 </art>
