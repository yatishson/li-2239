<art>
    <ui>s13054-014-0509-x</ui>
    <ji>1364-8535</ji>
    <fm>
       <dochead>Editorial</dochead>
       <bibl>
          <title>
             <p>A word of caution regarding proposed benefits of albumin from ALBIOS: a dose of healthy skepticism</p>
          </title>
          <aug>
             <au ca="yes" id="A1"><snm>Flannery</snm><mi>H</mi><fnm>Alexander</fnm><insr iid="I1"/><insr iid="I2"/><email>alex.flannery@uky.edu</email></au>
             <au id="A2"><snm>Kane</snm><mi>P</mi><fnm>Sean</fnm><insr iid="I3"/><email>sean.kane@rosalindfranklin.edu</email></au>
             <au id="A3"><snm>Coz-Yataco</snm><mi>O</mi><fnm>Angel</fnm><insr iid="I4"/><email>angel.coz@uky.edu</email></au>
          </aug>
          <insg>
             <ins id="I1"><p>Department of Pharmacy Services, University of Kentucky HealthCare, 800 Rose Street, H110, Lexington 40536, KY, USA</p></ins>
             <ins id="I2"><p>Department of Pharmacy Practice and Science, University of Kentucky College of Pharmacy, 800 Rose Street, H110, Lexington 40536, KY, USA</p></ins>
             <ins id="I3"><p>Department of Pharmacy Practice, Rosalind Franklin University of Medicine and Science, College of Pharmacy, Building: IPEC, Room: 2.803, 3333 Green Bay Road, North Chicago 60064-3095, IL, USA</p></ins>
             <ins id="I4"><p>Division of Pulmonary, Critical Care, and Sleep Medicine, University of Kentucky HealthCare, Kentucky Clinic, L543, 740S. Limestone, Lexington 40536, KY, USA</p></ins>
          </insg>
          <source>Critical Care</source>
          <issn>1364-8535</issn>
          <pubdate>2014</pubdate>
          <volume>18</volume>
          <issue>5</issue>
          <fpage>509</fpage>
          <url>http://ccforum.com/content/18/5/509</url>
		  <note>See related letter by Caironi and Gattinoni,<url>http://ccforum.com/content/18/5/510</url></note>
          <xrefbib><pubidlist><pubid idtype="doi">10.1186/s13054-014-0509-x</pubid><pubid idtype="pmpid">25599371</pubid></pubidlist></xrefbib>
       </bibl>
       <history><pub><date><day>23</day><month>9</month><year>2014</year></date></pub></history>
       <cpyrt><year>2014</year><collab>Flannery et al.; licensee BioMed Central Ltd.</collab><note>This is an Open Access article distributed under the terms of the Creative Commons Attribution License (<url>http://creativecommons.org/licenses/by/4.0</url>), which permits unrestricted use, distribution, and reproduction in any medium, provided the original work is properly credited. The Creative Commons Public Domain Dedication waiver (<url>http://creativecommons.org/publicdomain/zero/1.0/</url>) applies to the data made available in this article, unless otherwise stated.</note></cpyrt>
	   <abs><sec><st>
 <p>Abstract</p>
 </st>
 <p>No abstract.</p></sec></abs>
    </fm>
    <bdy>
       <sec>
          <st>
             <p>Introduction</p>
          </st>
          <p>The recently published Albumin Italian Outcome Sepsis (ALBIOS) study was a prospective, open-label, multicenter, controlled trial of 1,818 patients with severe sepsis or septic shock <abbrgrp>
                <abbr bid="B1">1</abbr>
             </abbrgrp>. Patients were randomized to receive either albumin 20% daily to maintain a serum albumin level &#8805;3&#160;g/dl or no albumin replacement. No differences were detected in overall 28-day mortality, 90-day mortality, or a number of other secondary outcomes. However, three potential benefits of albumin were proposed that deserve further scrutiny.</p>
       </sec>
       <sec>
          <st>
             <p>Caution in interpreting the subgroup analysis</p>
          </st>
          <p>A <it>post hoc</it>, exploratory analysis of patients with septic shock suggested a reduced relative risk of mortality at 90&#160;days in the albumin group (relative risk 0.87; 95% confidence interval 0.77 to 0.99). While preserved when adjusting for imbalances in baseline characteristics, statistical significance was lost when adjusted for clinically relevant variables (relative risk 0.88; 95% confidence interval 0.77 to 1.01). More importantly, one must consider why a subgroup analysis of 90-day mortality, a secondary outcome, was performed rather than a subgroup analysis of the primary outcome, 28-day mortality. After all, 28-day mortality was the outcome reported in the hypothesis-generating Saline versus Albumin Fluid Evaluation study subgroup analysis of patients with severe sepsis <abbrgrp>
                <abbr bid="B2">2</abbr>
             </abbrgrp>. The 28-day mortality comparison in the subgroup of patients with septic shock was presented by the ALBIOS study group at professional meetings, although not in the manuscript or appendix, and indeed is not significant (relative risk 0.95; 95% confidence interval 0.81 to 1.10) <abbrgrp>
                <abbr bid="B3">3</abbr>
             </abbrgrp>. It is clinically difficult to explain a mortality benefit with albumin that only emerges beyond 28&#160;days. Given the loss of statistical significance upon further regression analysis and the benefit seen only after 28&#160;days, the small difference in 90-day mortality that initially emerged in the subgroup analysis of patients with septic shock is very likely due to chance.</p>
       </sec>
       <sec>
          <st>
             <p>Caution in interpreting effect sizes</p>
          </st>
          <p>The ALBIOS trial suggested that albumin was associated with &#8216;small but significant hemodynamic advantages&#8217;, specifically a lower heart rate and higher mean arterial pressure <abbrgrp>
                <abbr bid="B1">1</abbr>
             </abbrgrp>. In this analysis, statistical significance fails to represent clinical significance. The statistically significant differences in heart rate and mean arterial pressure ranged from 2 to 5 beats/minute and from 1 to 2&#160;mm Hg, respectively. The lack of clinical significance is supported by the fact that meaningful differences failed to emerge at any time point in the central venous oxygen saturation and lactate values between the two groups. Similar claims of superiority are made for albumin with regard to net fluid balance. However, these differences were only statistically significant and marginally clinically significant on days 2 to 4, with the difference only favoring albumin by &#8804;300&#160;ml/day.</p>
       </sec>
       <sec>
          <st>
             <p>Caution in evaluating open-label endpoints at risk for bias</p>
          </st>
          <p>In the subgroup of patients with septic shock receiving vasopressors and/or inotropes at enrollment, a tertiary analysis suggested that use of albumin was associated with fewer days of vasopressor or inotrope support (median 3&#160;days (interquartile range 1 to 6) vs. 4&#160;days (interquartile range 2 to 7), <it>P</it>&#8201;=&#8201;0.007). Of note, the ALBIOS trial was an open-label study, which may have biased the vasopressor titrations. Additionally, analyzing hours as opposed to days of vasopressor duration may have clarified the true effect size. Nevertheless, vasopressor agents are not benign medications and a reduction in their use could be of benefit.</p>
       </sec>
       <sec>
          <st>
             <p>Implications of the ALBIOS trial</p>
          </st>
          <p>Aside from the benefit of potentially reducing vasopressor duration, the ALBIOS trial was largely a negative study for albumin in terms of improving outcomes compared with fluid alone. However, one must critically evaluate the data and published analysis or risk misinterpreting the study as favorable for albumin based on interpretations of the subgroup analysis and effect sizes. A multicenter French study investigating albumin in septic shock is expected to shed additional light on the relationship between albumin and vasopressor use <abbrgrp>
                <abbr bid="B4">4</abbr>
             </abbrgrp>.</p>
          <p>Considering all available data, albumin should be considered a safe alternative to crystalloids. The cost-effectiveness of its use, however, remains a valid concern. Volume for volume, albumin is roughly 30 times more expensive than crystalloids <abbrgrp>
                <abbr bid="B5">5</abbr>
             </abbrgrp>. Without any demonstrated superiority in clinical outcomes, it is difficult to justify extensive and unrestricted use of albumin at this time for resuscitation of patients with severe sepsis or septic shock.</p>
       </sec>
       <sec>
          <st>
             <p>Abbreviation</p>
          </st>
          <p>ALBIOS: Albumin Italian Outcome Sepsis</p>
       </sec>
       <sec>
          <st>
             <p>Competing interests</p>
          </st>
          <p>The authors declare that they have no competing interests.</p>
       </sec>
       <sec>
          <st>
             <p>Authors&#8217; contributions</p>
          </st>
          <p>AHF conceived the idea and contributed to the writing of the manuscript. SPK contributed to the writing of the manuscript with substantial editing. AOC-Y contributed to conceiving the idea and writing of the manuscript.</p>
       </sec>
    </bdy>
    <bm>
       <refgrp><bibl id="B1"><title><p>Albumin replacement in patients with severe sepsis or septic shock</p></title><aug><au><snm>Caironi</snm><fnm>P</fnm></au><au><snm>Tognoni</snm><fnm>G</fnm></au><au><snm>Masson</snm><fnm>S</fnm></au><au><snm>Fumagalli</snm><fnm>R</fnm></au><au><snm>Pesenti</snm><fnm>A</fnm></au><au><snm>Romero</snm><fnm>M</fnm></au><au><snm>Fanizza</snm><fnm>C</fnm></au><au><snm>Caspani</snm><fnm>L</fnm></au><au><snm>Faenza</snm><fnm>S</fnm></au><au><snm>Grasselli</snm><fnm>G</fnm></au><au><snm>Iapichino</snm><fnm>G</fnm></au><au><snm>Antonelli</snm><fnm>M</fnm></au><au><snm>Parrini</snm><fnm>V</fnm></au><au><snm>Fiore</snm><fnm>G</fnm></au><au><snm>Latini</snm><fnm>R</fnm></au><au><snm>Gattinoni</snm><fnm>L</fnm></au></aug><source>N Engl J Med</source><pubdate>2014</pubdate><volume>370</volume><fpage>1412</fpage><lpage>1421</lpage><xrefbib><pubidlist><pubid idtype="doi">10.1056/NEJMoa1305727</pubid><pubid idtype="pmpid" link="fulltext">24635772</pubid></pubidlist></xrefbib></bibl><bibl id="B2"><title><p>A comparison of albumin and saline for fluid resuscitation in the intensive care unit</p></title><aug><au><snm>Finfer</snm><fnm>S</fnm></au><au><snm>Bellomo</snm><fnm>R</fnm></au><au><snm>Boyce</snm><fnm>N</fnm></au><au><snm>French</snm><fnm>J</fnm></au><au><snm>Myburgh</snm><fnm>J</fnm></au><au><snm>Norton</snm><fnm>R</fnm></au></aug><source>N Engl J Med</source><pubdate>2004</pubdate><volume>350</volume><fpage>2247</fpage><lpage>2256</lpage><xrefbib><pubidlist><pubid idtype="doi">10.1056/NEJMoa040232</pubid><pubid idtype="pmpid" link="fulltext">15163774</pubid></pubidlist></xrefbib></bibl><bibl id="B3"><url>http://www.criticalcarecanada.com/presentations/2013/albios_trial_%E2%80%93_albumin_in_sepsis.pdf</url><note><b>ALBIOS trial: Albumin in Sepsis. </b>[]</note></bibl><bibl id="B4"><url>http://www.clinicaltrials.gov/ct2/show/NCT00327704</url><note><b>Early Albumin Resuscitation During Septic Shock. </b>[]</note></bibl><bibl id="B5"><title><p>Human albumin administration in critically ill patients: systematic review of randomised controlled trials</p></title><source>BMJ</source><pubdate>1998</pubdate><volume>317</volume><fpage>235</fpage><lpage>240</lpage><xrefbib><pubidlist><pubid idtype="doi">10.1136/bmj.317.7153.235</pubid><pubid idtype="pmcid">28613</pubid><pubid idtype="pmpid" link="fulltext">9677209</pubid></pubidlist></xrefbib></bibl></refgrp>
    </bm>
 </art>