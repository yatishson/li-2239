<art>
    <ui>s13054-014-0616-8</ui>
    <ji>1364-8535</ji>
    <fm>
       <dochead>Commentary</dochead>
       <bibl>
          <title>
             <p>Outcome prediction modelling for trauma patients: a German perspective</p>
          </title>
          <aug>
             <au ca="yes" id="A1">
                <snm>Bouamra</snm>
                <fnm>Omar</fnm>
                <insr iid="I1"/>
                <email>omar.bouamra@manchester.ac.uk</email>
             </au>
             <au id="A2">
                <snm>Lesko</snm>
                <mi>M</mi>
                <fnm>Mehdi</fnm>
                <insr iid="I1"/>
                <email>mehdimlesko@gmail.com</email>
             </au>
          </aug>
          <insg>
             <ins id="I1">
                <p>The Trauma Audit &amp; Research Network, The University of Manchester, Salford Royal Hospital Trust, 3rd Floor Mayo Building, Salford M6 8HD, UK</p>
             </ins>
          </insg>
          <source>Critical Care</source>
          <issn>1364-8535</issn>
          <pubdate>2014</pubdate>
          <volume>18</volume>
          <issue>5</issue>
          <fpage>616</fpage>
          <url>http://ccforum.com/content/18/5/616</url>
          <xrefbib>
             <pubidlist>
                <pubid idtype="doi">10.1186/s13054-014-0616-8</pubid>
             <pubid idtype="pmpid">25672913</pubid></pubidlist>
          </xrefbib>
       </bibl>
       <history>
          <pub>
             <date>
                <day>31</day>
                <month>10</month>
                <year>2014</year>
             </date>
          </pub>
       </history>
       <cpyrt>
          <year>2014</year>
          <collab>Bouamra and Lesko; licensee BioMed Central Ltd.</collab>
          <note>The licensee has exclusive rights to distribute this article, in any medium, for 12 months following its publication. After this time, the article is available under the terms of the Creative Commons Attribution License (<url>http://creativecommons.org/licenses/by/4.0</url>), which permits unrestricted use, distribution, and reproduction in any medium, provided the original work is properly credited. The Creative Commons Public Domain Dedication waiver (<url>http://creativecommons.org/publicdomain/zero/1.0/</url>) applies to the data made available in this article, unless otherwise stated.</note>
       </cpyrt>
       <abs>
          <sec>
             <st>
                <p>Abstract</p>
             </st>
             <p>Prognostic models may have some clinical advantages when predicting the outcome of individual trauma patients is relevant. The variables that are predicted to have a negative effect on outcome in a model can also guide clinicians in their resuscitation attempt of trauma victims.</p>
          </sec>
       </abs>
    </fm>
    <bdy><sec><st><p/></st><p>This issue of <it>Critical Care</it> presents an article from Lefering and colleagues on modelling prediction of outcome for trauma patients <abbrgrp>
             <abbr bid="B1">1</abbr>
          </abbrgrp>. This article is in fact an update of an existing model developed by the same authors.</p>
       <p>Trauma registries have been established in several countries; the most important in terms of size are the National Trauma Database in the USA, the Trauma Audit and Research Network in England and, among many others, the TraumaRegister DGU in Germany. The interest in trauma has increased in the last few years, and it is predicted that trauma will be a top-three cause of death in the world by 2020. Trauma is already the leading cause of mortality in the under-40 age group. All of these facts have attracted interest from researchers to develop prognostic models to predict outcome for trauma patients. An important aspect of prognostic models is external validation, because most of the in-house models show good calibration and discrimination using their own data but lose a lot of their predictive power as soon as the models are tested on an external set of data, mainly because different countries/regions have a different case mix of the trauma population.</p>
       <p>Lefering and colleagues conducted the derivation of the Revised Injury Severity Classification II model in a thorough way. The proposed model is an updated and improved version of a previous prediction model. Although the Injury Severity Score is used in most of the trauma-related prognostic models, the authors choose to ignore it &#8211; they rightly justified this because the Injury Severity Score has limitations such as disregarding multiple injuries in the same body region. Lefering and colleagues used the worst and the second-worst injury instead of the Injury Severity Score, which is the same approach used by Moore and colleagues <abbrgrp>
             <abbr bid="B2">2</abbr>
          </abbrgrp> and Osler and colleagues <abbrgrp>
             <abbr bid="B3">3</abbr>
          </abbrgrp>. It is well known that the Injury Severity Score overestimates mortality for higher values and underestimates mortality for lower values <abbrgrp>
             <abbr bid="B4">4</abbr>
          </abbrgrp>. However, the authors&#8217; approach is simple and avoids complexity.</p>
       <p>Missing data are dealt with in this article using a subtle technique where the missing values are allocated to the reference category. Inclusion of cases with missing data gives more credibility to the prediction model.</p>
       <p>As stated by Box and Draper, &#8216;Essentially, all models are wrong, but some are useful&#8217; <abbrgrp>
             <abbr bid="B5">5</abbr>
          </abbrgrp>. Prognostic models are useful when benchmarking trauma care between institutions using different outcome performance indicators. Despite the increasing number of trauma registries and the benchmarking models, the prognostic ability of these models hardly addresses individual patients in the clinical setting. There are two reasons for this. Firstly, there are concerns that the use of a model&#8217;s prediction may lead to a premature or inappropriate treatment withdrawal on the basis of a grim calculated probability. Secondly, the primary objective of these models is trauma care benchmarking and they are not meant to be used for prediction of individual patient outcome.</p>
       <p>Despite these arguments, the models may still have some value for clinical purpose, especially in the case of multiple casualties with limited resources. In such scenarios, the limited resources may have to be allocated to those victims who are likely to benefit the most by having a better prognosis. Such scenarios are not uncommon when highly sophisticated trauma care is required, such as intensive care. This possible advantage of prognostic models has not so far been investigated. Furthermore, the correctable variables that have been included in the final models can have clinical implications, in that a trauma clinician may have to focus on correcting abnormal physiological indexes such as blood pressure, international normalised ratio or haemoglobin. In the same way, fluid resuscitation should perhaps also address the correction of acidosis (or base-deficit) alongside blood pressure.</p></sec><sec>
          <st>
             <p>Competing interests</p>
          </st>
          <p>The authors declare that they have no competing interests.</p>
       </sec>
    </bdy>
    <bm>
       <refgrp>
          
          <bibl id="B1">
             <title>
                <p>Update of the trauma risk adjustment model of the TraumaRegister DGU&#8482;: the Revised Injury Severity Classification, version II</p>
             </title>
             <aug>
                <au>
                   <snm>Lefering</snm>
                   <fnm>R</fnm>
                </au>
                <etal/>
             </aug>
             <source>Crit Care</source>
             <pubdate>2014</pubdate>
             <volume>5</volume>
             <fpage>476</fpage>
             <xrefbib>
                <pubidlist>
                   <pubid idtype="doi">10.1186/s13054-014-0476-2</pubid>
                </pubidlist>
             </xrefbib>
          </bibl>
          <bibl id="B2">
             <title>
                <p>The trauma risk adjustment model: a new model for evaluating trauma care</p>
             </title>
             <aug>
                <au>
                   <snm>Moore</snm>
                   <fnm>L</fnm>
                </au>
                <au>
                   <snm>Lavoie</snm>
                   <fnm>A</fnm>
                </au>
                <au>
                   <snm>Turgeon</snm>
                   <fnm>AF</fnm>
                </au>
                <au>
                   <snm>Abdous</snm>
                   <fnm>B</fnm>
                </au>
                <au>
                   <snm>Le Sage</snm>
                   <fnm>N</fnm>
                </au>
                <au>
                   <snm>Emond</snm>
                   <fnm>M</fnm>
                </au>
                <au>
                   <snm>Liberman</snm>
                   <fnm>M</fnm>
                </au>
                <au>
                   <snm>Bergeron</snm>
                   <fnm>E</fnm>
                </au>
             </aug>
             <source>Ann Surg</source>
             <pubdate>2009</pubdate>
             <volume>249</volume>
             <fpage>1040</fpage>
             <lpage>1046</lpage>
             <xrefbib>
                <pubidlist>
                   <pubid idtype="doi">10.1097/SLA.0b013e3181a6cd97</pubid>
                   <pubid idtype="pmpid" link="fulltext">19474674</pubid>
                </pubidlist>
             </xrefbib>
          </bibl>
          <bibl id="B3">
             <title>
                <p>A trauma mortality prediction model based on the anatomic injury scale</p>
             </title>
             <aug>
                <au>
                   <snm>Osler</snm>
                   <fnm>T</fnm>
                </au>
                <au>
                   <snm>Glance</snm>
                   <fnm>L</fnm>
                </au>
                <au>
                   <snm>Buzas</snm>
                   <fnm>JS</fnm>
                </au>
                <au>
                   <snm>Mukamel</snm>
                   <fnm>D</fnm>
                </au>
                <au>
                   <snm>Wagner</snm>
                   <fnm>J</fnm>
                </au>
                <au>
                   <snm>Dick</snm>
                   <fnm>A</fnm>
                </au>
             </aug>
             <source>Ann Surg</source>
             <pubdate>2008</pubdate>
             <volume>247</volume>
             <fpage>1041</fpage>
             <lpage>1048</lpage>
             <xrefbib>
                <pubidlist>
                   <pubid idtype="doi">10.1097/SLA.0b013e31816ffb3f</pubid>
                   <pubid idtype="pmpid" link="fulltext">18520233</pubid>
                </pubidlist>
             </xrefbib>
          </bibl>
          <bibl id="B4">
             <title>
                <p>A new approach to outcome prediction in trauma: a comparison with the TRISS model</p>
             </title>
             <aug>
                <au>
                   <snm>Bouamra</snm>
                   <fnm>O</fnm>
                </au>
                <au>
                   <snm>Wrotchford</snm>
                   <fnm>A</fnm>
                </au>
                <au>
                   <snm>Hollis</snm>
                   <fnm>S</fnm>
                </au>
                <au>
                   <snm>Vail</snm>
                   <fnm>A</fnm>
                </au>
                <au>
                   <snm>Woodford</snm>
                   <fnm>M</fnm>
                </au>
                <au>
                   <snm>Lecky</snm>
                   <fnm>F</fnm>
                </au>
             </aug>
             <source>J Trauma</source>
             <pubdate>2006</pubdate>
             <volume>61</volume>
             <fpage>701</fpage>
             <lpage>710</lpage>
             <xrefbib>
                <pubidlist>
                   <pubid idtype="doi">10.1097/01.ta.0000197175.91116.10</pubid>
                   <pubid idtype="pmpid" link="fulltext">16967011</pubid>
                </pubidlist>
             </xrefbib>
          </bibl>
          <bibl id="B5">
             <aug>
                <au>
                   <snm>Box</snm>
                   <fnm>GEP</fnm>
                </au>
                <au>
                   <snm>Draper</snm>
                   <fnm>NR</fnm>
                </au>
             </aug>
             <source>Empirical Model-building and Response Surfaces</source>
             <publisher>Wiley, New York</publisher>
             <pubdate>1987</pubdate>
          </bibl>
       </refgrp>
    </bm>
 </art>