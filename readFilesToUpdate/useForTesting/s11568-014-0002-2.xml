<?xml version='1.0'?>
<!DOCTYPE art SYSTEM 'http://www.biomedcentral.com/xml/article.dtd'>
<art>
    <ui>s11568-014-0002-2</ui>
    <ji>1877-6566</ji>
    <fm>
       <dochead>Brief report</dochead>
       <bibl>
          <title>
             <p>Molecular genetics research in sub-Saharan Africa: how can the international community help?</p>
          </title>
          <aug>
             <au id="A1">
                <snm>Bekele</snm>
                <fnm>Endashaw</fnm>
                <insr iid="I1"/>
                <email>endashawbw@gmail.com</email>
             </au>
             <au id="A2">
                <snm>Bodmer</snm>
                <mi>F</mi>
                <fnm>Walter</fnm>
                <insr iid="I2"/>
                <email>walter.bodmer@hertford.ox.ac.uk</email>
             </au>
             <au id="A3">
                <snm>Bradman</snm>
                <fnm>Neil</fnm>
                <insr iid="I3"/>
                <email>neil.bradman@henrystewartgroup.com</email>
             </au>
             <au id="A4" ca="yes">
                <snm>Craig</snm>
                <mi>W</mi>
                <fnm>Ian</fnm>
                <insr iid="I4"/>
                <email>ian.craig@kcl.ac.uk</email>
             </au>
             <au id="A5">
                <snm>Makani</snm>
                <fnm>Julie</fnm>
                <insr iid="I5"/>
                <insr iid="I6"/>
                <email>Julie.Makani@muhimbili-wellcome.org</email>
             </au>
             <au id="A6">
                <snm>Povey</snm>
                <fnm>Sue</fnm>
                <insr iid="I7"/>
                <email>s.povey@ucl.ac.uk</email>
             </au>
             <au id="A7">
                <snm>Rotimi</snm>
                <fnm>Charles</fnm>
                <insr iid="I8"/>
                <email>rotimic@mail.nih.gov</email>
             </au>
          </aug>
          <insg>
             <ins id="I1">
                <p>College of Natural Sciences, Addis Ababa University, Addis Ababa, Ethiopia</p>
             </ins>
             <ins id="I2">
                <p>Department of Oncology and The Weatherall Institute of Molecular Medicine, University of Oxford, John Radcliffe Hospital, Oxford OX3 9DS, UK</p>
             </ins>
             <ins id="I3">
                <p>Henry Stewart Group, Russell House, 28/30 Little Russell Street, London WC1A 2HN, UK</p>
             </ins>
             <ins id="I4">
                <p>MRC SGDP Centre, Institute of Psychiatry, King&#8217;s College London, London SE5 8AF, UK</p>
             </ins>
             <ins id="I5">
                <p>Department of Haematology and Blood Transfusion, Muhimbili University of Health and Allied Sciences, Dar-es-Salaam, Tanzania</p>
             </ins>
             <ins id="I6">
                <p>Nuffield Department of Clinical Medicine, University of Oxford, Oxford, UK</p>
             </ins>
             <ins id="I7">
                <p>Department of Genetics, Evolution and Environment, Darwin Building, UCL, Gower Street, London WC1E 6BT, UK</p>
             </ins>
             <ins id="I8">
                <p>Center for Research on Genomics and Global Health, National Human Genome Research Institute, National Institutes of Health, Building 12A, Room 4047, 12 South Drive, MSC 5635, Bethesda 20892, MD, USA</p>
             </ins>
          </insg>
          <source>The HUGO Journal</source>
          <issn>1877-6566</issn>
          <pubdate>2014</pubdate>
          <volume>8</volume>
          <issue>1</issue>
          <fpage>2</fpage>
          <url>http://www.thehugojournal.com/content/8/1/2</url>
          <xrefbib>
             <pubidlist>
                <pubid idtype="doi">10.1186/s11568-014-0002-2</pubid>
                <pubid idtype="pmpid"><!-- Need to check source for data--></pubid>
             </pubidlist>
          </xrefbib>
       </bibl>
       <history>
          <rec>
             <date>
                <day>27</day>
                <month>3</month>
                <year>2014</year>
             </date>
          </rec>
          <acc>
             <date>
                <day>14</day>
                <month>6</month>
                <year>2014</year>
             </date>
          </acc>
          <pub>
             <date>
                <day>17</day>
                <month>9</month>
                <year>2014</year>
             </date>
          </pub>
       </history>
       <cpyrt>
          <year>2014</year>
          <collab>Bekele et al.; licensee Springer.</collab>
          <note>This is an Open Access article distributed under the terms of the Creative Commons Attribution License (<url>http://creativecommons.org/licenses/by/4.0</url>), which permits unrestricted use, distribution, and reproduction in any medium, provided the original work is properly credited.</note>
       </cpyrt>
       <abs>
          <sec>
             <st>
                <p>Abstract</p>
             </st>
             <sec>
                <st>
                   <p>Background</p>
                </st>
                <p>Opportunities provided by rapidly increasing access to educational resources, clinical and epidemiological data, DNA collections, cheaper technology and financial investment, suggest that researchers in sub-Saharan Africa outside South Africa (SSAOSA) could now join the genomics revolution on equal terms with those in the West.</p>
             </sec>
             <sec>
                <st>
                   <p>Findings</p>
                </st>
                <p>Current evidence, however, suggests that, in some cases, various factors may be compromising this development.  One interpretation is that urgent practical problems, which may compromise motivation, aspiration and ambition, are blocking opportunity.</p>
             </sec>
             <sec>
                <st>
                   <p>Conclusions</p>
                </st>
                <p>Those wishing to help should support the SSAOSA scientists both at the level of extending collaboration networks and in stimulating academic leadership at national and institutional levels to ensure adequate resources are allocated. Members of organisations representing the international community of human geneticists, such as HUGO, have a significant responsibility in supporting such activities.</p>
             </sec>
          </sec>
       </abs>
    </fm>
    <bdy><sec><st><p/></st><p>Scientists in sub-Saharan Africa have, over the next few years, a real opportunity to break through and join their colleagues on the world stage in pushing forward the frontiers of research in genomics. A number of separate elements have now come together to make this possible. They have that most important resource on their doorstep &#8211; human genetic diversity. Africa, the birthplace of anatomically modern man, has it in abundance. Furthermore, the costs of genotyping and sequencing DNA have been falling rapidly. Attention has turned increasingly to the importance of data, which includes creating good biobanks of DNA and associated medical and other records as a resource for biomedical data for genotype/phenotype analysis. Most African countries have had some form of investment in improving medical records in health facilities as well as population-based data such as census information. These activities have been enabled now by cheaper technology including mobile phone and web-based tools. However, they are nevertheless labour intensive. This can give African laboratories a cost advantage if they have the expertise and this expertise is harnessed by national and international funding agencies. So has a new day dawned? Is there now the environment that African researchers including geneticists, epidemiologists and clinicians have waited for? For some the answer may be yes. But for some, specifically geneticists in SSAOSA (sub-Saharan Africa outside South Africa), the answer is less certain. In attempting to answer this question, we evaluate three opportunities to enable active genomic research in Africa; access to knowledge through publications, adequate and sustainable funding and a critical mass of biomedical scientists in the field of genomics.</p></sec><sec>
          <st>
             <p>1 Findings</p>
          </st>
          <p>Acquiring up to date knowledge has always been a barrier to undertaking genetic research in SSAOSA. However, even here, there is good news. As a report by The Association of Commonwealth Universities (ACU - <url>http://www.acu.ac.uk/growing knowledge</url>) shows, this is no longer the problem it once was. Many, possibly all, universities in SSAOSA which are likely to have molecular geneticists on their staff should now have almost the same access to journals and ebooks as their counterparts in the North. The International Network for the Availability of Scientific Publications (INASP &#8211; <url>http://www.inasp.info/</url>) has done a superb job in securing this outcome. This transformation, combined with continuously improving internet access, means that very shortly the latest lectures, webinars and seminars accessed by the leading universities in the most developed countries will be available in real time. The recent initiative by Coursera (<url>https://coursera.org/</url>) in which entire courses from leading universities are being made available online completely free is one example of easily accessible educational resources. Another is provided by the collaboration between the Human Genome Organisation (HUGO) (<url>http://www.hugo-international.org/index.php</url>) and Henry Stewart Talks through which a collection of over 1,500 lectures on biomedicine and the life sciences by world leading experts including Nobel Laureates (<url>http://hstalks.com/</url>) is being provided either free or at a massive discount to SSAOSA universities. Unfortunately, there appear to be factors that hinder scientists in Africa accessing this freely available knowledge. To understand why this may be so, it is worth returning to the study by the ACU. Undertaking an in depth appraisal of four SSAOSA universities in Kenya, Malawi, Rwanda and Tanzania it concluded <it>&#8220;The problem of availability &#8211; that is the provision of affordable or free journals and other resources in online form &#8211; has been widely and successfully addressed over several years&#8221;</it>. However the study also reports that <it>&#8220;Awareness of the materials available amongst staff and students is low&#8221;</it>. One interpretation that reconciles these two observations is that other factors and urgent practical problems, which may compromise motivation, aspiration and ambition, are blocking opportunity. As all researchers know, however, access to samples, equipment and knowledge is usually not enough. Money is often the most important consumable. Here too there is good news in the launch of the Human Heredity and Health in Africans (H3Africa) programme funded by the UK&#8217;s Wellcome Trust and the US&#8217;s NIH (working with the African Society of Human Genetics (AfSHG)). Over the next five years they will, together, be awarding grants in excess of 70 million US dollars directly to African principal investigators based on the continent. This will in turn lead to increases in appropriately trained manpower which can promote both national and international collaborations. It is hoped that the launch of H3Africa will lead to an increase in funding by government and other organisations. This is critical as it is recognised that the initial investment by H3Africa will not be adequate to ensure the required growth of genomics in Africa. So far, the more than 70 million US dollars for H3Africa has funded several projects including disease-specific studies, one Pan-African bioinformatics Network and two pilot biobanking projects. MalariaGEN, a consortium looking at genetic determinants of malaria, was established in 2005 with funding of 16.4 million dollars from the Wellcome Trust and Gates foundation through the National Institutes of Health.</p>
          <p>The third element is human resource. To gain quickly some insight into the number of researchers active in human genetics at a senior level native to and currently working in SSAOSS, we did four things. Firstly we looked at the AfSHG website which listed the members of both the Association&#8217;s Executive Committee and the Scientific Programme Committee for the joint AfSHG/South African Society of Human Genetics 2011 meeting (<url>http://www.afshg.org</url>). Not one of the ten members of the Executive Committee nor of the 19 members of the Scientific Programme Committee had an affiliation solely to an institution in SSAOSA. Next we looked at the Wellcome Trust&#8217;s H3Africa Applicant Discussion Group website (<url>http://www.nature.com/scitable/groups/h3africa-applicant-discussion-group-22066126</url>) which is described as a <it>&#8220;discussion and collaborative space for prospective applicants&#8221;</it>. There were 62 individuals registered on 8 August 2013 of which 16 were from SSAOSA. Of these, nine had first or senior author positions in publications listed in a PubMed search. We then undertook PubMed searches selecting as filters four countries, two in the west (Nigeria and Cameroon) and two in the east (Ethiopia and Kenya) to identify publications on some aspect of human molecular genetics or the molecular genetics of pathogens of humans that appeared in 2012 with a SSAOSA country included in the Authors&#8217; address. In Nigeria we identified 17, in Cameroon 6, in Ethiopia 5 and in Kenya 15. Finally, we contacted the Wellcome Trust, NIH and AfSHG to gain information on the numbers of researchers primarily resident in SSAOSA active in human genetic research. While the results of our enquiries are partly anecdotal, it is clear that there is a small but active group of SSAOSA geneticists undertaking research sufficiently well to get published in internationally recognised peer reviewed journals.</p>
          <p>What can already established scientists in South Africa and elsewhere do to help SSAOSA researchers and extend the numbers of those who may wish to become involved? The first action is to support sustainable partnerships which ensure equitable involvement by all partners. The second is to work with institutions and funders to increase efforts to stem the loss, by emigration, of potential African field leaders and encourage &#8216;brain circulation&#8217; rather than &#8216;brain drain&#8217;. Researchers in the North should be open to collaborations where the &#8216;lead scientist&#8217; is from Africa. After all, as observed at the start of this article, SSAOSA researchers are interested in, and have opportunities to work on, questions that are of global interest. Established scientists must not crowd out emerging scientists from SSAOSA. If SSAOSA research is to be sustainable it has to be locally motivated and locally driven; but the necessary levels of support must be in place. As we have noted, the apparently available opportunity seems difficult to access locally; a situation that may require addressing at the political level. In short, moves to correct this mismatch will have to come from the academic leadership at national and institutional levels and from the staff and students themselves. Those wishing to help should support the SSAOSA scientists in these endeavours so that they may create the momentum, lobby their governments and ensure adequate resources are allocated. With the right environment SSAOSA researchers have, for the first time, the opportunity in many areas of human molecular genetics to contribute on playing fields that are becoming more level. Members of organisations representing the international community of human geneticists, such as HUGO, have a significant responsibility in supporting such opportunity.</p>
       </sec>
       <sec>
          <st>
             <p>Competing interests</p>
          </st>
          <p>The authors declare that they have no competing interests. Bodmer, Craig, Makani, Povey and Rotimi are members of HUGO Council and/or Trustees of HUGO (London, Ltd) and Bradman is Chairman of Henry Stewart Talks Ltd, the publisher of The Biomedical and Life Sciences Collection of audio-visual lectures which is sold on subscription to companies and universities. In a programme with HUGO the collection is made available free to over 100 universities in sub-Saharan Africa.</p>
       </sec>
       <sec>
          <st>
             <p>Authors&#8217; contributions</p>
          </st>
          <p>All authors contributed to preliminary drafts of the article and all authors read and approved the final manuscript.</p>
       </sec>
    </bdy>
    <bm>
       <ack>
          <sec>
             <st>
                <p>Acknowledgements</p>
             </st>
             <p>We are grateful to other Trustees of HUGO London, Elizabeth Bruford, Gertjan van Omen and Stylianos Antonarakis for support and encouragement.</p>
          </sec>
       </ack>
    </bm>
 </art>
