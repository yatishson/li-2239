<art>
    <ui>s12967-014-0270-6</ui>
    <ji>1479-5876</ji>
    <fm>
       <dochead>Erratum</dochead>
       <bibl>
          <title>
             <p>Erratum: Clinical use of Dieletrophoresis separation for live Adipose derived stem cells</p>
          </title>
          <aug>
             <au ca="yes" id="A1">
                <snm>Marincola</snm>
                <mi>M</mi>
                <fnm>Francesco</fnm>
                <insr iid="I1"/>
                <email>translational-medicine@biomedcentral.com</email>
             </au>
          </aug>
          <insg>
             <ins id="I1">
                <p>Sidra Medical and Research Center, Doha, Quatar</p>
             </ins>
          </insg>
          <source>Journal of Translational Medicine</source>
          <issn>1479-5876</issn>
          <pubdate>2014</pubdate>
          <volume>12</volume>
          <issue>1</issue>
          <fpage>270</fpage>
          <url>http://www.translational-medicine.com/content/12/1/270</url>
          <xrefbib>
             <pubidlist>
                <pubid idtype="doi">10.1186/s12967-014-0270-6</pubid>
                <pubid idtype="pmpid">25285403</pubid>
             </pubidlist>
          </xrefbib>
       </bibl>
       <history>
          <rec>
             <date>
                <day>11</day>
                <month>9</month>
                <year>2014</year>
             </date>
          </rec>
          <acc>
             <date>
                <day>11</day>
                <month>9</month>
                <year>2014</year>
             </date>
          </acc>
          <pub>
             <date>
                <day>30</day>
                <month>9</month>
                <year>2014</year>
             </date>
          </pub>
       </history>
       <cpyrt>
          <year>2014</year>
          <collab>Marincola; licensee BioMed Central Ltd.</collab>
          <note>This is an Open Access article distributed under the terms of the Creative Commons Attribution License (<url>http://creativecommons.org/licenses/by/4.0</url>), which permits unrestricted use, distribution, and reproduction in any medium, provided the original work is properly cited.</note>
       </cpyrt>
	   <abs><sec><st>
 <p>Abstract</p>
 </st>
 <p>No abstract.</p></sec></abs>
    </fm>
    <bdy><sec><st><p/></st><p>After publication of this article <abbrgrp>
             <abbr bid="B1">1</abbr>
          </abbrgrp> it was brought to the journal&#8217;s attention that David Morrow was unaware of the submission of this article in his name and did not consent to become an author. He points out that he was not involved in the creation of the article. Readers are also alerted that in the absence of any evidence from an institutional investigation to confirm or refute the reliability of the data, there are ongoing concerns and this article&#8217;s findings should be interpreted with caution. Appropriate editorial action will be taken if further information becomes available.</p></sec></bdy>
    <bm>
       <refgrp>
          <title>
             <p>Reference</p>
          </title>
          <bibl id="B1">
             <title>
                <p>Clinical use of Dieletrophoresis separation for live Adipose derived stem cells</p>
             </title>
             <aug>
                <au>
                   <snm>Wu</snm>
                   <fnm>AY</fnm>
                </au>
                <au>
                   <snm>Morrow</snm>
                   <fnm>DM</fnm>
                </au>
             </aug>
             <source>J Transl Med</source>
             <pubdate>2012</pubdate>
             <volume>10</volume>
             <fpage>99</fpage>
             <xrefbib>
                <pubidlist>
                   <pubid idtype="doi">10.1186/1479-5876-10-99</pubid>
                   <pubid idtype="pmpid" link="fulltext">22594610</pubid>
                </pubidlist>
             </xrefbib>
          </bibl>
       </refgrp>
    </bm>
 </art>