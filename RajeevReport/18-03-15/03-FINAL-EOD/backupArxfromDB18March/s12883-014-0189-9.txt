<art>
    <ui>s12883-014-0189-9</ui>
    <ji>1471-2377</ji>
    <fm>
       <dochead>Case report</dochead>
       <bibl>
          <title>
             <p>Extrapontine myelinolysis associated with pituitrin: case report and literature review</p>
          </title>
          <aug>
             <au id="A1">
                <snm>Zhuang</snm>
                <fnm>Liying</fnm>
                <insr iid="I1"/>
                <insr iid="I2"/>
                <email>zhuangliying43205409@126.com</email>
             </au>
             <au id="A2">
                <snm>Xu</snm>
                <fnm>Ziqi</fnm>
                <insr iid="I1"/>
                <email>ziqi2458@163.com</email>
             </au>
             <au id="A3" ca="yes">
                <snm>Li</snm>
                <fnm>Yaguo</fnm>
                <insr iid="I2"/>
                <email>tjqlyg@163.com</email>
             </au>
             <au id="A4" ca="yes">
                <snm>Luo</snm>
                <fnm>Benyan</fnm>
                <insr iid="I1"/>
                <email>luobenyan@zju.edu.cn</email>
             </au>
          </aug>
          <insg>
             <ins id="I1">
                <p>Department of Neurology, The First Affiliated Hospital, College of Medicine, Zhejiang University, Hangzhou 310003, Zhejiang, China</p>
             </ins>
             <ins id="I2">
                <p>Department of Neurology, Zhejiang Hospital, Hangzhou 310013, Zhejiang, China</p>
             </ins>
          </insg>
          <source>BMC Neurology</source>
          <issn>1471-2377</issn>
          <pubdate>2014</pubdate>
          <volume>14</volume>
          <issue>1</issue>
          <fpage>189</fpage>
          <url>http://www.biomedcentral.com/1471-2377/14/189</url>
          <xrefbib>
             <pubidlist>
                <pubid idtype="doi">10.1186/s12883-014-0189-9</pubid>
                <pubid idtype="pmpid"><!-- Need to check source for data--></pubid>
             </pubidlist>
          </xrefbib>
       </bibl>
       <history>
          <rec>
             <date>
                <day>16</day>
                <month>6</month>
                <year>2014</year>
             </date>
          </rec>
          <acc>
             <date>
                <day>23</day>
                <month>9</month>
                <year>2014</year>
             </date>
          </acc>
          <pub>
             <date>
                <day>9</day>
                <month>10</month>
                <year>2014</year>
             </date>
          </pub>
       </history>
       <cpyrt>
          <year>2014</year>
          <collab>Zhuang et al.; licensee BioMed Central Ltd.</collab>
          <note>This is an Open Access article distributed under the terms of the Creative Commons Attribution License (<url>http://creativecommons.org/licenses/by/4.0</url>), which permits unrestricted use, distribution, and reproduction in any medium, provided the original work is properly credited. The Creative Commons Public Domain Dedication waiver (<url>http://creativecommons.org/publicdomain/zero/1.0/</url>) applies to the data made available in this article, unless otherwise stated.</note>
       </cpyrt>
       <kwdg>
          <kwd>Pituitrin</kwd>
          <kwd>Hyponatremia</kwd>
          <kwd>Extrapontine myelinolysis</kwd>
       </kwdg>
       <abs>
          <sec>
             <st>
                <p>Abstract</p>
             </st>
             <sec>
                <st>
                   <p>Background</p>
                </st>
                <p>Hyponatremia is the most common electrolyte abnormality encountered in hospitalized patients, resulting from a varied spectrum of conditions. Both the primary disturbance and its correction can result in life-threatening neurological consequences. Extrapontine myelinolysis is one such complication that is associated with the rapid correction of hyponatremia. Here we describe a patient who developed extrapontine myelinolysis unexpectedly after the correction of hyponatremia, which involved the drug pituitrin.</p>
             </sec>
             <sec>
                <st>
                   <p>Case presentation</p>
                </st>
                <p>A 24-year-old Chinese woman was transferred to our neurology department with the symptoms of dysarthria and quadriparesis developing one day after the correction of hyponatremia (from 118&#160;mmol/L to 140&#160;mmol/L), which followed with a continuous intravenous drip of pituitrin used to control hemoptysis in the emergency room. During the course, she developed involuntary movement. Magnetic resonance imaging changes were consistent with extrapontine myelinolysis.</p>
             </sec>
             <sec>
                <st>
                   <p>Conclusion</p>
                </st>
                <p>This present case describes the mechanism of profound hyponatremia involving pituitrin, and the subsequent development of extrapontine myelinolysis. Physicians may approach effective clinical management of patients through awareness of the adverse effect of pituitrin on serum sodium levels, and avoid rapid correction of hyponatremia in clinical practice.</p>
             </sec>
          </sec>
       </abs>
    </fm>
    <bdy>
       <sec>
          <st>
             <p>1
 						Background</p>
          </st>
          <p>Hemoptysis, a common symptom in clinical practice, can sometimes become a life-threatening situation and require urgent management. Pituitrin is the best available drug for the control of severe pulmonary haemorrhage or repeated hemoptysis due to its ability of strong vasoconstriction <abbrgrp>
                <abbr bid="B1">1</abbr>
             </abbrgrp>. Untoward effects of pituitrin, such as hyponatremia, are often ignored by physicians, as most patients with hyponatremia are asymptomatic. Hyponatremia is generally defined as plasma sodium level of less than 135&#160;mmol/L <abbrgrp>
                <abbr bid="B2">2</abbr>
             </abbrgrp>. Although hyponatremia is a common and often underestimated problem, rapid correction of chronic hyponatremia can have devastating neurological consequences, i.e., central pontine myelinolysis (CPM) and extrapontine myelinolysis (EPM). CPM and EPM are two variants of osmotic demyelination syndrome, which is related to rapid osmotic changes, particularly an aggressive correction of hyponatremia <abbrgrp>
                <abbr bid="B3">3</abbr>
             </abbrgrp>. EPM may involve the cerebellum, lateral geniculate body, basal ganglia and cerebral white matter with varied spectrum of symptoms. Parkinsonism is common, while involuntary movements such as dystonia and myoclonus are less frequently observed <abbrgrp>
                <abbr bid="B3">3</abbr>
             </abbrgrp>,<abbrgrp>
                <abbr bid="B4">4</abbr>
             </abbrgrp>. We report a case of iatrogenic EPM presenting with dysarthria, quadriparesis and involuntary movements following correction of hyponatremia associated with pituitrin.</p>
       </sec>
       <sec>
          <st>
             <p>2
 						Case presentation</p>
          </st>
          <p>A 24-year-old Chinese woman was transferred to our neurology department presenting with dysarthria and quadriparesis. The patient had a medical history of tetralogy of Fallot since infancy with resulting cyanosis. Corrective surgery was undertaken 4&#160;years ago, and she had been stable and functionally well since then. She had no other comorbid conditions such as alcoholism, malnutrition, hepatic cirrhosis or renal insufficiency. The episode began when the patient contracted repeated minor hemoptysis, she received conventional therapy with aminomethylbenzoic acid and etamsylate without efficiency. On the fifth day, she began to get a fever and was transferred to the emergency room in our hospital.</p>
          <p>On admission, her symptoms worsened with a massive hemoptysis, and pituitrin was prescribed to control the bleeding. Further assessment of the origin of bleeding by computed tomography (CT) angiography of the chest showed that the right bronchial artery was slightly dilated, and a multidisciplinary consultation suggested expectant treatment with pituitrin instead of bronchial artery embolization. Pituitrin (18 U in 30&#160;ml of saline) was administered by intravenous drip continuously at a rate of 2&#8201;~&#8201;3&#160;ml/h via a pump for six days. On the fourth admission day, the patient became nauseous and exhibited generalized weakness. Blood tests revealed a profound hyponatremia with a sodium level of 118&#160;mmol/L, a serum osmolality of 253&#160;mOsm/kg H2O, and hepatic and renal functions were normal. Other biochemical tests showed normal thyroid function, and serum cortisol and adrenocorticotropic hormone were within the normal range. The patient was resuscitated with i.v. hypertonic saline, and the sodium level was 140&#160;mmol/L four days later (Figure&#160;<figr fid="F1">1</figr>). Unfortunately, the next day she deteriorated once more, initially developing dysarthria and quadriparesis. An immediate CT of the head presented normal, and she was transferred to our neurology department. During the course, she developed involuntary movements, mainly paroxysmal oromandibular dystonia and myoclonus in the left upper limb. Magnetic resonance imaging (MRI) showed bilateral symmetric basal ganglia lesions consistent with EPM (Figure&#160;<figr fid="F2">2</figr>). She was treated mainly with corticosteroid, diazepam and hyperbaric oxygen therapy and was discharged with few residual symptoms (speaking with relative fluency and walking on her own without involuntary movements).</p>
          
             <fig id="F1">
                <title>
                   <p>Figure 1</p>
                </title>
                <caption>
                   <p>Changes of the patient&#8217;s serum sodium concentration.</p>
                </caption>
                <text>
                   <p>
                      <b>Changes of the patient&#8217;s serum sodium concentration.</b> Triangles showing the serum sodium gradually decreased with the use of pituitrin, the black arrow pointing out the day beginning use of hypertonic saline.</p>
                </text>
                <graphic file="s12883-014-0189-9-1"/>
             </fig>
          
          
             <fig id="F2">
                <title>
                   <p>Figure 2</p>
                </title>
                <caption>
                   <p>Brain magnetic resonance imaging. A</p>
                </caption>
                <text>
                   <p>
                      <b>Brain magnetic resonance imaging. A</b>, axial T1-weighted MRI showing low signal intensity of bilateral symmetric basal ganglia; <b>B</b>, axial T2-weighted MRI showing high signal intensity of bilateral symmetric basal ganglia; <b>C</b>, axial T2-weighted MRI showing no lesion in the pons (the red arrows pointing out the lesions).</p>
                </text>
                <graphic file="s12883-014-0189-9-2"/>
             </fig>
          
       </sec>
       <sec>
          <st>
             <p>3
 						Conclusions</p>
          </st>
          <p>This patient developed EPM unexpectedly after the intravenous use of pituitrin to control hemoptysis. Pituitrin, extracted from the posterior pituitary, consists of oxytocin and vasopressin. The latter can activate type-1A receptors located in vascular smooth muscle cells resulting in vasoconstriction <abbrgrp>
                <abbr bid="B5">5</abbr>
             </abbrgrp>. Thus, pituitrin has been used in gastrointestinal or pulmonary haemorrhage, and the result is almost as if forceps had been applied directly to the bleeding vessel. In addition, water reabsorption is mediated by vasopressin activation of type-2 receptors in the basolateral membrane of cells in the renal collecting ducts, which causes a decrease in plasma osmolality <abbrgrp>
                <abbr bid="B5">5</abbr>
             </abbrgrp>. There was no medical history of hyponatremia, absence of adrenal, thyroid, pituitary or renal insufficiency, and no recent use of diuretic agents in this patient; pituitrin was implicated as the very probable contributory factor to the development of severe hyponatremia.</p>
          <p>Hyponatremia can be classified into acute (&lt;48&#160;h) and chronic (&#8805;48&#160;h) hyponatremia. When hyponatremia develops, the brain reduces the number of osmotically active particles within its cells (mostly organic solutes) in an attempt to adapt to the osmotic change, which takes 48&#160;h <abbrgrp>
                <abbr bid="B6">6</abbr>
             </abbrgrp>. CPM and EPM are acquired metabolic disorders of acute central demyelination strongly associated with rapid correction of hyponatremia <abbrgrp>
                <abbr bid="B7">7</abbr>
             </abbrgrp>. CPM typically involves the central pontine and EPM involves different brain regions, such as the cerebellum, thalamus, basal ganglia or subcortical white matter. Clinical heterogeneity due to CPM/EPM affecting the basal ganglia has been reviewed by de Souza A <abbrgrp>
                <abbr bid="B8">8</abbr>
             </abbrgrp>, such as dystonia, tremor, myoclonus, gait disorders, dysarthria, cognitive impairment, depression and others. Focal oromandibular dystonia and asymmetric myoclonus that developed in a delayed manner in the patient are rare presentations. The possibility that delayed movement disorders may arise in EPM due to ineffective or faulty reorganization in the basal ganglia has been considered in an earlier report <abbrgrp>
                <abbr bid="B9">9</abbr>
             </abbrgrp>. Sequential observation of symptoms and brain images in the case of CPM and EPM revealed that delayed movement disorders as a result of changes in the signal of the basal ganglia, were explained by the destruction of regional myelin <abbrgrp>
                <abbr bid="B10">10</abbr>
             </abbrgrp>. Special populations seem vulnerable to the development of osmotic myelinolysis, including not only alcoholics and malnourished patients but also those with liver disease, sepsis, adrenal insufficiency and severe burns <abbrgrp>
                <abbr bid="B11">11</abbr>
             </abbrgrp>. Previous cases have shown that the chronicity of hyponatremia before correction is a critical risk factor for the development of osmotic myelinolysis <abbrgrp>
                <abbr bid="B12">12</abbr>
             </abbrgrp>,<abbrgrp>
                <abbr bid="B13">13</abbr>
             </abbrgrp>. In this patient, chronic hyponatremia was documented to exist for at least 48&#160;h, and after a rapid increase of serum sodium from 118&#160;mmol/L to 134&#160;mmol/L in 24&#160;h, there was a delay of one day before progressive neurological decline. Recommendations suggest that ideally hyponatremia should be corrected by limiting the sodium increase to not more than 8&#8211;10&#160;mmol/L every 24&#160;h <abbrgrp>
                <abbr bid="B2">2</abbr>
             </abbrgrp>. We identified three cases, each reporting a single case in which deamino arginine vasopressin, a type of vasopressin analogue, was implicated as a possible contributory factor to the development of severe electrolyte imbalances that triggered osmotic demyelination <abbrgrp>
                <abbr bid="B14">14</abbr>
             </abbrgrp>-<abbrgrp>
                <abbr bid="B16">16</abbr>
             </abbrgrp>. Although the true aetiology of CPM/EPM remains unclear, abrupt osmotic shifts have been considered to play a critical role in their pathogenesis. Deamino arginine vasopressin and pituitrin may predispose patients to osmotic demyelination by causing hyponatremia and extreme serum sodium fluctuations rather than by exerting any direct myelinolytic effect. Further studies are needed to clarify whether the use of pituitrin or its analogue is associated with a risk of osmotic demyelination independent of the electrolyte disturbance.</p>
          <p>This case and literature review highlight that: 1) Hyponatremia is the most frequent electrolyte disturbance observed in hospitalized patients <abbrgrp>
                <abbr bid="B17">17</abbr>
             </abbrgrp>. Drugs such as pituitrin can cause profound hyponatremia, which should be considered in the differential diagnosis when approaching a patient with hyponatremia. 2) Osmotic myelinolysis is most frequently associated with rapid correction of hyponatremia <abbrgrp>
                <abbr bid="B3">3</abbr>
             </abbrgrp>. Prevention of CPM and EPM by exercising caution in the correction of hyponatremia, especially chronic hyponatremia, is more important than early diagnosis.</p>
          <sec>
             <st>
                <p>3.1 Consent</p>
             </st>
             <p>Written informed consent was obtained from the patient for publication of this case report and any accompanying images. A copy of the written consent is available for review by the Editor of this journal.</p>
          </sec>
       </sec>
       <sec>
          <st>
             <p>Abbreviations</p>
          </st>
          <p>CPM: Central pontine myelinolysis</p>
          <p>EPM: Extrapontine myelinolysis</p>
          <p>CT: Computed tomography</p>
          <p>MRI: Magnetic resonance imaging</p>
       </sec>
       <sec>
          <st>
             <p>Competing interests</p>
          </st>
          <p>The authors declare that they have no competing interests.</p>
       </sec>
       <sec>
          <st>
             <p>Authors' contributions</p>
          </st>
          <p>LZ contributed to manuscript writing and interpretation of the data. ZX contributed to acquisition and interpretation of the data. YL contributed to the critical revision of the manuscript for intellectual content. BL contributed to the critical revision of the manuscript for intellectual content. All authors read and approved the final manuscript.</p>
       </sec>
       <sec>
          <st>
             <p>Authors' information</p>
          </st>
          <p>Benyan Luo is a professor of Zhejiang University, the person in charge of the department of Neurology, The First Affiliated Hospital of Zhejiang University. She has done research on cognitive impairment and vascular disease.</p>
       </sec>
    </bdy>
    <bm>
       <ack>
          <sec>
             <st>
                <p>Acknowledgements</p>
             </st>
             <p>We thank Dr Min Yuan for helpful comments on the final version of the draft.</p>
          </sec>
       </ack>
       <refgrp>
          
          <bibl id="B1">
             <title>
                <p>Use of pituitrin by the intravenous drip method in gastrointestinal and pulmonary hemorrhage</p>
             </title>
             <aug>
                <au>
                   <snm>Repsys</snm>
                   <fnm>J</fnm>
                </au>
             </aug>
             <source>Sveikatos Apsauga</source>
             <pubdate>1963</pubdate>
             <volume>54</volume>
             <fpage>5</fpage>
             <lpage>8</lpage>
             <xrefbib>
                <pubidlist>
                   <pubid idtype="pmpid" link="fulltext">14072231</pubid>
                </pubidlist>
             </xrefbib>
          </bibl>
          <bibl id="B2">
             <title>
                <p>Clinical practice guideline on diagnosis and treatment of hyponatraemia</p>
             </title>
             <aug>
                <au>
                   <snm>Spasovski</snm>
                   <fnm>G</fnm>
                </au>
                <au>
                   <snm>Vanholder</snm>
                   <fnm>R</fnm>
                </au>
                <au>
                   <snm>Allolio</snm>
                   <fnm>B</fnm>
                </au>
                <au>
                   <snm>Annane</snm>
                   <fnm>D</fnm>
                </au>
                <au>
                   <snm>Ball</snm>
                   <fnm>S</fnm>
                </au>
                <au>
                   <snm>Bichet</snm>
                   <fnm>D</fnm>
                </au>
                <au>
                   <snm>Decaux</snm>
                   <fnm>G</fnm>
                </au>
                <au>
                   <snm>Fenske</snm>
                   <fnm>W</fnm>
                </au>
                <au>
                   <snm>Hoorn</snm>
                   <fnm>E</fnm>
                </au>
                <au>
                   <snm>Ichai</snm>
                   <fnm>C</fnm>
                </au>
                <au>
                   <snm>Joannidis</snm>
                   <fnm>M</fnm>
                </au>
                <au>
                   <snm>Soupart</snm>
                   <fnm>A</fnm>
                </au>
                <au>
                   <snm>Zietse</snm>
                   <fnm>R</fnm>
                </au>
                <au>
                   <snm>Haller</snm>
                   <fnm>M</fnm>
                </au>
                <au>
                   <snm>van der Veer</snm>
                   <fnm>S</fnm>
                </au>
                <au>
                   <snm>Van Biesen</snm>
                   <fnm>W</fnm>
                </au>
                <au>
                   <snm>Nagler</snm>
                   <fnm>E</fnm>
                </au>
             </aug>
             <source>Eur J Endocrinol</source>
             <pubdate>2014</pubdate>
             <volume>170</volume>
             <issue>3</issue>
             <fpage>G1</fpage>
             <lpage>G47</lpage>
             <xrefbib>
                <pubidlist>
                   <pubid idtype="doi">10.1530/EJE-13-1020</pubid>
                   <pubid idtype="pmpid" link="fulltext">24569125</pubid>
                </pubidlist>
             </xrefbib>
          </bibl>
          <bibl id="B3">
             <title>
                <p>Central pontine myelinolysis, an update</p>
             </title>
             <aug>
                <au>
                   <snm>Kumar</snm>
                   <fnm>S</fnm>
                </au>
                <au>
                   <snm>Fowler</snm>
                   <fnm>M</fnm>
                </au>
                <au>
                   <snm>Gonzalez-Toledo</snm>
                   <fnm>E</fnm>
                </au>
                <au>
                   <snm>Jaffe</snm>
                   <fnm>SL</fnm>
                </au>
             </aug>
             <source>Neurol Res</source>
             <pubdate>2006</pubdate>
             <volume>28</volume>
             <issue>3</issue>
             <fpage>360</fpage>
             <lpage>366</lpage>
             <xrefbib>
                <pubidlist>
                   <pubid idtype="doi">10.1179/016164106X110346</pubid>
                   <pubid idtype="pmpid" link="fulltext">16687066</pubid>
                </pubidlist>
             </xrefbib>
          </bibl>
          <bibl id="B4">
             <title>
                <p>Central pontine and extrapontine myelinolysis: the osmotic demyelination syndromes</p>
             </title>
             <aug>
                <au>
                   <snm>Martin</snm>
                   <fnm>RJ</fnm>
                </au>
             </aug>
             <source>J Neurol Neurosurg Psychiatry</source>
             <pubdate>2004</pubdate>
             <volume>75</volume>
             <issue>Suppl 3</issue>
             <fpage>i22</fpage>
             <lpage>i28</lpage>
          </bibl>
          <bibl id="B5">
             <title>
                <p>Physiology of the vasopressin receptors</p>
             </title>
             <aug>
                <au>
                   <snm>Maybauer</snm>
                   <fnm>MO</fnm>
                </au>
                <au>
                   <snm>Maybauer</snm>
                   <fnm>DM</fnm>
                </au>
                <au>
                   <snm>Enkhbaatar</snm>
                   <fnm>P</fnm>
                </au>
                <au>
                   <snm>Traber</snm>
                   <fnm>DL</fnm>
                </au>
             </aug>
             <source>Best Pract Res Clin Anaesthesiol</source>
             <pubdate>2008</pubdate>
             <volume>22</volume>
             <issue>2</issue>
             <fpage>253</fpage>
             <lpage>263</lpage>
             <xrefbib>
                <pubidlist>
                   <pubid idtype="doi">10.1016/j.bpa.2008.03.003</pubid>
                   <pubid idtype="pmpid" link="fulltext">18683472</pubid>
                </pubidlist>
             </xrefbib>
          </bibl>
          <bibl id="B6">
             <title>
                <p>Hyponatremia</p>
             </title>
             <aug>
                <au>
                   <snm>Adrogue</snm>
                   <fnm>HJ</fnm>
                </au>
                <au>
                   <snm>Madias</snm>
                   <fnm>NE</fnm>
                </au>
             </aug>
             <source>N Engl J Med</source>
             <pubdate>2000</pubdate>
             <volume>342</volume>
             <issue>21</issue>
             <fpage>1581</fpage>
             <lpage>1589</lpage>
             <xrefbib>
                <pubidlist>
                   <pubid idtype="doi">10.1056/NEJM200005253422107</pubid>
                   <pubid idtype="pmpid" link="fulltext">10824078</pubid>
                </pubidlist>
             </xrefbib>
          </bibl>
          <bibl id="B7">
             <title>
                <p>A fluctuation of serum osmolality inducing osmotic demyelination syndrome</p>
             </title>
             <aug>
                <au>
                   <snm>Khositseth</snm>
                   <fnm>S</fnm>
                </au>
                <au>
                   <snm>Intrakao</snm>
                   <fnm>S</fnm>
                </au>
                <au>
                   <snm>Pao-in</snm>
                   <fnm>W</fnm>
                </au>
                <au>
                   <snm>Visudtibhan</snm>
                   <fnm>A</fnm>
                </au>
             </aug>
             <source>J Med Assoc Thai</source>
             <pubdate>2010</pubdate>
             <volume>93</volume>
             <issue>Suppl 7</issue>
             <fpage>S299</fpage>
             <lpage>S302</lpage>
             <xrefbib>
                <pubidlist>
                   <pubid idtype="pmpid" link="fulltext">21294429</pubid>
                </pubidlist>
             </xrefbib>
          </bibl>
          <bibl id="B8">
             <title>
                <p>Movement disorders and the osmotic demyelination syndrome</p>
             </title>
             <aug>
                <au>
                   <snm>de Souza</snm>
                   <fnm>A</fnm>
                </au>
             </aug>
             <source>Parkinsonism Relat Disord</source>
             <pubdate>2013</pubdate>
             <volume>19</volume>
             <issue>8</issue>
             <fpage>709</fpage>
             <lpage>716</lpage>
             <xrefbib>
                <pubidlist>
                   <pubid idtype="doi">10.1016/j.parkreldis.2013.04.005</pubid>
                   <pubid idtype="pmpid" link="fulltext">23660544</pubid>
                </pubidlist>
             </xrefbib>
          </bibl>
          <bibl id="B9">
             <title>
                <p>Evolving spectrum of movement disorders in extrapontine and central pontine myelinolysis</p>
             </title>
             <aug>
                <au>
                   <snm>Seah</snm>
                   <fnm>AB</fnm>
                </au>
                <au>
                   <snm>Chan</snm>
                   <fnm>LL</fnm>
                </au>
                <au>
                   <snm>Wong</snm>
                   <fnm>MC</fnm>
                </au>
                <au>
                   <snm>Tan</snm>
                   <fnm>EK</fnm>
                </au>
             </aug>
             <source>Parkinsonism Relat Disord</source>
             <pubdate>2002</pubdate>
             <volume>9</volume>
             <issue>2</issue>
             <fpage>117</fpage>
             <lpage>119</lpage>
             <xrefbib>
                <pubidlist>
                   <pubid idtype="doi">10.1016/S1353-8020(02)00002-0</pubid>
                   <pubid idtype="pmpid" link="fulltext">12473403</pubid>
                </pubidlist>
             </xrefbib>
          </bibl>
          <bibl id="B10">
             <title>
                <p>Sequential observation of movement disorders and brain images in the case of central pontine myelinolysis and extrapontine myelinolysis</p>
             </title>
             <aug>
                <au>
                   <snm>Seok</snm>
                   <fnm>JI</fnm>
                </au>
                <au>
                   <snm>Youn</snm>
                   <fnm>J</fnm>
                </au>
                <au>
                   <snm>Chung</snm>
                   <fnm>EJ</fnm>
                </au>
                <au>
                   <snm>Lee</snm>
                   <fnm>WY</fnm>
                </au>
             </aug>
             <source>Parkinsonism Relat Disord</source>
             <pubdate>2006</pubdate>
             <volume>12</volume>
             <issue>7</issue>
             <fpage>462</fpage>
             <lpage>464</lpage>
             <xrefbib>
                <pubidlist>
                   <pubid idtype="doi">10.1016/j.parkreldis.2006.02.002</pubid>
                   <pubid idtype="pmpid" link="fulltext">16731027</pubid>
                </pubidlist>
             </xrefbib>
          </bibl>
          <bibl id="B11">
             <title>
                <p>Osmotic demyelination disorders: central pontine and extrapontine myelinolysis</p>
             </title>
             <aug>
                <au>
                   <snm>Brown</snm>
                   <fnm>WD</fnm>
                </au>
             </aug>
             <source>Curr Opin Neurol</source>
             <pubdate>2000</pubdate>
             <volume>13</volume>
             <issue>6</issue>
             <fpage>691</fpage>
             <lpage>697</lpage>
             <xrefbib>
                <pubidlist>
                   <pubid idtype="doi">10.1097/00019052-200012000-00014</pubid>
                   <pubid idtype="pmpid" link="fulltext">11148672</pubid>
                </pubidlist>
             </xrefbib>
          </bibl>
          <bibl id="B12">
             <title>
                <p>Osmotic demyelination syndrome after correction of chronic hyponatremia with normal saline</p>
             </title>
             <aug>
                <au>
                   <snm>Lin</snm>
                   <fnm>SH</fnm>
                </au>
                <au>
                   <snm>Chau</snm>
                   <fnm>T</fnm>
                </au>
                <au>
                   <snm>Wu</snm>
                   <fnm>CC</fnm>
                </au>
                <au>
                   <snm>Yang</snm>
                   <fnm>SS</fnm>
                </au>
             </aug>
             <source>Am J Med Sci</source>
             <pubdate>2002</pubdate>
             <volume>323</volume>
             <issue>5</issue>
             <fpage>259</fpage>
             <lpage>262</lpage>
             <xrefbib>
                <pubidlist>
                   <pubid idtype="doi">10.1097/00000441-200205000-00005</pubid>
                   <pubid idtype="pmpid" link="fulltext">12018668</pubid>
                </pubidlist>
             </xrefbib>
          </bibl>
          <bibl id="B13">
             <title>
                <p>Osmotic myelinolysis following chronic hyponatremia corrected at an overall rate consistent with current recommendations</p>
             </title>
             <aug>
                <au>
                   <snm>Dellabarca</snm>
                   <fnm>C</fnm>
                </au>
                <au>
                   <snm>Servilla</snm>
                   <fnm>KS</fnm>
                </au>
                <au>
                   <snm>Hart</snm>
                   <fnm>B</fnm>
                </au>
                <au>
                   <snm>Murata</snm>
                   <fnm>GH</fnm>
                </au>
                <au>
                   <snm>Tzamaloukas</snm>
                   <fnm>AH</fnm>
                </au>
             </aug>
             <source>Int Urol Nephrol</source>
             <pubdate>2005</pubdate>
             <volume>37</volume>
             <issue>1</issue>
             <fpage>171</fpage>
             <lpage>173</lpage>
             <xrefbib>
                <pubidlist>
                   <pubid idtype="doi">10.1007/s11255-004-4770-9</pubid>
                   <pubid idtype="pmpid" link="fulltext">16132782</pubid>
                </pubidlist>
             </xrefbib>
          </bibl>
          <bibl id="B14">
             <title>
                <p>Iatrogenic [corrected] extrapontine myelinolysis in central diabetes insipidus: are cyclosporine and 1-desamino-8-D-arginine vasopressin harmful in association?</p>
             </title>
             <aug>
                <au>
                   <snm>Maghnie</snm>
                   <fnm>M</fnm>
                </au>
                <au>
                   <snm>Genovese</snm>
                   <fnm>E</fnm>
                </au>
                <au>
                   <snm>Lundin</snm>
                   <fnm>S</fnm>
                </au>
                <au>
                   <snm>Bonetti</snm>
                   <fnm>F</fnm>
                </au>
                <au>
                   <snm>Arico</snm>
                   <fnm>M</fnm>
                </au>
             </aug>
             <source>J Clin Endocrinol Metab</source>
             <pubdate>1997</pubdate>
             <volume>82</volume>
             <issue>6</issue>
             <fpage>1749</fpage>
             <lpage>1751</lpage>
             <xrefbib>
                <pubidlist>
                   <pubid idtype="doi">10.1210/jcem.82.6.3973</pubid>
                   <pubid idtype="pmpid" link="fulltext">9177375</pubid>
                </pubidlist>
             </xrefbib>
          </bibl>
          <bibl id="B15">
             <title>
                <p>Osmotic myelinolysis syndrome after treatment of severe deamino arginine vasopressin-associated hyponatraemia: pitfalls in emergency medicine</p>
             </title>
             <aug>
                <au>
                   <snm>Gutenstein</snm>
                   <fnm>M</fnm>
                </au>
             </aug>
             <source>Emerg Med Australas</source>
             <pubdate>2007</pubdate>
             <volume>19</volume>
             <issue>1</issue>
             <fpage>68</fpage>
             <lpage>70</lpage>
             <xrefbib>
                <pubidlist>
                   <pubid idtype="doi">10.1111/j.1742-6723.2007.00931.x</pubid>
                   <pubid idtype="pmpid" link="fulltext">17305665</pubid>
                </pubidlist>
             </xrefbib>
          </bibl>
          <bibl id="B16">
             <title>
                <p>Osmotic myelinolysis with malignant cerebellar edema occurring after DDAVP-induced hyponatremia in a child</p>
             </title>
             <aug>
                <au>
                   <snm>Ranger</snm>
                   <fnm>A</fnm>
                </au>
                <au>
                   <snm>Szymczak</snm>
                   <fnm>A</fnm>
                </au>
                <au>
                   <snm>Levin</snm>
                   <fnm>S</fnm>
                </au>
                <au>
                   <snm>Salvadori</snm>
                   <fnm>M</fnm>
                </au>
                <au>
                   <snm>Fraser</snm>
                   <fnm>DD</fnm>
                </au>
             </aug>
             <source>Pediatr Neurosurg</source>
             <pubdate>2010</pubdate>
             <volume>46</volume>
             <issue>4</issue>
             <fpage>318</fpage>
             <lpage>323</lpage>
             <xrefbib>
                <pubidlist>
                   <pubid idtype="doi">10.1159/000320146</pubid>
                   <pubid idtype="pmpid" link="fulltext">21196800</pubid>
                </pubidlist>
             </xrefbib>
          </bibl>
          <bibl id="B17">
             <title>
                <p>Epidemiology of hyponatremia</p>
             </title>
             <aug>
                <au>
                   <snm>Upadhyay</snm>
                   <fnm>A</fnm>
                </au>
                <au>
                   <snm>Jaber</snm>
                   <fnm>BL</fnm>
                </au>
                <au>
                   <snm>Madias</snm>
                   <fnm>NE</fnm>
                </au>
             </aug>
             <source>Semin Nephrol</source>
             <pubdate>2009</pubdate>
             <volume>29</volume>
             <issue>3</issue>
             <fpage>227</fpage>
             <lpage>238</lpage>
             <xrefbib>
                <pubidlist>
                   <pubid idtype="doi">10.1016/j.semnephrol.2009.03.004</pubid>
                   <pubid idtype="pmpid" link="fulltext">19523571</pubid>
                </pubidlist>
             </xrefbib>
          </bibl>
       </refgrp>
    </bm>
 </art>
