<art>
    <ui>s13037-014-0038-1</ui>
    <ji>1754-9493</ji>
    <fm>
       <dochead>Editorial</dochead>
       <bibl>
          <title>
             <p>Lessons from aviation safety: &#8220;plan your operation &#8211; and operate your plan!&#8221;</p>
          </title>
          <aug>
             <au ca="yes" id="A1">
                <snm>Schelkun</snm>
                <mi>R</mi>
                <fnm>Steven</fnm>
                <insr iid="I1"/>
                <email>Schelkun@me.com</email>
             </au>
          </aug>
          <insg>
             <ins id="I1">
                <p>Surgical Consultant, Naval Medical Center, San Diego, USA</p>
             </ins>
          </insg>
          <source>Patient Safety in Surgery</source>
          <issn>1754-9493</issn>
          <pubdate>2014</pubdate>
          <volume>8</volume>
          <issue>1</issue>
          <fpage>38</fpage>
          <url>http://www.pssjournal.com/content/8/1/38</url>
          <xrefbib>
             <pubidlist>
                <pubid idtype="doi">10.1186/s13037-014-0038-1</pubid>
                <pubid idtype="pmpid">25278997</pubid>
             </pubidlist>
          </xrefbib>
       </bibl>
       <history>
          <rec>
             <date>
                <day>3</day>
                <month>9</month>
                <year>2014</year>
             </date>
          </rec>
          <acc>
             <date>
                <day>3</day>
                <month>9</month>
                <year>2014</year>
             </date>
          </acc>
          <pub>
             <date>
                <day>27</day>
                <month>9</month>
                <year>2014</year>
             </date>
          </pub>
       </history>
       <cpyrt>
          <year>2014</year>
          <collab>Schelkun; licensee BioMed Central Ltd.</collab>
          <note>This is an Open Access article distributed under the terms of the Creative Commons Attribution License (<url>http://creativecommons.org/licenses/by/4.0</url>), which permits unrestricted use, distribution, and reproduction in any medium, provided the original work is properly credited. The Creative Commons Public Domain Dedication waiver (<url>http://creativecommons.org/publicdomain/zero/1.0/</url>) applies to the data made available in this article, unless otherwise stated.</note>
       </cpyrt>
	   <abs><sec><st>
 <p>Abstract</p>
 </st>
 <p>No abstract.</p></sec></abs>
    </fm>
    <bdy><sec><st><p/></st><p>The systematic implementation of surgical safety checklists in the past 5 to 10&#160;years has contributed to a global decrease in morbidity and mortality after surgical procedures <abbrgrp>
             <abbr bid="B1">1</abbr>
          </abbrgrp>-<abbrgrp>
             <abbr bid="B5">5</abbr>
          </abbrgrp>. What I find intriguing about these data is that the significant reduction in adverse events with the utilization of a simple surgical check list did not require a costly, high-tech solution but rather a simple, almost &#8220;no-cost attitude&#8221; and procedural change in the operating theater. Going back in history, in response to adverse aircraft incidents, the Army-Air Corps test pilots developed a series of aircraft check-lists to reduce the work load on the pilot of the complex aircraft systems and instrumentation in 1935. The aviation community has adopted these check lists and procedures as standard not only in military, but commercial and civilian aviation procedures as well. Their premier goal has always been the safety of passengers and aircraft.</p>
       <p>Aviation flight planning consists of four distinct steps:</p><p>1. Plan the flight using aviation charts to determine how to get from point A to point B.</p>
          <p>2. File a flight plan with the FAA providing pertinent details of the aircraft, altitude of flight, route, aircraft fuel capacity and endurance and time enroute. This step also includes defining an alternate plan (i.e. alternate airport) in case the airplane cannot safely land at the intended destination.</p>
          <p>3. A detailed pre-flight equipment and instrument check of the aircraft itself, engine and navigation instruments, and fuel, oil and aircraft systems to ensure that all systems are working properly.</p>
          <p>4. Standardized communication with ground control, the tower, and constantly along the route of flight with Air Traffic Control.</p>
       <p>This &#8216;4-step procedure&#8217; in conjunction with use of the standardized checklists and mandatory &#8216;readbacks&#8217; represent the basic tenet of proven aviation safety principles <abbrgrp>
             <abbr bid="B6">6</abbr>
          </abbrgrp>. In flight training, a pilot is taught to &#8220;<it>Plan your flight</it> &#8211; <it>and fly your plan</it>!&#8221; As a result, the aviation community has boasted a safety record that is unparalleled in the military, civilian and commercial transportation world. Granted that there are aircraft accidents, but the vast majority can be traced to deviation of the standard procedures.</p>
       <p>As an orthopaedic trauma surgeon, my colleagues and I routinely face urgent and emergency situations in patients with multiple injuries and complex medical problems. On a daily basis, we have to orchestrate in the operating room with a team of personnel dealing with an outrageous number of complex instrument sets and a huge inventory of different implants. It is easy to understand how errors of omission of one of the steps could potentially lead to adverse results and a higher incidence of morbidity and mortality. While the implementation of surgical safety checklists certainly had a global impact in reducing preventable adverse events, I strongly feel that checklists alone fall short of addressing the entire aspect of pre-operative planning. As surgeons, we not only desire improved patient safety, but we also strive for improved patient functional outcomes. Checklists can help with the mechanical procedures in the operating room, but we also need to address the quality of the operative procedure by developing a mindset and intent for a more successful operation. This process starts long before the patient gets to the operating room.</p>
       <p>The aviation flight planning scenario is remarkably applicable to the surgical field.</p>
       <p>Modeled on the aviation safety principle, I propose a similar &#8216;4-step procedure&#8217; for a standardized pre-operative planning in surgery:</p><p>1. Plan your operation. First of all understand the patient, the injury, the goals of the operation and choose a surgical procedure to get you there. In orthopaedic trauma surgery this is where templating of the fracture with the surgical implants will help you better understand the fracture, understand the goals of reduction and alignment as well as determine the type, location and size of the implant that you need.</p>
          <p>2. Develop a detailed surgical tactic by thinking through the operation chronologically, one step at a time. Decide what operating table, patient position, drape pack, prepping technique and surgical approach you will use. This is a good time to review the details of a less frequently used approach. Be prepared for obstacles along the way in the way of important anatomic structures and have an alternate &#8220;Plan B&#8221; to handle potential problems along the way.</p>
          <p>3. Develop a surgical instrument checklist to let the operative team know what you need. Every fracture can have different nuances that may require a different instrument and implant set from the inventory of over 85 fracture sets in my hospital. I cannot expect the surgical team to know what I need nor pull the proper sets unless I tell them specifically. Pre-printed surgeon preference cards do not work well in trauma surgery due to the variability of our cases and the number of sets.</p>
          <p>4. Communicate all along the way with your operative team. The team is there to help you achieve your goals with the patient. They are professionals as well and want to be part of a successful team. By discussion with them the patient&#8217;s problem, your goals and intended plan of approach you are building teamwork for success. When you give them your equipment and instrument list before the case you are allowing them to prepare with you for the case as well. When the patient is in the room, positioned on the table and the entire team is assembled you can then call for a &#8220;Time Out&#8221; <abbrgrp>
                <abbr bid="B7">7</abbr>
             </abbrgrp>. Each member of the team has a specific task. I normally stand at the foot of the bed with my check list and address the items one at a time, requiring a verbal reply, the &#8220;readback&#8221; in aviation terminology, for each item on the list <abbrgrp>
                <abbr bid="B6">6</abbr>
             </abbrgrp>.</p>
       <p>Each member should be given the opportunity to ask questions or express any concerns about the patient&#8217;s condition, the plans or completeness of the preparations. I was brought up to believe that the operating surgeon was the &#8220;Captain of the Ship&#8221; and what he or she said was the rule. I no longer feel that is a sustainable paradigm in the operating room. Better yet, I like to think of the surgeon as the leader of a professional team, who is willing to listen to the other members of the team with mutual respect. It is this respect and open communication that will allow a member of the team to speak up if he observes something that he thinks may impact or contribute to the success of the operation. A standardized checklist usually takes no more than 30&#8211;45 seconds but reaps enormous benefits in the confidence the team has in you and your case <abbrgrp>
             <abbr bid="B4">4</abbr>
          </abbrgrp>,<abbrgrp>
             <abbr bid="B8">8</abbr>
          </abbrgrp>.</p>
       <p>As a formal &#8216;debriefing&#8217; at the end of each case, I place the post-operative x-rays on the view box (or DICOM equivalent for comparison) next to the pre-op x-rays and my pre-operative planning template and compare my results with the plan. If they don&#8217;t match, I need to know why.</p>
       <p>I then ask myself and my team two questions:</p><p>1. &#8220;<it>What went well with this operation</it>?&#8221; (to reinforce successful planning)</p>
          <p>2. &#8220;<it>What could I do differently next time to improve the results</it>?&#8221; (to close the loop in the learning process in each case)</p>
       <p>This structured debriefing approach validates good decisions and allows us to learn what could be done better next time. Pre-operative planning is much more than signing the site and reading through a few checklists so we don&#8217;t forget to give the antibiotics before the case. Pre-operative planning is a mind-set for success of the operation and a process that if done conscientiously will not only make you a better surgeon, build a better surgical team, but will improve your surgical results and improve the safety for your patient.</p>
       <p>As surgeons and educators we are obligated to teach our residents and fellows not only how to operate, but we need to help them develop a mind-set and process that will improve the surgical results and keep our patient safe in the future.</p>
       <p>Therefore, in every case, it is imperative for a surgeon to &#8220;<it>Plan your operation</it> &#8211; <it>and operate your plan</it>!&#8221;.</p></sec><sec>
          <st>
             <p>Competing interest</p>
          </st>
          <p>The author has no affiliations that would constitute a conflict of interest.</p>
       </sec>
    </bdy>
    <bm>
       <refgrp>
          
          <bibl id="B1">
             <title>
                <p>A surgical checklist to reduce morbidity and mortality in a global population</p>
             </title>
             <aug>
                <au>
                   <snm>Hays</snm>
                   <fnm>AB</fnm>
                </au>
                <au>
                   <snm>Weiser</snm>
                   <fnm>TG</fnm>
                </au>
                <au>
                   <snm>Berry</snm>
                   <fnm>WR</fnm>
                </au>
                <au>
                   <snm>Lipsitz</snm>
                   <fnm>SR</fnm>
                </au>
                <au>
                   <snm>Breizat</snm>
                   <fnm>AS</fnm>
                </au>
                <au>
                   <snm>Dellinger</snm>
                   <fnm>EP</fnm>
                </au>
                <au>
                   <snm>Herboas</snm>
                   <fnm>T</fnm>
                </au>
                <au>
                   <snm>Joseph</snm>
                   <fnm>S</fnm>
                </au>
                <au>
                   <snm>Kibatala</snm>
                   <fnm>PL</fnm>
                </au>
                <au>
                   <snm>Lapitan</snm>
                   <fnm>MC</fnm>
                </au>
                <au>
                   <snm>Merry</snm>
                   <fnm>AF</fnm>
                </au>
                <au>
                   <snm>Moorthy</snm>
                   <fnm>K</fnm>
                </au>
                <au>
                   <snm>Reznick</snm>
                   <fnm>RK</fnm>
                </au>
                <au>
                   <snm>Taylor</snm>
                   <fnm>B</fnm>
                </au>
                <au>
                   <snm>Gawande</snm>
                   <fnm>AA</fnm>
                </au>
             </aug>
             <source>N Engl J Med</source>
             <pubdate>2009</pubdate>
             <volume>360</volume>
             <fpage>491</fpage>
             <lpage>499</lpage>
             <xrefbib>
                <pubidlist>
                   <pubid idtype="doi">10.1056/NEJMsa0810119</pubid>
                </pubidlist>
             </xrefbib>
          </bibl>
          <bibl id="B2">
             <title>
                <p>The SURgical PAtient Safety System (SURPASS) checklist optimizes timing of antibiotic prophylaxis</p>
             </title>
             <aug>
                <au>
                   <snm>de Vries</snm>
                   <fnm>EN</fnm>
                </au>
                <au>
                   <snm>Dijkstra</snm>
                   <fnm>L</fnm>
                </au>
                <au>
                   <snm>Smorenburg</snm>
                   <fnm>SM</fnm>
                </au>
                <au>
                   <snm>Meijer</snm>
                   <fnm>RP</fnm>
                </au>
                <au>
                   <snm>Boermeester</snm>
                   <fnm>MA</fnm>
                </au>
             </aug>
             <source>Patient Saf Surg</source>
             <pubdate>2010</pubdate>
             <volume>4</volume>
             <fpage>6</fpage>
             <xrefbib>
                <pubidlist>
                   <pubid idtype="doi">10.1186/1754-9493-4-6</pubid>
                   <pubid idtype="pmpid" link="fulltext">20388204</pubid>
                </pubidlist>
             </xrefbib>
          </bibl>
          <bibl id="B3">
             <title>
                <p>Surgical safety</p>
             </title>
             <aug>
                <au>
                   <snm>Ram</snm>
                   <fnm>K</fnm>
                </au>
                <au>
                   <snm>Boermeester</snm>
                   <fnm>MA</fnm>
                </au>
             </aug>
             <source>Br J Surg</source>
             <pubdate>2013</pubdate>
             <volume>100</volume>
             <fpage>1257</fpage>
             <lpage>1259</lpage>
             <xrefbib>
                <pubidlist>
                   <pubid idtype="doi">10.1002/bjs.9162_1</pubid>
                   <pubid idtype="pmpid" link="fulltext">23939838</pubid>
                </pubidlist>
             </xrefbib>
          </bibl>
          <bibl id="B4">
             <title>
                <p>The tenth year of the &#8220;Universal Protocol&#8221;: are our patients safer today?</p>
             </title>
             <aug>
                <au>
                   <snm>Stahel</snm>
                   <fnm>PF</fnm>
                </au>
             </aug>
             <source>Bone &amp; Joint360</source>
             <pubdate>2014</pubdate>
             <volume>3</volume>
             <fpage>7</fpage>
             <lpage>10</lpage>
          </bibl>
          <bibl id="B5">
             <title>
                <p>Implementation of the WHO Surgical Safety Checklist in an Ethiopian referral hospital</p>
             </title>
             <aug>
                <au>
                   <snm>Bashford</snm>
                   <fnm>T</fnm>
                </au>
                <au>
                   <snm>Reshamwalla</snm>
                   <fnm>S</fnm>
                </au>
                <au>
                   <snm>McAuley</snm>
                   <fnm>J</fnm>
                </au>
                <au>
                   <snm>Allen</snm>
                   <fnm>NH</fnm>
                </au>
                <au>
                   <snm>McNatt</snm>
                   <fnm>Z</fnm>
                </au>
                <au>
                   <snm>Gebremedhen</snm>
                   <fnm>YD</fnm>
                </au>
             </aug>
             <source>Patient Saf Surg</source>
             <pubdate>2014</pubdate>
             <volume>8</volume>
             <fpage>16</fpage>
             <xrefbib>
                <pubidlist>
                   <pubid idtype="doi">10.1186/1754-9493-8-16</pubid>
                   <pubid idtype="pmpid" link="fulltext">24678854</pubid>
                </pubidlist>
             </xrefbib>
          </bibl>
          <bibl id="B6">
             <title>
                <p>Learning from aviation safety: a call for formal &#8220;readbacks&#8221; in surgery</p>
             </title>
             <aug>
                <au>
                   <snm>Stahel</snm>
                   <fnm>PF</fnm>
                </au>
             </aug>
             <source>Patient Saf Surg</source>
             <pubdate>2008</pubdate>
             <volume>2</volume>
             <fpage>21</fpage>
             <xrefbib>
                <pubidlist>
                   <pubid idtype="doi">10.1186/1754-9493-2-21</pubid>
                   <pubid idtype="pmpid" link="fulltext">18798989</pubid>
                </pubidlist>
             </xrefbib>
          </bibl>
          <bibl id="B7">
             <title>
                <p>Wrong site and wrong patient procedures in the era of the Universal Protocol &#8211; analysis of a prospective database of physician self-reported occurrences</p>
             </title>
             <aug>
                <au>
                   <snm>Stahel</snm>
                   <fnm>PF</fnm>
                </au>
                <au>
                   <snm>Sabel</snm>
                   <fnm>AL</fnm>
                </au>
                <au>
                   <snm>Victoroff</snm>
                   <fnm>MS</fnm>
                </au>
                <au>
                   <snm>Varnell</snm>
                   <fnm>J</fnm>
                </au>
                <au>
                   <snm>Lembitz</snm>
                   <fnm>A</fnm>
                </au>
                <au>
                   <snm>Boyle</snm>
                   <fnm>DJ</fnm>
                </au>
                <au>
                   <snm>Clarke</snm>
                   <fnm>TJ</fnm>
                </au>
                <au>
                   <snm>Smith</snm>
                   <fnm>WR</fnm>
                </au>
                <au>
                   <snm>Mehler</snm>
                   <fnm>PS</fnm>
                </au>
             </aug>
             <source>Arch Surg</source>
             <pubdate>2010</pubdate>
             <volume>145</volume>
             <fpage>78</fpage>
             <lpage>84</lpage>
             <xrefbib>
                <pubidlist>
                   <pubid idtype="doi">10.1001/archsurg.2010.185</pubid>
                </pubidlist>
             </xrefbib>
          </bibl>
          <bibl id="B8">
             <title>
                <p>Surgical checklists: the human factor</p>
             </title>
             <aug>
                <au>
                   <snm>O&#8217;Connor</snm>
                   <fnm>P</fnm>
                </au>
                <au>
                   <snm>Reddin</snm>
                   <fnm>C</fnm>
                </au>
                <au>
                   <snm>O&#8217;Sullivan</snm>
                   <fnm>M</fnm>
                </au>
                <au>
                   <snm>O&#8217;Duffy</snm>
                   <fnm>F</fnm>
                </au>
                <au>
                   <snm>Keogh</snm>
                   <fnm>I</fnm>
                </au>
             </aug>
             <source>Patient Saf Surg</source>
             <pubdate>2013</pubdate>
             <volume>7</volume>
             <fpage>14</fpage>
             <xrefbib>
                <pubidlist>
                   <pubid idtype="doi">10.1186/1754-9493-7-14</pubid>
                   <pubid idtype="pmpid" link="fulltext">23672665</pubid>
                </pubidlist>
             </xrefbib>
          </bibl>
       </refgrp>
    </bm>
 </art>