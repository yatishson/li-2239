<art>
    <ui>s13019-014-0138-0</ui>
    <ji>1749-8090</ji>
    <fm>
       <dochead>Case report</dochead>
       <bibl>
          <title>
             <p>Bilateral trans-radial approach in stenting of occluded right axillary artery</p>
          </title>
          <aug>
             <au id="A1" ca="yes">
                <snm>Kedev</snm>
                <fnm>Sasko</fnm>
                <insr iid="I1"/>
                <email>skedev@gmail.com</email>
             </au>
             <au id="A2">
                <snm>Jovkovski</snm>
                <fnm>Aleksandar</fnm>
                <insr iid="I1"/>
                <email>aleksandarjovkovski@yahoo.com</email>
             </au>
             <au id="A3">
                <snm>Zafirovska</snm>
                <fnm>Biljana</fnm>
                <insr iid="I1"/>
                <email>bibi_zafir@yahoo.com</email>
             </au>
          </aug>
          <insg>
             <ins id="I1">
                <p>Interventional Cardiology Department, University Clinic of Cardiology, Medical Faculty, University of St.Cyril &amp; Methodius, Vodnjanska 17, Skopje 1000, Macedonia</p>
             </ins>
          </insg>
          <source>Journal of Cardiothoracic Surgery</source>
          <issn>1749-8090</issn>
          <pubdate>2014</pubdate>
          <volume>9</volume>
          <issue>1</issue>
          <fpage>138</fpage>
          <url>http://www.cardiothoracicsurgery.org/content/9/1/138</url>
          <xrefbib>
             <pubidlist>
                <pubid idtype="doi">10.1186/s13019-014-0138-0</pubid>
                <pubid idtype="pmpid"><!-- Need to check source for data--></pubid>
             </pubidlist>
          </xrefbib>
       </bibl>
       <history>
          <rec>
             <date>
                <day>16</day>
                <month>2</month>
                <year>2014</year>
             </date>
          </rec>
          <acc>
             <date>
                <day>18</day>
                <month>7</month>
                <year>2014</year>
             </date>
          </acc>
          <pub>
             <date>
                <day>23</day>
                <month>8</month>
                <year>2014</year>
             </date>
          </pub>
       </history>
       <cpyrt>
          <year>2014</year>
          <collab>Kedev et al.; licensee BioMed Central.</collab>
          <note>This is an Open Access article distributed under the terms of the Creative Commons Attribution License (<url>http://creativecommons.org/licenses/by/4.0</url>), which permits unrestricted use, distribution, and reproduction in any medium, provided the original work is properly credited. The Creative Commons Public Domain Dedication waiver (<url>http://creativecommons.org/publicdomain/zero/1.0/</url>) applies to the data made available in this article, unless otherwise stated.</note>
       </cpyrt>
       <kwdg>
          <kwd>Transradial approach (TRA)</kwd>
          <kwd>Axillary artery stenting</kwd>
          <kwd>Chronic total occlusion (CTO)</kwd>
          <kwd>Peripheral artery disease</kwd>
       </kwdg>
       <abs>
          <sec>
             <st>
                <p>Abstract</p>
             </st>
             <p>With recent advancement in percutaneous endovascular interventions, angioplasty and stenting of axillary artery lesions could become the treatment of choice vs. surgical intervention owing to its lower complication and mortality rates and shorter hospital stay.</p>
             <p>We report a Caucasian female case with axillary artery chronic total occlusion (CTO) with dual etiology (atherosclerotic and radiation induced), which was successfully managed with stent angioplasty. The strategy used was right radial retrograde approach with contralateral injections from left radial catheter. Two year follow-up revealed widely patent axillary stents.</p>
          </sec>
       </abs>
    </fm>
    <bdy>
       <sec>
          <st>
             <p>Background</p>
          </st>
          <p>There is no published data for stenting of occluded axillary artery, through the radial access. We report a case with a novel, retrograde approach for stenting of occluded right axillary artery using bilateral radial approach. The procedure was successful with patent stents after two years of follow up with color duplex-ultrasonography and angiography.</p>
       </sec>
       <sec>
          <st>
             <p>Case presentation</p>
          </st>
          <p>Our patient is 77&#160;years old Caucasian female with coldness and pain in the right arm during minimal physical strain and some movement inability in the same arm. The symptoms began 1&#160;year prior to hospitalization with worsening of the symptoms 2&#8211;3 weeks ago, with augmentation of the pain in the arm that limited her everyday&#8217;s home based activities. The patient had undergone duplex-ultrasonography of the upper limbs, with high suspicion of occlusion of the right axillary artery. The patient did not have prior history of trauma of the upper limbs, but had prior history of mastectomy 16&#160;years ago treated with concomitant radiotherapy as well. From risk factors she was a smoker with hypertension and diabetes. On physical finding there was difference in systolic blood pressure readings of 50&#160;mmHg (80&#160;mmHg in the right brachial artery and 130&#160;mmHg in the left brachial artery). The right radial and ulnar arteries were hardly palpable. The patient did not have any cardiac symptoms, 2D echo showed EF of 65% without enlargement of the cardiac chambers or valvular disease. All relevant laboratory examinations were within normal range. Patient was loaded with 600&#160;mg clopidogrel and was already on 100&#160;mg of aspirin daily before intervention. She was also pre-hydrated with 0,9% normal saline 6&#160;hours before intervention. Written, informed consent was obtained and the procedure has been approved by the IRB of University Clinic of Cardiology, Medical Faculty in Skopje under the Presidency of Prof. Lidija Dobrkovic and members Associate Prof. Marija Vavlukis and Katerina Dimovska RN.</p>
          <p>Coronary and peripheral angiography were performed through the left radial artery before attempting angioplasty of the occluded right axillary artery in order to exclude significant and relevant concomitant coronary artery disease.</p>
          <p>There was no significant coronary artery disease. Peripheral angiography with Simmons 2 5&#160;F diagnostic catheter, (Supertorque plus, Cordis) revealed long segment of calcified CTO of the right axillary artery. We decided on using bilateral radial access and retrograde approach from right radial artery (Figure&#160;<figr fid="F1">1</figr>).</p>
          
             <fig id="F1">
                <title>
                   <p>Figure 1</p>
                </title>
                <caption>
                   <p>Bilateral transradial approach with 6&#160;F introducers.</p>
                </caption>
                <text>
                   <p>
                      <b>Bilateral transradial approach with 6&#160;F introducers.</b>
                   </p>
                </text>
                <graphic file="s13019-014-0138-0-1"/>
             </fig>
          
          <p>Puncturing the right radial artery due to diminished and nearly absent radial pulse was practically challenging. Radial artery was accessed after local infiltration with 1 to 1.5&#160;ml 2% lidocaine, using counter puncture technique with a 20&#160;G plastic iv cannula and 0.025 inch mini guide-wire of 45&#160;cm and followed by 6Fr hydrophilic introducer sheath (Terumo, Japan) placement. Spasmolytic cocktail (5&#160;mg verapamil) was given intra-arterially through the radial sheath. The delayed digital subtraction angiograms before procedure confirmed long chronic total occlusion of right axillary artery.</p>
          <p>Based on the experience with long coronary CTO lesions, retrograde approach is preferred owing to the softer plaque consistency on the distal part of the CTO. During the angioplasty we needed frequent antegrade contrast injections in order to control guidewire advancement and to ensure true lumen positioning. Contralateral injections are essential in providing evidence for wiring through the true lumen and safe distal reentry of the CTO wire.</p>
          <p>From the right arm we used JR 4.0 6&#160;F guiding catheter (Launcher, Medtronic), and from the left arm Simmons 2 5Fr diagnostic catheter was utilized for contralateral injections. The occlusion and collaterals were documented with simultaneous bilateral injections (Figure&#160;<figr fid="F2">2</figr>). Contralateral injection confirmed proper wire advancement within the lesion. The occlusion was successfully crossed with hydrophilic 0.014&#8243; wire (HT Pilot 150, Abbott Vascular) (Figure&#160;<figr fid="F3">3</figr>). Balloon predilatation was made with balloon catheter 3.00/30&#160;mm, (Sprinter, Medtronic) (Figure&#160;<figr fid="F4">4</figr>). Afterward, CTO was recrossed with the 6&#160;F GC in order to exchange the HT Pilot 150 guidewire with heavily supportive 0.014&#8243; Iron Man 300&#160;mm guidewire (Abbott Vascular). Two Xpert self-expandable stents 8.0/40&#160;mm (Abbott Vascular) were advanced solely through guidewire without guiding catheter (Figure&#160;<figr fid="F5">5</figr>).</p>
          
             <fig id="F2">
                <title>
                   <p>Figure 2</p>
                </title>
                <caption>
                   <p>Peripheral angiogram of occluded right axillary artery with bilateral transradial approach.</p>
                </caption>
                <text>
                   <p>
                      <b>Peripheral angiogram of occluded right axillary artery with bilateral transradial approach.</b> Simultaneous injection through JR 4.0 6&#160;F GC from the right radial artery and Simmons 2 5&#160;F diagnostic catheter from the left RA.</p>
                </text>
                <graphic file="s13019-014-0138-0-2"/>
             </fig>
          
          
             <fig id="F3">
                <title>
                   <p>Figure 3</p>
                </title>
                <caption>
                   <p>Crossing the lesion with hydrophilic 0.014&#8243; guidewire.</p>
                </caption>
                <text>
                   <p>
                      <b>Crossing the lesion with hydrophilic 0.014&#8243; guidewire.</b>
                   </p>
                </text>
                <graphic file="s13019-014-0138-0-3"/>
             </fig>
          
          
             <fig id="F4">
                <title>
                   <p>Figure 4</p>
                </title>
                <caption>
                   <p>Predilatation with balloon catheter 3.0/30&#160;mm.</p>
                </caption>
                <text>
                   <p>
                      <b>Predilatation with balloon catheter 3.0/30&#160;mm.</b>
                   </p>
                </text>
                <graphic file="s13019-014-0138-0-4"/>
             </fig>
          
          
             <fig id="F5">
                <title>
                   <p>Figure 5</p>
                </title>
                <caption>
                   <p>Implantation of two self-expandable stents Xpert 8.0/40&#160;mm.</p>
                </caption>
                <text>
                   <p>
                      <b>Implantation of two self-expandable stents Xpert 8.0/40&#160;mm.</b>
                   </p>
                </text>
                <graphic file="s13019-014-0138-0-5"/>
             </fig>
          
          <p>Proper positioning was reconfirmed with contralateral injections through the Simmons 2 catheter. Post dilatation was made with 7.00/30&#160;mm balloon catheter (Viatrac 14 plus, Abbott Vascular) at high pressure. Final angiography showed normal brisk flow through the deployed stents (Figure&#160;<figr fid="F6">6</figr>). We applied patent haemostasis on the puncture sites. Total amount of 320&#160;ml of contrast has been used, with fluoroscopy time of 29&#160;min. The day after intervention control duplex-ultrasonography revealed normal and symmetric arterial signals of both arms. The pain in the right arm was relieved after intervention and patient was discharged the following day without any bleeding or vascular complications.</p>
          
             <fig id="F6">
                <title>
                   <p>Figure 6</p>
                </title>
                <caption>
                   <p>Final angiography with normal flow through the two stents deployed over the axillary artery occlusion.</p>
                </caption>
                <text>
                   <p>
                      <b>Final angiography with normal flow through the two stents deployed over the axillary artery occlusion.</b>
                   </p>
                </text>
                <graphic file="s13019-014-0138-0-6"/>
             </fig>
          
          <p>At two years follow up patient was completely asymptomatic, without any signs of right arm ischemia. Ipsilateral radial and ulnar artery pulsations were normal. Control duplex ultrasonography showed patent stent and normal flow in the right axillary artery. Both stents were widely patent at two years follow up angiography. (Figures&#160;<figr fid="F7">7</figr> and <figr fid="F8">8</figr>).</p>
          
             <fig id="F7">
                <title>
                   <p>Figure 7</p>
                </title>
                <caption>
                   <p>Two years follow up fluoroscopy.</p>
                </caption>
                <text>
                   <p>
                      <b>Two years follow up fluoroscopy.</b>
                   </p>
                </text>
                <graphic file="s13019-014-0138-0-7"/>
             </fig>
          
          
             <fig id="F8">
                <title>
                   <p>Figure 8</p>
                </title>
                <caption>
                   <p>Two years follow up angiography.</p>
                </caption>
                <text>
                   <p>
                      <b>Two years follow up angiography.</b>
                   </p>
                </text>
                <graphic file="s13019-014-0138-0-8"/>
             </fig>
          
       </sec>
       <sec>
          <st>
             <p>Discussion</p>
          </st>
          <p>There are various reasons for axillary artery stenosis as mechanical injuries, Takayasu aoro-arteritis, giant cell arteritis and radiation induced arteritis. However, atherosclerotic axillary artery disease is most commonly met in clinical practice, especially in hemodynamically unstable lesions <abbrgrp>
                <abbr bid="B1">1</abbr>
             </abbrgrp>,<abbrgrp>
                <abbr bid="B2">2</abbr>
             </abbrgrp>. Most lesions on the axillary artery in patients are symptomatic <abbrgrp>
                <abbr bid="B1">1</abbr>
             </abbrgrp>. In our case there was dual etiology for the CTO: radiation induced arteritis and atherosclerosis as well <abbrgrp>
                <abbr bid="B3">3</abbr>
             </abbrgrp>.</p>
          <p>Angioplasty and recanalisation of the CTO lesion in this particular case relieved the disabling symptoms and enabled patient&#8217;s normal functioning in daily activities.</p>
          <p>There are few published cases of stenting of axillary artery <abbrgrp>
                <abbr bid="B1">1</abbr>
             </abbrgrp>&#8211;<abbrgrp>
                <abbr bid="B6">6</abbr>
             </abbrgrp>, and no data about using bilateral transradial approach. Surgical interventions in these lesions showed higher rate of complications as stroke and TIA and also higher mortality rate <abbrgrp>
                <abbr bid="B6">6</abbr>
             </abbrgrp>,<abbrgrp>
                <abbr bid="B7">7</abbr>
             </abbrgrp>. However, percutaneous angioplasty of the axillary artery is associated with less neurological complications <abbrgrp>
                <abbr bid="B5">5</abbr>
             </abbrgrp>,<abbrgrp>
                <abbr bid="B6">6</abbr>
             </abbrgrp>,<abbrgrp>
                <abbr bid="B8">8</abbr>
             </abbrgrp>. Technically, the most important aspect is to avoid vertebral and common carotid artery take off. In our particular case occlusion was distal to the vertebral artery as well as common carotid artery. The retrograde right radial access allowed wire manipulation without involving takeoffs of supraaortic vessels.</p>
          <p>The alternative transfemoral acces would necessitate larger bore guiding catheter (7Fr) and antegrade access of the CTO. Most of the currently available peripheral stents require at least 7&#160;F guiding catheter or long 6&#160;F guiding sheath. In our case we performed axillary artery stenting through short 6&#160;F sheath (10&#160;cm) from the right radial artery. This was only possible with contralateral control from the diagnostic catheter from the left radial access that allowed proper stent positioning. Proximal cap of the CTO is usually harder than the distal end of the CTO. In our case, contralateral injections allowed delivery and proper positioning of two stents only through the 0.014&#8243; guidewire; without guiding catheter and without the risk of involving the ipsilateral vertebral and innominate arteries.</p>
          <p>Furthermore, transradial approach has been proven to have less bleeding and vascular complications in comparison to transfemoral approach in patients undergoing PCI or endovascular procedures <abbrgrp>
                <abbr bid="B9">9</abbr>
             </abbrgrp>&#8211;<abbrgrp>
                <abbr bid="B11">11</abbr>
             </abbrgrp>.</p>
       </sec>
       <sec>
          <st>
             <p>Conclusion</p>
          </st>
          <p>Retrograde approach with bilateral radial access is feasible and safe strategy in recanalisation of the axillary artery chronic total occlusion. Further reports and studies are needed to compare alternative techniques in treating complex axillary artery lesions.</p>
       </sec>
       <sec>
          <st>
             <p>Consent</p>
          </st>
          <p>Written informed consent was obtained from the patient for publication of this Case report and any accompanying images. A copy of the written consent is available for review by the Editor-in-Chief of this journal.</p>
       </sec>
       <sec>
          <st>
             <p>Abbreviations</p>
          </st>
          <p>TRA: Transradial approach</p>
          <p>CTO: Chronic total occlusion</p>
       </sec>
       <sec>
          <st>
             <p>Competing interests</p>
          </st>
          <p>The authors declare that they have no competing interests.</p>
       </sec>
       <sec>
          <st>
             <p>Authors&#8217; contributions</p>
          </st>
          <p>SK has participated in conceiving of the study, has performed the interventional procedure and participated in drafting and finalizing the manuscript. AJ has participated in the management of the patient, interpretation of the results, and drafting of the manuscript. BZ has conceived of the study and participated in drafting and finalizing the manuscript and has written most of the manuscript. All authors read and approved the final manuscript.</p>
       </sec>
       <sec>
          <st>
             <p>Authors&#8217; information</p>
          </st>
          <p>SK &#8211; is the director of the University Clinic of Cardiology where the patient was treated. He is also a professor at the Ss. Cyril and Methodius University Medical Faculty in Skopje, Macedonia.</p>
       </sec>
    </bdy>
    <bm>
       <refgrp>
          
          <bibl id="B1">
             <title>
                <p>Primary stenting of subclavian and innominate artery occlusive disease: a single center&#8217;s experience</p>
             </title>
             <aug>
                <au>
                   <snm>Brountzos</snm>
                   <fnm>EN</fnm>
                </au>
                <au>
                   <snm>Petersen</snm>
                   <fnm>B</fnm>
                </au>
                <au>
                   <snm>Binkert</snm>
                   <fnm>C</fnm>
                </au>
                <au>
                   <snm>Panagiotou</snm>
                   <fnm>I</fnm>
                </au>
                <au>
                   <snm>Kaufman</snm>
                   <fnm>JA</fnm>
                </au>
             </aug>
             <source>Cardiovasc Intervent Radiol</source>
             <pubdate>2004</pubdate>
             <volume>27</volume>
             <fpage>616</fpage>
             <lpage>623</lpage>
             <xrefbib>
                <pubidlist>
                   <pubid idtype="doi">10.1007/s00270-004-0218-y</pubid>
                   <pubid idtype="pmpid" link="fulltext">15578138</pubid>
                </pubidlist>
             </xrefbib>
          </bibl>
          <bibl id="B2">
             <title>
                <p>Results after balloon angioplasty or stenting of atherosclerotic subclavian artery obstruction</p>
             </title>
             <aug>
                <au>
                   <snm>Sixt</snm>
                   <fnm>S</fnm>
                </au>
                <au>
                   <snm>Rastan</snm>
                   <fnm>A</fnm>
                </au>
                <au>
                   <snm>Schwarzwalder</snm>
                   <fnm>U</fnm>
                </au>
                <au>
                   <snm>B&#252;rgelin</snm>
                   <fnm>K</fnm>
                </au>
                <au>
                   <snm>Noory</snm>
                   <fnm>E</fnm>
                </au>
                <au>
                   <snm>Schwarz</snm>
                   <fnm>T</fnm>
                </au>
                <au>
                   <snm>Beschorner</snm>
                   <fnm>U</fnm>
                </au>
                <au>
                   <snm>Frank</snm>
                   <fnm>U</fnm>
                </au>
                <au>
                   <snm>M&#252;ller</snm>
                   <fnm>C</fnm>
                </au>
                <au>
                   <snm>Hauk</snm>
                   <fnm>M</fnm>
                </au>
                <au>
                   <snm>Leppanen</snm>
                   <fnm>O</fnm>
                </au>
                <au>
                   <snm>Hauswald</snm>
                   <fnm>K</fnm>
                </au>
                <au>
                   <snm>Brantner</snm>
                   <fnm>R</fnm>
                </au>
                <au>
                   <snm>Nazary</snm>
                   <fnm>T</fnm>
                </au>
                <au>
                   <snm>Neumann</snm>
                   <fnm>FJ</fnm>
                </au>
                <au>
                   <snm>Zeller</snm>
                   <fnm>T</fnm>
                </au>
             </aug>
             <source>Cathet Cardiovasc Interv</source>
             <pubdate>2009</pubdate>
             <volume>73</volume>
             <fpage>395</fpage>
             <lpage>403</lpage>
             <xrefbib>
                <pubidlist>
                   <pubid idtype="doi">10.1002/ccd.21836</pubid>
                </pubidlist>
             </xrefbib>
          </bibl>
          <bibl id="B3">
             <title>
                <p>Percutaneous intervention for radiation damage to axillary arteries</p>
             </title>
             <aug>
                <au>
                   <snm>McBride</snm>
                   <fnm>KD</fnm>
                </au>
                <au>
                   <snm>Beard</snm>
                   <fnm>JD</fnm>
                </au>
                <au>
                   <snm>Gaines</snm>
                   <fnm>PA</fnm>
                </au>
             </aug>
             <source>Clin Radiol</source>
             <pubdate>1994</pubdate>
             <volume>49</volume>
             <fpage>630</fpage>
             <lpage>633</lpage>
             <xrefbib>
                <pubidlist>
                   <pubid idtype="doi">10.1016/S0009-9260(05)81881-1</pubid>
                   <pubid idtype="pmpid" link="fulltext">7955891</pubid>
                </pubidlist>
             </xrefbib>
          </bibl>
          <bibl id="B4">
             <title>
                <p>Percutaneous endovascular management of atherosclerotic axillary artery stenosis: Report of 2 cases and review of literature</p>
             </title>
             <aug>
                <au>
                   <snm>Vijayvergiya</snm>
                   <fnm>R</fnm>
                </au>
                <au>
                   <snm>Yadav</snm>
                   <fnm>M</fnm>
                </au>
                <au>
                   <snm>Grover</snm>
                   <fnm>A</fnm>
                </au>
             </aug>
             <source>World J Cardiol</source>
             <pubdate>2011</pubdate>
             <volume>3</volume>
             <issue>5</issue>
             <fpage>65</fpage>
             <lpage>168</lpage>
             <note>ISSN 1949&#8211;8462</note>
             <xrefbib>
                <pubidlist>
                   <pubid idtype="doi">10.4330/wjc.v3.i5.165</pubid>
                </pubidlist>
             </xrefbib>
          </bibl>
          <bibl id="B5">
             <title>
                <p>Percutaneous endovascular treatment of innominate artery lesions: a single-centre experience on 77 lesions</p>
             </title>
             <aug>
                <au>
                   <snm>Paukovits</snm>
                   <fnm>TM</fnm>
                </au>
                <au>
                   <snm>Lukacs</snm>
                   <fnm>L</fnm>
                </au>
                <au>
                   <snm>Berczi</snm>
                   <fnm>V</fnm>
                </au>
                <au>
                   <snm>Hirschberg</snm>
                   <fnm>K</fnm>
                </au>
                <au>
                   <snm>Nemes</snm>
                   <fnm>B</fnm>
                </au>
                <au>
                   <snm>Huttl</snm>
                   <fnm>K</fnm>
                </au>
             </aug>
             <source>Eur J Vasc Endovasc Surg</source>
             <pubdate>2010</pubdate>
             <volume>40</volume>
             <fpage>35</fpage>
             <lpage>43</lpage>
             <xrefbib>
                <pubidlist>
                   <pubid idtype="doi">10.1016/j.ejvs.2010.03.017</pubid>
                   <pubid idtype="pmpid" link="fulltext">20435490</pubid>
                </pubidlist>
             </xrefbib>
          </bibl>
          <bibl id="B6">
             <title>
                <p>Open and endovascular management of subclavian and innominate arterial pathology</p>
             </title>
             <aug>
                <au>
                   <snm>Aiello</snm>
                   <fnm>F</fnm>
                </au>
                <au>
                   <snm>Morrissey</snm>
                   <fnm>NJ</fnm>
                </au>
             </aug>
             <source>Semin Vasc Surg</source>
             <pubdate>2011</pubdate>
             <volume>24</volume>
             <issue>1</issue>
             <fpage>31</fpage>
             <lpage>35</lpage>
             <xrefbib>
                <pubidlist>
                   <pubid idtype="doi">10.1053/j.semvascsurg.2011.04.001</pubid>
                   <pubid idtype="pmpid" link="fulltext">21718930</pubid>
                </pubidlist>
             </xrefbib>
          </bibl>
          <bibl id="B7">
             <title>
                <p>Atherosclerotic innominate artery occlusive disease: early and long-term results of surgical reconstruction</p>
             </title>
             <aug>
                <au>
                   <snm>Kieffer</snm>
                   <fnm>E</fnm>
                </au>
                <au>
                   <snm>Sabatier</snm>
                   <fnm>J</fnm>
                </au>
                <au>
                   <snm>Koskas</snm>
                   <fnm>F</fnm>
                </au>
                <au>
                   <snm>Bahnini</snm>
                   <fnm>A</fnm>
                </au>
             </aug>
             <source>J Vasc Surg</source>
             <pubdate>1995</pubdate>
             <volume>21</volume>
             <fpage>326</fpage>
             <lpage>337</lpage>
             <xrefbib>
                <pubidlist>
                   <pubid idtype="doi">10.1016/S0741-5214(95)70273-3</pubid>
                   <pubid idtype="pmpid" link="fulltext">7853604</pubid>
                </pubidlist>
             </xrefbib>
          </bibl>
          <bibl id="B8">
             <title>
                <p>Angioplasty of the innominate artery in 89 patients: experience over 19&#160;years</p>
             </title>
             <aug>
                <au>
                   <snm>Huttl</snm>
                   <fnm>K</fnm>
                </au>
                <au>
                   <snm>Nemes</snm>
                   <fnm>B</fnm>
                </au>
                <au>
                   <snm>Simonffy</snm>
                   <fnm>A</fnm>
                </au>
                <au>
                   <snm>Entz</snm>
                   <fnm>L</fnm>
                </au>
                <au>
                   <snm>Berczi</snm>
                   <fnm>V</fnm>
                </au>
             </aug>
             <source>Cardiovasc Intervent Radiol</source>
             <pubdate>2002</pubdate>
             <volume>25</volume>
             <fpage>109</fpage>
             <lpage>114</lpage>
             <xrefbib>
                <pubidlist>
                   <pubid idtype="doi">10.1007/s00270-001-0074-y</pubid>
                   <pubid idtype="pmpid" link="fulltext">11901427</pubid>
                </pubidlist>
             </xrefbib>
          </bibl>
          <bibl id="B9">
             <note>Caputo RP, Tremmel JA, Rao S, Gilchrist IC, Pyne C, Pancholy S, Frasier D, Gulati R, Skelding K, Bertrand O, Patel T: <b>Transradial arterial access for coronary and peripheral procedures: Executive summary by the transradial committee of the SCAI.</b>
                <it>Catheter Cardiovasc Interv</it> 2011; 10.1002/ccd.23052.</note>
          </bibl>
          <bibl id="B10">
             <title>
                <p>Radial versus femoral access for coronary angiography or intervention and the impact on major bleeding and ischemic events: A systematic review and meta-analysis of randomized trials</p>
             </title>
             <aug>
                <au>
                   <snm>Jolly</snm>
                   <fnm>SS</fnm>
                </au>
                <au>
                   <snm>Amlani</snm>
                   <fnm>S</fnm>
                </au>
                <au>
                   <snm>Hamon</snm>
                   <fnm>M</fnm>
                </au>
                <au>
                   <snm>Mehta</snm>
                   <fnm>SR</fnm>
                </au>
             </aug>
             <source>Am Heart J</source>
             <pubdate>2009</pubdate>
             <volume>157</volume>
             <fpage>132</fpage>
             <lpage>140</lpage>
             <xrefbib>
                <pubidlist>
                   <pubid idtype="doi">10.1016/j.ahj.2008.08.023</pubid>
                   <pubid idtype="pmpid" link="fulltext">19081409</pubid>
                </pubidlist>
             </xrefbib>
          </bibl>
          <bibl id="B11">
             <title>
                <p>Radial versus femoral approach for percutaneous coronary diagnostic and interventional procedures; Systematic overview and meta-analysis of randomized trials</p>
             </title>
             <aug>
                <au>
                   <snm>Agostoni</snm>
                   <fnm>P</fnm>
                </au>
                <au>
                   <snm>Biondi-Zoccai</snm>
                   <fnm>GG</fnm>
                </au>
                <au>
                   <snm>de Benedictis</snm>
                   <fnm>ML</fnm>
                </au>
                <au>
                   <snm>Rigattieri</snm>
                   <fnm>S</fnm>
                </au>
                <au>
                   <snm>Turri</snm>
                   <fnm>M</fnm>
                </au>
                <au>
                   <snm>Anselmi</snm>
                   <fnm>M</fnm>
                </au>
                <au>
                   <snm>Vassanelli</snm>
                   <fnm>C</fnm>
                </au>
                <au>
                   <snm>Zardini</snm>
                   <fnm>P</fnm>
                </au>
                <au>
                   <snm>Louvard</snm>
                   <fnm>Y</fnm>
                </au>
                <au>
                   <snm>Hamon</snm>
                   <fnm>M</fnm>
                </au>
             </aug>
             <source>J Am Coll Cardiol</source>
             <pubdate>2004</pubdate>
             <volume>44</volume>
             <fpage>349</fpage>
             <lpage>356</lpage>
             <xrefbib>
                <pubidlist>
                   <pubid idtype="doi">10.1016/j.jacc.2004.04.034</pubid>
                   <pubid idtype="pmpid" link="fulltext">15261930</pubid>
                </pubidlist>
             </xrefbib>
          </bibl>
       </refgrp>
    </bm>
 </art>
