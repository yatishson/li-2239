<art>
    <ui>s12918-014-0105-3</ui>
    <ji>1752-0509</ji>
    <fm>
       <dochead>Retraction Note</dochead>
       <bibl>
          <title>
             <p>Retraction Note: Predicting new molecular targets for rhein using network pharmacology</p>
          </title>
          <aug>
             <au id="A1">
                <snm>Zhang</snm>
                <fnm>Aihua</fnm>
                <insr iid="I1"/>
                <insr iid="I2"/>
                <email>xijunwangls@126.com</email>
             </au>
             <au id="A2">
                <snm>Sun</snm>
                <fnm>Hui</fnm>
                <insr iid="I1"/>
                <insr iid="I2"/>
                <email>xijunwangls@126.com</email>
             </au>
             <au id="A3">
                <snm>Yang</snm>
                <fnm>Bo</fnm>
                <insr iid="I1"/>
                <insr iid="I2"/>
                <email>xijunwangls@126.com</email>
             </au>
             <au id="A4" ca="yes">
                <snm>Wang</snm>
                <fnm>Xijun</fnm>
                <insr iid="I1"/>
                <insr iid="I2"/>
                <email>xijunwangls@126.com</email>
             </au>
          </aug>
          <insg>
             <ins id="I1">
                <p>National TCM Key Lab of Serum Pharmacochemistry, Heilongjiang University, of Chinese Medicine, Heping Road 24, Harbin 150040, China</p>
             </ins>
             <ins id="I2">
                <p>Key Pharmacometabolomics Platform of Chinese Medicines, Heping Road 24, Harbin 150040, China</p>
             </ins>
          </insg>
          <source>BMC Systems Biology</source>
          <issn>1752-0509</issn>
          <pubdate>2014</pubdate>
          <volume>8</volume>
          <issue>1</issue>
          <fpage>105</fpage>
          <url>http://www.biomedcentral.com/1752-0509/8/105</url>
          <xrefbib>
             <pubidlist>
                <pubid idtype="doi">10.1186/s12918-014-0105-3</pubid>
                <pubid idtype="pmpid"><!-- Need to check source for data--></pubid>
             </pubidlist>
          </xrefbib>
       </bibl>
       <history>
          <rec>
             <date>
                <day>12</day>
                <month>8</month>
                <year>2014</year>
             </date>
          </rec>
          <acc>
             <date>
                <day>14</day>
                <month>8</month>
                <year>2014</year>
             </date>
          </acc>
          <pub>
             <date>
                <day>18</day>
                <month>9</month>
                <year>2014</year>
             </date>
          </pub>
       </history>
       <cpyrt>
          <year>2014</year>
          <collab>Zhang et al.; licensee BioMed Central</collab>
          <note>This is an Open Access article distributed under the terms of the Creative Commons Attribution License (<url>http://creativecommons.org/licenses/by/4.0</url>), which permits unrestricted use, distribution, and reproduction in any medium, provided the original work is properly credited. The Creative Commons Public Domain Dedication waiver (<url>http://creativecommons.org/publicdomain/zero/1.0/</url>) applies to the data made available in this article, unless otherwise stated.</note>
       </cpyrt>
	   <abs><sec><st>
 <p>Abstract</p>
 </st>
 <p>No abstract.</p></sec></abs>
    </fm>
    <bdy>
       <sec>
          <st>
             <p>Retraction</p>
          </st>
          <p>The Editors regretfully retract the article <abbrgrp>
                <abbr bid="B1">1</abbr>
             </abbrgrp> as we believe the peer-review process was compromised and inappropriately influenced by the authors. Following further post-publication peer review the Editors no longer have confidence in the soundness of the findings. We apologise to all affected parties for the inconvenience caused.</p>
       </sec>
    </bdy>
    <bm>
       <refgrp>
          <title>
             <p>Reference</p>
          </title>
          <bibl id="B1">
             <title>
                <p>Predicting new molecular targets for rhein using network pharmacology</p>
             </title>
             <aug>
                <au>
                   <snm>Zhang</snm>
                   <fnm>A</fnm>
                </au>
                <au>
                   <snm>Sun</snm>
                   <fnm>H</fnm>
                </au>
                <au>
                   <snm>Yang</snm>
                   <fnm>B</fnm>
                </au>
                <au>
                   <snm>Wang</snm>
                   <fnm>X</fnm>
                </au>
             </aug>
             <source>BMC Syst Biol</source>
             <pubdate>2012</pubdate>
             <volume>6</volume>
             <fpage>20</fpage>
             <xrefbib>
                <pubidlist>
                   <pubid idtype="doi">10.1186/1752-0509-6-20</pubid>
                   <pubid idtype="pmpid" link="fulltext">22433437</pubid>
                </pubidlist>
             </xrefbib>
          </bibl>
       </refgrp>
    </bm>
 </art>
