<art>
    <ui>s12879-014-0614-0</ui>
    <ji>1471-2334</ji>
    <fm>
       <dochead>Case report</dochead>
       <bibl>
          <title>
             <p>Tick borne encephalitis without cerebrospinal fluid pleocytosis</p>
          </title>
          <aug>
             <au id="A1" ca="yes">
                <snm>Stupica</snm>
                <fnm>Da&#353;a</fnm>
                <insr iid="I1"/>
                <email>dasa.stupica@kclj.si</email>
             </au>
             <au id="A2">
                <snm>Strle</snm>
                <fnm>Franc</fnm>
                <insr iid="I1"/>
                <email>franc.strle@kclj.si</email>
             </au>
             <au id="A3">
                <snm>Av&#353;i&#269;-&#381;upanc</snm>
                <fnm>Tatjana</fnm>
                <insr iid="I2"/>
                <email>tatjana.avsic@mf.uni-lj.si</email>
             </au>
             <au id="A4">
                <snm>Logar</snm>
                <fnm>Mateja</fnm>
                <insr iid="I1"/>
                <email>mateja.logar@kclj.si</email>
             </au>
             <au id="A5">
                <snm>Pe&#269;avar</snm>
                <fnm>Bla&#382;</fnm>
                <insr iid="I1"/>
                <email>blaz.pecavar@kclj.si</email>
             </au>
             <au id="A6" ca="yes">
                <snm>Bajrovi&#263;</snm>
                <mi>F</mi>
                <fnm>Fajko</fnm>
                <insr iid="I3"/>
                <insr iid="I4"/>
                <email>fajko.bajrovic@mf.uni-lj.si</email>
             </au>
          </aug>
          <insg>
             <ins id="I1">
                <p>Department of Infectious Diseases, University Medical Center Ljubljana, Japljeva 2, Ljubljana, 1525, Slovenia</p>
             </ins>
             <ins id="I2">
                <p>Institute of Microbiology and Immunology, Faculty of Medicine Ljubljana, Ljubljana, Slovenia</p>
             </ins>
             <ins id="I3">
                <p>Institute of Pathophysiology, Faculty of Medicine Ljubljana, Zalo&#353;ka 4, Ljubljana, 2211, Slovenia</p>
             </ins>
             <ins id="I4">
                <p>Department of Neurology, University Medical Center Ljubljana, Ljubljana, Slovenia</p>
             </ins>
          </insg>
          <source>BMC Infectious Diseases</source>
          <section>
             <title>
                <p>Viral diseases</p>
             </title>
          </section>
          <issn>1471-2334</issn>
          <pubdate>2014</pubdate>
          <volume>14</volume>
          <issue>1</issue>
          <fpage>614</fpage>
          <url>http://www.biomedcentral.com/1471-2334/14/614</url>
          <xrefbib>
             <pubidlist>
                <pubid idtype="doi">10.1186/s12879-014-0614-0</pubid>
             </pubidlist>
          </xrefbib>
       </bibl>
       <history>
          <rec>
             <date>
                <day>23</day>
                <month>4</month>
                <year>2014</year>
             </date>
          </rec>
          <acc>
             <date>
                <day>5</day>
                <month>11</month>
                <year>2014</year>
             </date>
          </acc>
          <pub>
             <date>
                <day>18</day>
                <month>11</month>
                <year>2014</year>
             </date>
          </pub>
       </history>
       <cpyrt>
          <year>2014</year>
          <collab>Stupica et al.; licensee BioMed Central Ltd.</collab>
          <note>This is an Open Access article distributed under the terms of the Creative Commons Attribution License (<url>http://creativecommons.org/licenses/by/4.0</url>), which permits unrestricted use, distribution, and reproduction in any medium, provided the original work is properly credited. The Creative Commons Public Domain Dedication waiver (<url>http://creativecommons.org/publicdomain/zero/1.0/</url>) applies to the data made available in this article, unless otherwise stated.</note>
       </cpyrt>
       <abs>
          <sec>
             <st>
                <p>Abstract</p>
             </st>
             <sec>
                <st>
                   <p>Background</p>
                </st>
                <p>Tick borne encephalitis is the most frequent vector-transmitted infectious disease of the central nervous system in Europe and Asia. The disease caused by European subtype of tick borne encephalitis virus has typically a biphasic clinical course with the second phase presenting as meningitis, meningoencephalitis, or meningoencephalomyelitis. Cerebrospinal fluid pleocytosis is considered a condition sine qua non for the diagnosis of neurologic involvement in tick borne encephalitis, which in routine clinical practice is confirmed by demonstration of serum IgM and IgG antibodies to tick borne encephalitis virus.</p>
             </sec>
             <sec>
                <st>
                   <p>Case presentation</p>
                </st>
                <p>Here we present a patient from Slovenia, an area highly endemic for tick borne encephalitis, with encephalitis but without cerebrospinal fluid pleocytosis in whom tick borne encephalitis virus infection of the central nervous system was demonstrated.</p>
             </sec>
             <sec>
                <st>
                   <p>Conclusion</p>
                </st>
                <p>Cerebrospinal fluid pleocytosis is not mandatory in encephalitis caused by tick borne encephalitis virus. In daily clinical practice, in patients with neurologic symptoms/signs compatible with tick borne encephalitis and the risk of exposure to ticks in a tick borne encephalitis endemic region, the search for central nervous system infection with tick borne encephalitis virus is warranted despite the lack of cerebrospinal fluid pleocytosis.</p>
             </sec>
          </sec>
       </abs>
    </fm>
    <bdy>
       <sec>
          <st>
             <p>Background</p>
          </st>
          <p>Tick borne encephalitis (TBE) is the most frequent vector-transmitted infectious disease of the central nervous system (CNS) in Europe and Asia and is considered an emerging disease due to its rising incidence and the spread of endemic areas in recent decades <abbrgrp>
                <abbr bid="B1">1</abbr>
             </abbrgrp>. TBE caused by European subtype of TBE virus (TBEV) has typically a biphasic clinical course with the second phase presenting as meningitis, meningoencephalitis, or meningoencephalomyelitis <abbrgrp>
                <abbr bid="B2">2</abbr>
             </abbrgrp>. Cerebrospinal fluid (CSF) pleocytosis is considered a condition sine qua non for the diagnosis of CNS involvement in TBE, which in routine clinical practice is confirmed by demonstration of serum IgM and IgG antibodies to TBEV <abbrgrp>
                <abbr bid="B2">2</abbr>
             </abbrgrp>-<abbrgrp>
                <abbr bid="B4">4</abbr>
             </abbrgrp>. Cases of TBE with neurologic involvement but without CSF pleocytosis have been published <abbrgrp>
                <abbr bid="B5">5</abbr>
             </abbrgrp>-<abbrgrp>
                <abbr bid="B7">7</abbr>
             </abbrgrp>, however, only the case recently reported by P&#246;schl et al. was convincingly substantiated <abbrgrp>
                <abbr bid="B5">5</abbr>
             </abbrgrp>. Here we present a patient from Slovenia, an area highly endemic for TBE <abbrgrp>
                <abbr bid="B8">8</abbr>
             </abbrgrp>, with clinical features of encephalitis, who fulfilled criteria for recent CNS infection with TBEV although he had no CSF pleocytosis.</p>
       </sec>
       <sec>
          <st>
             <p>Case presentation</p>
          </st>
          <p>A 79-year-old man with arterial hypertension and chronic venous ulcers on both shins, fell ill acutely with diarrhea, fatigue and sleepiness in midsummer 2013. After a week, diarrhea stopped, but he became febrile up to 38.5&#176;C and was no longer able to walk independently due to general weakness. As a beekeeper he had been exposed to ticks in the past but could not remember having had a tick bite during the preceding few months. At admission to hospital on day 8 of his illness, he was lethargic, disoriented, but without signs of meningeal irritation. His blood pressure was 133/83&#160;mmHg, heart rate 99/min, breathing rate 30/min and axillary temperature 38.9&#176;C. Routine laboratory blood tests revealed normal blood cell count, mild hyponatremia (Na 129; normal 135&#8211;145&#160;mmol/l), and slightly elevated concentrations of C-reactive protein (32&#160;mg/l; normal 0&#8211;5&#160;mg/l), liver enzymes (aspartate aminotransferase 0.73; normal &#8804;0.58 &#956;kat/l, gamma-glutamyl transpeptidase 1.12 &#956;kat/l; normal &#8804;0.92 &#956;kat/l) and creatinine (101&#160;&#956;mol/l; normal 44&#8211;97&#160;&#956;mol/l). CSF examination yielded elevated protein concentration (1.31&#160;g/l; normal 0.15&#8211;0.45&#160;g/l), but normal leukocyte count (3&#8201;&#215;&#8201;10<sup>6</sup>/l; normal &#8804;5&#8201;&#215;&#8201;10<sup>6</sup>/l) and glucose concentration. In the following days the patient remained febrile up to 39.4&#176;C. On day 10, tremor of hands and tongue appeared and his mental status deteriorated to somnolence. Computed tomography of the brain showed only mild periventricular leukopathy. Repeated CSF analyses on day 14 and 23 revealed elevated protein concentrations (1.23, and 2.02&#160;g/l, respectively), but still no pleocytosis (CSF leukocyte count 1, and 2&#8201;&#215;&#8201;10<sup>6</sup>/l, respectively). PCR analyses of CSF for the presence of TBEV on day 8 and 23 were negative as were for HSV 1, HSV 2, VZV, and enteroviruses. Based on serological results the patient did not have Lyme neuroborreliosis. However, serum IgM and IgG antibodies to TBEV were demonstrated using enzyme linked immunosorbent assay - ELISA (Enzygnost Anti-TBE/FSME Virus IgG, IgM; Siemens, Marburg, Germany) (Table&#160;<tblr tid="T1">1</tblr>). The follow-up levels of specific serum antibodies and the avidity of specific serum IgG (12.7%, 15.4%, and 51.6% on day 14, 21, and 65, respectively) indicated recent infection with TBEV. In addition, demonstration of intrathecal production of anti-TBEV IgM and IgG verified CNS infection with TBEV (Table&#160;<tblr tid="T1">1</tblr>). From day 14 the patient was no longer febrile and his mental and physical status progressively improved. Hospitalization was prolonged because of hospital acquired pneumonia which was treated with amoxicillin/clavulanate. At transfer to a nursing facility on day 32 the patient was afebrile, completely oriented, feeble, but without focal neurological deficit. Routine laboratory test results were unremarkable.
                </p>
          
             <table id="T1">
                <title>
                   <p>Table 1</p>
                </title>
                <caption>
                   <p>
                          <b>Enzyme linked immunosorbent assay findings</b>                        
                   </p>
                </caption>
                <tgroup align="left" cols="7">
                   <colspec align="left" colname="c1" colnum="1" colwidth="*"/>
                   <colspec align="left" colname="c2" colnum="2" colwidth="*"/>
                   <colspec align="left" colname="c3" colnum="3" colwidth="*"/>
                   <colspec align="left" colname="c4" colnum="4" colwidth="*"/>
                   <colspec align="left" colname="c5" colnum="5" colwidth="*"/>
                   <colspec align="left" colname="c6" colnum="6" colwidth="*"/>
                   <colspec align="left" colname="c7" colnum="7" colwidth="*"/>
                   <thead>
                      <row valign="top">
                         <entry colname="c1" morerows="1"/>
                         <entry nameend="c5" namest="c2" rowsep="1">
                            <p>
                              <b>Anti-TBEV</b>                            
                            </p>
                         </entry>
                         <entry colname="c6" morerows="1">
                            <p>
                              <b>IgM antibody index</b>                              
                               <sup>
                                  <b>a</b> 
                               </sup>
                            </p>
                         </entry>
                         <entry colname="c7" morerows="1">
                            <p>
                              <b>IgG antibody index</b>                              
                               <sup>
                                  <b>a</b> 
                               </sup>
                            </p>
                         </entry>
                      </row>
                      <row valign="top">
                         <entry colname="c2">
                            <p>
                              <b>IgM in serum</b>                            
                            </p>
                         </entry>
                         <entry colname="c3">
                            <p>
                              <b>IgG in serum</b>                            
                            </p>
                         </entry>
                         <entry colname="c4">
                            <p>
                              <b>IgM in CSF</b>                            
                            </p>
                         </entry>
                         <entry colname="c5">
                            <p>
                              <b>IgG in CSF</b>                            
                            </p>
                         </entry>
                      </row>
                   </thead>
                   <tfoot>
                      <p>TBEV, tick-borne encephalitis virus; CSF, cerebrospinal fluid; NA, not available.</p>
                      <p>
                        <sup>a</sup>Antibody index of intrathecal synthesis of anti-TBEV IgM and IgG calculated according to Reiber&#8217;s formula <abbrgrp>
                            <abbr bid="B9">9</abbr>
                         </abbrgrp>.</p>
                   </tfoot>
                   <tbody>
                      <row valign="top">
                         <entry colname="c1">
                            <p>Day 14</p>
                         </entry>
                         <entry colname="c2">
                            <p>1.473</p>
                         </entry>
                         <entry colname="c3">
                            <p>78.4</p>
                         </entry>
                         <entry colname="c4">
                            <p>0.601</p>
                         </entry>
                         <entry colname="c5">
                            <p>53.7</p>
                         </entry>
                         <entry colname="c6">
                            <p>NA</p>
                         </entry>
                         <entry colname="c7">
                            <p>NA</p>
                         </entry>
                      </row>
                      <row valign="top">
                         <entry colname="c1">
                            <p>Day 23</p>
                         </entry>
                         <entry colname="c2">
                            <p>1.133</p>
                         </entry>
                         <entry colname="c3">
                            <p>130</p>
                         </entry>
                         <entry colname="c4">
                            <p>0.676</p>
                         </entry>
                         <entry colname="c5">
                            <p>89.7</p>
                         </entry>
                         <entry colname="c6">
                            <p>48.6</p>
                         </entry>
                         <entry colname="c7">
                            <p>40.2</p>
                         </entry>
                      </row>
                   </tbody>
                </tgroup>
             </table>
          
       </sec>
       <sec>
          <st>
             <p>Conclusions</p>
          </st>
          <p>Presenting symptoms and signs of TBE are nonspecific and similar as in acute aseptic meningoencephalitis of other etiologies with fever in 92%, vomiting in 38%, headache in 67-100%, altered consciousness in 12&#8211;35.5%, seizures in 0.3-3.3%, tremor in 7-78%, dysphasia in 0.7-3.8%, spinal nerve paralysis in 2.7-15%, and cranial nerve paralysis in 3.3-11% of cases <abbrgrp>
                <abbr bid="B2">2</abbr>
             </abbrgrp>,<abbrgrp>
                <abbr bid="B10">10</abbr>
             </abbrgrp>. Accordingly, our patient with fever, altered consciousness and tremor of the hands and tongue had clinical indications of acute encephalitis.</p>
          <p>In our patient, recent infection with TBEV was demonstrated by the presence of specific IgM and IgG antibodies in serum and further substantiated with the appropriate increase in antibody levels and IgG avidity in convalescent samples. Since detailed history revealed that he had not had TBE in the past, had not traveled outside Slovenia, nor was he vaccinated against TBE, yellow fever or Japanese encephalitis, and he had negative serum antibodies against West Nile virus, the only potential coinfecting flavivirus in Slovenia, the possibility of false positive serological results for TBE due to cross-reaction is highly improbable. In routine clinical practice the finding of specific IgM and IgG serum antibodies in a patient residing in endemic area, who has clinical signs/symptoms of meningitis/encephalitis associated with elevated CSF leukocyte count, is considered confirmatory for TBE <abbrgrp>
                <abbr bid="B2">2</abbr>
             </abbrgrp>,<abbrgrp>
                <abbr bid="B3">3</abbr>
             </abbrgrp>. However, demonstration of recent infection with TBEV by itself does not attest for CNS involvement; in fact the majority of infections with TBEV are asymptomatic or without prominent morbidity <abbrgrp>
                <abbr bid="B2">2</abbr>
             </abbrgrp>. For a reliable diagnosis of unusual TBE cases such as ours without CSF pleocytosis, which is most likely rather rare and therefore associated with low pre-test probability, not only evidence of a recent infection, but also evidence of CNS infection with TBEV is needed. We were unable to demonstrate the presence of TBEV in CSF by PCR, which is not surprising in regard to earlier reports that the virus can be detected by PCR in blood during the initial phase of TBE, but extremely rarely in blood or CSF during the second phase of the disease <abbrgrp>
                <abbr bid="B11">11</abbr>
             </abbrgrp>. However, we ascertained CNS infection by demonstration of intrathecal synthesis of specific antibodies to TBEV, which is a much more sensitive approach for verifying CNS infection. The main source of CSF antibodies are lymphocytes B, but in patients with TBE lymphocytes B comprise only 3-5% of CSF lymphocyte population <abbrgrp>
                <abbr bid="B12">12</abbr>
             </abbrgrp>. Therefore, one may hypothesize, that the major source of CSF antibodies are lymphocytes B accumulated in the perivascular spaces of brain parenchyma <abbrgrp>
                <abbr bid="B13">13</abbr>
             </abbrgrp>.</p>
          <p>In reports including large number of patients with proven TBE, CSF pleocytosis was established in all cases <abbrgrp>
                <abbr bid="B2">2</abbr>
             </abbrgrp>,<abbrgrp>
                <abbr bid="B4">4</abbr>
             </abbrgrp>,<abbrgrp>
                <abbr bid="B10">10</abbr>
             </abbrgrp>,<abbrgrp>
                <abbr bid="B14">14</abbr>
             </abbrgrp>. This finding, however, might be due to a selection bias because in these studies CSF pleocytosis was one of the inclusion criteria for the diagnosis of TBE <abbrgrp>
                <abbr bid="B15">15</abbr>
             </abbrgrp>. Our case, together with the recently published case by P&#246;schl et al. <abbrgrp>
                <abbr bid="B5">5</abbr>
             </abbrgrp>, and some earlier less convincing reports <abbrgrp>
                <abbr bid="B6">6</abbr>
             </abbrgrp>,<abbrgrp>
                <abbr bid="B7">7</abbr>
             </abbrgrp>, show that CSF pleocytosis is not mandatory in patients with encephalitis caused by TBEV. This is in accordance with the reports on encephalitis caused by some other viruses such as herpes simplex 1 in which about 3% of cases take place with normal CSF leukocyte count <abbrgrp>
                <abbr bid="B16">16</abbr>
             </abbrgrp>. The frequency of encephalitis without CSF pleocytosis caused by TBEV infection remains to be determined. However, the paucity of reports implies that such presentation is most probably very rare.</p>
          <p>The findings in our patient suggest that in daily clinical practice, in patients with symptoms/signs of encephalitis compatible with TBE and the risk of exposure to ticks in a TBE endemic region, the search for infection with TBEV is warranted despite the lack of CSF pleocytosis. However, to reliably prove that the encephalitis with normal CSF white cell count is caused by TBEV, a proof of TBEV CNS infection is needed besides the usual diagnostic requirements i.e., the evidence of a recent infection with TBEV denoted by the presence of specific serum IgM and IgG antibodies.</p>
       </sec>
       <sec>
          <st>
             <p>Consent</p>
          </st>
          <p>Written informed consent was obtained from the patient for publication of this Case report. A copy of the written consent is available for review by the Editor of this journal.</p>
       </sec>
       <sec>
          <st>
             <p>Competing interests</p>
          </st>
          <p>The authors declare that they have no competing interests.</p>
       </sec>
       <sec>
          <st>
             <p>Authors&#8217; contributions</p>
          </st>
          <p>ML, BP, and DS were patient' clinical doctors. TA&#381; carried out the microbiological analyses. DS, FFB., and FS helped to draft the manuscript. All authors read and approved the final manuscript.</p>
       </sec>
    </bdy>
    <bm>
       <refgrp>
          
          <bibl id="B1">
             
                <url>http://ecdc.europa.eu/en/publications/publications/tbe-in-eu-efta.pdf</url>
             
<note>
                <b>European Centre for Disease Prevention and Control. Epidemiological situation of tick-borne encephalitis in the European Union and European Free Trade Association countries</b> []</note>
          </bibl>
          <bibl id="B2">
             <title>
                <p>Tick-borne encephalitis</p>
             </title>
             <aug>
                <au>
                   <snm>Lindquist</snm>
                   <fnm>L</fnm>
                </au>
                <au>
                   <snm>Vapalahti</snm>
                   <fnm>O</fnm>
                </au>
             </aug>
             <source>Lancet</source>
             <pubdate>2008</pubdate>
             <volume>371</volume>
             <fpage>1861</fpage>
             <lpage>1871</lpage>
             <xrefbib>
                <pubidlist>
                   <pubid idtype="doi">10.1016/S0140-6736(08)60800-4</pubid>
                   <pubid idtype="pmpid" link="fulltext">18514730</pubid>
                </pubidlist>
             </xrefbib>
          </bibl>
          <bibl id="B3">
             <title>
                <p>Intrathecal IgM, IgA and IgG antibody response in tick-borne encephalitis. Long-term follow-up related to clinical course and outcome</p>
             </title>
             <source>Clin Diag Virol</source>
             <pubdate>1997</pubdate>
             <volume>8</volume>
             <issue>Suppl 1</issue>
             <fpage>17</fpage>
             <lpage>29</lpage>
             <xrefbib>
                <pubidlist>
                   <pubid idtype="doi">10.1016/S0928-0197(97)00273-0</pubid>
                </pubidlist>
             </xrefbib>
          </bibl>
          <bibl id="B4">
             <title>
                <p>Tickborne encephalitis in an area of high endemicity in Lithuania: disease severity and long-term prognosis</p>
             </title>
             <aug>
                <au>
                   <snm>Mickiene</snm>
                   <fnm>A</fnm>
                </au>
                <au>
                   <snm>Laiskonis</snm>
                   <fnm>A</fnm>
                </au>
                <au>
                   <snm>Gunther</snm>
                   <fnm>G</fnm>
                </au>
                <au>
                   <snm>Vene</snm>
                   <fnm>S</fnm>
                </au>
                <au>
                   <snm>Lundkvist</snm>
                   <fnm>A</fnm>
                </au>
                <au>
                   <snm>Lindquist</snm>
                   <fnm>L</fnm>
                </au>
             </aug>
             <source>Clin Infect Dis</source>
             <pubdate>2002</pubdate>
             <volume>35</volume>
             <issue>Suppl 6</issue>
             <fpage>650</fpage>
             <lpage>658</lpage>
             <xrefbib>
                <pubidlist>
                   <pubid idtype="doi">10.1086/342059</pubid>
                   <pubid idtype="pmpid" link="fulltext">12203160</pubid>
                </pubidlist>
             </xrefbib>
          </bibl>
          <bibl id="B5">
             <title>
                <p>Severe tick-borne encephalomyelitis with lack of cerebrospinal fluid pleocytosis [in German]</p>
             </title>
             <aug>
                <au>
                   <snm>P&#246;schl</snm>
                   <fnm>P</fnm>
                </au>
                <au>
                   <snm>Kleiter</snm>
                   <fnm>I</fnm>
                </au>
                <au>
                   <snm>Grubwinkler</snm>
                   <fnm>S</fnm>
                </au>
                <au>
                   <snm>Bumes</snm>
                   <fnm>E</fnm>
                </au>
                <au>
                   <snm>Bogdahn</snm>
                   <fnm>U</fnm>
                </au>
                <au>
                   <snm>Dobler</snm>
                   <fnm>G</fnm>
                </au>
                <au>
                   <snm>Steinbrecher</snm>
                   <fnm>A</fnm>
                </au>
             </aug>
             <source>Fortschr Neurol Psychiatr</source>
             <pubdate>2009</pubdate>
             <volume>77</volume>
             <fpage>591</fpage>
             <lpage>593</lpage>
             <xrefbib>
                <pubidlist>
                   <pubid idtype="doi">10.1055/s-0028-1109768</pubid>
                   <pubid idtype="pmpid" link="fulltext">19821222</pubid>
                </pubidlist>
             </xrefbib>
          </bibl>
          <bibl id="B6">
             <title>
                <p>Neurological manifestation of tick-borne encephalitis in North-Eastern Italy</p>
             </title>
             <aug>
                <au>
                   <snm>Cruciatti</snm>
                   <fnm>B</fnm>
                </au>
                <au>
                   <snm>Beltrame</snm>
                   <fnm>A</fnm>
                </au>
                <au>
                   <snm>Ruscio</snm>
                   <fnm>M</fnm>
                </au>
                <au>
                   <snm>Viale</snm>
                   <fnm>P</fnm>
                </au>
                <au>
                   <snm>Gigli</snm>
                   <fnm>GL</fnm>
                </au>
             </aug>
             <source>Neurol Sci</source>
             <pubdate>2006</pubdate>
             <volume>27</volume>
             <fpage>122</fpage>
             <lpage>124</lpage>
             <xrefbib>
                <pubidlist>
                   <pubid idtype="doi">10.1007/s10072-006-0612-0</pubid>
                   <pubid idtype="pmpid" link="fulltext">16816910</pubid>
                </pubidlist>
             </xrefbib>
          </bibl>
          <bibl id="B7">
             <title>
                <p>Tick-borne encephalitis in North-eastern Poland in 1997&#8211;2001: a retrospective study</p>
             </title>
             <aug>
                <au>
                   <snm>Grygorczuk</snm>
                   <fnm>S</fnm>
                </au>
                <au>
                   <snm>Mierzynska</snm>
                   <fnm>D</fnm>
                </au>
                <au>
                   <snm>Zdrodowska</snm>
                   <fnm>A</fnm>
                </au>
                <au>
                   <snm>Zajakowska</snm>
                   <fnm>J</fnm>
                </au>
                <au>
                   <snm>Pancewicz</snm>
                   <fnm>S</fnm>
                </au>
                <au>
                   <snm>Kondrusik</snm>
                   <fnm>M</fnm>
                </au>
                <au>
                   <snm>Swierzzbinska</snm>
                   <fnm>R</fnm>
                </au>
                <au>
                   <snm>Pryszmont</snm>
                   <fnm>J</fnm>
                </au>
                <au>
                   <snm>Hermanowska-Szpakowicz</snm>
                   <fnm>T</fnm>
                </au>
             </aug>
             <source>Scand J Infect Dis</source>
             <pubdate>2002</pubdate>
             <volume>2002</volume>
             <issue>34</issue>
             <fpage>904</fpage>
             <lpage>909</lpage>
             <xrefbib>
                <pubidlist>
                   <pubid idtype="doi">10.1080/0036554021000026979</pubid>
                </pubidlist>
             </xrefbib>
          </bibl>
          <bibl id="B8">
             
                <url>http://www.ivz.si/gradiva_nalezljive_bolezni?pi=5&amp;_5_Filename=7402.pdf&amp;_5_MediaId=7402&amp;_5_AutoResize=false&amp;pl=105-5.3</url>
             
<note>
                <b>Institute of Public health of the Republic of Slovenia. Tick-borne encephalitis. Epidemiological surveillance of infectious diseases in Slovenia in year 2012</b> []</note>
          </bibl>
          <bibl id="B9">
             <title>
                <p>Quantification of virus-specific antibodies in cerebrospinal fluid and serum: sensitive and specific detection of antibody synthesis in brain</p>
             </title>
             <aug>
                <au>
                   <snm>Reiber</snm>
                   <fnm>H</fnm>
                </au>
                <au>
                   <snm>Lange</snm>
                   <fnm>P</fnm>
                </au>
             </aug>
             <source>Clin Chem</source>
             <pubdate>1991</pubdate>
             <volume>37</volume>
             <fpage>1153</fpage>
             <lpage>1160</lpage>
             <xrefbib>
                <pubidlist>
                   <pubid idtype="pmpid" link="fulltext">1855284</pubid>
                </pubidlist>
             </xrefbib>
          </bibl>
          <bibl id="B10">
             <title>
                <p>Tick-borne encephalitis in Slovenia from 2000 to 2004: comparison of the course in adult and elderly patients</p>
             </title>
             <aug>
                <au>
                   <snm>Logar</snm>
                   <fnm>M</fnm>
                </au>
                <au>
                   <snm>Bogovi&#269;</snm>
                   <fnm>P</fnm>
                </au>
                <au>
                   <snm>Cerar</snm>
                   <fnm>D</fnm>
                </au>
                <au>
                   <snm>Av&#353;i&#269;-&#381;upanc</snm>
                   <fnm>T</fnm>
                </au>
                <au>
                   <snm>Strle</snm>
                   <fnm>F</fnm>
                </au>
             </aug>
             <source>Wien Klin Wochenschr</source>
             <pubdate>2004</pubdate>
             <volume>2006</volume>
             <issue>118</issue>
             <fpage>702</fpage>
             <lpage>707</lpage>
          </bibl>
          <bibl id="B11">
             <title>
                <p>The importance of tick-borne encephalitis virus RNA detection for early differential diagnosis of tick-borne encephalitis</p>
             </title>
             <aug>
                <au>
                   <snm>Saksida</snm>
                   <fnm>A</fnm>
                </au>
                <au>
                   <snm>Duh</snm>
                   <fnm>D</fnm>
                </au>
                <au>
                   <snm>Lotric-Furlan</snm>
                   <fnm>S</fnm>
                </au>
                <au>
                   <snm>Strle</snm>
                   <fnm>F</fnm>
                </au>
                <au>
                   <snm>Petrovec</snm>
                   <fnm>M</fnm>
                </au>
                <au>
                   <snm>Avsic-Zupanc</snm>
                   <fnm>T</fnm>
                </au>
             </aug>
             <source>J Clin Virol</source>
             <pubdate>2005</pubdate>
             <volume>33</volume>
             <fpage>331</fpage>
             <lpage>335</lpage>
             <xrefbib>
                <pubidlist>
                   <pubid idtype="doi">10.1016/j.jcv.2004.07.014</pubid>
                   <pubid idtype="pmpid" link="fulltext">15919235</pubid>
                </pubidlist>
             </xrefbib>
          </bibl>
          <bibl id="B12">
             <title>
                <p>Flow cytometric analysis of lymphocytes in cerebrospinal fluid in patients with tick-borne encephalitis</p>
             </title>
             <aug>
                <au>
                   <snm>Toma&#382;i&#269;</snm>
                   <fnm>J</fnm>
                </au>
                <au>
                   <snm>Ihan</snm>
                   <fnm>A</fnm>
                </au>
             </aug>
             <source>Acta Neurol Scand</source>
             <pubdate>1997</pubdate>
             <volume>95</volume>
             <fpage>29</fpage>
             <lpage>33</lpage>
             <xrefbib>
                <pubidlist>
                   <pubid idtype="doi">10.1111/j.1600-0404.1997.tb00064.x</pubid>
                   <pubid idtype="pmpid" link="fulltext">9048982</pubid>
                </pubidlist>
             </xrefbib>
          </bibl>
          <bibl id="B13">
             <title>
                <p>Inflammatory response in human tick-borne encephalitis: analysis of postmortem brain tissue</p>
             </title>
             <aug>
                <au>
                   <snm>Gelpi</snm>
                   <fnm>E</fnm>
                </au>
                <au>
                   <snm>Preusser</snm>
                   <fnm>M</fnm>
                </au>
                <au>
                   <snm>Laggner</snm>
                   <fnm>U</fnm>
                </au>
                <au>
                   <snm>Garzuly</snm>
                   <fnm>F</fnm>
                </au>
                <au>
                   <snm>Holzmann</snm>
                   <fnm>H</fnm>
                </au>
                <au>
                   <snm>Xaver Heinz</snm>
                   <fnm>F</fnm>
                </au>
                <au>
                   <snm>Budka</snm>
                   <fnm>H</fnm>
                </au>
             </aug>
             <source>J Neurovirol</source>
             <pubdate>2006</pubdate>
             <volume>12</volume>
             <fpage>322</fpage>
             <lpage>327</lpage>
             <xrefbib>
                <pubidlist>
                   <pubid idtype="doi">10.1080/13550280600848746</pubid>
                   <pubid idtype="pmpid" link="fulltext">16966222</pubid>
                </pubidlist>
             </xrefbib>
          </bibl>
          <bibl id="B14">
             <title>
                <p>Tick-borne encephalitis in Sweden in relation to aseptic meningo-encephalitis of other etiology: a prospective study of clinical course and outcome</p>
             </title>
             <aug>
                <au>
                   <snm>G&#252;nther</snm>
                   <fnm>G</fnm>
                </au>
                <au>
                   <snm>Haglund</snm>
                   <fnm>M</fnm>
                </au>
                <au>
                   <snm>Lindquist</snm>
                   <fnm>L</fnm>
                </au>
                <au>
                   <snm>Forsgren</snm>
                   <fnm>M</fnm>
                </au>
                <au>
                   <snm>Sk&#246;ldenberg</snm>
                   <fnm>B</fnm>
                </au>
             </aug>
             <source>J Neurol</source>
             <pubdate>1997</pubdate>
             <volume>244</volume>
             <fpage>30</fpage>
             <lpage>38</lpage>
             <xrefbib>
                <pubidlist>
                   <pubid idtype="doi">10.1007/s004150050077</pubid>
                </pubidlist>
             </xrefbib>
          </bibl>
          <bibl id="B15">
             <title>
                <p>Surveillance of tick-borne encephalitis in Europe and case definition</p>
             </title>
             <aug>
                <au>
                   <snm>G&#252;nther</snm>
                   <fnm>G</fnm>
                </au>
                <au>
                   <snm>Lindquist</snm>
                   <fnm>L</fnm>
                </au>
             </aug>
             <source>Euro Surveill</source>
             <pubdate>2005</pubdate>
             <volume>10</volume>
             <fpage>2</fpage>
             <lpage>3</lpage>
             <xrefbib>
                <pubidlist>
                   <pubid idtype="pmpid" link="fulltext">15701940</pubid>
                </pubidlist>
             </xrefbib>
          </bibl>
          <bibl id="B16">
             <title>
                <p>Herpes simplex encephalitis: adolescents and adults</p>
             </title>
             <aug>
                <au>
                   <snm>Whitley</snm>
                   <fnm>RJ</fnm>
                </au>
             </aug>
             <source>Antiviral Res</source>
             <pubdate>2006</pubdate>
             <volume>71</volume>
             <fpage>141</fpage>
             <lpage>148</lpage>
             <xrefbib>
                <pubidlist>
                   <pubid idtype="doi">10.1016/j.antiviral.2006.04.002</pubid>
                   <pubid idtype="pmpid" link="fulltext">16675036</pubid>
                </pubidlist>
             </xrefbib>
          </bibl>
       </refgrp>
    </bm>
 </art>
