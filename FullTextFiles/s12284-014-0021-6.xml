<?xml version='1.0'?>
<!DOCTYPE art SYSTEM 'http://www.biomedcentral.com/xml/article.dtd'>
<art>
    <ui>s12284-014-0021-6</ui>
    <ji>1939-8433</ji>
    <fm>
       <dochead>Short report</dochead>
       <bibl>
          <title>
             <p>A candidate factor that interacts with RF2, a restorer of fertility of Lead rice-type cytoplasmic male sterility in rice</p>
          </title>
          <aug>
             <au id="A1">
                <snm>Fujii</snm>
                <fnm>Shinya</fnm>
                <insr iid="I1"/>
                <email>b3ad1210@s.tohoku.ac.jp</email>
             </au>
             <au id="A2">
                <snm>Kazama</snm>
                <fnm>Tomohiko</fnm>
                <insr iid="I1"/>
                <email>tomo-kazama@bios.tohoku.ac.jp</email>
             </au>
             <au id="A3">
                <snm>Ito</snm>
                <fnm>Yukihiro</fnm>
                <insr iid="I1"/>
                <email>yukito@bios.tohoku.ac.jp</email>
             </au>
             <au id="A4">
                <snm>Kojima</snm>
                <fnm>Soichi</fnm>
                <insr iid="I1"/>
                <email>kojimasoichi@biochem.tohoku.ac.jp</email>
             </au>
             <au id="A5" ca="yes">
                <snm>Toriyama</snm>
                <fnm>Kinya</fnm>
                <insr iid="I1"/>
                <email>torikin@bios.tohoku.ac.jp</email>
             </au>
          </aug>
          <insg>
             <ins id="I1">
                <p>Graduate School of Agricultural Science, Tohoku University, Sendai 981-8555, Japan</p>
             </ins>
          </insg>
          <source>Rice</source>
          <issn>1939-8433</issn>
          <pubdate>2014</pubdate>
          <volume>7</volume>
          <issue>1</issue>
          <fpage>21</fpage>
          <url>http://www.thericejournal.com/content/7/1/21</url>
          <xrefbib>
             <pubidlist>
                <pubid idtype="doi">10.1186/s12284-014-0021-6</pubid>
                <pubid idtype="pmpid"><!-- Need to check source for data--></pubid>
             </pubidlist>
          </xrefbib>
       </bibl>
       <history>
          <rec>
             <date>
                <day>29</day>
                <month>5</month>
                <year>2014</year>
             </date>
          </rec>
          <acc>
             <date>
                <day>19</day>
                <month>8</month>
                <year>2014</year>
             </date>
          </acc>
          <pub>
             <date>
                <day>7</day>
                <month>10</month>
                <year>2014</year>
             </date>
          </pub>
       </history>
       <cpyrt>
          <year>2014</year>
          <collab>Fujii et al.; licensee Springer.</collab>
          <note>This is an Open Access article distributed under the terms of the Creative Commons Attribution License (<url>http://creativecommons.org/licenses/by/4.0</url>), which permits unrestricted use, distribution, and reproduction in any medium, provided the original work is properly credited.</note>
       </cpyrt>
       <abs>
          <sec>
             <st>
                <p>Abstract</p>
             </st>
             <sec>
                <st>
                   <p>Background</p>
                </st>
                <p>The pollen function of cytoplasmic male sterile (CMS) plants is often recovered by the <it>Restorer of fertility</it> (<it>Rf</it>) gene encoded by the nuclear genome. An <it>Rf</it> gene of Lead rice type CMS, <it>Rf2</it>, encodes a small mitochondrial glycine-rich protein. RF2 is expected to function by interacting with other proteins, because RF2 has no motifs except for glycine-rich domain.</p>
             </sec>
             <sec>
                <st>
                   <p>Findings</p>
                </st>
                <p>To elucidate the protein that interacts with RF2, we performed yeast two-hybrid screening. We identified four genes and named <it>RF2-interacting candidate factors</it> (<it>RIF1</it> to <it>RIF4</it>). A study of subcellular localization demonstrated that only RIF2 was targeted to mitochondria. A pull-down assay using <it>E. coli</it>-produced recombinant GST-tagged RF2 and His-tagged RIF2 confirmed that RF2 interacted with RIF2. <it>RIF2</it> encodes ubiquitin domain-containing protein.</p>
             </sec>
             <sec>
                <st>
                   <p>Conclusions</p>
                </st>
                <p>These results suggest that RIF2 is a candidate factor of a fertility restoration complex of RF2.</p>
             </sec>
          </sec>
       </abs>
    </fm>
    <bdy>
       <sec>
          <st>
             <p>1
 						Findings</p>
          </st>
          <p>Cytoplasmic male sterility (CMS), which is caused by an aberrant mitochondrial gene, is unable to produce functional pollen. Pollen function is often recovered by a <it>Restorer of fertility</it> (<it>Rf</it>) gene encoded by the nuclear genome. Many <it>Rf</it> genes are reported to encode pentatricopeptide repeat (PPR) protein, which is involved in the processing of mitochondrial RNA (Fujii and Toriyama <abbrgrp>
                <abbr bid="B3">2008</abbr>
             </abbrgrp> for a review). Different from PPR-type <it>Rf</it> genes, <it>Rf2,</it> which is a fertility restorer gene of Lead Rice (LD) type CMS, encodes a mitochondria-targeted 152-amino acid protein containing glycine-rich domain (Itabashi et al. <abbrgrp>
                <abbr bid="B6">2011</abbr>
             </abbrgrp>). <it>Rf2</it> is, therefore, considered to restore fertility by a novel mechanism. RF2 is expected to function by interacting with other proteins, because mature RF2 is a small protein that putatively consists of 80 amino acids after removing a predicted mitochondrial targeting signal sequence; it has no motifs except for a 29-amino acid long glycine-rich domain (Itabashi et al. <abbrgrp>
                <abbr bid="B6">2011</abbr>
             </abbrgrp>).</p>
          <p>To elucidate a protein that interacts with RF2, we performed Y2H screening. We screened the library (1.8&#8201;&#215;&#8201;10<sup>6</sup> clones) made from the mature anther RNA of rice (<it>Oryza sativa</it> cv. Taichung 65; Fujii et al. <abbrgrp>
                <abbr bid="B4">2009</abbr>
             </abbrgrp>) using Matchmaker Yeast Two-Hybrid System (Clontech, Tokyo, Japan) according to the manufacturer&#8217;s protocol. To generate a bait construct, a coding sequence of mature RF2, which lacks a mitochondrial targeting signal sequence, was amplified by PCR using primers Rf2_CDS_EcoRI217F and Rf2_CDS_BamHI456R (primer sequences were shown in Additional file <supplr sid="S1">1</supplr>: Table S1) and inserted downstream of a sequence for GAL4 DNA-binding domain in pGBKT7. This construct was introduced into yeast strain Y187, and then Y187 was mated with another yeast strain AH109 that contained a prey construct in which cDNA was inserted downstream of a sequence for GAL4 activation domain in pGADT7. Screening was carried out on the SD medium that lacks tryptophan, leucine, histidine and adenine, and is supplemented with 5&#160;mM 3-AT. Positive clones were re-grown on the same SD medium additionally supplemented with X-&#945;-Gal. Interaction was confirmed by re-transformation of yeast. By this screening we obtained 22 positive clones. Nucleotide sequence analysis showed that these clones contained cDNA derived from four distinct genes, which were named <it>RF2-interacting candidate factors</it> (<it>RIF1, RIF2, RIF3</it> and <it>RIF4</it>; Table&#160;<tblr tid="T1">1</tblr>). Interaction between RF2 and RIFs was also confirmed by prey-bait swapping experiments in which RF2 was fused to GAL4-activation domain and each RIF protein was fused to GAL4 DNA-binding domain (Figure&#160;<figr fid="F1">1</figr>).</p>
             <table id="T1">
                <title>
                   <p>Table 1</p>
                </title>
                <caption>
                   <p>
                      <b>Genes identified by Y2H screening</b>
                   </p>
                </caption>
                <tgroup align="left" cols="5">
                   <colspec align="left" colname="c1" colnum="1" colwidth="*"/>
                   <colspec align="left" colname="c2" colnum="2" colwidth="*"/>
                   <colspec align="left" colname="c3" colnum="3" colwidth="*"/>
                   <colspec align="left" colname="c4" colnum="4" colwidth="*"/>
                   <colspec align="left" colname="c5" colnum="5" colwidth="*"/>
                   <thead>
                      <row>
                         <entry colname="c1">
                            <p>
                               <b>Name</b>
                            </p>
                         </entry>
                         <entry colname="c2">
                            <p>
                               <b>Number of clones</b>
                            </p>
                         </entry>
                         <entry colname="c3">
                            <p>
                               <b>RAP-DB Locus ID</b>
                            </p>
                         </entry>
                         <entry colname="c4">
                            <p>
                               <b>cDNA accession No.</b>
                            </p>
                         </entry>
                         <entry colname="c5">
                            <p>
                               <b>Annotation</b>
                            </p>
                         </entry>
                      </row>
                   </thead>
                   <tbody>
                      <row>
                         <entry colname="c1">
                            <p>
                               <it>RIF1</it>
                            </p>
                         </entry>
                         <entry colname="c2">
                            <p>7</p>
                         </entry>
                         <entry colname="c3">
                            <p>Os04g0404900</p>
                         </entry>
                         <entry colname="c4">
                            <p>AK107531</p>
                         </entry>
                         <entry colname="c5">
                            <p>Conserved hypothetical protein</p>
                         </entry>
                      </row>
                      <row>
                         <entry colname="c1">
                            <p>
                               <it>RIF2</it>
                            </p>
                         </entry>
                         <entry colname="c2">
                            <p>2</p>
                         </entry>
                         <entry colname="c3">
                            <p>Os10g0542200</p>
                         </entry>
                         <entry colname="c4">
                            <p>AK065246</p>
                         </entry>
                         <entry colname="c5">
                            <p>Ubiquitin domain containing protein</p>
                         </entry>
                      </row>
                      <row>
                         <entry colname="c1">
                            <p>
                               <it>RIF3</it>
                            </p>
                         </entry>
                         <entry colname="c2">
                            <p>10</p>
                         </entry>
                         <entry colname="c3">
                            <p>Os04g0229100</p>
                         </entry>
                         <entry colname="c4">
                            <p>AK071484</p>
                         </entry>
                         <entry colname="c5">
                            <p>Similar to Sinapyl alcohol dehydrogenase</p>
                         </entry>
                      </row>
                      <row>
                         <entry colname="c1">
                            <p>
                               <it>RIF4</it>
                            </p>
                         </entry>
                         <entry colname="c2">
                            <p>3</p>
                         </entry>
                         <entry colname="c3">
                            <p>Os12g0507600</p>
                         </entry>
                         <entry colname="c4">
                            <p>AK070613</p>
                         </entry>
                         <entry colname="c5">
                            <p>Conserved hypothetical protein</p>
                         </entry>
                      </row>
                   </tbody>
                </tgroup>
             </table>
          
          
             <fig id="F1">
                <title>
                   <p>Figure 1</p>
                </title>
                <caption>
                   <p>Interaction between each AD-RIF and BD-RF2 (A), or between AD-RF2 and each BD-RIF (B) by Y2H assay.</p>
                </caption>
                <text>
                   <p>
                      <b>Interaction between each AD-RIF and BD-RF2 (A), or between AD-RF2 and each BD-RIF (B) by Y2H assay.</b> Colonies were assayed for histidine autotrophy (-His columns) or beta-galactosidase activity (+X-Gal columns). Vector indicates pGADT7 for AD and pBGKT7 for BD, respectively.</p>
                </text>
                <graphic file="s12284-014-0021-6-1"/>
             </fig>
          
          <p>A fertility restoration factor is expected to function in mitochondria. We therefore investigated the subcellular localization of RIF1, RIF2, RIF3 and RIF4. Coding sequences of RIF proteins were PCR-amplified using primers listed in Additional file <supplr sid="S1">1</supplr>: Table S1 and using full-length cDNAs obtained from the Rice Genome Resource Center (National Institute of Agrobiological Sciences, Tsukuba, Japan). They were cloned into pENTR D-TOPO vector (Invitrogen, Tokyo, Japan), and then inserted between CaMV 35S promoter and GFP gene in pGWB5 vector (Nakagawa et al. <abbrgrp>
                <abbr bid="B8">2007</abbr>
             </abbrgrp>).</p><p>Each of the resulting plasmids that express GFP fusion proteins was introduced into rice protoplasts with a plasmid containing CaMV35S promoter-F1F0 ATPase mitochondrial targeting signal-RFP (Arimura and Tsutsumi <abbrgrp>
                <abbr bid="B1">2002</abbr>
             </abbrgrp>) using the polyethylene glycol method (Chen et al. <abbrgrp>
                <abbr bid="B2">2010</abbr>
             </abbrgrp>). GFP fluorescence of RIF2-GFP was co-localized with mitochondrial-localizing RFP, whereas GFP fluorescence of RIF1-GFP, RIF3-GFP and RIF4-GFP was observed in cytoplasm (Figure&#160;<figr fid="F2">2</figr>). These results indicated that only RIF2 was targeted to mitochondria.</p>
          
             <fig id="F2">
                <title>
                   <p>Figure 2</p>
                </title>
                <caption>
                   <p>Subcellular localization of each RIF-GFP.</p>
                </caption>
                <text>
                   <p>
                      <b>Subcellular localization of each RIF-GFP.</b> Subcellular localization of RIF proteins was examined as GFP fusions (RIF-GFP) in rice protoplasts. Mitochondrial-localizing RFP (Mt-RFP) was used as a marker for mitochondrial localization. Scale bars&#8201;=&#8201;10&#160;&#956;m.</p>
                </text>
                <graphic file="s12284-014-0021-6-2"/>
             </fig>
          
          <p>Next we performed pull-down assay using <it>E. coli</it>-produced recombinant GST-tagged RF2 (GST-RF2) and His-tagged RIF2 (His-RIF2). The coding sequence of mature RF2 that lacks a predicted mitochondrial targeting signal sequence was PCR-amplified using primers Rf2_CDS_CACC217F and Rf2_CDS_BamHI456R (Additional file <supplr sid="S1">1</supplr>: Table S1), and inserted into pENTR D-TOPO using Gateway technology (Invitrogen), and then transferred to pDEST17 for a GST fusion protein (Invitrogen). The coding sequence of RIF2 was similarly cloned into pDEST15 for a His fusion protein. Expression of GST-RF2, His-RIF2 and GST alone was induced for 2&#160;h at 37&#176;C by adding L-arabinose. A protein extract of GST-RF2 or GST alone was mixed with a protein extract of His-RIF2, and the GST proteins were purified with Glutathione sepharose 4B (GE Healthcare, Tokyo, Japan). Western blotting using anti-His-tag antibody (Quiagen, Tokyo, Japan) detected His-RIF2 in the purified fraction of GST-RF2, but not in the fraction of GST alone (Figure&#160;<figr fid="F3">3</figr>). This result confirms that RF2 interacts with RIF2.</p>
             <fig id="F3">
                <title>
                   <p>Figure 3</p>
                </title>
                <caption>
                   <p>Pull-down assay to test</p>
                </caption>
                <text>
                   <p>
                      <b>Pull-down assay to test</b>
                      <b>
                         <it>in vitro</it>
                      </b>
                      <b>interaction between RF2 and RIF2.</b> Pull-down assay was performed using a His-RIF2 protein with GST (control) or GST-RF2. Input lane contains His-RIF2.</p>
                </text>
                <graphic file="s12284-014-0021-6-3"/>
             </fig>
          
          <p>RIF2 encodes a 575 amino acid protein containing ubiquitin family domain and ubiquitin-associated/thermostable-N terminal domain (Figure&#160;<figr fid="F4">4</figr>). The most similar protein with known function is ubiqulin 1 (NCBI Protein Accession Nos. NP_444295. 1 for humans and NP_689420. 1 for mice), which is reported to be associated with ubiquitin ligases and proteasomes (Ko et al. <abbrgrp>
                <abbr bid="B7">2004</abbr>
             </abbrgrp>). Although a ubiquitin proteasome proteolysis system has not been reported in mitochondria, we could not rule out the possibility that RIF2 is involved in ubiquitin-proteasome proteolysis of impaired mitochondrial proteins after their translocation to the outer membrane as reported for quality control of human mitochondria (Shanbhag et al. <abbrgrp>
                <abbr bid="B9">2012</abbr>
             </abbrgrp>). In this case, a CMS-causing protein would be a target of degradation for fertility restoration.</p>
          
             <fig id="F4">
                <title>
                   <p>Figure 4</p>
                </title>
                <caption>
                   <p>Motif of RIF2 protein predicted by pfam (</p>
                </caption>
                <text>
                   <p>
                      <b>Motif of RIF2 protein predicted by pfam (</b>
                      <url>http://pfam.xfam.org/</url>
                      <b>) analysis.</b>
                   </p>
                </text>
                <graphic file="s12284-014-0021-6-4"/>
             </fig>
          
          <p>Another possibility for RIF2 function in regard to fertility restoration is that RIF2 plays a role in an RNA processing complex. In this case the complex would contain a factor that directly recognizes and processes a target RNA, since neither RF2 nor RIF2 harbors a motif associated with RNA processing. Formation of an RNA processing complex was reported in Hong-Lian CMS. In this CMS, GRP162, which contains a glycine-rich motif and an RNA recognition motif, has been reported to interact with a PPR protein RF5 and unknown components to form a fertility restoration-complex that mediates the processing of CMS-associated <it>atp6-orf79</it> RNA (Hu et al. <abbrgrp>
                <abbr bid="B5">2012</abbr>
             </abbrgrp>). RF2 of LD-CMS showed limited amino acid sequence identity (28%) to GRP162 but lacks an RNA recognition motif. The RIF2, which encodes ubiquitin-domain containing protein, might be one of the components of a fertility restoration-complex in the LD-CMS/<it>Rf2</it> system. Further study is now in progress to discover other components, which might include RNA recognition/binding proteins in the RF2 fertility restoration-complex that mediate processing of a CMS-associated mitochondrial RNA.</p>
       </sec>
       <sec>
          <st>
             <p>Abbreviations</p>
          </st>
          <p>3-AT: 3-aminotriazole</p>
          <p>AD: Activation domain</p>
          <p>BD: Binding domain</p>
          <p>CMS: Cytoplasmic male sterility</p>
          <p>RF: Restorer of fertility</p>
          <p>X-&#945;-Gal: 5-Bromo-4-chloro-3-indolyl-&#945;-D-galactopyranoside</p>
          <p>Y2H: Yeast two hybrid</p>
       </sec>
       <sec>
          <st>
             <p>Competing interests</p>
          </st>
          <p>The authors declare no potential competing interests.</p>
       </sec>
       <sec>
          <st>
             <p>Authors&#8217; contributions</p>
          </st>
          <p>KT and TK conceived and designed the experiments. SF performed the experiments and drafted the manuscript. KT and YI revised the manuscript. YI and SK supervised Y2H experiments. All authors read and approved the final manuscript.</p>
       </sec>
       <sec>
          <st>
             <p>Additional file</p>
          </st>
          <suppl id="S1">
             <title>
                <p>Additional file 1: Table S1.</p>
             </title>
             <text>
                <p>Primers used in this study.</p>
             </text>
             <file name="s12284-014-0021-6-S1.xls">
                <p>Click here for file</p>
             </file>
          </suppl>
          
       </sec>
    </bdy>
    <bm>
       <ack>
          <sec>
             <st>
                <p>Acknowledgments</p>
             </st>
             <p>This work was supported by MEXT/JSPS KAKENHI Grant Numbers 2338002, 24117502 and 26292002, and by Science and technology research promotion program for agriculture, forestry, fisheries and food industry (No. 26010A).</p>
          </sec>
       </ack>
       <refgrp>
          
          <bibl id="B1">
             <title>
                <p>A dynamin-like protein (ADL2b), rather than FtsZ, is involved in Arabidopsis mitochondrial division</p>
             </title>
             <aug>
                <au>
                   <snm>Arimura</snm>
                   <fnm>S</fnm>
                </au>
                <au>
                   <snm>Tsutsumi</snm>
                   <fnm>N</fnm>
                </au>
             </aug>
             <source>Proc Natl Acad Sci USA</source>
             <pubdate>2002</pubdate>
             <volume>99</volume>
             <fpage>5727</fpage>
             <lpage>5731</lpage>
             <note>doi:10.1073/pnas.082663299</note>
             <xrefbib>
                <pubidlist>
                   <pubid idtype="doi">10.1073/pnas.082663299</pubid>
                   <pubid idtype="pmpid" link="fulltext">11960028</pubid>
                </pubidlist>
             </xrefbib>
          </bibl>
          <bibl id="B2">
             <title>
                <p>Analysis of the Rac/Rop small GTPase family in rice: expression, subcellular localization and role in disease resistance</p>
             </title>
             <aug>
                <au>
                   <snm>Chen</snm>
                   <fnm>L</fnm>
                </au>
                <au>
                   <snm>Shiotani</snm>
                   <fnm>K</fnm>
                </au>
                <au>
                   <snm>Togashi</snm>
                   <fnm>T</fnm>
                </au>
                <au>
                   <snm>Miki</snm>
                   <fnm>D</fnm>
                </au>
                <au>
                   <snm>Aoyama</snm>
                   <fnm>M</fnm>
                </au>
                <au>
                   <snm>Wong</snm>
                   <fnm>HL</fnm>
                </au>
                <au>
                   <snm>Kawasaki</snm>
                   <fnm>T</fnm>
                </au>
                <au>
                   <snm>Shimamoto</snm>
                   <fnm>K</fnm>
                </au>
             </aug>
             <source>Plant Cell Physiol</source>
             <pubdate>2010</pubdate>
             <volume>51</volume>
             <fpage>585</fpage>
             <lpage>595</lpage>
             <note>doi:10.1093/pcp/pcq024</note>
             <xrefbib>
                <pubidlist>
                   <pubid idtype="doi">10.1093/pcp/pcq024</pubid>
                   <pubid idtype="pmpid" link="fulltext">20203239</pubid>
                </pubidlist>
             </xrefbib>
          </bibl>
          <bibl id="B3">
             <title>
                <p>Genome barriers between nuclei and mitochondria exemplified by cytoplasmic male sterility</p>
             </title>
             <aug>
                <au>
                   <snm>Fujii</snm>
                   <fnm>S</fnm>
                </au>
                <au>
                   <snm>Toriyama</snm>
                   <fnm>K</fnm>
                </au>
             </aug>
             <source>Plant Cell Physiol</source>
             <pubdate>2008</pubdate>
             <volume>49</volume>
             <fpage>1484</fpage>
             <lpage>1494</lpage>
             <note>doi:10.1093/pcp/pcn102</note>
             <xrefbib>
                <pubidlist>
                   <pubid idtype="doi">10.1093/pcp/pcn102</pubid>
                   <pubid idtype="pmpid" link="fulltext">18625609</pubid>
                </pubidlist>
             </xrefbib>
          </bibl>
          <bibl id="B4">
             <title>
                <p>Cytoplasmic male sterility-related protein kinase, OsNek3, is regulated downstream of mitochondrial protein phosphatase 2C, DCW11</p>
             </title>
             <aug>
                <au>
                   <snm>Fujii</snm>
                   <fnm>S</fnm>
                </au>
                <au>
                   <snm>Yamada</snm>
                   <fnm>M</fnm>
                </au>
                <au>
                   <snm>Toriyama</snm>
                   <fnm>K</fnm>
                </au>
             </aug>
             <source>Plant Cell Physiol</source>
             <pubdate>2009</pubdate>
             <volume>50</volume>
             <fpage>828</fpage>
             <lpage>837</lpage>
             <note>doi:10.1093/pcp/pcp026</note>
             <xrefbib>
                <pubidlist>
                   <pubid idtype="doi">10.1093/pcp/pcp026</pubid>
                   <pubid idtype="pmpid" link="fulltext">19224952</pubid>
                </pubidlist>
             </xrefbib>
          </bibl>
          <bibl id="B5">
             <title>
                <p>The rice pentatricopeptide repeat protein RF5 restores fertility in Hong-Lian cytoplasmic male-sterile lines via a complex with the glycine-rich protein GRP162</p>
             </title>
             <aug>
                <au>
                   <snm>Hu</snm>
                   <fnm>J</fnm>
                </au>
                <au>
                   <snm>Wang</snm>
                   <fnm>K</fnm>
                </au>
                <au>
                   <snm>Huang</snm>
                   <fnm>W</fnm>
                </au>
                <au>
                   <snm>Liu</snm>
                   <fnm>G</fnm>
                </au>
                <au>
                   <snm>Wang</snm>
                   <fnm>J</fnm>
                </au>
                <au>
                   <snm>Huang</snm>
                   <fnm>Q</fnm>
                </au>
                <au>
                   <snm>Ji</snm>
                   <fnm>Y</fnm>
                </au>
                <au>
                   <snm>Qin</snm>
                   <fnm>X</fnm>
                </au>
                <au>
                   <snm>Wan</snm>
                   <fnm>L</fnm>
                </au>
                <au>
                   <snm>Zhu</snm>
                   <fnm>R</fnm>
                </au>
                <au>
                   <snm>Li</snm>
                   <fnm>S</fnm>
                </au>
                <au>
                   <snm>Yang</snm>
                   <fnm>D</fnm>
                </au>
                <au>
                   <snm>Zhu</snm>
                   <fnm>Y</fnm>
                </au>
             </aug>
             <source>Plant Cell</source>
             <pubdate>2012</pubdate>
             <volume>24</volume>
             <fpage>109</fpage>
             <lpage>122</lpage>
             <note>doi:10.1105/tpc.111.093211</note>
             <xrefbib>
                <pubidlist>
                   <pubid idtype="doi">10.1105/tpc.111.093211</pubid>
                   <pubid idtype="pmpid" link="fulltext">22247252</pubid>
                </pubidlist>
             </xrefbib>
          </bibl>
          <bibl id="B6">
             <title>
                <p>The fertility restorer gene, Rf2, for Lead Rice-type cytoplasmic male sterility of rice encodes a mitochondrial glycine-rich protein</p>
             </title>
             <aug>
                <au>
                   <snm>Itabashi</snm>
                   <fnm>E</fnm>
                </au>
                <au>
                   <snm>Iwata</snm>
                   <fnm>N</fnm>
                </au>
                <au>
                   <snm>Fujii</snm>
                   <fnm>S</fnm>
                </au>
                <au>
                   <snm>Kazama</snm>
                   <fnm>T</fnm>
                </au>
                <au>
                   <snm>Toriyama</snm>
                   <fnm>K</fnm>
                </au>
             </aug>
             <source>Plant J</source>
             <pubdate>2011</pubdate>
             <volume>65</volume>
             <fpage>359</fpage>
             <lpage>367</lpage>
             <note>doi:10.1111/j.1365-313X.2010.04427.x</note>
             <xrefbib>
                <pubidlist>
                   <pubid idtype="doi">10.1111/j.1365-313X.2010.04427.x</pubid>
                   <pubid idtype="pmpid" link="fulltext">21265890</pubid>
                </pubidlist>
             </xrefbib>
          </bibl>
          <bibl id="B7">
             <title>
                <p>Ubiquilin interacts with ubiquitylated proteins and prooteasome through its ubiquitin-associated and ubiquitin-like domains</p>
             </title>
             <aug>
                <au>
                   <snm>Ko</snm>
                   <fnm>HS</fnm>
                </au>
                <au>
                   <snm>Uehara</snm>
                   <fnm>T</fnm>
                </au>
                <au>
                   <snm>Tsuruma</snm>
                   <fnm>K</fnm>
                </au>
                <au>
                   <snm>Nomura</snm>
                   <fnm>Y</fnm>
                </au>
             </aug>
             <source>FEBS Let</source>
             <pubdate>2004</pubdate>
             <volume>566</volume>
             <fpage>110</fpage>
             <lpage>114</lpage>
             <note>doi:10.1016/j.febslet.2004.04.031</note>
             <xrefbib>
                <pubidlist>
                   <pubid idtype="doi">10.1016/j.febslet.2004.04.031</pubid>
                </pubidlist>
             </xrefbib>
          </bibl>
          <bibl id="B8">
             <title>
                <p>Development of series of Gateway Binary vectors, pGWBs, for realizing efficient construction of fusion genes for plant transformation</p>
             </title>
             <aug>
                <au>
                   <snm>Nakagawa</snm>
                   <fnm>T</fnm>
                </au>
                <au>
                   <snm>Kurose</snm>
                   <fnm>T</fnm>
                </au>
                <au>
                   <snm>Hino</snm>
                   <fnm>T</fnm>
                </au>
                <au>
                   <snm>Tanaka</snm>
                   <fnm>K</fnm>
                </au>
                <au>
                   <snm>Kawamukai</snm>
                   <fnm>M</fnm>
                </au>
                <au>
                   <snm>Niwa</snm>
                   <fnm>Y</fnm>
                </au>
                <au>
                   <snm>Toyooka</snm>
                   <fnm>K</fnm>
                </au>
                <au>
                   <snm>Matsuoka</snm>
                   <fnm>K</fnm>
                </au>
                <au>
                   <snm>Jinbo</snm>
                   <fnm>T</fnm>
                </au>
                <au>
                   <snm>Kimura</snm>
                   <fnm>T</fnm>
                </au>
             </aug>
             <source>J Biosci Bioeng</source>
             <pubdate>2007</pubdate>
             <volume>104</volume>
             <fpage>34</fpage>
             <lpage>41</lpage>
             <note>doi:10.1263/jbb.104.34</note>
             <xrefbib>
                <pubidlist>
                   <pubid idtype="doi">10.1263/jbb.104.34</pubid>
                   <pubid idtype="pmpid" link="fulltext">17697981</pubid>
                </pubidlist>
             </xrefbib>
          </bibl>
          <bibl id="B9">
             <title>
                <p>The emerging role of proteolysis in mitochondrial quality control and the etiology of Parkinson&#8217;s disease</p>
             </title>
             <aug>
                <au>
                   <snm>Shanbhag</snm>
                   <fnm>R</fnm>
                </au>
                <au>
                   <snm>Shi</snm>
                   <fnm>G</fnm>
                </au>
                <au>
                   <snm>Rujiviphat</snm>
                   <fnm>J</fnm>
                </au>
                <au>
                   <snm>McQuibban</snm>
                   <fnm>GA</fnm>
                </au>
             </aug>
             <source>Parkinson&#8217;s Dis</source>
             <pubdate>2012</pubdate>
             <volume>2012</volume>
             <fpage>382175</fpage>
             <note>doi:10. 1155/2012/382175</note>
          </bibl>
       </refgrp>
    </bm>
 </art>
