package com.java.util;

public class StringUtil {

	/*working when we have singly digit with string*/
	public static String removeExtraSpaceInStringWhichHasSingleDigit(String toParse){
		String pattern = "((\\d)(\\s+))";
		
		String updatedString=toParse.replaceAll(pattern, "");
		return updatedString;
	}	
	
	public static boolean ifSpaceBetweenDigits(String toParse){
		char previous='0';
		boolean ifSpaceBetweenDigits=false;
		boolean foundWhitespace=false;
		
		for(int j=0;j<toParse.length();j++){
			char a=toParse.charAt(j);
			if(Character.isLetter(a)){
				break;
			}else if(Character.isDigit(a)){
				previous=a;
				if(foundWhitespace){
					ifSpaceBetweenDigits=true;
					break;
				}
				continue;
			}else if(Character.isWhitespace(a)){
				foundWhitespace=true;
				continue;
			}
		}
		return ifSpaceBetweenDigits;
	}

	public static String removeSpaceBetweenDigits(String toParse){
		boolean ifSpaceBetweenDigits=false;
		String substring=toParse;
		boolean foundWhitespace=false;
		char previous='0';
		
		for(int j=0;j<toParse.length();j++){
			char a=toParse.charAt(j);
			if(Character.isLetter(a)){
				break;
			}else if(Character.isDigit(a)){
				previous=a;
				if(foundWhitespace){
					ifSpaceBetweenDigits=true;
					substring=toParse.substring(j,toParse.length());
				}
				continue;
			}else if(Character.isWhitespace(a)){
				foundWhitespace=true;
				continue;
			}
		}
		return substring;
	}
	
	public static String removeDigitsWhenWeHaveMultipleDigitsWithNoSpace(String toParse){
		String substring=toParse;
		int positionOfWhiteSpaceChar=0;
		
		for(int j=0;j<toParse.length();j++){
			char a=toParse.charAt(j);
			if(Character.isWhitespace(a)){
				positionOfWhiteSpaceChar=j;
			} else if(Character.isLetter(a)){
				substring=toParse.substring(j,toParse.length());
				break;
			}
		}
		return substring;
	}
 
	public static boolean isStringContainsDigit(String toParse){
		boolean isDigit=false;
		for(int j=0;j<toParse.length();j++){
				if(Character.isDigit(toParse.charAt(j))){
					isDigit=true;
					break;
				}
		}
		
		return isDigit;
	 }
	
	public static boolean isStringContainsSingleDigit(String toParse){
		boolean isStringContainsSingleDigit=false;
		int digitCount=0; 
		
		for(int j=0;j<toParse.length();j++){
				if(Character.isDigit(toParse.charAt(j))){
					isStringContainsSingleDigit=true;
					digitCount++;
					if(digitCount>1){
						break;
					}
					continue;
				}
		}
		
		if (digitCount > 1){
			isStringContainsSingleDigit=false;
		}else
			isStringContainsSingleDigit=true;
		
		return isStringContainsSingleDigit;
	 }
	
}
