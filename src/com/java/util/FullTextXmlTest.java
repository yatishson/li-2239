package com.java.util;


import java.io.InputStream;
import java.util.Properties;

import org.apache.log4j.PropertyConfigurator;

public class FullTextXmlTest {

	public static final String LOG4J_FILE_LOCATION=".\\resources\\log4j.properties";
		
	public FullTextXmlTest() {
		ClassLoader loader = Thread.currentThread().getContextClassLoader();
		Properties props = new Properties();
		try{
			InputStream resourceStream = loader.getResourceAsStream("log4j.properties");
			props.load(resourceStream);
		} catch(Exception e) {
			
		}
	}
	
	public static void main(String[] args) {
		PropertyConfigurator.configure(LOG4J_FILE_LOCATION);
		ReadFullAbstractMultipleFilesUpdated read=new ReadFullAbstractMultipleFilesUpdated();
		read.readXML();
	}
	
}	