package com.java.util;

public interface ConfigConstant {
	
	String JDBC_FILE_PATH =  ".\\resources\\jdbc.properties";
	String LOG_FILE_PATH = ".\\resources\\log4j.properties";

	String APLUS_PLSE_FILES_FOLDER = ".\\APLUSFiles";
	//String APLUS_PLSE_FILES_FOLDER = ".\\delete1";
	String FULLTEXT_FILES_FOLDER = ".\\FullTextFiles";
	//String FULLTEXT_FILES_FOLDER = ".\\delete2";
	
	String BMC_URL= "http://www.biomedcentral.com/content/download/xml/";
}
