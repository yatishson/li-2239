package com.java.util;
import java.io.File;  
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;  
import javax.xml.parsers.DocumentBuilderFactory;  
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.log4j.Logger;
import org.w3c.dom.Document;  
import org.w3c.dom.Node;  
import org.w3c.dom.NodeList;  
import org.xml.sax.SAXException;

import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;


public class ReadFullAbstractMultipleFilesUpdated {
	
	public static final String ALL_P_TAG_UNDER_ST_TAG="/art/bdy//sec/st/p";
	//public static final String FROM_FOLDER=".\\readFilesToUpdate";
	public static final String FROM_FOLDER=".\\delete2";
	//public static final String DESTINATION_FOLDER=".\\updatedFullTextFiles";
	public static final String DESTINATION_FOLDER=".\\delete3";
	private static Logger logger=Logger.getLogger("ReadFullAbstractMultipleFilesUpdated");
	
	
	public void readXML() {  
	    File fromFolder=new File(FROM_FOLDER);
	    File[] listOfFiles = fromFolder.listFiles();
	    
	    for (File file : listOfFiles) {
	    	if(file.isFile()){
	    		processFile(file);
	    	}
	    }
	}

	//private void processFile(File xmlfile,File toFolder){
	private void processFile(File xmlfile){
	 	logger.info("*** Processing started for full-text file [" + xmlfile.getName() + "] from location[" + xmlfile.getPath() + "].");
	 	
		FileInputStream file=null;
		try{
			file = new FileInputStream(xmlfile);
			DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
			  
			DocumentBuilder builder =  builderFactory.newDocumentBuilder();
			  
			Document xmlDocument = builder.parse(file);
			 
			XPath xPath =  XPathFactory.newInstance().newXPath();
			 
			NodeList pTagNodeList = (NodeList) xPath.compile(ALL_P_TAG_UNDER_ST_TAG).evaluate(xmlDocument, XPathConstants.NODESET);
			
			for (int i = 0; i < pTagNodeList.getLength(); i++) {
			  	 Node childNode=pTagNodeList.item(i);
			   	 if(childNode.getNodeName().equalsIgnoreCase("p")){
			   	   	if((!childNode.getTextContent().isEmpty() || childNode.getTextContent()!=null) && childNode.getTextContent().trim().length()!=0){
			   	   		
			   	   		String pTagValue=childNode.getTextContent().trim();
			   	    	logger.info("original value P tag:"+pTagValue);
			   	   		if(StringUtil.isStringContainsDigit(pTagValue)){
			   	   				if(StringUtil.isStringContainsSingleDigit(pTagValue)){
			   	   					logger.info("string contains single digit only:"+pTagValue);
			   	   					String updatedValue=StringUtil.removeExtraSpaceInStringWhichHasSingleDigit(pTagValue);
			   	   				    childNode.setTextContent(updatedValue);	
			   	   				    logger.info("updated Value of P tag:"+updatedValue);
			   	   				}else if (StringUtil.ifSpaceBetweenDigits(pTagValue)){
			   	   					logger.info("string contains multiple digits:"+pTagValue+" and there is space between them");
			   	   					String updatedValue=StringUtil.removeSpaceBetweenDigits(pTagValue);
			   	   					childNode.setTextContent(updatedValue);	
			   	   					logger.info("updated Value of P tag:"+updatedValue);
			   	   				}else{
			   	   					logger.info("string contains multiple digits:"+pTagValue+" and there is space between them");
			   	   					String updatedValue=StringUtil.removeDigitsWhenWeHaveMultipleDigitsWithNoSpace(pTagValue);
			   	   					childNode.setTextContent(updatedValue);	
			   	   					logger.info("updated Value of P tag:"+updatedValue);
			   	   				}
			   	   		}
			   	  	}
			   	 }
			 }		
				    		
			 Transformer xformer = TransformerFactory.newInstance().newTransformer();
			 xformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
			 StreamResult result = new StreamResult(new FileWriter(new File(DESTINATION_FOLDER+"\\"+xmlfile.getName())));
			 xformer.transform(new DOMSource(xmlDocument), result);
			 
			 result.getWriter().flush();
			 result = null;
			 
			 logger.info("*** Processing Completed for full-text file [" + xmlfile.getName() + "] and copied to folder[" + DESTINATION_FOLDER + "].");
		} catch (FileNotFoundException e) {
			logger.error("### Article-Full-Text file not found." + e.getMessage());
		} catch (SAXException e) {
			logger.error("### Error while parsing Article-Full-Text file." + e.getMessage());
		} catch (IOException e) {
			logger.error("### Processing failed for full-text file. Error: " + e.getMessage());
		} catch (ParserConfigurationException e) {
			logger.error("### Processing failed for full-text file. Error: " + e.getMessage());
		} catch (XPathExpressionException e) {
			logger.error("### Error while traversing xml with xpath." + e.getMessage());
		} catch (TransformerFactoryConfigurationError e) {
			logger.error("### Processing failed for full-text file. Error: " + e.getMessage());
		} catch (TransformerException e) {
			logger.error("### Processing failed for full-text file. Error: " + e.getMessage());
		} catch (Exception e) {  
			logger.error("### Processing failed for full-text file. Error: " + e.getMessage());
		} finally{
			if(file!=null){
				try {
					file.close();
				} catch (IOException e) {
					logger.error("### Processing failed for full-text file while closing file. Error: " + e.getMessage());
				}
			}
	  }
	}
}
