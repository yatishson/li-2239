package com.java.util;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Properties;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.java.util.ConfigConstant;

public class DownloadXMLFiles {

	//public static final File APLUS_PLUS_FILES_FOLDER = new File(ConfigConstant.APLUS_PLSE_FILES_FOLDER);
	public static final String FULLTEXT_FILES_FOLDER= ConfigConstant.FULLTEXT_FILES_FOLDER;
	public static final String BMC_URL = ConfigConstant.BMC_URL;
	//public static final String LOG4J_FILE_LOCATION = ConfigConstant.LOG_FILE_PATH;
	
	
	final static Logger logger = Logger.getLogger(DownloadXMLFiles.class);
	
//	public static void main(String[] args) {
//		PropertyConfigurator.configure(LOG4J_FILE_LOCATION);
//		
//		DownloadXMLFiles xmlFiles=new DownloadXMLFiles();
//		try {
//			 xmlFiles.displayDirectoryContents((APLUS_PLUS_FILES_FOLDER));
//    	} catch (Exception ex){
//    		ex.printStackTrace();
//    	}
//	}
	
	public String readArticleIDFromXML(File file){
		 
		String articleID=null;	
		try {  
			 	FileInputStream inFile = new FileInputStream(file);
          
	            DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
	             
	            DocumentBuilder builder =  builderFactory.newDocumentBuilder();
	             
	            Document xmlDocument = builder.parse(inFile);
	 
	            XPath xPath =  XPathFactory.newInstance().newXPath();
		  
	            String expression = "/Publisher/Journal/Volume/Issue/Article";
	            Node node=(Node)xPath.compile(expression).evaluate(xmlDocument, XPathConstants.NODE);
	            String articleIDFromXML= node.getAttributes()
	                    .getNamedItem("ID")
	                    .getNodeValue();
	            logger.debug("Article id[" + articleIDFromXML + "] found from A++ file '" + file.getName() + "'.");
	            if(articleIDFromXML!=null && !articleIDFromXML.isEmpty()){
	            	String updatedArticleIDURL = BMC_URL + articleIDFromXML + ".xml";
	            	logger.debug("*** Hiting BMC-URL[ " + updatedArticleIDURL + "] to download full-text xml.");
	            	
            		download(updatedArticleIDURL, FULLTEXT_FILES_FOLDER);
	            } else {
	            	logger.info("ArticleID missing in A++ xml '" + file.getName() + "'.");
	            }
	           
			} catch (FileNotFoundException e) {
	            logger.error("### got an error:"+e.getMessage());
	        } catch (SAXException e) {
	            logger.error("### got an error:"+e.getMessage());
	        } catch (IOException e) {
	            logger.error("### got an error:"+e.getMessage());
	        } catch (ParserConfigurationException e) {
	            logger.error("### got an error:"+e.getMessage());
	        } catch (XPathExpressionException e) {
	            logger.error("### got an error:"+e.getMessage());
	        } catch (TransformerFactoryConfigurationError e) {
	            e.printStackTrace();
	            logger.error("### got an error:"+e.getMessage());
	        }catch (Exception e) {  
	        	logger.error("got an error:"+e.getMessage());
		  }  
		 return articleID;
	}
	
	public  void displayDirectoryContents(File dir) {
		try {
			File[] files = dir.listFiles();
			for (File file : files) {
				if (file.isDirectory()) {
					displayDirectoryContents(file);
				} else {
					logger.debug("*** Found A++ File: " + file.getName());
					readArticleIDFromXML(file);
				}
			}
		} catch (Exception ex){
			ex.printStackTrace();
			logger.error("got an error:"+ex.getMessage());
		}
	}
	
	 private void download(String fileURL, String destinationDirectory) throws IOException {
	        // File name that is being downloaded
	        String downloadedFileName = fileURL.substring(fileURL.lastIndexOf("/")+1);
	         
	        // Open connection to the file
	        URL url = new URL(fileURL);
	        InputStream is = url.openStream();
	        // Stream to the destionation file
	        FileOutputStream fos = new FileOutputStream(destinationDirectory + "/" + downloadedFileName);
	  
	        // Read bytes from URL to the local file
	        byte[] buffer = new byte[4096];
	        int bytesRead = 0;
	         
	        logger.debug("*** Downloading started for " + downloadedFileName);
	        try {
		        while ((bytesRead = is.read(buffer)) != -1) {
		            System.out.print(".");  // Progress bar :)
		            fos.write(buffer,0,bytesRead);
		        }
		        logger.info("*** Downloaded " + downloadedFileName + " succesfully.");
		    } catch (Exception ex){
		    	logger.error("### Error while downloading: "+ downloadedFileName + "\n" + ex.getMessage());
		    } finally {
		        // Close destination stream
		        fos.close();
		        // Close URL stream
		        is.close();
		    }
	    }
}
