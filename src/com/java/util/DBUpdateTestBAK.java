package com.java.util;

import java.io.File;
import java.io.FileReader;
import java.io.StringReader;

import org.apache.commons.io.*;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import java.sql.*;

public class DBUpdateTestBAK {
	
	final static Logger logger = Logger.getLogger(DBUpdateTestBAK.class);

	static final String JDBC_DRIVER = "oracle.jdbc.driver.OracleDriver";  
	static final String DB_URL = "jdbc:oracle:oci:@bmcmaindb";

	static final String USER = "bmc_user";
	static final String PASS = "bmcu53r999";
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		PropertyConfigurator.configure("D:\\LI-2239-Card\\LI2239\\resources\\log4j.properties");
		Connection conn = null;
		PreparedStatement pStmt = null;
			
		try{
			Class.forName(JDBC_DRIVER);
			conn = DriverManager.getConnection(DB_URL, USER, PASS);
			
//		    String arx_id = "1471-244X-12-235";
//		    String UPDATE_SQL = "UPDATE arx set arx_full = ? where arx_id= '" + arx_id + "'";
			String UPDATE_SQL = "UPDATE arx set arx_full = ? where arx_id= ?";
		    
		    File folder = new File("D:\\LI-2239-Card\\LI2239\\updatedFullTextFiles");
		    File[] updatedFiles = folder.listFiles();

		    for(File updatedFile : updatedFiles){
		    	if(updatedFile.isFile()) {
		    		String basename = FilenameUtils.getBaseName(updatedFile.getName());
		    		
				    String correctedFullText = FileUtils.readFileToString(new File(updatedFile.getPath()));
				    StringReader reader = new StringReader(correctedFullText);
				    
				    pStmt = conn.prepareStatement(UPDATE_SQL);
				    pStmt.setCharacterStream(1, reader, correctedFullText.length());
				    pStmt.executeUpdate();
				    
//				    System.out.println("Clob data with arx_id: " + arx_id + " is updated successfully.");
//				    logger.info("Clob data with arx_id: " + arx_id + " is updated successfully.");
		    	}
		    }
		    
		   }catch(SQLException se){
		      se.printStackTrace();
		   }catch(Exception e){
		      e.printStackTrace();
		   }finally{
		      try{
		         if(pStmt != null)
		        	 pStmt.close();
		      }catch(SQLException se2){
		      }// nothing we can do
		      try{
		         if(conn != null)
		            conn.close();
		      }catch(SQLException se){
		         se.printStackTrace();
		      }
		   }
	}
}
