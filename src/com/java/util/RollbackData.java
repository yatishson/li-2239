package com.java.util;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.StringReader;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

public class RollbackData {

	final static Logger logger = Logger.getLogger(RollbackData.class);
	
	public static void process(Connection conn) throws IOException, SQLException {
		final String UPDATE_SQL = "UPDATE arx set arx_full = ? where arx_id= ?";
		File folder = new File(".\\backupArxfromDB");
		
		PreparedStatement pStmt = null;
		
		for(File updatedFile : folder.listFiles()){
	    	if(updatedFile.isFile()) {
	    		String arxID = FilenameUtils.getBaseName(updatedFile.getName());
	    		
				    String correctedFullText = FileUtils.readFileToString(new File(updatedFile.getPath()));
				    StringReader reader = new StringReader(correctedFullText);
				    
				    // Updating article full with older value
				    pStmt = conn.prepareStatement(UPDATE_SQL);
				    pStmt.setCharacterStream(1, reader, correctedFullText.length());
				    pStmt.setString(2, arxID);
				    pStmt.executeUpdate();
				    
				    logger.info("Clob data with Article-ID [" + arxID + "] is updated with older value successfully.");
	    	}
	    }
	}
	
	public static void main(String[] args) {
		PropertyConfigurator.configure(ConfigConstant.LOG_FILE_PATH);
		
		Connection conn = null; 
		
		try{
			conn = DBUtil.connectToDB();
			process(conn);
		} catch(SQLException e){
			logger.info("### Error while connecting with Oracle Database.");
			e.printStackTrace();
		} catch(ClassNotFoundException e){
			logger.info("### JDBC Driver not found.");
			e.printStackTrace();
		} catch(FileNotFoundException e) {
			logger.info("### Resources not found.");
			e.printStackTrace();
		} catch(IOException e) {
			logger.info("### Error while IO operation.");
			e.printStackTrace();
		} catch(Exception e) {
			logger.info("### Unkown problem.");
			e.printStackTrace();
		}finally{
	         if(conn != null)
				try {
					conn.close();
				} catch (SQLException e) {
					logger.info("### Problem while closing connection." + e.getMessage());
				}
		}
	}

}
