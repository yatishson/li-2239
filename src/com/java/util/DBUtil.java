package com.java.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;

import org.apache.commons.io.*;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import java.sql.*;
import java.util.Properties;

public class DBUtil {
	
	final static Logger logger = Logger.getLogger(DBUtil.class);

	public static Connection connectToDB() throws ClassNotFoundException, SQLException, FileNotFoundException, IOException {
		Properties prop = new Properties();		
		prop.load(new FileInputStream(ConfigConstant.JDBC_FILE_PATH));
		String dbDriver = prop.getProperty("jdbc.driver");
		String dbUrl = prop.getProperty("jdbc.url");
		String dbUser = prop.getProperty("jdbc.username");
		String dbPwd = prop.getProperty("jdbc.password");
		
		Class.forName(dbDriver);
		Connection conn = DriverManager.getConnection(dbUrl, dbUser, dbPwd);
		logger.debug("*** Database connection established successfully.");
		
		return conn;
	}
	
	public static void process(Connection conn) throws IOException, SQLException {
		final String UPDATE_SQL = "UPDATE arx set arx_full = ? where arx_id= ?";
		File folder = new File(".\\updatedFullTextFiles");
		
		PreparedStatement pStmt = null;
		
		for(File updatedFile : folder.listFiles()){
	    	if(updatedFile.isFile()) {
	    		String arxID = FilenameUtils.getBaseName(updatedFile.getName());
	    		
	    		if (backupArxFull(conn, arxID)) {
	    			try {
					    String correctedFullText = FileUtils.readFileToString(new File(updatedFile.getPath()));
					    StringReader reader = new StringReader(correctedFullText);
					    
					    // Updating article full 
					    pStmt = conn.prepareStatement(UPDATE_SQL);
					    pStmt.setCharacterStream(1, reader, correctedFullText.length());
					    pStmt.setString(2, arxID);
					    pStmt.executeUpdate();
					    
					    logger.info("Clob data with Article-ID [" + arxID + "] is updated successfully.");
	    			}finally {
	    				try {
	    					pStmt.close();
	    				} catch (SQLException e) {
	    					logger.error("### Error while closing resource.");
	    					e.printStackTrace();
	    				}
	    			}
	    		}
	    	}
	    }
	}
	
	public static Boolean backupArxFull(Connection conn, String arxID) throws  SQLException {
		boolean isBackedUp = false;
		final String SELECT_SQL = "SELECT arx_full FROM arx where arx_id= ?";
		
		
		PreparedStatement pStmt = conn.prepareStatement(SELECT_SQL);
	    pStmt.setString(1, arxID);
	    ResultSet rs = pStmt.executeQuery();
	    String arxFull = null;
	    if( rs != null && rs.next()) {
	    	arxFull = rs.getString("arx_full");
	    
	    FileOutputStream fop = null;
	    try {
	    	fop = new FileOutputStream("D:\\LI-2239-Card\\LI2239\\backupArxfromDB\\" + arxID + ".txt");

		    fop.write(arxFull.getBytes());
		    fop.flush();
		    logger.info("Clob data with Article-ID [" + arxID + "] is backed-up successfully.");
		    isBackedUp = true;
	    } catch (IOException e) {
	    	logger.error("### Error while backing-up.");
	    	e.printStackTrace();
		} finally {
			try {
				rs.close();
				pStmt.close();
				fop.close();
			} catch (IOException e) {
				logger.error("### Error while closing resource.");
				e.printStackTrace();
			}
		}
	    }
	    
	    return isBackedUp;
	}
	
	/**
	 * @param args
	 * @throws SQLException 
	 * @throws IOException 
	 * @throws FileNotFoundException 
	 */
	public static void main(String[] args) {
		PropertyConfigurator.configure(ConfigConstant.LOG_FILE_PATH);
		
		Connection conn = null; 
		
		try{
			conn = connectToDB();
			process(conn);
		} catch(SQLException e){
			logger.info("### Error while connecting with Oracle Database.");
			e.printStackTrace();
		} catch(ClassNotFoundException e){
			logger.info("### JDBC Driver not found.");
			e.printStackTrace();
		} catch(FileNotFoundException e) {
			logger.info("### Resources not found.");
			e.printStackTrace();
		} catch(IOException e) {
			logger.info("### Error while IO operation.");
			e.printStackTrace();
		} catch(Exception e) {
			logger.info("### Unkown problem.");
			e.printStackTrace();
		}finally{
	         if(conn != null)
				try {
					conn.close();
				} catch (SQLException e) {
					logger.info("### Problem while closing connection." + e.getMessage());
				}
		    }
		}
	}
