<art>
    <ui>s12916-014-0199-x</ui>
    <ji>1741-7015</ji>
    <fm>
       <dochead>Commentary</dochead>
       <bibl rating="0">
          <title lang="en">
             <p>Why would associations between cardiometabolic risk factors and depressive symptoms be linear?</p>
          </title>
          <aug>
             <au ca="yes" ce="no" da="no" id="A1" pa="no"><snm>de Jonge</snm><fnm>Peter</fnm><insr iid="I1"/><email>peter.de.jonge@umcg.nl</email></au>
             <au ca="no" ce="no" da="no" id="A2" pa="no"><snm>Roest</snm><mi>M</mi><fnm>Annelieke</fnm><insr iid="I1"/><email>a.m.roest@umcg.nl</email></au>
          </aug>
          <insg>
             <ins id="I1"><p>Interdisciplinary Center Psychopathology and Emotion Regulation (ICPE), University Medical Center Groningen (UMCG), University of Groningen, Groningen 9700 RB, the Netherlands</p></ins>
          </insg>
          <source>BMC Medicine</source>
          <issn>1741-7015</issn>
          <pubdate>2014</pubdate>
          <volume>12</volume>
          <issue>1</issue>
          <fpage>199</fpage>
          <url>http://www.biomedcentral.com/1741-7015/12/199</url>
          <xrefbib><pubidlist><pubid idtype="pmpid">25363297</pubid><pubid idtype="doi">10.1186/s12916-014-0199-x</pubid></pubidlist></xrefbib>
       </bibl>
       <history><rec><date><day>3</day><month>10</month><year>2014</year></date></rec><acc><date><day>3</day><month>10</month><year>2014</year></date></acc><pub><date><day>28</day><month>10</month><year>2014</year></date></pub></history>
       <cpyrt><year>2014</year><collab>de Jonge and Roest; licensee BioMed Central Ltd.</collab><note>This is an Open Access article distributed under the terms of the Creative Commons Attribution License (<url>http://creativecommons.org/licenses/by/4.0</url>), which permits unrestricted use, distribution, and reproduction in any medium, provided the original work is properly credited. The Creative Commons Public Domain Dedication waiver (<url>http://creativecommons.org/publicdomain/zero/1.0/</url>) applies to the data made available in this article, unless otherwise stated.</note></cpyrt>
       <kwdg lang="en">
          <kwd>Cardiovascular disease</kwd>
          <kwd>Depression</kwd>
          <kwd>Non-linear</kwd>
          <kwd>J-shaped curve</kwd>
          <kwd>Dynamic systems</kwd>
       </kwdg>
       <abs lang="en">
          <sec>
             <st>
                <p>Abstract</p>
             </st>
             <p>In medical science, researchers mostly use the linear model to determine associations among variables, while in reality many associations are likely to be non-linear. Recent advances have shown that associations may be regarded as parts of complex, dynamic systems for which the linear model does not yield valid results. Using as an example the interdepencies between organisms in a small ecosystem, we present the work of Sugihara <it>et al</it>. in <it>Science</it> 2012, <b>338:</b>496�500 who developed an alternative non-parametric method to determine the true associations among variables in a complex dynamic system. In this context, we discuss the work of Jani <it>et al</it>. recently published in <it>BMC Cardiovascular Disorders,</it> describing a non-linear, J-shaped curve between a series of cardiometabolic risk factors and depression. Although the exact meaning of these findings may not yet be clear, they represent a first step in a different way of thinking about the relationships among medical variables, namely going beyond the linear model.</p>
             <p>Please see related article: <url>http://www.biomedcentral.com/1471-2261/14/139</url>
             </p>
          </sec>
       </abs>
    </fm>
    <bdy>
       <sec>
          <st>
             <p>Background</p>
          </st>
          <p>Medical science is all about associations. Science becomes relevant if we can predict a variable <it>y</it> in which we are interested, from a variable <it>x</it> that we have at hand. We aim to develop models capable of making the most accurate predictions, or in other words, to develop the best representations of reality. Modern medical research is centered on associations, with the particular aim to assess whether these associations are causal. Many forms of associations are possible and linear associations are only a particular case. Linearity of an association is, in fact, a rather strong assumption to be made and most associations are likely to be non-linear. Yet, the majority of statistical tests used in medical research are based on that assumption.</p>
          <sec>
             <st>
                <p>Dynamic systems</p>
             </st>
             <p>In 2012, Sugihara <it>et al</it>. published a paper on how causality may be detected in complex ecosystems <abbrgrp>
                   <abbr bid="B1">1</abbr>
                </abbrgrp>. Take as an example a pond. In the pond, there is a variety of animal life, including daphnia (�water fleas�), small prey fish and larger predator fish. The pond is an ecosystem in which the presence of several organisms is causally related: an increase in the number of daphnia may be followed by an increase in the number of small prey fish as their food potential has increased. This increase of small prey fish may be followed by an increase in predator fish, effectively reducing the number of small prey fish. By that time, the number of daphnia may have decreased as well, or further increased as a function of the reduction in small fish. These associations are not linear: the association between two variables depends on the magnitude of the two variables and the presence of other internal (in the example, the daphnia and prey fish under the influence of the presence of predator fish) and external variables (for example, water temperature). In this system, the numbers of daphnia and small fish may be positively correlated at some point in time but negatively correlated or not correlated at all at other time points. In other words, in a complex, dynamic system such as a pond, associations between causally linked variables are <it>dynamic</it>, and summarizing them in a single value based on a linear function would give an inadequate representation of the true relationships.</p>
          </sec>
          <sec>
             <st>
                <p>Linear and non-linear associations in medical research</p>
             </st>
             <p>In medical research, we are interested in associations between variables describing the functions of human organisms. The ways associations have been studied are notably different for ecosystems and human organisms. Medical research has mostly been based on small numbers of assessments of many individuals, while ecosystems are often studied as a single entity with many assessments over time. Nevertheless, it is likely that human organisms behave in rather similar ways as ecosystems: there are many interdependent variables, and the association between two of these variables may depend on the magnitude of these variables and the levels of external variables. Take as an example the association between physical activity and depressive symptoms. The linear model would assume that any association between the two would be independent of the magnitude of the two variables (that is, a certain unit of increase in physical activity would lead to a certain increase in mood). However, the reality would probably be that an increase in mood would be achieved particularly when a person does not exercise regularly and would start exercising. However, when someone already exhibits high levels of regular exercise, a further increase in activity level would probably not lead to much mood gains. In addition, the relationship between physical activity and mood is probably bidirectional, meaning that mood could influence physical activity as well. To further complicate matters, the associations between exercise and mood may, in part, depend on external variables too, for example season of the year. As both mood and exercise are probably variables that tend to normalize, there may be complex feedback loops over time that can only be modelled using non-linear techniques applied to repeated assessments of both variables.</p>
          </sec>
          <sec>
             <st>
                <p>The link between depression and cardiometabolic disease</p>
             </st>
             <p>It is well established that an association between depression and cardiometabolic diseases exists, and that this association is complex and bidirectional <abbrgrp>
                   <abbr bid="B2">2</abbr>
                </abbrgrp>,<abbrgrp>
                   <abbr bid="B3">3</abbr>
                </abbrgrp>. The work by Jani <it>et al.</it>, published in <it>BMC Cardiovascular Disorders</it>, is one of the first steps in attempting to clarify the nature of these associations <abbrgrp>
                   <abbr bid="B4">4</abbr>
                </abbrgrp>. In a large sample of individuals with cardiometabolic disease, Jani <it>et al</it>. examined the association between blood pressure, body mass index (BMI), cholesterol, and HbA1c and elevated depressive symptoms (that is, a score &gt;7 on the depression subscale of the Hospital Anxiety and Depression Scale (HADS)). All associations were significant and non-linear; more specifically, they were J-shaped. This is remarkable as many of the cardiometabolic risk factors also have J-shaped associations with cardiovascular disease and mortality, for example as shown in <abbrgrp>
                   <abbr bid="B5">5</abbr>
                </abbrgrp>-<abbrgrp>
                   <abbr bid="B7">7</abbr>
                </abbrgrp>. While the meaning of these J-shaped associations are unclear at the moment, Jani <it>et al</it>. draw a pragmatic conclusion from their data: if the association between cardiometabolic risk factors and depression is J-shaped in this high-risk population, perhaps there is most use in depression screening in persons with either low or high cardiometabolic risk scores (if there is any use in screening).</p>
          </sec>
       </sec>
       <sec>
          <st>
             <p>Conclusions</p>
          </st>
          <p>Jani <it>et al</it>. took an important intellectual step by deviating from the linear model while examining the association between cardiometabolic risk factors and depression. On a conceptual level it may be concluded that we should dedicate more effort into adequate estimation of the shape of associations, instead of only testing the significance of a linear one. We hope that future research will continue on this road and look at these types of associations from a wider perspective, optimizing the shape of estimated associations, while increasingly taking into account potential bidirectional effects and external influences. Hopefully, this will ultimately lead to a better representation of the complexity of reality.</p>
       </sec>
       <sec>
          <st>
             <p>Competing interests</p>
          </st>
          <p>The authors declare that they have no competing interests.</p>
       </sec>
       <sec>
          <st>
             <p>Authors� contributions</p>
          </st>
          <p>PdJ initiated this work and wrote a first draft. AMR commented on the draft. PdJ takes full responsibility for the content of this work. Both authors read and approved the final manuscript.</p>
       </sec>
    </bdy>
    <bm>
       <ack>
          <sec>
             <st>
                <p>Acknowledgements</p>
             </st>
             <p>This study was supported by a VICI grant (no: 91812607) received by PdJ from the Netherlands organization for Scientific research (NWO-ZonMW).</p>
          </sec>
       </ack>
       <refgrp><bibl id="B1" rating="0"><title lang="en"><p>Detecting causality in complex ecosystems</p></title><aug><au ca="no" ce="no" da="no" pa="no"><snm>Sugihara</snm><fnm>G</fnm></au><au ca="no" ce="no" da="no" pa="no"><snm>May</snm><fnm>R</fnm></au><au ca="no" ce="no" da="no" pa="no"><snm>Ye</snm><fnm>H</fnm></au><au ca="no" ce="no" da="no" pa="no"><snm>Hsieh</snm><fnm>CH</fnm></au><au ca="no" ce="no" da="no" pa="no"><snm>Deyle</snm><fnm>E</fnm></au><au ca="no" ce="no" da="no" pa="no"><snm>Fogarty</snm><fnm>M</fnm></au><au ca="no" ce="no" da="no" pa="no"><snm>Munch</snm><fnm>S</fnm></au></aug><source>Science</source><pubdate>2012</pubdate><volume>338</volume><fpage>496</fpage><lpage>500</lpage><xrefbib><pubidlist><pubid idtype="doi">10.1126/science.1227079</pubid><pubid idtype="pmpid" link="fulltext">22997134</pubid></pubidlist></xrefbib></bibl><bibl id="B2" rating="0"><title lang="en"><p>Bidirectional association between depression and metabolic syndrome: a systematic review and meta-analysis of epidemiological studies</p></title><aug><au ca="no" ce="no" da="no" pa="no"><snm>Pan</snm><fnm>A</fnm></au><au ca="no" ce="no" da="no" pa="no"><snm>Keum</snm><fnm>N</fnm></au><au ca="no" ce="no" da="no" pa="no"><snm>Okereke</snm><fnm>OI</fnm></au><au ca="no" ce="no" da="no" pa="no"><snm>Sun</snm><fnm>Q</fnm></au><au ca="no" ce="no" da="no" pa="no"><snm>Kivimaki</snm><fnm>M</fnm></au><au ca="no" ce="no" da="no" pa="no"><snm>Rubin</snm><fnm>RR</fnm></au><au ca="no" ce="no" da="no" pa="no"><snm>Hu</snm><fnm>FB</fnm></au></aug><source>Diabetes Care</source><pubdate>2012</pubdate><volume>35</volume><fpage>1171</fpage><lpage>1180</lpage><xrefbib><pubidlist><pubid idtype="doi">10.2337/dc11-2055</pubid><pubid idtype="pmcid">3329841</pubid><pubid idtype="pmpid" link="fulltext">22517938</pubid></pubidlist></xrefbib></bibl><bibl id="B3" rating="0"><title lang="en"><p>Depression and cardiovascular disease: the end of simple models</p></title><aug><au ca="no" ce="no" da="no" pa="no"><snm>De Jonge</snm><fnm>P</fnm></au><au ca="no" ce="no" da="no" pa="no"><snm>Roest</snm><fnm>AM</fnm></au></aug><source>Br J Psychiatry</source><pubdate>2012</pubdate><volume>201</volume><fpage>337</fpage><lpage>338</lpage><xrefbib><pubidlist><pubid idtype="doi">10.1192/bjp.bp.112.110502</pubid><pubid idtype="pmpid" link="fulltext">23118031</pubid></pubidlist></xrefbib></bibl><bibl id="B4" rating="0"><title lang="en"><p>Revisiting the J shaped curve, exploring the association between cardiovascular risk factors and concurrent depressive symptoms in patients with cardiometabolic disease: findings from a large cross-sectional study</p></title><aug><au ca="no" ce="no" da="no" pa="no"><snm>Jani</snm><fnm>BD</fnm></au><au ca="no" ce="no" da="no" pa="no"><snm>Cavanagh</snm><fnm>J</fnm></au><au ca="no" ce="no" da="no" pa="no"><snm>Barry</snm><fnm>S</fnm></au><au ca="no" ce="no" da="no" pa="no"><snm>Der</snm><fnm>G</fnm></au><au ca="no" ce="no" da="no" pa="no"><snm>Sattar</snm><fnm>N</fnm></au><au ca="no" ce="no" da="no" pa="no"><snm>Mair</snm><fnm>FS</fnm></au></aug><source>BMC Cardiovasc Disord</source><pubdate>2014</pubdate><volume>14</volume><fpage>139</fpage><xrefbib><pubidlist><pubid idtype="doi">10.1186/1471-2261-14-139</pubid><pubid idtype="pmpid" link="fulltext">25352020</pubid></pubidlist></xrefbib></bibl><bibl id="B5" rating="0"><title lang="en"><p>The J-curve between blood pressure and coronary artery disease or essential hypertension: exactly how essential?</p></title><aug><au ca="no" ce="no" da="no" pa="no"><snm>Messerli</snm><fnm>FH</fnm></au><au ca="no" ce="no" da="no" pa="no"><snm>Panjrath</snm><fnm>GS</fnm></au></aug><source>J Am Coll Cardiol</source><pubdate>2009</pubdate><volume>54</volume><fpage>1827</fpage><lpage>1834</lpage><xrefbib><pubidlist><pubid idtype="doi">10.1016/j.jacc.2009.05.073</pubid><pubid idtype="pmpid" link="fulltext">19892233</pubid></pubidlist></xrefbib></bibl><bibl id="B6" rating="0"><title lang="en"><p>Association of all-cause mortality with overweight and obesity using standard body mass index categories: a systematic review and meta-analysis</p></title><aug><au ca="no" ce="no" da="no" pa="no"><snm>Flegal</snm><fnm>KM</fnm></au><au ca="no" ce="no" da="no" pa="no"><snm>Kit</snm><fnm>BK</fnm></au><au ca="no" ce="no" da="no" pa="no"><snm>Orpana</snm><fnm>H</fnm></au><au ca="no" ce="no" da="no" pa="no"><snm>Graubard</snm><fnm>BI</fnm></au></aug><source>JAMA</source><pubdate>2013</pubdate><volume>309</volume><fpage>71</fpage><lpage>82</lpage><xrefbib><pubidlist><pubid idtype="doi">10.1001/jama.2012.113905</pubid><pubid idtype="pmpid" link="fulltext">23280227</pubid></pubidlist></xrefbib></bibl><bibl id="B7" rating="0"><title lang="en"><p>Effects of intensive glucose lowering in type 2 diabetes</p></title><aug><au ca="no" ce="no" da="no" pa="no"><snm>Gerstein</snm><fnm>HC</fnm></au><au ca="no" ce="no" da="no" pa="no"><snm>Miller</snm><fnm>ME</fnm></au><au ca="no" ce="no" da="no" pa="no"><snm>Byington</snm><fnm>RP</fnm></au><au ca="no" ce="no" da="no" pa="no"><snm>Goff</snm><fnm>DC</fnm></au><au ca="no" ce="no" da="no" pa="no"><snm>Bigger</snm><fnm>JT</fnm></au><au ca="no" ce="no" da="no" pa="no"><snm>Buse</snm><fnm>JB</fnm></au><au ca="no" ce="no" da="no" pa="no"><snm>Cushman</snm><fnm>WC</fnm></au><au ca="no" ce="no" da="no" pa="no"><snm>Genuth</snm><fnm>S</fnm></au><au ca="no" ce="no" da="no" pa="no"><snm>Ismail-Beigi</snm><fnm>F</fnm></au><au ca="no" ce="no" da="no" pa="no"><snm>Grimm</snm><fnm>RH</fnm></au><au ca="no" ce="no" da="no" pa="no"><snm>Probstfield</snm><fnm>JL</fnm></au><au ca="no" ce="no" da="no" pa="no"><snm>Simons-Morton</snm><fnm>DG</fnm></au><au ca="no" ce="no" da="no" pa="no"><snm>Friedewald</snm><fnm>WT</fnm></au></aug><source>N Engl J Med</source><pubdate>2008</pubdate><volume>358</volume><fpage>2545</fpage><lpage>2559</lpage><xrefbib><pubidlist><pubid idtype="doi">10.1056/NEJMoa0802743</pubid><pubid idtype="pmpid" link="fulltext">18539917</pubid></pubidlist></xrefbib></bibl></refgrp>
    <sec><st><p>Pre-publication history</p></st><p>The pre-publication history for this paper can be accessed here:</p><p><url>http://www.biomedcentral.com/1741-7015/12/199/prepub</url></p></sec></bm>
 </art>