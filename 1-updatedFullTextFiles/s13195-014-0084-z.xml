<art>
    <ui>s13195-014-0084-z</ui>
    <ji>1758-9193</ji>
    <fm>
       <dochead>Commentary</dochead>
       <bibl rating="0">
          <title lang="en">
             <p>Applying a cumulative deficit model of frailty to dementia: progress and future challenges</p>
          </title>
          <aug>
             <au ca="yes" ce="no" da="no" id="A1" pa="no">
                <snm>Anstey</snm>
                <mi>J</mi>
                <fnm>Kaarin</fnm>
                <insr iid="I1"/>
                <email>kaarin.anstey@anu.edu.au</email>
             </au>
             <au ca="no" ce="no" da="no" id="A2" pa="no">
                <snm>Dixon</snm>
                <mi>A</mi>
                <fnm>Roger</fnm>
                <insr iid="I2"/>
                <email>rdixon@ualberta.ca</email>
             </au>
          </aug>
          <insg>
             <ins id="I1">
                <p>Centre for Research on Ageing, Health and Wellbeing, Australian National University, 63 Eggleston Road, Canberra ACT 0200, Australia</p>
             </ins>
             <ins id="I2">
                <p>Department of Psychology, P217 Biological Sciences Building, University of Alberta, Edmonton T6G 2E9, AB, Canada</p>
             </ins>
          </insg>
          <source>Alzheimer's Research &amp; Therapy</source>
          <issn>1758-9193</issn>
          <pubdate>2014</pubdate>
          <volume>6</volume>
          <issue>9</issue>
          <fpage>84</fpage>
          <url>http://alzres.com/content/6/9/84</url>
          <xrefbib>
             <pubidlist>
                <pubid idtype="doi">10.1186/s13195-014-0084-z</pubid>
             <pubid idtype="pmpid">25426173</pubid></pubidlist>
          </xrefbib>
       </bibl>
       <history>
          <pub>
             <date>
                <day>26</day>
                <month>11</month>
                <year>2014</year>
             </date>
          </pub>
       </history>
       <cpyrt>
          <year>2014</year>
          <collab>Anstey and Dixon; licensee BioMed Central Ltd.</collab>
          <note>The licensee has exclusive rights to distribute this article, in any medium, for 12 months following its publication. After this time, the article is available under the terms of the Creative Commons Attribution License (<url>http://creativecommons.org/licenses/by/4.0</url>), which permits unrestricted use, distribution, and reproduction in any medium, provided the original work is properly credited. The Creative Commons Public Domain Dedication waiver (<url>http://creativecommons.org/publicdomain/zero/1.0/</url>) applies to the data made available in this article, unless otherwise stated.</note>
       </cpyrt>
       <abs lang="en">
          <sec>
             <st>
                <p>Abstract</p>
             </st>
             <p>The article by Song and colleagues presents findings from the Canadian Study of Health and Aging showing that the accumulation of health deficits, defined dichotomously and unqualified by severity or domain, predicted late-life dementia independent of chronological age. We identify strengths of this model, and also areas for future research. Importantly, this article broadens the perspective of research into measuring risk of dementia from focusing on specific neuropathological markers of dementia subtypes, to mechanisms underlying more general bodily vitality and health, as well as dysfunctions in repair. This work places late-life dementia in a new context, influenced more broadly by health maintenance, and less by specific neurological disease. While useful at a global level, the lack of specificity of this approach may ultimately limit its application to individual patients because without linking risk to etiology, assessment does not indicate an intervention. Ultimately, the article has value for stimulating debate about approaches to risk identification and risk reduction, suggesting that the current focus on cardiometabolic risk factors may be too limited.</p>
          </sec>
       </abs>
    </fm>
    <bdy>
       <sec>
          <st>
             <p>Commentary</p>
          </st>
          <p>In their recent article Song and colleagues <abbrgrp>
                <abbr bid="B1">1</abbr>
             </abbrgrp> present findings from the Canadian Study of Health and Aging showing that the accumulation of health deficits, defined dichotomously and unqualified by severity or domain, predicted late-life dementia independent of chronological age. Many of the deficits included in their statistical models were not traditional or specific dementia risk factors (for example, vascular), but were health problems related to various conditions associated with skin, ears, eyes, foot, nose and even attitudes. Such health problems have been used previously in accumulation indices of frailty <abbrgrp>
                <abbr bid="B2">2</abbr>
             </abbrgrp> and dementia <abbrgrp>
                <abbr bid="B3">3</abbr>
             </abbrgrp>. The rigorous statistical models applied demonstrate that these associations were robust findings <abbrgrp>
                <abbr bid="B3">3</abbr>
             </abbrgrp>,<abbrgrp>
                <abbr bid="B4">4</abbr>
             </abbrgrp>.</p>
          <p>The authors� programmatic work makes several contributions to epidemiology of dementia. They demonstrate the predictive value of considering multiple health deficits in the aggregate, and that various combinations predict adverse outcomes, including falls, delirium, disability, and now dementia <abbrgrp>
                <abbr bid="B1">1</abbr>
             </abbrgrp>-<abbrgrp>
                <abbr bid="B3">3</abbr>
             </abbrgrp>,<abbrgrp>
                <abbr bid="B5">5</abbr>
             </abbrgrp>. An important conclusion is that overall health status is a major or contributing risk factor for dementia. Although specific mechanisms are deliberately not evaluated in this work, the authors propose that the generality of associations of poor health with dementia may be related to impaired repair mechanisms, or exhausted repair responses. These deficient processes manifest across multiple domains of health and are not necessarily or directly related to neurological function.</p>
          <p>We turn now to brief considerations of selected conceptual, methodological, and clinical challenges for future progress in this important line of research. The general proposition of the authors <abbrgrp>
                <abbr bid="B1">1</abbr>
             </abbrgrp> is that the accumulation of health deficits provides an index of general health. However, there is a potential conceptual circularity to the explanation if general health is defined simply (and dichotomously) by the presence or absence of physical health problems and feelings of vitality and energy. Would additional conceptual clarity be achieved by consideration of severities, durations, clusters, or weightings of conditions (along a continuum from the proximal to distal <it>vis-�-vis</it> dementia)? Also absent from the accumulation of deficits index is any temporal ordering of deficits. Temporal ordering is a feature of cascade theories of ageing in which accumulation of deficits occur in a general sequence, potentially leading to an acceleration of decline once a threshold is reached. The most prominent example of a cascade theory is the amyloid hypothesis of Alzheimer�s disease <abbrgrp>
                <abbr bid="B6">6</abbr>
             </abbrgrp>.</p>
          <p>Song and colleagues� index <abbrgrp>
                <abbr bid="B1">1</abbr>
             </abbrgrp> focuses on deficits, in contrast to indices of functional capacity that include measures of capacity or biological health, referred to as �Bioage�. Bioage indices typically weight the component measures, and represent general and available biological functions and ability. They have been linked to a variety of adverse outcomes (for example, cognitive decline, longevity), comparing favorably with chronological age <abbrgrp>
                <abbr bid="B7">7</abbr>
             </abbrgrp>.</p>
          <p>Although a health status index predicts important adverse outcomes (from falls to dementia), the authors acknowledge theoretical and clinical limitations. Framed as challenges for future development, these include the notion that dementia is a broad clinical outcome produced by underlying but heterogeneous neuropathological pathways. The mechanisms associated with dementia are likely to differ depending on underlying neurodegenerative disease processes. The cumulative health deficit approach is remarkably successful at predicting dementia, but dementia <it>per se</it> may not be the principal outcome of interest theoretically or clinically (in terms of interventions). Whereas it is true (as the authors point out) that single or candidate risk factors are also unlikely to produce good status predictions or unqualified insights into mechanisms, the dementia biomarker and risk factor field is moving rapidly to close the gap between the single and the multiple markers and moderators of sporadic neurodegenerative disease. Contemporary approaches include additive (similar to the authors) or panel models, but also multiplicative (interactional) models, multiple and cross-domain approaches, and even broad-based OMICs-type approaches (linking the global and specific) <abbrgrp>
                <abbr bid="B8">8</abbr>
             </abbrgrp>. A single dementia predictive index of health deficits does not allow for specificity in relation to the type of dementia or the etiology of cognitive impairment. Such specific information may be useful, if not crucial, in devising interventions to delay the onset of dementia because intervention work requires some degree of knowledge about mechanisms. In the future, the deficit accumulation approach may benefit from coordinating information that provides specificity or discrimination of dementias. Much current research into biomarkers and risk factors for dementia focuses on identifying biomarkers that relate to specific disease processes <abbrgrp>
                <abbr bid="B9">9</abbr>
             </abbrgrp>-<abbrgrp>
                <abbr bid="B11">11</abbr>
             </abbrgrp> which will ultimately inform diagnosis and treatment. Hence, their eventual utility may be greater than (but supplemented by) a generic index.</p>
          <p>To promote understanding of the statistical associations between this index and dementia, a tangible biological mechanism or biomarker of the repair mechanism underlying health failures is required. The significance of this index could be better understood by estimating the variance it explains that is either shared with, or independent of, traditional risk factors or indices that have been developed from these. Demonstration of the utility of the index in identifying preclinical cognitive decline would increase its significance. At present, the authors� approach could be enhanced by a focus on early detection and selective diagnoses, as this preclinical period is likely the one most promising for specific interventions.</p>
       </sec>
       <sec>
          <st>
             <p>Conclusion</p>
          </st>
          <p>Importantly, Song and colleagues <abbrgrp>
                <abbr bid="B1">1</abbr>
             </abbrgrp> broaden the perspective of research into measuring risk of dementia from focusing on specific causes of neuropathological markers of dementia subtypes, to mechanisms underlying more general bodily vitality and health, as well as dysfunctions in repair. This places late-life dementia in a new context, influenced more broadly by health maintenance, and less by specific neurological disease. This approach may benefit from further integration - rather than separation - into the emerging multi-variable and mechanism-related approaches to biomarker and risk factor research. An intriguing question is whether the identification of a poor repair mechanism would explain the occurrence of the individual risk factors (or vice versa). It would also be interesting to know how the frailty index relates to genetic markers of longevity (and neurodegenerative disease). Most importantly, it would be useful to discover whether an intervention to improve �repair mechanisms� could have widespread benefits across multiple cognitive health conditions or is better supplemented by specific mechanism-related therapeutics.</p>
       </sec>
       <sec>
          <st>
             <p>Competing interests</p>
          </st>
          <p>The authors declare that they have no competing interests.</p>
       </sec>
    </bdy>
    <bm>
       <ack>
          <sec>
             <st>
                <p>Acknowledgements</p>
             </st>
             <p>KJA is funded by NHMRC Research Fellowship # 1002560. RAD is supported in part by a Canada Research Chair (Tier 1). The research is supported by the Dementia Collaborative Research Centres (to KJA) and the National Institutes of Health (National Institute on Aging, R01 AG008235, to RAD).</p>
          </sec>
       </ack>
       <refgrp>
          
          <bibl id="B1" rating="0">
             <title lang="en">
                <p>Age-related deficit accumulation and the risk of late-life dementia</p>
             </title>
             <aug>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Song</snm>
                   <fnm>X</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Mitnitski</snm>
                   <fnm>A</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Rockwood</snm>
                   <fnm>K</fnm>
                </au>
             </aug>
             <source>Alzheimer�s Res Ther</source>
             <pubdate>2014</pubdate>
             <volume>6</volume>
             <fpage>54</fpage>
             <xrefbib>
                <pubidlist>
                   <pubid idtype="doi">10.1186/s13195-014-0054-5</pubid>
                </pubidlist>
             </xrefbib>
          </bibl>
          <bibl id="B2" rating="0">
             <title lang="en">
                <p>Frailty in relation to the risk of falls, fractures, and mortality in older Chinese adults: results from the Beijing Longitudinal Study of Aging</p>
             </title>
             <aug>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Fang</snm>
                   <fnm>X</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Shi</snm>
                   <fnm>J</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Song</snm>
                   <fnm>X</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Mitnitski</snm>
                   <fnm>A</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Tang</snm>
                   <fnm>Z</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Wang</snm>
                   <fnm>C</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Yu</snm>
                   <fnm>P</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Rockwood</snm>
                   <fnm>K</fnm>
                </au>
             </aug>
             <source>J Nutr Health Aging</source>
             <pubdate>2012</pubdate>
             <volume>16</volume>
             <fpage>903</fpage>
             <lpage>907</lpage>
             <xrefbib>
                <pubidlist>
                   <pubid idtype="doi">10.1007/s12603-012-0368-6</pubid>
                   <pubid idtype="pmpid" link="fulltext">23208030</pubid>
                </pubidlist>
             </xrefbib>
          </bibl>
          <bibl id="B3" rating="0">
             <title lang="en">
                <p>Nontraditional risk factors combine to predict Alzheimer disease and dementia</p>
             </title>
             <aug>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Song</snm>
                   <fnm>X</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Mitnitski</snm>
                   <fnm>A</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Rockwood</snm>
                   <fnm>K</fnm>
                </au>
             </aug>
             <source>Neurology</source>
             <pubdate>2011</pubdate>
             <volume>77</volume>
             <fpage>227</fpage>
             <lpage>234</lpage>
             <xrefbib>
                <pubidlist>
                   <pubid idtype="doi">10.1212/WNL.0b013e318225c6bc</pubid>
                   <pubid idtype="pmpid" link="fulltext">21753161</pubid>
                </pubidlist>
             </xrefbib>
          </bibl>
          <bibl id="B4" rating="0">
             <title lang="en">
                <p>The Canadian Study of Health and Aging: risk factors for vascular dementia</p>
             </title>
             <aug>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Lindsay</snm>
                   <fnm>J</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Hebert</snm>
                   <fnm>R</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Rockwood</snm>
                   <fnm>K</fnm>
                </au>
             </aug>
             <source>Stroke</source>
             <pubdate>1997</pubdate>
             <volume>28</volume>
             <fpage>526</fpage>
             <lpage>530</lpage>
             <xrefbib>
                <pubidlist>
                   <pubid idtype="doi">10.1161/01.STR.28.3.526</pubid>
                   <pubid idtype="pmpid" link="fulltext">9056606</pubid>
                </pubidlist>
             </xrefbib>
          </bibl>
          <bibl id="B5" rating="0">
             <title lang="en">
                <p>Frailty in elderly people</p>
             </title>
             <aug>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Clegg</snm>
                   <fnm>A</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Young</snm>
                   <fnm>J</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Iliffe</snm>
                   <fnm>S</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Rikkert</snm>
                   <fnm>MO</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Rockwood</snm>
                   <fnm>K</fnm>
                </au>
             </aug>
             <source>Lancet</source>
             <pubdate>2013</pubdate>
             <volume>381</volume>
             <fpage>752</fpage>
             <lpage>762</lpage>
             <xrefbib>
                <pubidlist>
                   <pubid idtype="doi">10.1016/S0140-6736(12)62167-9</pubid>
                   <pubid idtype="pmpid" link="fulltext">23395245</pubid>
                </pubidlist>
             </xrefbib>
          </bibl>
          <bibl id="B6" rating="0">
             <title lang="en">
                <p>Hypothetical model of dynamic biomarkers of the Alzheimer�s pathological cascade</p>
             </title>
             <aug>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Jack</snm>
                   <fnm>CR</fnm>
                   <suf>Jr</suf>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Knopman</snm>
                   <fnm>DS</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Jagust</snm>
                   <fnm>WJ</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Shaw</snm>
                   <fnm>LM</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Aisen</snm>
                   <fnm>PS</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Weiner</snm>
                   <fnm>MW</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Petersen</snm>
                   <fnm>RC</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Trojanowski</snm>
                   <fnm>JQ</fnm>
                </au>
             </aug>
             <source>Lancet Neurol</source>
             <pubdate>2010</pubdate>
             <volume>9</volume>
             <fpage>119</fpage>
             <lpage>128</lpage>
             <xrefbib>
                <pubidlist>
                   <pubid idtype="doi">10.1016/S1474-4422(09)70299-6</pubid>
                   <pubid idtype="pmpid" link="fulltext">20083042</pubid>
                </pubidlist>
             </xrefbib>
          </bibl>
          <bibl id="B7" rating="0">
             <title lang="en">
                <p>Linking biological and cognitive aging: toward improving characterizations of developmental time</p>
             </title>
             <aug>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>MacDonald</snm>
                   <fnm>SWS</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>DeCarlo</snm>
                   <fnm>CA</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Dixon</snm>
                   <fnm>RA</fnm>
                </au>
             </aug>
             <source>J Gerontol B Psychol Sci Soc Sci</source>
             <pubdate>2011</pubdate>
             <volume>66</volume>
             <fpage>i59</fpage>
             <lpage>i70</lpage>
             <xrefbib>
                <pubidlist>
                   <pubid idtype="doi">10.1093/geronb/gbr039</pubid>
                   <pubid idtype="pmpid" link="fulltext">21743053</pubid>
                </pubidlist>
             </xrefbib>
          </bibl>
          <bibl id="B8" rating="0">
             <title lang="en">
                <p>Changing perspectives regarding late-life dementia</p>
             </title>
             <aug>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Fotuhi</snm>
                   <fnm>M</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Hachinski</snm>
                   <fnm>V</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Whitehouse</snm>
                   <fnm>PJ</fnm>
                </au>
             </aug>
             <source>Nat Rev Neurol</source>
             <pubdate>2009</pubdate>
             <volume>5</volume>
             <fpage>649</fpage>
             <lpage>658</lpage>
             <xrefbib>
                <pubidlist>
                   <pubid idtype="doi">10.1038/nrneurol.2009.175</pubid>
                   <pubid idtype="pmpid" link="fulltext">19918254</pubid>
                </pubidlist>
             </xrefbib>
          </bibl>
          <bibl id="B9" rating="0">
             <title lang="en">
                <p>Predicting risk of dementia in older adults: the late-life dementia risk index</p>
             </title>
             <aug>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Barnes</snm>
                   <fnm>DE</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Covinsky</snm>
                   <fnm>KE</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Whitmer</snm>
                   <fnm>RA</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Kuller</snm>
                   <fnm>LH</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Lopez</snm>
                   <fnm>OL</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Yaffe</snm>
                   <fnm>K</fnm>
                </au>
             </aug>
             <source>Neurology</source>
             <pubdate>2009</pubdate>
             <volume>73</volume>
             <fpage>173</fpage>
             <lpage>179</lpage>
             <xrefbib>
                <pubidlist>
                   <pubid idtype="doi">10.1212/WNL.0b013e3181a81636</pubid>
                   <pubid idtype="pmpid" link="fulltext">19439724</pubid>
                </pubidlist>
             </xrefbib>
          </bibl>
          <bibl id="B10" rating="0">
             <title lang="en">
                <p>Risk score for the prediction of dementia risk in 20�years among middle aged people: a longitudinal, population-based study</p>
             </title>
             <aug>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Kivipelto</snm>
                   <fnm>M</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Ngandu</snm>
                   <fnm>T</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Laatikainen</snm>
                   <fnm>T</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Winblad</snm>
                   <fnm>B</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Soininen</snm>
                   <fnm>H</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Tuomilehto</snm>
                   <fnm>J</fnm>
                </au>
             </aug>
             <source>Lancet Neurol</source>
             <pubdate>2006</pubdate>
             <volume>5</volume>
             <fpage>735</fpage>
             <lpage>741</lpage>
             <xrefbib>
                <pubidlist>
                   <pubid idtype="doi">10.1016/S1474-4422(06)70537-3</pubid>
                   <pubid idtype="pmpid" link="fulltext">16914401</pubid>
                </pubidlist>
             </xrefbib>
          </bibl>
          <bibl id="B11" rating="0">
             <title lang="en">
                <p>A self report risk index to predict occurrence of dementia in three independent cohorts of older adults: the ANU-ADRI</p>
             </title>
             <aug>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Anstey</snm>
                   <fnm>KJ</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Cherbuin</snm>
                   <fnm>N</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Herath</snm>
                   <fnm>P</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Qui</snm>
                   <fnm>C</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Kuller</snm>
                   <fnm>LH</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Lopez</snm>
                   <fnm>OL</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Wilson</snm>
                   <fnm>RS</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Fratiglioni</snm>
                   <fnm>L</fnm>
                </au>
             </aug>
             <source>PLoS One</source>
             <pubdate>2014</pubdate>
             <volume>9</volume>
             <fpage>e86141</fpage>
             <xrefbib>
                <pubidlist>
                   <pubid idtype="doi">10.1371/journal.pone.0086141</pubid>
                   <pubid idtype="pmpid" link="fulltext">24465922</pubid>
                </pubidlist>
             </xrefbib>
          </bibl>
       </refgrp>
    </bm>
 </art>