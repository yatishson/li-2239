<art>
    <ui>s13073-014-0104-4</ui>
    <ji>1756-994X</ji>
    <fm>
       <dochead>Research highlight</dochead>
       <bibl rating="0">
          <title lang="en">
             <p>Stopping outbreaks with real-time genomic epidemiology</p>
          </title>
          <aug>
             <au ca="yes" ce="no" da="no" id="A1" pa="no">
                <snm>Tang</snm>
                <fnm>Patrick</fnm>
                <insr iid="I1"/>
                <insr iid="I2"/>
                <email>patrick.tang@bccdc.ca</email>
             </au>
             <au ca="no" ce="no" da="no" id="A2" pa="no">
                <snm>Gardy</snm>
                <mi>L</mi>
                <fnm>Jennifer</fnm>
                <insr iid="I1"/>
                <insr iid="I3"/>
                <email>jennifer.gardy@bccdc.ca</email>
             </au>
          </aug>
          <insg>
             <ins id="I1">
                <p>British Columbia Centre for Disease Control, 655 West 12th Avenue, Vancouver V5Z 4R4, BC, Canada</p>
             </ins>
             <ins id="I2">
                <p>Department of Pathology and Laboratory Medicine, University of British Columbia, Vancouver V6T 2B5, BC, Canada</p>
             </ins>
             <ins id="I3">
                <p>School of Population and Public Health, University of British Columbia, Vancouver V6T 1Z3, BC, Canada</p>
             </ins>
          </insg>
          <source>Genome Medicine</source>
          <issn>1756-994X</issn>
          <pubdate>2014</pubdate>
          <volume>6</volume>
          <issue>11</issue>
          <fpage>104</fpage>
          <url>http://genomemedicine.com/content/6/11/104</url>
          <xrefbib>
             <pubidlist>
                <pubid idtype="doi">10.1186/s13073-014-0104-4</pubid>
             <pubid idtype="pmpid">25593591</pubid></pubidlist>
          </xrefbib>
       </bibl>
       <history>
          <pub>
             <date>
                <day>20</day>
                <month>11</month>
                <year>2014</year>
             </date>
          </pub>
       </history>
       <cpyrt>
          <year>2014</year>
          <collab>Tang and Gardy; licensee BioMed Central Ltd.</collab>
          <note>The licensee has exclusive rights to distribute this article, in any medium, for 12 months following its publication. After this time, the article is available under the terms of the Creative Commons Attribution License (<url>http://creativecommons.org/licenses/by/4.0</url>), which permits unrestricted use, distribution, and reproduction in any medium, provided the original work is properly credited. The Creative Commons Public Domain Dedication waiver (<url>http://creativecommons.org/publicdomain/zero/1.0/</url>) applies to the data made available in this article, unless otherwise stated.</note>
       </cpyrt>
       <abs lang="en">
          <sec>
             <st>
                <p>Abstract</p>
             </st>
             <p>One of the most successful public health applications of next-generation sequencing is whole-genome sequencing of pathogens to not only detect and characterize outbreaks, but also to inform outbreak management. Using genomics, infection control teams can now track, with extraordinarily high resolution, the transmission events within outbreaks, opening up possibilities for targeted interventions. These successes are positioning the emerging field of genomic epidemiology to replace traditional molecular epidemiology, and increasing our ability to limit the spread of multidrug-resistant organisms.</p>
          </sec>
       </abs>
    </fm>
    <bdy>
       <sec>
          <st>
             <p>Genomic epidemiology for healthcare-associated infections</p>
          </st>
          <p>Healthcare-associated infections (HAIs) are a significant cause of morbidity and mortality in hospitalized patients and represent a major economic burden for healthcare systems. In the United Kingdom, it has been estimated that as many as 300,000 HAIs occur annually at a cost of over �1 billion per year, and that, at any given time, one in every fifteen hospital patients has an HAI <abbrgrp>
                <abbr bid="B1">1</abbr>
             </abbrgrp>. Evidence suggests that approximately 20% of HAIs are preventable <abbrgrp>
                <abbr bid="B2">2</abbr>
             </abbrgrp> and, indeed, HAI point prevalence - the percentage of hospitalized patients with an HAI at any point in time - is decreasing in the United Kingdom, down to 6.4% in 2011 from a high of 9.2% in 1980. However, factors including breakdowns in infection prevention and control practices, unrecognized transmission in the community, and importation of new strains of antimicrobial-resistant pathogens from endemic regions of the world mean that hospitals are regularly seeing the introduction and onward transmission of HAIs in their settings. While surveillance and screening, in combination with molecular genotyping, can indicate the presence of a nosocomial outbreak, conventional molecular epidemiology methods lack sufficient resolution to reveal the origins and transmission dynamics of these outbreaks - information integral to implementing appropriate and effective infection control strategies.</p>
          <p>Over the past few decades, a series of molecular epidemiology methods, including pulsed field gel electrophoresis and multi-locus sequence typing, have been developed to estimate phylogenetic relationships between bacterial isolates - each one trying to improve upon the speed, accuracy, reproducibility, ease of use or discriminatory power of previous methods. However, the introduction of next-generation genome sequencing technology has trumped most of these iterative improvements by offering the ultimate in discriminatory power at a relatively low cost. It has the additional benefits of being able to predict antimicrobial resistance phenotypes and identify virulence factors. The potential of this new �genomic� epidemiology for the detection, characterization and management of infectious disease outbreaks, as demonstrated by Pallen and colleagues in this issue of <it>Genome Medicine</it>
             <abbrgrp>
                <abbr bid="B3">3</abbr>
             </abbrgrp>, is tremendous. Genomic epidemiology has been instrumental for resolving hospital outbreaks, sometimes disproving previous assumptions regarding nosocomial pathogen transmission. For example, in a recent study of <it>Staphylococcus aureus</it> transmission in an intensive care unit (ICU), whole-genome sequencing revealed new transmission events that were missed, and disproved transmission events that were falsely predicted by conventional genotyping <abbrgrp>
                <abbr bid="B4">4</abbr>
             </abbrgrp>. Another study using genomics to study vancomycin-resistant enterococci (VRE) revealed that <it>de novo</it> acquisition of vancomycin resistance in <it>Enterococcus faecium</it> is probably underappreciated in the hospital environment and that VRE screening at admission may not be sufficient to control VRE within hospitals <abbrgrp>
                <abbr bid="B5">5</abbr>
             </abbrgrp>.</p>
       </sec>
       <sec>
          <st>
             <p>Recent examples of genomic epidemiology in real time</p>
          </st>
          <p>Most genomic epidemiology studies to date have retrospectively analyzed outbreaks, and although this has revealed important insights into pathogen transmission dynamics, the challenge has been to apply genomic epidemiology to directly impact an ongoing outbreak. Only a handful of nosocomial outbreak studies have been performed in real time with the goal of reducing the duration and impact of transmission, including important early work in an outbreak of methicillin-resistant <it>S. aureus</it> on a neonatal intensive care ward <abbrgrp>
                <abbr bid="B6">6</abbr>
             </abbrgrp> and a carbapenem-resistant <it>Klebsiella pneumoniae</it> outbreak that persisted despite early infection control measures <abbrgrp>
                <abbr bid="B7">7</abbr>
             </abbrgrp>.</p>
          <p>In 2010, Pallen and colleagues were the first to use whole-genome sequencing to identify a person-to-person transmission event in an infectious disease outbreak, sequencing six isolates of multi-drug resistant (MDR) <it>Acinetobacter baumannii</it> from a 2008 hospital cluster to trace transmission between a military and a civilian patient <abbrgrp>
                <abbr bid="B8">8</abbr>
             </abbrgrp>. Now, they report the results of a genomic investigation of a protracted MDR <it>A. baumannii</it> outbreak involving a novel strain of the bacterium not previously observed in hospitals in the United Kingdom or other strain collections <abbrgrp>
                <abbr bid="B9">9</abbr>
             </abbrgrp>.</p>
          <p>The outbreak began with the importation of the novel MDR <it>A. baumannii</it> via a military patient, with early secondary cases - linked through conventional molecular epidemiology techniques - occurring on the same ward. At week 40 of what ultimately became an 80-week outbreak, the authors replaced traditional molecular epidemiology methods with whole-genome sequencing, noting that with the less than 1-week turnaround time they achieved with genomics, they were able to more rapidly rule in or rule out isolates as belonging to the outbreak. Of the 102 clinical isolates successfully sequenced, a threshold of less than or equal to 8 single nucleotide variants (SNVs) ruled in 74 genomes as belonging to a single large outbreak, including 52 from individual patient isolates and 10 from environmental sampling.</p>
          <p>Phylogenetic analysis of the 74 genomes identified 32 distinct genotypes belonging to seven major clusters. Using a Python script that factors in patient genotype, the ward patients are housed in, and the date of their first positive test, the authors refined the 273 possible transmission events suggested by epidemiology alone to the 57 supported by the genomic data. In this fashion, they established the most parsimonious source of infection for all but 10 patients. The genomic epidemiology suggested that early transmissions occurred through ward-based contact but also through long-term environmental contamination of specific wards, which prompted improved ward decontamination procedures. The genomics also implicated a specific operating theatre for burns patients in several transmissions, leading the infection control team to perform a deep clean of the theatre.</p>
          <p>Despite the ward and theatre decontaminations, which had initially appeared to halt the outbreak�s spread, another series of cases occurred from week 70 onwards. The genomic investigation linked the first of these to a contaminated bed, prompting the development of a cleaning protocol specific to this type of bed, with subsequent cases traced again to the burns theatre. Following a second deep cleaning of the theatre, no further transmissions were observed and the outbreak was declared over at week 80.</p>
          <p>The real-time use of genomics to reveal transmissions and target infection control interventions to the correct place - be it a ward, operating theatre, or bed - is the most notable aspect of this comprehensive and important work, clearly demonstrating the immediate impact that genomics-informed interventions can have on stopping transmission. It is also worth noting the authors� use of a software script to develop a putative transmission network - automated approaches can make genomic epidemiology more tractable for infection control teams that may not have specific expertise in interpreting genomic data through the lens of traditional epidemiological relationships.</p>
       </sec>
       <sec>
          <st>
             <p>What the future holds</p>
          </st>
          <p>With this work, Pallen and colleagues make a convincing case for the utility of whole-genome sequencing as an integral part of infection control practice, demonstrating that it can be done in a clinical setting in real time and that it can lead to evidence-based and effective interventions to stop even a large and protracted hospital outbreak. Given continued advances in technology, such as single-molecule sequencing <abbrgrp>
                <abbr bid="B10">10</abbr>
             </abbrgrp> and bioinformatics methods to resolve mixed infections <abbrgrp>
                <abbr bid="B11">11</abbr>
             </abbrgrp>, the accurate and rapid response platform provided by next-generation sequencing will become a cornerstone of infection control. We envision a near future in which hospital laboratories are equipped with genome sequencing technology, enabling pathogen genomes to be derived from direct sequencing of clinical samples, with automated analysis methods to predict drug resistance or to identify clusters of related genomes suggestive of an outbreak. These data will inform the hospital�s infection control program, allowing for real-time evidence-based management of outbreaks, and ultimately decreasing the prevalence of HAIs.</p>
       </sec>
       <sec>
          <st>
             <p>Abbreviations</p>
          </st>
          <p>HAI: Healthcare-associated infection</p>
          <p>MDR: Multi-drug resistant</p>
          <p>SNV: Single nucleotide variant</p>
          <p>VRE: Vancomycin-resistant enterococci</p>
       </sec>
       <sec>
          <st>
             <p>Competing interests</p>
          </st>
          <p>The authors declare that they have no competing interests.</p>
       </sec>
    </bdy>
    <bm>
       <refgrp>
          
          <bibl id="B1" rating="0">
             <source>English National Point Prevalence Survey on Healthcare Associated Infections and Antimicrobial Use, 2011: Preliminary Data</source>
             <publisher>Health Protection Agency, London</publisher>
             <pubdate>2012</pubdate>
          </bibl>
          <bibl id="B2" rating="0">
             <title lang="en">
                <p>The preventable proportion of nosocomial infections: an overview of published reports</p>
             </title>
             <aug>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Harbarth</snm>
                   <fnm>S</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Sax</snm>
                   <fnm>H</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Gastmeier</snm>
                   <fnm>P</fnm>
                </au>
             </aug>
             <source>J Hosp Infect</source>
             <pubdate>2003</pubdate>
             <volume>54</volume>
             <fpage>258</fpage>
             <lpage>266</lpage>
             <xrefbib>
                <pubidlist>
                   <pubid idtype="doi">10.1016/S0195-6701(03)00150-6</pubid>
                   <pubid idtype="pmpid" link="fulltext">12919755</pubid>
                </pubidlist>
             </xrefbib>
          </bibl>
          <bibl id="B3" rating="0">
             <title lang="en">
                <p>Genomics and outbreak investigation: from sequence to consequence</p>
             </title>
             <aug>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Robinson</snm>
                   <fnm>ER</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Walker</snm>
                   <fnm>TM</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Pallen</snm>
                   <fnm>MJ</fnm>
                </au>
             </aug>
             <source>Genome Med</source>
             <pubdate>2013</pubdate>
             <volume>5</volume>
             <fpage>36</fpage>
             <xrefbib>
                <pubidlist>
                   <pubid idtype="pmpid" link="fulltext">23673226</pubid>
                </pubidlist>
             </xrefbib>
          </bibl>
          <bibl id="B4" rating="0">
             <title lang="en">
                <p>Whole-genome sequencing shows that patient-to-patient transmission rarely accounts for acquisition of <it>Staphylococcus aureus</it> in an intensive care unit</p>
             </title>
             <aug>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Price</snm>
                   <fnm>JR</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Golubchik</snm>
                   <fnm>T</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Cole</snm>
                   <fnm>K</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Wilson</snm>
                   <fnm>DJ</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Crook</snm>
                   <fnm>DW</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Thwaites</snm>
                   <fnm>GE</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Bowden</snm>
                   <fnm>R</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Walker</snm>
                   <fnm>AS</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Peto</snm>
                   <fnm>TE</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Paul</snm>
                   <fnm>J</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Llewelyn</snm>
                   <fnm>MJ</fnm>
                </au>
             </aug>
             <source>Clin Infect Dis</source>
             <pubdate>2014</pubdate>
             <volume>58</volume>
             <fpage>609</fpage>
             <lpage>618</lpage>
             <xrefbib>
                <pubidlist>
                   <pubid idtype="doi">10.1093/cid/cit807</pubid>
                   <pubid idtype="pmpid" link="fulltext">24336829</pubid>
                </pubidlist>
             </xrefbib>
          </bibl>
          <bibl id="B5" rating="0">
             <title lang="en">
                <p>Genomic insights to control the emergence of vancomycin-resistant enterococci</p>
             </title>
             <aug>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Howden</snm>
                   <fnm>BP</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Holt</snm>
                   <fnm>KE</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Lam</snm>
                   <fnm>MM</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Seemann</snm>
                   <fnm>T</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Ballard</snm>
                   <fnm>S</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Coombs</snm>
                   <fnm>GW</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Tong</snm>
                   <fnm>SY</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Grayson</snm>
                   <fnm>ML</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Johnson</snm>
                   <fnm>PD</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Stinear</snm>
                   <fnm>TP</fnm>
                </au>
             </aug>
             <source>MBio</source>
             <pubdate>2013</pubdate>
             <volume>4</volume>
             <fpage>e00412</fpage>
             <lpage>e00413</lpage>
             <xrefbib>
                <pubidlist>
                   <pubid idtype="doi">10.1128/mBio.00412-13</pubid>
                   <pubid idtype="pmpid" link="fulltext">23943759</pubid>
                </pubidlist>
             </xrefbib>
          </bibl>
          <bibl id="B6" rating="0">
             <title lang="en">
                <p>Rapid whole-genome sequencing for investigation of a neonatal MRSA outbreak</p>
             </title>
             <aug>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>K�ser</snm>
                   <fnm>CU</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Holden</snm>
                   <fnm>MT</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Ellington</snm>
                   <fnm>MJ</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Cartwright</snm>
                   <fnm>EJ</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Brown</snm>
                   <fnm>NM</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Ogilvy-Stuart</snm>
                   <fnm>AL</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Hsu</snm>
                   <fnm>LY</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Chewapreecha</snm>
                   <fnm>C</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Croucher</snm>
                   <fnm>NJ</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Harris</snm>
                   <fnm>SR</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Sanders</snm>
                   <fnm>M</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Enright</snm>
                   <fnm>MC</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Dougan</snm>
                   <fnm>G</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Bentley</snm>
                   <fnm>SD</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Parkhill</snm>
                   <fnm>J</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Fraser</snm>
                   <fnm>LJ</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Betley</snm>
                   <fnm>JR</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Schulz-Trieglaff</snm>
                   <fnm>OB</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Smith</snm>
                   <fnm>GP</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Peacock</snm>
                   <fnm>SJ</fnm>
                </au>
             </aug>
             <source>N Engl J Med</source>
             <pubdate>2012</pubdate>
             <volume>366</volume>
             <fpage>2267</fpage>
             <lpage>2275</lpage>
             <xrefbib>
                <pubidlist>
                   <pubid idtype="doi">10.1056/NEJMoa1109910</pubid>
                   <pubid idtype="pmpid" link="fulltext">22693998</pubid>
                </pubidlist>
             </xrefbib>
          </bibl>
          <bibl id="B7" rating="0">
             <title lang="en">
                <p>Tracking a hospital outbreak of carbapenem-resistant <it>Klebsiella pneumoniae</it> with whole-genome sequencing</p>
             </title>
             <aug>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Snitkin</snm>
                   <fnm>ES</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Zelazny</snm>
                   <fnm>AM</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Thomas</snm>
                   <fnm>PJ</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Stock</snm>
                   <fnm>F</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Henderson</snm>
                   <fnm>DK</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Palmore</snm>
                   <fnm>TN</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Segre</snm>
                   <fnm>JA</fnm>
                </au>
             </aug>
             <source>Sci Transl Med</source>
             <pubdate>2012</pubdate>
             <volume>4</volume>
             <fpage>148ra116</fpage>
             <xrefbib>
                <pubidlist>
                   <pubid idtype="doi">10.1126/scitranslmed.3004129</pubid>
                   <pubid idtype="pmpid" link="fulltext">22914622</pubid>
                </pubidlist>
             </xrefbib>
          </bibl>
          <bibl id="B8" rating="0">
             <title lang="en">
                <p>High-throughput whole-genome sequencing to dissect the epidemiology of <it>Acinetobacter baumannii</it> isolates from a hospital outbreak</p>
             </title>
             <aug>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Lewis</snm>
                   <fnm>T</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Loman</snm>
                   <fnm>NJ</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Bingle</snm>
                   <fnm>L</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Jumaa</snm>
                   <fnm>P</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Weinstock</snm>
                   <fnm>GM</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Mortiboy</snm>
                   <fnm>D</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Pallen</snm>
                   <fnm>MJ</fnm>
                </au>
             </aug>
             <source>J Hosp Infect</source>
             <pubdate>2010</pubdate>
             <volume>75</volume>
             <fpage>37</fpage>
             <lpage>41</lpage>
             <xrefbib>
                <pubidlist>
                   <pubid idtype="doi">10.1016/j.jhin.2010.01.012</pubid>
                   <pubid idtype="pmpid" link="fulltext">20299126</pubid>
                </pubidlist>
             </xrefbib>
          </bibl>
          <bibl id="B9" rating="0">
             <title lang="en">
                <p>Genomic epidemiology of a protracted hospital outbreak caused by multidrug-resistant Acinetobacter baumannii in Birmingham, England</p>
             </title>
             <aug>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Halachev</snm>
                   <fnm>MR</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Chan</snm>
                   <fnm>JZ-M</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Constantinidou</snm>
                   <fnm>CI</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Cumley</snm>
                   <fnm>N</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Bradley</snm>
                   <fnm>C</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Smith-Banks</snm>
                   <fnm>M</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Oppenheim</snm>
                   <fnm>B</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Pallen</snm>
                   <fnm>MJ</fnm>
                </au>
             </aug>
             <source>Genome Med</source>
             <pubdate>2014</pubdate>
             <volume>6</volume>
             <fpage>70</fpage>
             <xrefbib>
                <pubidlist>
                   <pubid idtype="doi">10.1186/s13073-014-0070-x</pubid>
                </pubidlist>
             </xrefbib>
          </bibl>
          <bibl id="B10" rating="0">
             <title lang="en">
                <p>Single-molecule sequencing to track plasmid diversity of hospital-associated carbapenemase-producing Enterobacteriaceae</p>
             </title>
             <aug>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Conlan</snm>
                   <fnm>S</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Thomas</snm>
                   <fnm>PJ</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Deming</snm>
                   <fnm>C</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Park</snm>
                   <fnm>M</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Lau</snm>
                   <fnm>AF</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Dekker</snm>
                   <fnm>JP</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Snitkin</snm>
                   <fnm>ES</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Clark</snm>
                   <fnm>TA</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Luong</snm>
                   <fnm>K</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Song</snm>
                   <fnm>Y</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Tsai</snm>
                   <fnm>YC</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Boitano</snm>
                   <fnm>M</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Dayal</snm>
                   <fnm>J</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Brooks</snm>
                   <fnm>SY</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Schmidt</snm>
                   <fnm>B</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Young</snm>
                   <fnm>AC</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Thomas</snm>
                   <fnm>JW</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Bouffard</snm>
                   <fnm>GG</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Blakesley</snm>
                   <fnm>RW</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Mullikin</snm>
                   <fnm>JC</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Korlach</snm>
                   <fnm>J</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Henderson</snm>
                   <fnm>DK</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Frank</snm>
                   <fnm>KM</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Palmore</snm>
                   <fnm>TN</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Segre</snm>
                   <fnm>JA</fnm>
                </au>
             </aug>
             <source>Sci Transl Med</source>
             <pubdate>2014</pubdate>
             <volume>6</volume>
             <fpage>254ra126</fpage>
             <xrefbib>
                <pubidlist>
                   <pubid idtype="doi">10.1126/scitranslmed.3009845</pubid>
                   <pubid idtype="pmpid" link="fulltext">25232178</pubid>
                </pubidlist>
             </xrefbib>
          </bibl>
          <bibl id="B11" rating="0">
             <title lang="en">
                <p>Detection of mixed infection from bacterial whole genome sequence data allows assessment of its role in <it>Clostridium difficile</it> transmission</p>
             </title>
             <aug>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Eyre</snm>
                   <fnm>DW</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Cule</snm>
                   <fnm>ML</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Griffiths</snm>
                   <fnm>D</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Crook</snm>
                   <fnm>DW</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Peto</snm>
                   <fnm>TE</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Walker</snm>
                   <fnm>AS</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Wilson</snm>
                   <fnm>DJ</fnm>
                </au>
             </aug>
             <source>PLoS Comput Biol</source>
             <pubdate>2013</pubdate>
             <volume>9</volume>
             <fpage>e1003059</fpage>
             <xrefbib>
                <pubidlist>
                   <pubid idtype="doi">10.1371/journal.pcbi.1003059</pubid>
                   <pubid idtype="pmpid" link="fulltext">23658511</pubid>
                </pubidlist>
             </xrefbib>
          </bibl>
       </refgrp>
    </bm>
 </art>