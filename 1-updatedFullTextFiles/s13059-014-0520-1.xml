<art>
    <ui>s13059-014-0520-1</ui>
    <ji>1465-6906</ji>
    <fm>
       <dochead>Research highlight</dochead>
       <bibl rating="0">
          <title lang="en">
             <p>The road to drug resistance in <it>Mycobacterium tuberculosis</it>
             </p>
          </title>
          <aug>
             <au ca="no" ce="no" da="no" id="A1" pa="no">
                <snm>Koch</snm>
                <fnm>Anastasia</fnm>
                <insr iid="I1"/>
                <email>KCHANA001@myuct.ac.za</email>
             </au>
             <au ca="yes" ce="no" da="no" id="A2" pa="no">
                <snm>Wilkinson</snm>
                <mnm>John</mnm>
                <fnm>Robert</fnm>
                <insr iid="I2"/>
                <insr iid="I3"/>
                <insr iid="I4"/>
                <email>r.j.wilkinson@imperial.ac.uk</email>
             </au>
          </aug>
          <insg>
             <ins id="I1">
                <p>MRC/NHLS/UCT Molecular Mycobacteriology Research Unit and NRF/DST Centre of Excellence, Institute of Infectious Disease and Molecular Medicine, Department of Clinical Laboratory Sciences, University of Cape Town, Observatory, Cape Town 7925, South Africa</p>
             </ins>
             <ins id="I2">
                <p>Clinical Infectious Diseases Research Initiative, Institute of Infectious Diseases and Molecular Medicine, University of Cape Town, Observatory, Cape Town 7925, South Africa</p>
             </ins>
             <ins id="I3">
                <p>MRC National Institute for Medical Research, London NW7 1AA, UK</p>
             </ins>
             <ins id="I4">
                <p>Department of Medicine, Imperial College London, London W2 1PG, UK</p>
             </ins>
          </insg>
          <source>Genome Biology</source>
          <issn>1465-6906</issn>
          <pubdate>2014</pubdate>
          <volume>15</volume>
          <issue>11</issue>
          <fpage>520</fpage>
          <url>http://genomebiology.com/2014/15/11/520</url>
          <xrefbib>
             <pubidlist>
                <pubid idtype="doi">10.1186/s13059-014-0520-1</pubid>
             <pubid idtype="pmpid">25417849</pubid></pubidlist>
          </xrefbib>
       </bibl>
       <history>
          <pub>
             <date>
                <day>13</day>
                <month>11</month>
                <year>2014</year>
             </date>
          </pub>
       </history>
       <cpyrt>
          <year>2014</year>
          <collab>Koch and Wilkinson; licensee BioMed Central Ltd.</collab>
          <note>The licensee has exclusive rights to distribute this article, in any medium, for 12 months following its publication. After this time, the article is available under the terms of the Creative Commons Attribution License (<url>http://creativecommons.org/licenses/by/4.0</url>), which permits unrestricted use, distribution, and reproduction in any medium, provided the original work is properly credited. The Creative Commons Public Domain Dedication waiver (<url>http://creativecommons.org/publicdomain/zero/1.0/</url>) applies to the data made available in this article, unless otherwise stated.</note>
       </cpyrt>
       <abs lang="en">
          <sec>
             <st>
                <p>Abstract</p>
             </st>
             <p>Sequencing of serial isolates of extensively drug-resistant tuberculosis highlights how drug resistance develops within a single patient and reveals unexpected levels of pathogen diversity.</p>
          </sec>
       </abs>
    </fm>
    <bdy><sec><st><p/></st><p>Tuberculosis (TB) remains a crucial public health problem, with increasing drug resistance posing a challenge to current control efforts. Treatment regimens for drug-susceptible TB are onerous, requiring a minimum of six months of treatment with four antitubercular drugs. There are patients who develop multi-drug-resistant (MDR), extensively drug-resistant (XDR) and totally drug-resistant (TDR) forms, which are successively more difficult to treat. In these circumstances, treatment regimens involve the use of a larger number of less-effective drugs, which have a narrower therapeutic margin.</p>
       <p>In many bacteria, drug-resistance determinants are carried on mobile genetic elements. However, in <it>Mycobacterium tuberculosis</it> (Mtb), drug resistance is exclusively associated with point mutations and chromosomal rearrangements. Poor or intermittent therapy has long been thought to be the major explanation for drug resistance, and it is believed that drug-resistant strains develop through the sequential fixation of a small set of mutations, such that the pathogen samples only a small proportion of possible evolutionary paths <abbrgrp>
             <abbr bid="B1">1</abbr>
          </abbrgrp>.</p>
       <p>The application of whole-genome sequencing (WGS) has revealed previously underappreciated levels of genetic diversity within circulating Mtb populations, and the implications of this diversity for transmission and disease outcomes are increasingly being acknowledged. By contrast, mycobacterial heterogeneity within a single host, and any concomitant biological or clinical significance, has been explored but seldom documented.</p>
       <p>In a study published in this issue of <it>Genome Biology</it>, Eldholm and colleagues apply WGS to investigate the evolution from drug-sensitive to XDR-TB within a single patient <abbrgrp>
             <abbr bid="B2">2</abbr>
          </abbrgrp>. This adds to an emerging body of evidence that suggests that intra-host microbial diversity is substantial and might have significant consequences when inferring transmission. There are few instances, if any, in the literature where this has been investigated in such detail.</p></sec><sec>
          <st>
             <p>Mapping drug-resistant tuberculosis in a single patient</p>
          </st>
          <p>The study team took advantage of nine serial bacterial isolates collected from a single patient, from the time of first diagnosis of drug-susceptible TB through to the development of XDR-TB. The infection ultimately resolved following the addition of linezolid to the treatment regimen - linezolid is known to be a moderately effective treatment for drug-resistant TB when other options have failed <abbrgrp>
                <abbr bid="B2">2</abbr>
             </abbrgrp>. Illumina sequencing was applied to the isolates, which were collected over a period of 42�months, achieving a high depth of coverage. By relaxing the stringency of filters applied to detect single-nucleotide polymorphisms (SNPs), the authors observed unexpected levels of heterogeneity within individual samples. It is worth mentioning that DNA was not extracted from single colonies - instead �loopfuls� of bacterial cells were harvested for DNA. This approach allowed the group to detect SNPs present at a frequency of approximately 25% at sites with a minimum read-depth of 50. Of the 35 SNPs identified in this way, 20 were transient and 15 eventually became fixed. Twelve of the observed mutations were associated with drug resistance, and phenotypic resistance was observed at the same time that genotypic resistance emerged.</p>
          <p>Although the patient was infected with only one Mtb strain, multiple resistance alleles were observed for antitubercular drugs throughout the course of infection, with the exception of rifampicin and kanamycin <abbrgrp>
                <abbr bid="B2">2</abbr>
             </abbrgrp>. The levels of micro-heterogeneity reported are consistent with those of previous studies <abbrgrp>
                <abbr bid="B3">3</abbr>
             </abbrgrp>, <abbrgrp>
                <abbr bid="B4">4</abbr>
             </abbrgrp>.</p>
          <p>These observations suggest that, at any given time, there might be significant Mtb diversity within a single patient. High levels of diversity could affect the accurate interpretation of WGS data that are used to infer transmission. Current WGS of Mtb requires subculture of the bacilli from patient samples to ensure that sufficient DNA is available for analysis, and bacterial subpopulations that are not culturable are not captured during downstream sequencing <abbrgrp>
                <abbr bid="B5">5</abbr>
             </abbrgrp>. Moreover, one cannot be certain that a single sputum sample represents all regions of the lung; indeed significant intra-lesional heterogeneity has been observed in humans <abbrgrp>
                <abbr bid="B6">6</abbr>
             </abbrgrp>. Therefore, intra-patient variability could be even higher than reported by the aforementioned studies and might not be detectable by current WGS approaches.</p>
       </sec>
       <sec>
          <st>
             <p>Advantages of adaptability: survival of the fittest</p>
          </st>
          <p>By virtue of access to multiple samples from the entire course of disease, Eldholm and colleagues were able to calculate mutation rates for the sample set <abbrgrp>
                <abbr bid="B2">2</abbr>
             </abbrgrp>. The results are unique in that they suggest that non-resistance mutations hitchhike with, or link to, resistance SNPs to become fixed within the population. This phenomenon could partly explain the high numbers of SNPs recently found to occur in global collections of drug-resistant Mtb isolates <abbrgrp>
                <abbr bid="B7">7</abbr>
             </abbrgrp>.</p>
          <p>When the authors removed both resistance and hitchhiking mutations from the analysis, they found that the mutation rate was only slightly higher than that recently reported in cohorts with drug-susceptible disease <abbrgrp>
                <abbr bid="B8">8</abbr>
             </abbrgrp>. Their findings suggest that the selective pressure conferred by antibiotics provides Mtb with an opportunity to diversify and adapt. Drug resistance represents a readily measureable determinant of genetic change because its effects can be attributed to a set of discrete SNPs. There are additional factors whose impact on the genome of Mtb are not well established, such as the vaccination status of individual patients or their status with respect to human immunodeficiency virus (HIV). These factors are much more complicated to consider, but important and interesting nonetheless.</p>
          <p>Significantly, Eldholm and colleagues complemented genomic data with phenotypic evaluation of the fitness of isolates with different resistance mutations <abbrgrp>
                <abbr bid="B2">2</abbr>
             </abbrgrp>. Results of growth-rate measurements in the presence of the drug indicated that the fittest strains contained mutations that eventually became fixed. While there may be caveats when assaying Mtb fitness under laboratory conditions, these results provide further support for the notion that resistance mutations with the lowest fitness costs will eventually become fixed within a population.</p>
          <p>Additionally, through RNA sequencing of a subset of samples, the authors observed a stable change in transcriptional patterns as drug resistance emerged <abbrgrp>
                <abbr bid="B2">2</abbr>
             </abbrgrp>. Genes that were differentially regulated included those involved in efflux and synthesis of cell-wall components. The targets of antibiotics are, by definition, functionally essential <abbrgrp>
                <abbr bid="B1">1</abbr>
             </abbrgrp>, and the changes in transcriptional remodeling could indicate an important mechanism that facilitates adaptation to the fitness costs of having mutations in multiple essential genes <abbrgrp>
                <abbr bid="B2">2</abbr>
             </abbrgrp>.</p>
       </sec>
       <sec>
          <st>
             <p>For the future</p>
          </st>
          <p>The study by Eldholm and colleagues has provided valuable insights into the evolution of Mtb drug resistance within a single host, and in the process it has demonstrated the considerable diversity and adaptability of the pathogen <abbrgrp>
                <abbr bid="B2">2</abbr>
             </abbrgrp>. As any good study should, it also raises important questions for further investigation.</p>
          <p>The SNPs identified in the current study were validated by PCR <abbrgrp>
                <abbr bid="B2">2</abbr>
             </abbrgrp> - however, the use of a low threshold to assign a SNP as genuine imparts the possibility of false positives. Additionally, the sequencing of DNA from batches of cells cannot determine the single-cell origin of specific mutations, and therefore epistatic interactions, which have been shown to be important in the evolution of drug resistance in Mtb <abbrgrp>
                <abbr bid="B9">9</abbr>
             </abbrgrp>, cannot be investigated by this approach. However, the application of WGS technologies to areas such as culture-independent <abbrgrp>
                <abbr bid="B10">10</abbr>
             </abbrgrp> and single-cell sequencing will provide the necessary tools to investigate these issues further.</p>
          <p>Although the phenotypic implications of resistance mutations have been extensively studied <abbrgrp>
                <abbr bid="B1">1</abbr>
             </abbrgrp>, the biological and clinical significance of the hitchhiking mutations is largely unknown. Nevertheless, the study by Eldholm <it>et al</it>., which was conducted in in Norway <abbrgrp>
                <abbr bid="B2">2</abbr>
             </abbrgrp>, is a striking reminder that TB drug resistance can quite easily develop, even within the context of very low TB rates in a well-functioning control program. It will be of vital importance to determine similarly how micro-heterogeneity influences disease outcomes and treatment efficacy in regions where a high burden of TB and elevated infection pressure might result in unique inter- and intra-host-strain competition dynamics <abbrgrp>
                <abbr bid="B5">5</abbr>
             </abbrgrp>.</p>
       </sec>
       <sec>
          <st>
             <p>Abbreviations</p>
          </st>
          <p>HIV: Human immunodeficiency virus</p>
          <p>MDR: Multi-drug-resistant</p>
          <p>Mtb: <it>Mycobacterium tuberculosis</it>
          </p>
          <p>SNP: Single-nucleotide polymorphism</p>
          <p>TB: Tuberculosis</p>
          <p>TDR: Totally drug-resistant</p>
          <p>WGS: Whole-genome sequencing</p>
          <p>XDR: Extensively drug-resistant.</p>
       </sec>
    </bdy>
    <bm>
       <ack>
          <sec>
             <st>
                <p>Acknowledgements</p>
             </st>
             <p>RJW receives support from the Wellcome Trust (084323 and 104803), Medical Research Council (U1175.02.002.00014) and European Union FP7- HEALTH-F3-2012-305578. AK acknowledges support from the National Research Foundation of South Africa and the Carnegie Corporation �Developing the Next Generation of Academics� program.</p>
          </sec>
       </ack>
       <refgrp>
          
          <bibl id="B1" rating="0">
             <title lang="en">
                <p>The impact of drug resistance on <it>Mycobacterium tuberculosis</it> physiology: what can we learn from rifampicin?</p>
             </title>
             <aug>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Koch</snm>
                   <fnm>A</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Mizrahi</snm>
                   <fnm>V</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Warner</snm>
                   <fnm>DF</fnm>
                </au>
             </aug>
             <source>Emerg Microb Infect</source>
             <pubdate>2014</pubdate>
             <volume>3</volume>
             <fpage>e17</fpage>
             <xrefbib>
                <pubidlist>
                   <pubid idtype="doi">10.1038/emi.2014.17</pubid>
                </pubidlist>
             </xrefbib>
          </bibl>
          <bibl id="B2" rating="0">
             <title lang="en">
                <p>Evolution of extensively drug resistant <it>Mycobacterium tuberculosis</it> from a susceptible ancestor in a single patient</p>
             </title>
             <aug>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Eldholm</snm>
                   <fnm>V</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Gunnstein</snm>
                   <fnm>N</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>von der Lippe</snm>
                   <fnm>B</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Kinander</snm>
                   <fnm>W</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Dahle</snm>
                   <fnm>UR</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Caugant</snm>
                   <fnm>DA</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Mannsaker</snm>
                   <fnm>T</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Mengshoel</snm>
                   <fnm>AT</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Dyrhol-Riise</snm>
                   <fnm>AM</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Balloux</snm>
                   <fnm>F</fnm>
                </au>
             </aug>
             <source>Genome Biol</source>
             <pubdate>2014</pubdate>
             <volume>15</volume>
             <fpage>490</fpage>
             <xrefbib>
                <pubidlist>
                   <pubid idtype="doi">10.1186/s13059-014-0490-3</pubid>
                </pubidlist>
             </xrefbib>
          </bibl>
          <bibl id="B3" rating="0">
             <title lang="en">
                <p>Dynamic population changes in <it>Mycobacterium tuberculosis</it> during acquisition and fixation of drug resistance in patients</p>
             </title>
             <aug>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Sun</snm>
                   <fnm>G</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Luo</snm>
                   <fnm>T</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Yang</snm>
                   <fnm>C</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Dong</snm>
                   <fnm>X</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Li</snm>
                   <fnm>J</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Zhu</snm>
                   <fnm>Y</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Zheng</snm>
                   <fnm>H</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Tian</snm>
                   <fnm>W</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Wang</snm>
                   <fnm>S</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Barry</snm>
                   <fnm>CE</fnm>
                   <suf>3rd</suf>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Mei</snm>
                   <fnm>J</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Gao</snm>
                   <fnm>Q</fnm>
                </au>
             </aug>
             <source>J Infect Dis</source>
             <pubdate>2012</pubdate>
             <volume>206</volume>
             <fpage>1724</fpage>
             <lpage>1733</lpage>
             <xrefbib>
                <pubidlist>
                   <pubid idtype="doi">10.1093/infdis/jis601</pubid>
                   <pubid idtype="pmpid" link="fulltext">22984115</pubid>
                </pubidlist>
             </xrefbib>
          </bibl>
          <bibl id="B4" rating="0">
             <title lang="en">
                <p>Whole genome sequencing analysis of intrapatient microevolution in <it>Mycobacterium tuberculosis</it>: potential impact on the inference of tuberculosis transmission</p>
             </title>
             <aug>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Perez-Lago</snm>
                   <fnm>L</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Comas</snm>
                   <fnm>I</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Navarro</snm>
                   <fnm>Y</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Gonzalez-Candelas</snm>
                   <fnm>F</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Herranz</snm>
                   <fnm>M</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Bouza</snm>
                   <fnm>E</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Garcia-de-Viedma</snm>
                   <fnm>D</fnm>
                </au>
             </aug>
             <source>J Infect Dis</source>
             <pubdate>2014</pubdate>
             <volume>209</volume>
             <fpage>98</fpage>
             <lpage>108</lpage>
             <xrefbib>
                <pubidlist>
                   <pubid idtype="doi">10.1093/infdis/jit439</pubid>
                   <pubid idtype="pmpid" link="fulltext">23945373</pubid>
                </pubidlist>
             </xrefbib>
          </bibl>
          <bibl id="B5" rating="0">
             <title lang="en">
                <p>Diversity and disease pathogenesis in <it>Mycobacterium tuberculosis</it>
                </p>
             </title>
             <aug>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Warner</snm>
                   <fnm>DF</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Koch</snm>
                   <fnm>A</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Mizrahi</snm>
                   <fnm>V</fnm>
                </au>
             </aug>
             <source>Trends Microbiol</source>
             <pubdate>2014</pubdate>
             <volume>?</volume>
             <fpage>?</fpage>
             <note>doi:10.1016/j.tim.2014.10.005</note>
          </bibl>
          <bibl id="B6" rating="0">
             <title lang="en">
                <p>
                   <it>Mycobacterium tuberculosis</it> growth at the cavity surface: a microenvironment with failed immunity</p>
             </title>
             <aug>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Kaplan</snm>
                   <fnm>G</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Post</snm>
                   <fnm>FA</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Moreira</snm>
                   <fnm>AL</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Wainwright</snm>
                   <fnm>H</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Kreiswirth</snm>
                   <fnm>BN</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Tanverdi</snm>
                   <fnm>M</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Mathema</snm>
                   <fnm>B</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Ramaswamy</snm>
                   <fnm>SV</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Walther</snm>
                   <fnm>G</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Steyn</snm>
                   <fnm>LM</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Barry</snm>
                   <fnm>CE</fnm>
                   <suf>3rd</suf>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Bekker</snm>
                   <fnm>LG</fnm>
                </au>
             </aug>
             <source>Infect Immun</source>
             <pubdate>2003</pubdate>
             <volume>71</volume>
             <fpage>7099</fpage>
             <lpage>7108</lpage>
             <xrefbib>
                <pubidlist>
                   <pubid idtype="doi">10.1128/IAI.71.12.7099-7108.2003</pubid>
                   <pubid idtype="pmpid" link="fulltext">14638800</pubid>
                </pubidlist>
             </xrefbib>
          </bibl>
          <bibl id="B7" rating="0">
             <title lang="en">
                <p>The complex genetics of drug resistance in <it>Mycobacterium tuberculosis</it>
                </p>
             </title>
             <aug>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Warner</snm>
                   <fnm>DF</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Mizrahi</snm>
                   <fnm>V</fnm>
                </au>
             </aug>
             <source>Nat Genet</source>
             <pubdate>2013</pubdate>
             <volume>45</volume>
             <fpage>1107</fpage>
             <lpage>1108</lpage>
             <xrefbib>
                <pubidlist>
                   <pubid idtype="doi">10.1038/ng.2769</pubid>
                   <pubid idtype="pmpid" link="fulltext">24071843</pubid>
                </pubidlist>
             </xrefbib>
          </bibl>
          <bibl id="B8" rating="0">
             <title lang="en">
                <p>Whole-genome sequencing to delineate <it>Mycobacterium tuberculosis</it> outbreaks: a retrospective observational study</p>
             </title>
             <aug>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Walker</snm>
                   <fnm>TM</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Ip</snm>
                   <fnm>CL</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Harrell</snm>
                   <fnm>RH</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Evans</snm>
                   <fnm>JT</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Kapatai</snm>
                   <fnm>G</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Dedicoat</snm>
                   <fnm>MJ</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Eyre</snm>
                   <fnm>DW</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Wilson</snm>
                   <fnm>DJ</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Hawkey</snm>
                   <fnm>PM</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Crook</snm>
                   <fnm>DW</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Parkhill</snm>
                   <fnm>J</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Harris</snm>
                   <fnm>D</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Walker</snm>
                   <fnm>AS</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Bowden</snm>
                   <fnm>R</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Monk</snm>
                   <fnm>P</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Smith</snm>
                   <fnm>EG</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Peto</snm>
                   <fnm>TEA</fnm>
                </au>
             </aug>
             <source>Lancet Infect Dis</source>
             <pubdate>2012</pubdate>
             <volume>13</volume>
             <fpage>137</fpage>
             <lpage>146</lpage>
             <xrefbib>
                <pubidlist>
                   <pubid idtype="doi">10.1016/S1473-3099(12)70277-3</pubid>
                   <pubid idtype="pmpid" link="fulltext">23158499</pubid>
                </pubidlist>
             </xrefbib>
          </bibl>
          <bibl id="B9" rating="0">
             <title lang="en">
                <p>Epistasis between antibiotic resistance mutations drives the evolution of extensively drug-resistant tuberculosis</p>
             </title>
             <aug>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Borrell</snm>
                   <fnm>S</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Teo</snm>
                   <fnm>Y</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Giardina</snm>
                   <fnm>F</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Streicher</snm>
                   <fnm>EM</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Klopper</snm>
                   <fnm>M</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Feldmann</snm>
                   <fnm>J</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Muller</snm>
                   <fnm>B</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Victor</snm>
                   <fnm>TC</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Gagneux</snm>
                   <fnm>S</fnm>
                </au>
             </aug>
             <source>Evol Med Public Health</source>
             <pubdate>2013</pubdate>
             <volume>1</volume>
             <fpage>65</fpage>
             <lpage>74</lpage>
             <xrefbib>
                <pubidlist>
                   <pubid idtype="doi">10.1093/emph/eot003</pubid>
                </pubidlist>
             </xrefbib>
          </bibl>
          <bibl id="B10" rating="0">
             <title lang="en">
                <p>Culture-independent detection and characterization of <it>Mycobacterium tuberculosis</it> and <it>M. africanum</it> in sputum sample using shotgun metagenomics on a benchtop sequencer</p>
             </title>
             <aug>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Doughty</snm>
                   <fnm>EL</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Sergeant</snm>
                   <fnm>MJ</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Adetifa</snm>
                   <fnm>MOI</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Antonio</snm>
                   <fnm>M</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Pallen</snm>
                   <fnm>MJ</fnm>
                </au>
             </aug>
             <source>Peer J</source>
             <pubdate>2014</pubdate>
             <volume>2</volume>
             <fpage>e482v1</fpage>
             <xrefbib>
                <pubidlist>
                   <pubid idtype="doi">10.7717/peerj.585</pubid>
                </pubidlist>
             </xrefbib>
          </bibl>
       </refgrp>
    </bm>
 </art>