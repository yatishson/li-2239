<art>
    <ui>s13058-014-0457-z</ui>
    <ji>1465-5411</ji>
    <fm>
       <dochead>Letter</dochead>
       <bibl rating="0">
          <title lang="en">
             <p>Is immune checkpoint modulation a potential therapeutic option in triple negative breast cancer?</p>
          </title>
          <aug>
             <au ca="yes" ce="no" da="no" id="A1" pa="no">
                <snm>Klinke</snm>
                <mi>J</mi>
                <fnm>David</fnm>
                <insr iid="I1"/>
                <insr iid="I2"/>
                <email>david.klinke@mail.wvu.edu</email>
             </au>
          </aug>
          <insg>
             <ins id="I1">
                <p>Department of Chemical Engineering and Mary Babb Randolph Cancer Center, West Virginia University, Morgantown 25606, WV, USA</p>
             </ins>
             <ins id="I2">
                <p>Department of Microbiology, Immunology, and Cell Biology, West Virginia University, Morgantown 25606, WV, USA</p>
             </ins>
          </insg>
          <source>Breast Cancer Research</source>
          <issn>1465-5411</issn>
          <pubdate>2014</pubdate>
          <volume>16</volume>
          <issue>6</issue>
          <fpage>457</fpage>
          <url>http://breast-cancer-research.com/content/16/6/457</url>
          <xrefbib>
             <pubidlist>
                <pubid idtype="doi">10.1186/s13058-014-0457-z</pubid>
             </pubidlist>
          </xrefbib>
       </bibl>
       <history>
          <pub>
             <date>
                <day>7</day>
                <month>11</month>
                <year>2014</year>
             </date>
          </pub>
       </history>
       <cpyrt>
          <year>2014</year>
          <collab>Klinke; licensee BioMed Central Ltd.</collab>
          <note>This is an Open Access article distributed under the terms of the Creative Commons Attribution License (<url>http://creativecommons.org/licenses/by/4.0</url>), which permits unrestricted use, distribution, and reproduction in any medium, provided the original work is properly credited. The Creative Commons Public Domain Dedication waiver (<url>http://creativecommons.org/publicdomain/zero/1.0/</url>) applies to the data made available in this article, unless otherwise stated.</note>
       </cpyrt>
	   <abs lang="en"><sec><st>
 <p>Abstract</p>
 </st>
 <p>No abstract.</p></sec></abs>
    </fm>
    <bdy><sec><st><p/></st><p>The emergence of molecular targeted therapies has revolutionized the clinical treatment of breast cancer. To guide treatment, patient samples are screened for expression of hormone receptors for estrogen and progesterone and the epidermal growth factor receptor HER2. Patients with tumors that do not express any of these three receptors (that is, triple-negative breast cancer) exhibit a worse outcome <abbrgrp>
             <abbr bid="B1">1</abbr>
          </abbrgrp>. Sequencing of cancer genomes suggests that over-expressing an oncogene or eliminating a tumor suppressor gene is also associated with passenger mutations. The presence of these passenger mutations may provide a collective signature that distinguishes malignant from normal cells. Conceptually, the adaptive immune system recognizes cells that present a different antigenic signature and provides a mechanism to control for malignant transformation. One approach to enhance anti-tumor immunity is to increase the number of T cells, either systemically, through inhibiting the action of CTLA-4, or locally, through inhibiting the programmed cell death 1 pathway. Therapeutic inhibition of these pathways is called immune checkpoint modulation <abbrgrp>
             <abbr bid="B2">2</abbr>
          </abbrgrp>. The clinical benefit received by a subset of patients with metastatic melanoma demonstrates proof-of-principle for this therapeutic approach <abbrgrp>
             <abbr bid="B3">3</abbr>
          </abbrgrp>.</p>
       <p>In a retrospective study of invasive breast cancer, we found that increased expression of genes associated with type 1 immunity was a predictor of increased survival independent of molecular pathology <abbrgrp>
             <abbr bid="B4">4</abbr>
          </abbrgrp>. This gene signature includes type 1�T-cell polarization and enhanced cytotoxic T-cell and natural killer cell recruitment. While this finding is consistent with a number of other studies (for example, <abbrgrp>
             <abbr bid="B5">5</abbr>
          </abbrgrp>), we also found that 70% of patients with invasive triple-negative breast cancer clustered with the cohort characterized by an increased type 1 immune signature. In examining the gene expression signature, we found that the expression of several type 1 immunity genes aligned along the direction of principal coordinate 1 (Figure�<figr fid="F1">1</figr>A). In addition, expression of members of the programmed cell death 1 pathway (<it>PDCD1</it> and <it>CD274</it>) that can be therapeutically targeted also aligned in the same direction (Figure�<figr fid="F1">1</figr>B). Other genes typically associated with local immunosuppression, including <it>TGFB1</it>, <it>MICB/MICA</it>, <it>HMGB1</it>, <it>HIF1A</it>, <it>FOXP3</it>, and <it>IL10</it>, were not significantly different than random noise. Collectively, these findings suggest two points: first, patients with invasive triple-negative breast cancer have an increased propensity for on-going anti-tumor immunity; and second, therapeutic relief of the programmed cell death 1 pathway may improve overall survival in patients with triple-negative breast cancer.</p>
       
          <fig id="F1">
             <title lang="en">
                <p>Figure 1</p>
             </title>
             <caption>
                <p>Expression of PD-1 (PDCD1) and PD-L1 (CD274) mRNA within tumor samples correlates with a type 1 immune gene signature.</p>
             </caption>
             <text>
                <p>
                   <b>Expression of PD-1 (PDCD1) and PD-L1 (CD274) mRNA within tumor samples correlates with a type 1 immune gene signature.</b> Principal coordinate analysis was applied to expression data for a subset of immune-related genes obtained from the invasive breast cancer arm of the Cancer Genome Atlas (Figure S3 in <abbrgrp>
                      <abbr bid="B4">4</abbr>
                   </abbrgrp>). Biplot projections of the genes along the first two principal coordinate directions, where principal coordinate 1 corresponds to a type 1 immune signature and principal coordinate 2 corresponds to oncogenic transformation in invasive breast cancer. Type 1 immune related genes are shown in black in <b>(A)</b> while immunosuppressive genes are highlighted in <b>(B)</b>. The first two principal coordinates capture 33% of the overall variance in the data. As principal coordinates are independent, the projection of a gene along the corresponding axes indicates the degree to which the expression of two genes are related and the distance from the origin indicates the strength of the covariation within the data set. The remaining principal coordinates capture progressively less variance in the data and provide little additional information. The colored ovals radiating out from the origin indicate principal coordinate values that cannot be distinguished from random noise, that is, a null hypothesis, with increasing levels of statistical stringency. These colored ovals were obtained by bootstrap resampling.</p>
             </text>
             <graphic file="s13058-014-0457-z-1"/>
          </fig>
       </sec><sec>
          <st>
             <p>Competing interests</p>
          </st>
          <p>The author declares that he has no competing interests.</p>
       </sec>
       <sec>
          <st>
             <p>Authors� contributions</p>
          </st>
          <p>DJK conceived the study, performed the bioinformatic analysis, analyzed the experimental data, and wrote the manuscript.</p>
       </sec>
    </bdy>
    <bm>
       <ack>
          <sec>
             <st>
                <p>Acknowledgements</p>
             </st>
             <p>This work was supported by grants from the National Science Foundation (CAREER 1053490) and the National Cancer Institute (NCI) R15CA123123. The content is solely the responsibility of the author and does not necessarily represent the official views of the NCI, the National Institutes of Health, or the National Science Foundation.</p>
          </sec>
       </ack>
       <refgrp>
          
          <bibl id="B1" rating="0">
             <title lang="en">
                <p>Triple-negative breast cancer: clinical features and patterns of recurrence</p>
             </title>
             <aug>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Dent</snm>
                   <fnm>R</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Trudeau</snm>
                   <fnm>M</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Pritchard</snm>
                   <fnm>KI</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Hanna</snm>
                   <fnm>WM</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Kahn</snm>
                   <fnm>HK</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Sawka</snm>
                   <fnm>CA</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Lickley</snm>
                   <fnm>LA</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Rawlinson</snm>
                   <fnm>E</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Sun</snm>
                   <fnm>P</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Narod</snm>
                   <fnm>SA</fnm>
                </au>
             </aug>
             <source>Clin Cancer Res</source>
             <pubdate>2007</pubdate>
             <volume>13</volume>
             <fpage>4429</fpage>
             <lpage>4434</lpage>
             <xrefbib>
                <pubidlist>
                   <pubid idtype="doi">10.1158/1078-0432.CCR-06-3045</pubid>
                   <pubid idtype="pmpid" link="fulltext">17671126</pubid>
                </pubidlist>
             </xrefbib>
          </bibl>
          <bibl id="B2" rating="0">
             <title lang="en">
                <p>Targeting the PD-1/B7-H1(PD-L1) pathway to activate anti-tumor immunity</p>
             </title>
             <aug>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Topalian</snm>
                   <fnm>SL</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Drake</snm>
                   <fnm>CG</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Pardoll</snm>
                   <fnm>DM</fnm>
                </au>
             </aug>
             <source>Curr Opin Immunol</source>
             <pubdate>2012</pubdate>
             <volume>24</volume>
             <fpage>207</fpage>
             <lpage>212</lpage>
             <xrefbib>
                <pubidlist>
                   <pubid idtype="doi">10.1016/j.coi.2011.12.009</pubid>
                   <pubid idtype="pmpid" link="fulltext">22236695</pubid>
                </pubidlist>
             </xrefbib>
          </bibl>
          <bibl id="B3" rating="0">
             <title lang="en">
                <p>Improved survival with ipilimumab in patients with metastatic melanoma</p>
             </title>
             <aug>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Hodi</snm>
                   <fnm>FS</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>O�Day</snm>
                   <fnm>SJ</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>McDermott</snm>
                   <fnm>DF</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Weber</snm>
                   <fnm>RW</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Sosman</snm>
                   <fnm>JA</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Haanen</snm>
                   <fnm>JB</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Gonzalez</snm>
                   <fnm>R</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Robert</snm>
                   <fnm>C</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Schadendorf</snm>
                   <fnm>D</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Hassel</snm>
                   <fnm>JC</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Akerley</snm>
                   <fnm>W</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>van den Eertwegh</snm>
                   <fnm>AJ</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Lutzky</snm>
                   <fnm>J</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Lorigan</snm>
                   <fnm>P</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Vaubel</snm>
                   <fnm>JM</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Linette</snm>
                   <fnm>GP</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Hogg</snm>
                   <fnm>D</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Ottensmeier</snm>
                   <fnm>CH</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Lebbe</snm>
                   <fnm>C</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Peschel</snm>
                   <fnm>C</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Quirt</snm>
                   <fnm>I</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Clark</snm>
                   <fnm>JI</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Wolchok</snm>
                   <fnm>JD</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Weber</snm>
                   <fnm>JS</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Tian</snm>
                   <fnm>J</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Yellin</snm>
                   <fnm>MJ</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Nichol</snm>
                   <fnm>GM</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Hoos</snm>
                   <fnm>A</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Urba</snm>
                   <fnm>WJ</fnm>
                </au>
             </aug>
             <source>N Engl J Med</source>
             <pubdate>2010</pubdate>
             <volume>363</volume>
             <fpage>711</fpage>
             <lpage>723</lpage>
             <xrefbib>
                <pubidlist>
                   <pubid idtype="doi">10.1056/NEJMoa1003466</pubid>
                   <pubid idtype="pmpid" link="fulltext">20525992</pubid>
                </pubidlist>
             </xrefbib>
          </bibl>
          <bibl id="B4" rating="0">
             <title lang="en">
                <p>Induction of Wnt-inducible signaling protein-1 correlates with invasive breast cancer oncogenesis and reduced type 1 cell-mediated cytotoxic immunity: a retrospective study</p>
             </title>
             <aug>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Klinke</snm>
                   <fnm>DJ</fnm>
                </au>
             </aug>
             <source>PLoS Comput Biol</source>
             <pubdate>2014</pubdate>
             <volume>10</volume>
             <fpage>e1003409</fpage>
             <xrefbib>
                <pubidlist>
                   <pubid idtype="doi">10.1371/journal.pcbi.1003409</pubid>
                   <pubid idtype="pmpid" link="fulltext">24426833</pubid>
                </pubidlist>
             </xrefbib>
          </bibl>
          <bibl id="B5" rating="0">
             <title lang="en">
                <p>In situ Tumor PD-L1 mRNA expression is associated with increased TILs and better outcome in breast carcinomas</p>
             </title>
             <aug>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Schalper</snm>
                   <fnm>KA</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Velcheti</snm>
                   <fnm>V</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Carvajal</snm>
                   <fnm>D</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Wimberly</snm>
                   <fnm>H</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Brown</snm>
                   <fnm>J</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Pusztai</snm>
                   <fnm>L</fnm>
                </au>
                <au ca="no" ce="no" da="no" pa="no">
                   <snm>Rimm</snm>
                   <fnm>DL</fnm>
                </au>
             </aug>
             <source>Clin Cancer Res</source>
             <pubdate>2014</pubdate>
             <volume>20</volume>
             <fpage>2773</fpage>
             <lpage>2782</lpage>
             <xrefbib>
                <pubidlist>
                   <pubid idtype="doi">10.1158/1078-0432.CCR-13-2702</pubid>
                   <pubid idtype="pmpid" link="fulltext">24647569</pubid>
                </pubidlist>
             </xrefbib>
          </bibl>
       </refgrp>
    </bm>
 </art>