package com.java.utility;
import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.java.util.ConfigConstant;

public class DownloadXMLFiles {

	//public static final File APLUSFILESFOLDER=new File("D:\\workspace\\kepler_workspace_2239\\LI-2239\\APLUSFiles");
	public static final File APLUSFILESFOLDER = new File(ConfigConstant.APLUS_PLSE_FILES_FOLDER);
	//public static final String FullTextFILESFOLDER="D:\\workspace\\kepler_workspace_2239\\LI-2239\\FullTextFiles";
	public static final String FullTextFILESFOLDER= ConfigConstant.FULLTEXT_FILES_FOLDER;
	public static final String bmcURL = ConfigConstant.BMC_URL;
	public static final String LOG4J_FILE_LOCATION = ConfigConstant.LOG_FILE_PATH;
	
	private static Logger logger=null;
	
	public static void main(String[] args) {
		DownloadXMLFiles xmlFiles=new DownloadXMLFiles();
		PropertyConfigurator.configure(LOG4J_FILE_LOCATION);
		logger=Logger.getLogger("DownloadXMLFiles");
		try {
			 xmlFiles.displayDirectoryContents((APLUSFILESFOLDER));
    	}catch (Exception ex){
    		ex.printStackTrace();
    	}
	}
	
	public String readArticleIDFromXML(String filePath){
		 
		String articleID=null;	
		try {  
			 	
			 	logger.debug("filePath:"+filePath);
			 	FileInputStream file = new FileInputStream(new File(filePath));
          
	            DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
	             
	            DocumentBuilder builder =  builderFactory.newDocumentBuilder();
	             
	            Document xmlDocument = builder.parse(file);
	 
	            XPath xPath =  XPathFactory.newInstance().newXPath();
		  
	            String expression = "/Publisher/Journal/Volume/Issue/Article";
	            Node node=(Node)xPath.compile(expression).evaluate(xmlDocument, XPathConstants.NODE);
	            String articleIDFromXML= node.getAttributes()
	                    .getNamedItem("ID")
	                    .getNodeValue();
	            System.out.println("article id found from xml:"+articleIDFromXML);
	            logger.debug("article id found from xml:"+articleIDFromXML);
	            if(articleIDFromXML!=null && !articleIDFromXML.isEmpty()){
	            	long start_time = System.currentTimeMillis();
	            	long wait_time = 40000;
	            	long end_time = start_time + wait_time;

	            	
	            	String updatedArticleIDURL=bmcURL+articleIDFromXML+".xml";
	            	logger.debug("BMC URL we are going to hit to download xml"+updatedArticleIDURL);
	            	
	            		download(updatedArticleIDURL, FullTextFILESFOLDER);
	            		logger.debug("downloaded for article id "+articleIDFromXML);
	            }
	           
			} catch (FileNotFoundException e) {
	            e.printStackTrace();
	            logger.error("got an error:"+e.getMessage());
	        } catch (SAXException e) {
	            e.printStackTrace();
	            logger.error("got an error:"+e.getMessage());
	        } catch (IOException e) {
	            e.printStackTrace();
	            logger.error("got an error:"+e.getMessage());
	        } catch (ParserConfigurationException e) {
	            e.printStackTrace();
	            logger.error("got an error:"+e.getMessage());
	        } catch (XPathExpressionException e) {
	            e.printStackTrace();
	            logger.error("got an error:"+e.getMessage());
	        } catch (TransformerFactoryConfigurationError e) {
	            e.printStackTrace();
	            logger.error("got an error:"+e.getMessage());
	        }catch (Exception e) {  
	        	e.printStackTrace();  
	        	logger.error("got an error:"+e.getMessage());
		  }  
		 return articleID;
	}
	
	public  void displayDirectoryContents(File dir) {
		try {
			File[] files = dir.listFiles();
			for (File file : files) {
				if (file.isDirectory()) {
					displayDirectoryContents(file);
				} else {
					System.out.println(" absolute filepath:" + file.getAbsolutePath());
					logger.debug(" absolute filepath:" + file.getAbsolutePath());
					readArticleIDFromXML(file.getAbsolutePath());
				}
			}
		} catch (Exception ex){
			ex.printStackTrace();
			logger.error("got an error:"+ex.getMessage());
		}
	}
	
	 private void download(String fileURL, String destinationDirectory) throws IOException {
	        // File name that is being downloaded
	        String downloadedFileName = fileURL.substring(fileURL.lastIndexOf("/")+1);
	         
	        // Open connection to the file
	        URL url = new URL(fileURL);
	        InputStream is = url.openStream();
	        // Stream to the destionation file
	        FileOutputStream fos = new FileOutputStream(destinationDirectory + "/" + downloadedFileName);
	  
	        // Read bytes from URL to the local file
	        byte[] buffer = new byte[4096];
	        int bytesRead = 0;
	         
	        System.out.print("Downloading " + downloadedFileName);
	        logger.debug("Downloading " + downloadedFileName);
	        try{
		        while ((bytesRead = is.read(buffer)) != -1) {
		            System.out.print(".");  // Progress bar :)
		            fos.write(buffer,0,bytesRead);
		        }
		       // System.out.println("done!");
		        logger.debug("done!!");
		    }catch (Exception ex){
		    	logger.error("got an error while downloading:"+downloadedFileName);
		    	logger.error("error message:"+ex.getMessage());
		    	ex.printStackTrace();
		    }
	        // Close destination stream
	        fos.close();
	        // Close URL stream
	        is.close();
	    }
}
