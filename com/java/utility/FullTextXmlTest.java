package com.java.utility;


import org.apache.log4j.PropertyConfigurator;

public class FullTextXmlTest {

	public static final String LOG4J_FILE_LOCATION=".\\resources\\log4j.properties";
		
	public static void main(String[] args) {
	
		PropertyConfigurator.configure(LOG4J_FILE_LOCATION);
		ReadFullAbstractMultipleFilesUpdated read=new ReadFullAbstractMultipleFilesUpdated();
		read.readXML();
	}
	
}	