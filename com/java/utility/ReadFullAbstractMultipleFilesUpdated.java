package com.java.utility;
import java.io.File;  
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;  
import javax.xml.parsers.DocumentBuilderFactory;  
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.log4j.Logger;
import org.w3c.dom.Document;  
import org.w3c.dom.Node;  
import org.w3c.dom.NodeList;  
import org.xml.sax.SAXException;

import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;


public class ReadFullAbstractMultipleFilesUpdated {
	
	public static final String ALL_P_TAG_UNDER_ST_TAG="/art/bdy//sec/st/p";
	public static final String FROM_FOLDER=".\\readFilesToUpdate";
	public static final String DESTINATION_FOLDER=".\\updatedFullTextFiles";
	private static Logger logger=Logger.getLogger("ReadFullAbstractMultipleFilesUpdated");
	
	
	public void readXML() {  
		 try {  
		 	
			    File fromFolder=new File(FROM_FOLDER);
			    File toFolder=new File(DESTINATION_FOLDER);
			    File[] listOfFiles = fromFolder.listFiles();
			    
			    for (File file : listOfFiles) {
			    	if(file.isFile()){
			    		System.out.println("file name:"+file.getName());
			    		logger.debug("file name:"+file.getName());
			    		processFile(file,toFolder);
			    		System.out.println("file:"+file.getName()+" prcessed successfully");
			    		logger.debug("file:"+file.getName()+" prcessed successfully");
			    	}
			    	 
			    }
		 }catch (Exception e) {  
	        	e.printStackTrace();  
		  }   
	} 
	

	private void processFile(File xmlfile,File toFolder){
	 	
		FileInputStream file=null;
		try{ 
		
		 file = new FileInputStream(xmlfile);
         DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
          
         DocumentBuilder builder =  builderFactory.newDocumentBuilder();
          
         Document xmlDocument = builder.parse(file);
         
         XPath xPath =  XPathFactory.newInstance().newXPath();
         
         NodeList pTagNodeList = (NodeList) xPath.compile(ALL_P_TAG_UNDER_ST_TAG).evaluate(xmlDocument, XPathConstants.NODESET);
        
         for (int i = 0; i < pTagNodeList.getLength(); i++) {
          	 Node childNode=pTagNodeList.item(i);
 		   	 if(childNode.getNodeName().equalsIgnoreCase("p")){
 		   	   	if((!childNode.getTextContent().isEmpty() || childNode.getTextContent()!=null) && childNode.getTextContent().trim().length()!=0){
 		   	   		
 		   	   		String pTagValue=childNode.getTextContent().trim();
 		   	    	System.out.println("original value P tag:"+pTagValue);
 		   	   		if(StringUtil.isStringContainsDigit(pTagValue)){
 		   	   				if(StringUtil.isStringContainsSingleDigit(pTagValue)){
 		   	   					System.out.println("string contains single digit only:"+pTagValue);
 		   	   					String updatedValue=StringUtil.removeExtraSpaceInStringWhichHasSingleDigit(pTagValue);
 		   	   				    childNode.setTextContent(updatedValue);	
 		   	   				    System.out.println("updated Value of P tag:"+updatedValue);
 		   	   				}else if (StringUtil.ifSpaceBetweenDigits(pTagValue)){
 		   	   					System.out.println("string contains multiple digits:"+pTagValue+" and there is space between them");
 		   	   					String updatedValue=StringUtil.removeSpaceBetweenDigits(pTagValue);
 		   	   					childNode.setTextContent(updatedValue);	
		   	   				    System.out.println("updated Value of P tag:"+updatedValue);
		   	   				}else{
		   	   					System.out.println("string contains multiple digits:"+pTagValue+" and there is space between them");
		   	   					String updatedValue=StringUtil.removeDigitsWhenWeHaveMultipleDigitsWithNoSpace(pTagValue);
		   	   					childNode.setTextContent(updatedValue);	
		   	   					System.out.println("updated Value of P tag:"+updatedValue);
		   	   				}
 		   	   		}
 		   	  	}
 			    		
 		   	 }
 		 }		
 			    		
         Transformer xformer = TransformerFactory.newInstance().newTransformer();
         //TODO: Test to omit xml-declaration
         //xformer.setParameter(OutputKeys.OMIT_XML_DECLARATION, "yes");
         StreamResult result = new StreamResult(new FileWriter(new File(DESTINATION_FOLDER+"\\"+xmlfile.getName())));
         xformer.transform(new DOMSource(xmlDocument), result);
         
         result.getWriter().flush();
         result = null;
		} catch (FileNotFoundException e) {
         e.printStackTrace();
     } catch (SAXException e) {
         e.printStackTrace();
     } catch (IOException e) {
         e.printStackTrace();
     } catch (ParserConfigurationException e) {
         e.printStackTrace();
     } catch (XPathExpressionException e) {
         e.printStackTrace();
     } catch (TransformerFactoryConfigurationError e) {
         e.printStackTrace();
     } catch (TransformerException e) {
         e.printStackTrace();
     } catch (Exception e) {  
     	e.printStackTrace();  
	  }  finally{
		  if(file!=null){
			  try {
				file.close();
			} catch (IOException e) {
				
				e.printStackTrace();
			}
			  
		  }
		  
	  }
	  

	}

	
}
