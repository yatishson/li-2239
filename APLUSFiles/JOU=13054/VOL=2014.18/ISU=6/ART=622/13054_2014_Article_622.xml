<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE Publisher PUBLIC "-//Springer-Verlag//DTD A++ V2.4//EN"
                           "http://devel.springer.de/A++/V2.4/DTD/A++V2.4.dtd">
<Publisher>
  <PublisherInfo>
    <PublisherName>BioMed Central</PublisherName>
    <PublisherLocation>London</PublisherLocation>
    <PublisherImprintName>BioMed Central</PublisherImprintName>
  </PublisherInfo>
  <Journal OutputMedium="Online">
    <JournalInfo JournalProductType="ArchiveJournal" NumberingStyle="Unnumbered">
      <JournalID>13054</JournalID>
      <JournalElectronicISSN>1364-8535</JournalElectronicISSN>
      <JournalTitle>Critical Care</JournalTitle>
      <JournalAbbreviatedTitle>Crit Care</JournalAbbreviatedTitle>
      <JournalSubjectGroup>
        <JournalSubject Code="SCH" Type="Primary">Medicine &amp; Public Health</JournalSubject>
        <JournalSubject Code="SCH3100X" Priority="1" Type="Secondary">Intensive / Critical Care Medicine</JournalSubject>
        <JournalSubject Code="SCH22000" Priority="2" Type="Secondary">Emergency Medicine</JournalSubject>
        <SubjectCollection Code="SC11">Medicine</SubjectCollection>
      </JournalSubjectGroup>
    </JournalInfo>
    <Volume OutputMedium="Online">
      <VolumeInfo TocLevels="0" VolumeType="Regular">
        <VolumeIDStart>18</VolumeIDStart>
        <VolumeIDEnd>18</VolumeIDEnd>
        <VolumeIssueCount>0</VolumeIssueCount>
      </VolumeInfo>
      <Issue IssueType="Regular" OutputMedium="Online">
        <IssueInfo IssueType="Regular" TocLevels="0">
          <IssueIDStart>6</IssueIDStart>
          <IssueIDEnd>6</IssueIDEnd>
          <IssueArticleCount>0</IssueArticleCount>
          <IssueHistory>
            <CoverDate>
              <Year>2014</Year>
              <Month>12</Month>
            </CoverDate>
            <PricelistYear>2014</PricelistYear>
          </IssueHistory>
          <IssueCopyright>
            <CopyrightHolderName>The Author(s)</CopyrightHolderName>
            <CopyrightYear>2014</CopyrightYear>
          </IssueCopyright>
        </IssueInfo>
        <Article ID="s13054-014-0622-x" OutputMedium="Online">
          <ArticleInfo ArticleType="OriginalPaper" ContainsESM="No" Language="En" NumberingStyle="Unnumbered" TocLevels="0">
            <ArticleID>622</ArticleID>
            <ArticleDOI>10.1186/s13054-014-0622-x</ArticleDOI>
            <ArticleCitationID>622</ArticleCitationID>
            <ArticleSequenceNumber>21</ArticleSequenceNumber>
            <ArticleTitle Language="En" OutputMedium="All">Burns to be alive: a complication of transcutaneous cardiac stimulation</ArticleTitle>
            <ArticleCategory>Letter</ArticleCategory>
            <ArticleFirstPage>1</ArticleFirstPage>
            <ArticleLastPage>2</ArticleLastPage>
            <ArticleHistory>
              <RegistrationDate>
                <Year>2014</Year>
                <Month>10</Month>
                <Day>24</Day>
              </RegistrationDate>
              <OnlineDate>
                <Year>2014</Year>
                <Month>11</Month>
                <Day>12</Day>
              </OnlineDate>
            </ArticleHistory>
            <ArticleCopyright>
              <CopyrightHolderName>Muschart; licensee BioMed Central Ltd.</CopyrightHolderName>
              <CopyrightYear>2014</CopyrightYear>
              <CopyrightComment>
                <SimplePara>This is an Open Access article distributed under the terms of the Creative Commons Attribution License (<ExternalRef>
                    <RefSource>http://creativecommons.org/licenses/by/4.0</RefSource>
                    <RefTarget Address="http://creativecommons.org/licenses/by/4.0" TargetType="URL"/>
                  </ExternalRef>), which permits unrestricted use, distribution, and reproduction in any medium, provided the original work is properly credited. The Creative Commons Public Domain Dedication waiver (<ExternalRef>
                    <RefSource>http://creativecommons.org/publicdomain/zero/1.0/</RefSource>
                    <RefTarget Address="http://creativecommons.org/publicdomain/zero/1.0/" TargetType="URL"/>
                  </ExternalRef>) applies to the data made available in this article, unless otherwise stated.</SimplePara>
              </CopyrightComment>
            </ArticleCopyright>
            <ArticleGrants Type="OpenChoice">
              <MetadataGrant Grant="OpenAccess"/>
              <AbstractGrant Grant="OpenAccess"/>
              <BodyPDFGrant Grant="OpenAccess"/>
              <BodyHTMLGrant Grant="OpenAccess"/>
              <BibliographyGrant Grant="OpenAccess"/>
              <ESMGrant Grant="OpenAccess"/>
            </ArticleGrants>
          </ArticleInfo>
          <ArticleHeader>
            <AuthorGroup>
              <Author AffiliationIDS="Aff1" CorrespondingAffiliationID="Aff1">
                <AuthorName DisplayOrder="Western">
                  <GivenName>Xavier</GivenName>
                  <FamilyName>Muschart</FamilyName>
                </AuthorName>
                <Contact>
                  <Email>xavier.muschart@uclouvain.be</Email>
                </Contact>
              </Author>
              <Affiliation ID="Aff1">
                <OrgName>CHU Dinant Godinne, UCL-NAMUR 1</OrgName>
                <OrgAddress>
                  <Street>av. Gaston Therasse</Street>
                  <Postcode>5530</Postcode>
                  <City>Yvoir</City>
                  <Country>Belgium</Country>
                </OrgAddress>
              </Affiliation>
            </AuthorGroup>
            <AbbreviationGroup>
              <Heading>Abbreviation</Heading>
              <DefinitionList>
                <DefinitionListEntry>
                  <Term>ECPD</Term>
                  <Description>
                    <Para ID="Par1">External cardiac pacing device</Para>
                  </Description>
                </DefinitionListEntry>
              </DefinitionList>
            </AbbreviationGroup>
          </ArticleHeader>
          <Body>
            <Para ID="Par2">External cardiac pacing devices (ECPDs) are commonly employed in emergency situations. The main indication for their use is untolerated bradycardia, especially in cases where other medical treatments have no effect [<CitationRef CitationID="CR1">1</CitationRef>]. Although the efficacy and safety of ECPDs are well documented, a classic side effect associated with their use is pain secondary to the electrically induced muscular contraction. Therefore, correct sedation-analgesia is critical for avoiding pain when reaching the correct voltages required for effective electrostimulation.</Para>
            <Para ID="Par3">Here, we report the case of an 86-year-old patient with third-degree skin burns secondary to the use of an ECPD (a Zoll M Series Biphasic® defibrillator along with Stat Padz Multi-Function® adult electrodes with high viscosity polymer gel; Zoll Medical Corporation, USA) (Figure <InternalRef RefID="Fig1">1</InternalRef>). To date, this observation represents the most serious ECPD-associated adult complication described in the literature.<Figure Category="Standard" Float="Yes" ID="Fig1">
                <Caption Language="En">
                  <CaptionNumber>Figure 1</CaptionNumber>
                  <CaptionContent>
                    <SimplePara>
                      <Emphasis Type="Bold">Two little third-degree skin burns under the breast following use of an external cardiac pacing device.</Emphasis>
                    </SimplePara>
                  </CaptionContent>
                </Caption>
                <MediaObject ID="MO1">
                  <ImageObject Color="Color" FileRef="MediaObjects/13054_2014_622_Fig1_HTML.gif" Format="GIF" Rendition="HTML" Type="Halftone"/>
                </MediaObject>
              </Figure>
            </Para>
            <Para ID="Par4">Electric cardioversion, defibrillation or ECPDs saves lives [<CitationRef CitationID="CR1">1</CitationRef>]. In particular, external pacing is indicated for untolerated bradyarrhythmia where medications have no effect. Classic complications of transcutaneous cardiac stimulation are related to electricity [<CitationRef CitationID="CR2">2</CitationRef>]. Indeed, electricity can lead to accidental injury of the medical team via lack of precautions and iatrogenic skin burns. However, no ECPD-related skin burn has been described for adults in the literature [<CitationRef CitationID="CR3">3</CitationRef>,<CitationRef CitationID="CR4">4</CitationRef>].</Para>
            <Para ID="Par5">Although our patient benefited from the use of the ECPD, she acquired severe skin burns as a result of the treatment. Several possible explanations might explain this unusual complication. First of all, we cannot rule out that this issue resulted from a technical problem with the patch or glue. However, a poorly bonded patch or trapped air pockets between the gel and the skin could also be to blame. Additionally, greater impedance due to the breast weight of the patient could constitute a plausible explanation. Finally, the long wait time (2 hours) prior to placement of the internal pacing device might be a contributing factor.</Para>
            <Para ID="Par6">Pain secondary to transcutaneous cardiac stimulation is usually due to electrically induced muscular contraction. Even though sedation-analgesia is indicated to prevent this common side effect, it can mask other complications. Skin burns must always be considered when patients continue to display pain under sedation-analgesia, especially in cases where the ECPD is used for a prolonged period of time.</Para>
          </Body>
          <BodyRef FileRef="BodyRef/PDF/13054_2014_Article_622.pdf" TargetType="OnlinePDF"/>
          <ArticleBackmatter>
            <ArticleNote Type="Misc">
              <Heading>Competing interests</Heading>
              <SimplePara>The author declares that he had no competing interests.</SimplePara>
            </ArticleNote>
            <Acknowledgments>
              <Heading>Acknowledgements</Heading>
              <SimplePara>Written informed consent for publication was provided by the patient and son; consent form is held by the author and the patient. Reviewing is possible (French version).</SimplePara>
            </Acknowledgments>
            <Bibliography ID="Bib1">
              <Heading>References</Heading>
              <Citation ID="CR1">
                <CitationNumber>1.</CitationNumber>
                <BibArticle>
                  <BibAuthorName>
                    <Initials>CD</Initials>
                    <FamilyName>Deakin</FamilyName>
                  </BibAuthorName>
                  <BibAuthorName>
                    <Initials>JP</Initials>
                    <FamilyName>Nolan</FamilyName>
                  </BibAuthorName>
                  <BibAuthorName>
                    <Initials>K</Initials>
                    <FamilyName>Sunde</FamilyName>
                  </BibAuthorName>
                  <BibAuthorName>
                    <Initials>RW</Initials>
                    <FamilyName>Koster</FamilyName>
                  </BibAuthorName>
                  <Year>2010</Year>
                  <ArticleTitle Language="En">Electrical therapies: automated external defibrillators, defibrillation, cardioversion and pacing</ArticleTitle>
                  <JournalTitle>Resuscitation</JournalTitle>
                  <VolumeID>81</VolumeID>
                  <FirstPage>1293</FirstPage>
                  <LastPage>1304</LastPage>
                  <Occurrence Type="PID">
                    <Handle>20956050</Handle>
                  </Occurrence>
                  <Occurrence Type="DOI">
                    <Handle>10.1016/j.resuscitation.2010.08.008</Handle>
                  </Occurrence>
                </BibArticle>
                <BibUnstructured>Deakin CD, Nolan JP, Sunde K, Koster RW: <Emphasis Type="Bold">Electrical therapies: automated external defibrillators, defibrillation, cardioversion and pacing.</Emphasis>
                  <Emphasis Type="Italic">Resuscitation</Emphasis> 2010, <Emphasis Type="Bold">81:</Emphasis>1293–1304.</BibUnstructured>
              </Citation>
              <Citation ID="CR2">
                <CitationNumber>2.</CitationNumber>
                <BibArticle>
                  <BibAuthorName>
                    <Initials>P</Initials>
                    <FamilyName>Birkui</FamilyName>
                  </BibAuthorName>
                  <BibAuthorName>
                    <Initials>JA</Initials>
                    <FamilyName>Trigano</FamilyName>
                  </BibAuthorName>
                  <BibAuthorName>
                    <Initials>J</Initials>
                    <FamilyName>Degonde</FamilyName>
                  </BibAuthorName>
                  <Year>1990</Year>
                  <ArticleTitle Language="En">Then hemodynamic efficacy of transcutaneous cardiac stimulation</ArticleTitle>
                  <JournalTitle>Bull Acad Natl Med</JournalTitle>
                  <VolumeID>174</VolumeID>
                  <FirstPage>1361</FirstPage>
                  <LastPage>1370</LastPage>
                  <Occurrence Type="COI">
                    <Handle>1:STN:280:DyaK3M3ktlSgsA%3D%3D</Handle>
                  </Occurrence>
                  <Occurrence Type="PID">
                    <Handle>2094564</Handle>
                  </Occurrence>
                  <BibComments>discussion 1370–1372</BibComments>
                </BibArticle>
                <BibUnstructured>Birkui P, Trigano JA, Degonde J: <Emphasis Type="Bold">Then hemodynamic efficacy of transcutaneous cardiac stimulation.</Emphasis>
                  <Emphasis Type="Italic">Bull Acad Natl Med</Emphasis> 1990, <Emphasis Type="Bold">174:</Emphasis>1361–1370. discussion 1370–1372.</BibUnstructured>
              </Citation>
              <Citation ID="CR3">
                <CitationNumber>3.</CitationNumber>
                <BibArticle>
                  <BibAuthorName>
                    <Initials>DC</Initials>
                    <FamilyName>Walund</FamilyName>
                  </BibAuthorName>
                  <BibAuthorName>
                    <Initials>AM</Initials>
                    <FamilyName>Lynn</FamilyName>
                  </BibAuthorName>
                  <BibAuthorName>
                    <Initials>DG</Initials>
                    <FamilyName>Hall</FamilyName>
                  </BibAuthorName>
                  <Year>1992</Year>
                  <ArticleTitle Language="En">A third-degree burn associated with external cardiac pacing in a five-year-old boy</ArticleTitle>
                  <JournalTitle>J Thorac Cardiovasc Surg</JournalTitle>
                  <VolumeID>104</VolumeID>
                  <FirstPage>1754</FirstPage>
                  <LastPage>1755</LastPage>
                  <Occurrence Type="COI">
                    <Handle>1:STN:280:DyaK3s%2Fos1WhtQ%3D%3D</Handle>
                  </Occurrence>
                  <Occurrence Type="PID">
                    <Handle>1453745</Handle>
                  </Occurrence>
                </BibArticle>
                <BibUnstructured>Walund DC, Lynn AM, Hall DG: <Emphasis Type="Bold">A third-degree burn associated with external cardiac pacing in a five-year-old boy.</Emphasis>
                  <Emphasis Type="Italic">J Thorac Cardiovasc Surg</Emphasis> 1992, <Emphasis Type="Bold">104:</Emphasis>1754–1755.</BibUnstructured>
              </Citation>
              <Citation ID="CR4">
                <CitationNumber>4.</CitationNumber>
                <BibArticle>
                  <BibAuthorName>
                    <Initials>HB</Initials>
                    <FamilyName>Pride</FamilyName>
                  </BibAuthorName>
                  <BibAuthorName>
                    <Initials>DF</Initials>
                    <FamilyName>McKinley</FamilyName>
                  </BibAuthorName>
                  <Year>1990</Year>
                  <ArticleTitle Language="En">Third-degree burns from the use of an external cardiac pacing device</ArticleTitle>
                  <JournalTitle>Crit Care Med</JournalTitle>
                  <VolumeID>18</VolumeID>
                  <FirstPage>572</FirstPage>
                  <LastPage>573</LastPage>
                  <Occurrence Type="COI">
                    <Handle>1:STN:280:DyaK3c3isl2qtQ%3D%3D</Handle>
                  </Occurrence>
                  <Occurrence Type="PID">
                    <Handle>2328601</Handle>
                  </Occurrence>
                  <Occurrence Type="DOI">
                    <Handle>10.1097/00003246-199005000-00020</Handle>
                  </Occurrence>
                </BibArticle>
                <BibUnstructured>Pride HB, McKinley DF: <Emphasis Type="Bold">Third-degree burns from the use of an external cardiac pacing device.</Emphasis>
                  <Emphasis Type="Italic">Crit Care Med</Emphasis> 1990, <Emphasis Type="Bold">18:</Emphasis>572–573.</BibUnstructured>
              </Citation>
            </Bibliography>
          </ArticleBackmatter>
        </Article>
      </Issue>
    </Volume>
  </Journal>
</Publisher>
